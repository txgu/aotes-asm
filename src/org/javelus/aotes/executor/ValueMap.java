package org.javelus.aotes.executor;


import java.util.Map;
import java.util.Set;

public interface ValueMap {
    Executor getExecutor();
    int size();
    void put(Object key, Object value);
    Object get(Object key);
    void checkOrFail(Object key1, Object key2, Object comparator);
    Map<Object, Set<Object>> getConflictValues();
}
