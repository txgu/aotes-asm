package org.javelus.aotes.executor;

public class FieldMeta {
    private String className;
    private String name;
    private String desc;

    public String className() {
        return className;
    }

    public String name() {
        return name;
    }

    public String desc() {
        return desc;
    }
}
