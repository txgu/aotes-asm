package org.javelus.aotes.executor;


public interface Executor {

    ValueMap getValueMap();
    ObjectMap getObjectMap();
    EvalMap getEvalMap();

    Object transformClass(Class<?> old);
    Object transform(Object old);

    void abort(String message);
}
