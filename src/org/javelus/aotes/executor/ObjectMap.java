package org.javelus.aotes.executor;

import java.util.Set;



public interface ObjectMap {

    void begin();
    void commit();
    void rollback();

    boolean isDone();
    boolean isStrictDone();

    Object getTarget();

    int size();

    /**
     * load the value of a field in the given object
     * @param object
     * @param field
     * @return
     */
    Object getField(Object object, Object field);
    /**
     * load and clear the value of a field in the given object
     * @param object
     * @param field
     * @return
     */
    Object popField(Object object, Object field);
    /**
     * store a value to a field in the given object
     * @param object
     * @param field
     * @return
     */
    void revertField(Object object, Object field);
    void revertField(Object object, Object field, Object value);

    Object getStatic(Object field);
    Object popStatic(Object field);
    void revertStatic(Object field);
    void revertStatic(Object field, Object value);

    Object getArrayElement(Object array, Object index);
    Object popArrayElement(Object array, Object index);
    void revertArrayElement(Object array, Object index);
    void revertArrayElement(Object array, Object index, Object value);
    Object getArrayLength(Object array);
    Object guessArrayIndex(Object array);
    Object newObject(Object cls);
    Object newArray(Object cls, Object length);
    Object cloneObject(Object object);
    Set<?> getTargets();

    Object newDefaultValue(Object cls);

    int getDelta();

    Executor getExecutor();
}
