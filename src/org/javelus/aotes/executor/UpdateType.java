package org.javelus.aotes.executor;

public @interface UpdateType {
    String value();
}
