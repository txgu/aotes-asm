package org.javelus.aotes.executor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.METHOD})
public @interface IM {
    String clazz();
    String name();
    String desc();
    String[] revertedInput() default {};
    String[] definedOutput() default {};
    String[] mutatedReceivers() default {};
    String[] delta() default {};
}
