package org.javelus.aotes.executor;

import java.util.ArrayList;
import java.util.List;

import org.javelus.aotes.executor.java.JavaExecutorDriver;

public class AOTESTestRunner {
    private List<Object> staleObjects = new ArrayList<Object>();

    private List<AOTESTestCase<?>> testCases = new ArrayList<AOTESTestCase<?>>();

    public void doAll() {
        buildAll();
        JavaExecutorDriver.doAll(staleObjects, System.getProperty("aotes.tos.heap", "heap.hpref"));

        // TODO: the test may fail on the old version if there is no dynamic updating.
        checkAll();

        printStatistics();
    }

    private void printStatistics() {
        int numOfTests = this.staleObjects.size();
        int numOfFailed = 0;
        for (AOTESTestCase<?> test:testCases) {
            if (test.isFailed()) {
                numOfFailed ++;
            }
        }
        System.out.println("Total\tFailed");
        System.out.format("%d\t%d\n", numOfTests, numOfFailed);
    }

    protected void addStaleObject(Object staleObject) {
        this.staleObjects.add(staleObject);
    }

    public void addTestCase(AOTESTestCase<?> test) {
        testCases.add(test);
    }

    public void buildAll() {
        for (AOTESTestCase<?> test:testCases) {
            addStaleObject(test.build());
        }
    }

    public void checkAll() {
        for (AOTESTestCase<?> test:testCases) {
            test.check();;
        }
    }

    @SuppressWarnings("unchecked")
    public static void run(Class<? extends AOTESTestCase<?>> ... testClasses) throws Exception {
        AOTESTestRunner runner = new AOTESTestRunner();
        for (Class<? extends AOTESTestCase<?>> testClass:testClasses) {
            runner.addTestCase(testClass.newInstance());
        }
        runner.doAll();
    }
}
