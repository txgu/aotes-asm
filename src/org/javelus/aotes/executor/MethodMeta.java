package org.javelus.aotes.executor;

public class MethodMeta {

    private String className;
    private String name;
    private String desc;

    public MethodMeta(String className, String name, String desc) {
        this.className = className;
        this.name = name;
        this.desc = desc;
    }

    public String className() {
        return className;
    }
    public String name() {
        return name;
    }
    public String desc() {
        return desc;
    }

    public String toString() {
        return toLine(this.className, this.name, this.desc);
    }

    public static MethodMeta toMeta(String className, String name, String desc) {
        return new MethodMeta(className, name, desc);
    }

    public static String toLine(String className, String name, String desc) {
        return className + "." + name + desc;
    }

    public static MethodMeta parse(String line) {
        int lastDot = line.lastIndexOf('.');
        if (lastDot == -1) {
            throw new IllegalArgumentException("Incorrect format of method line: " + line);
        }

        String className = line.substring(0, lastDot);
        int firstBracket = line.indexOf('(');
        if (firstBracket == -1) {
            throw new IllegalArgumentException("Incorrect format of method line: " + line);
        }

        String methodName = line.substring(lastDot + 1, firstBracket);
        String methodDesc = line.substring(firstBracket);

        return new MethodMeta(className, methodName, methodDesc);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((className == null) ? 0 : className.hashCode());
        result = prime * result + ((desc == null) ? 0 : desc.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MethodMeta other = (MethodMeta) obj;
        if (className == null) {
            if (other.className != null)
                return false;
        } else if (!className.equals(other.className))
            return false;
        if (desc == null) {
            if (other.desc != null)
                return false;
        } else if (!desc.equals(other.desc))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
