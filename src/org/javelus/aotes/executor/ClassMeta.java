package org.javelus.aotes.executor;

public class ClassMeta {

    private String name;

    public ClassMeta(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }
}
