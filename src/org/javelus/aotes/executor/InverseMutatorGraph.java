package org.javelus.aotes.executor;

import java.util.Collection;
import java.util.List;

public interface InverseMutatorGraph {

    public abstract List<InverseMutator> getHeads();

    public abstract List<InverseMutator> getTails();

    public abstract List<InverseMutator> getPreds(String uniqueName);

    public abstract List<InverseMutator> getSuccs(String uniqueName);

    public abstract Collection<InverseMutator> getInverseMutators();

    public abstract boolean isPredOf(InverseMutator im, InverseMutator im2);

}