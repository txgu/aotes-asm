package org.javelus.aotes.executor;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class DirectedGraph<T extends Serializable> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6618752441178225182L;
    /**
     * 
     */
    private Set<T> nodes;
    private Map<T, Set<T>> nodeToPreds;
    private Map<T, Set<T>> nodeToSuccs;

    private Set<T> tails;
    private Set<T> heads;

    public DirectedGraph(Set<T> nodes, Map<T, Set<T>> nodeToPreds,
            Map<T, Set<T>> nodeToSuccs, Set<T> heads, Set<T> tails) {
        this.nodes = nodes;
        this.nodeToPreds = nodeToPreds;
        this.nodeToSuccs = nodeToSuccs;
        this.heads = heads;
        this.tails = tails;
    }

    public Set<T> getTails() {
        return tails;
    }

    public Set<T> getHeads() {
        return heads;
    }

    public Set<T> getNodes() {
        return nodes;
    }

    public Set<T> getPreds(T node) {
        return this.nodeToPreds.get(node);
    }

    public Set<T> getSuccs(T node) {
        return this.nodeToSuccs.get(node);
    }
}
