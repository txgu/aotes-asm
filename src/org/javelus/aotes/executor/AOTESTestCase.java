package org.javelus.aotes.executor;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

import org.javelus.DeveloperInterface;
import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.java.JavaExecutorDriver;

public abstract class AOTESTestCase<E> {

    protected E staleObject;

    private boolean failed;

    public E build() {
        staleObject = buildInternal();
        return staleObject;
    }

    public void check() {
        try {
            checkInternal(staleObject);
        } catch (Exception e) {
            failed = true;
            e.printStackTrace();
            System.out.println("Run test " + getClass().getName() + " failed! ");
        }
    }

    public final boolean isFailed() {
        return failed;
    }

    public void assertTrue(boolean value) {
        if (value != true) {
            throw new RuntimeException("Assertion failed!");
        }
    }

    static Object getMixThat(Object object) {
        if (JavaExecutorDriver.isJavelusMode()) {
            return DeveloperInterface.getMixThat(object);
        }
        return object;
    }
    
    public void assertMixedFieldEquals(Object object, String fieldName, Object value) {
        Object phantomObject = getMixThat(object);
        assertFieldEquals(phantomObject, fieldName, value);
    }

    public void assertMixedFieldNotEquals(Object object, String fieldName, Object value) {
        Object phantomObject = getMixThat(object);
        assertFieldNotEquals(phantomObject, fieldName, value);
    }

    public void assertFieldEquals(Object object, String fieldName, Object value) {
        if (object == null) {
            throw new RuntimeException("Object should not be null!");
        }
        Object existing = readField(object, fieldName);
        assertEquals(existing, value);
    }

    public void assertEquals(Object existing, Object value) {
        if (existing == null) {
            if (value != null) {
                throw new RuntimeException("Assertion failed! exisiting is null but value is not");
            }
            return;
        }
        if (!existing.equals(value)) {
            throw new RuntimeException("Assertion failed!");
        }
    }

    public void assertArrayEquals(Object array1, Object array2) {
        if (array1 == array2) {
            return;
        }
        if (array1 == null || array2 == null) {
            throw new RuntimeException("Assertion failed! input must not be null");
        }
        Class<?> class1 = array1.getClass();
        Class<?> class2 = array2.getClass();
        if (!class1.isArray() || !class2.isArray()) {
            throw new RuntimeException("Assertion failed! input must be arrays");
        }

        int length1 = Array.getLength(array1);
        int length2 = Array.getLength(array2);
        if (length1 != length2) {
            throw new RuntimeException("Assertion failed! array length must be equal");
        }

        for (int i = 0; i < length1; i++) {
            Object value1 = ReflectionHelper.getArrayElement(array1, i);
            Object value2 = ReflectionHelper.getArrayElement(array2, i);
            assertEquals(value1, value2);
        }
    }

    public void assertFieldNotEquals(Object object, String fieldName, Object value) {
        if (object == null) {
            throw new RuntimeException("Object should not be null!");
        }
        Object existing = readField(object, fieldName);
        assertNotEquals(existing, value);
    }

    public void assertNotEquals(Object existing, Object value) {
        if (existing == null) {
            if (value == null) {
                throw new RuntimeException("Assertion failed!");
            }
            return;
        }

        if (existing.equals(value)) {
            throw new RuntimeException("Assertion failed!");
        }
    }

    public static Field getField(Object object, String fieldName) {
        Class<?> cls = object.getClass();
        Field field = null;
        try {
            while (cls != Object.class) {
                try {
                    field = cls.getDeclaredField(fieldName);
                    field.setAccessible(true);
                    return field;
                } catch(NoSuchFieldException e) {
                    cls = cls.getSuperclass();
                }
            }
        } catch (Exception e) {

        }

        throw new RuntimeException("Cannot get field " + fieldName + " in " + object.getClass().getName());
    }

    protected Object readField(Object object, String fieldName) {
        Field field = getField(object, fieldName);
        try {
            return field.get(object);
        } catch (Exception e) {}
        throw new RuntimeException("Cannot read field " + fieldName);
    }
    
    protected Object readMixField(Object object, String fieldName) {
        object = getMixThat(object);
        Field field = getField(object, fieldName);
        try {
            return field.get(object);
        } catch (Exception e) {}
        throw new RuntimeException("Cannot read field " + fieldName);
    }

    protected abstract E buildInternal();

    protected abstract void checkInternal(E updated) throws Exception;
}
