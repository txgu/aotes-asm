package org.javelus.aotes.executor;

import java.lang.reflect.Method;
import java.util.Set;

public interface InverseMutator {

    public abstract IM im();

    public abstract String getUniqueName();

    public abstract Set<Object> getDefinedOutput();

    public abstract Set<Object> getMutatedReceivers();

    public abstract int getTotalMutated();

    public abstract Set<Object> getRevertedInput();

    public abstract Set<Object> getDelta();

    public abstract String[] getPredecessors();

    public abstract String[] getSuccessors();

    public abstract Method getMethod();

    public boolean isStatic();

    /**
     * May be a java.lang.reflect.Constructor or java.lang.reflect.Method
     * @return
     */
    public abstract Object getMutator();

    public abstract Class<?> getMutatorClass();

    public abstract boolean isInit();

    public abstract boolean isClinit();

}