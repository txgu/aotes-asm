package org.javelus.aotes.executor;

public interface Eval {
    Executor getExecutor();
    Object eval(Object...args);
}
