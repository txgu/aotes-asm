package org.javelus.aotes.executor;


public interface Naming {
    Object getVariableName(Object v);
}
