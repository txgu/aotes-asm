package org.javelus.aotes.executor.java;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.javelus.aotes.executor.AbortTransformationException;
import org.javelus.aotes.executor.InverseMutator;
import org.javelus.aotes.executor.java.mov.MethodSequenceElement;

public class DynamicDeltaJavaExecutor extends JavaExecutor {

    private static final boolean NO_MDG = true;
    private static final boolean validate = false;
    private static final boolean useDefaultComparator = true;


    protected Set<String> whiteLists = new HashSet<String>();
    Comparator<Result> comparator;
    public DynamicDeltaJavaExecutor(Class<?> mutatorClass, boolean writeBack) {
        super(mutatorClass, writeBack);
        if (useDefaultComparator) {
            comparator = new DefaultComparator();
        } else {
            comparator = new Comparator<Result>() {

                @Override
                public int compare(Result o1, Result o2) {
                    boolean predOf1 = img.isPredOf(o1.im, o2.im);
                    boolean predOf2 = img.isPredOf(o2.im, o1.im);

                    if (predOf1 && !predOf2) {
                        return -1;
                    }

                    if (!predOf1 && predOf2) {
                        return 1;
                    }

                    return o1.compareTo(o2);
                }

            };
        }

        fillWhiteList();
    }


    private void fillWhiteList() {
        String whiteListString = System.getProperty("aotes.executor.whitelist", null);
        if (whiteListString == null) {
            return;
        }
        String[] whiteLists = whiteListString.split(",");
        for (String w : whiteLists) {
            this.whiteLists.add(w);
        }
    }

    static class DefaultComparator implements Comparator<Result> {

        @Override
        public int compare(Result o1, Result o2) {
            return o1.compareTo(o2);
        }

    }

    public static class Result implements Comparable<Result> {
        public final int delta;
        public final int liveLocationsCount;
        public final InverseMutator im;
        public final int aggressiveCount;
        public final int anyValueParameterCount;
        Result(int delta, int liveLocations, InverseMutator im, int aggressiveCount, int anyValueParameterCount) {
            this.delta = delta;
            this.liveLocationsCount = liveLocations;
            this.im = im;
            this.aggressiveCount = aggressiveCount;
            this.anyValueParameterCount = anyValueParameterCount;
        }

        private boolean hasAggressiveOther() {
            return aggressiveCount > 0;
        }

        private boolean hasAggressiveParameter() {
            return anyValueParameterCount > 0;
        }

        boolean hasAggressive() {
            return hasAggressiveOther() || hasAggressiveParameter();
        }
        
        @Override
        public int compareTo(Result other) {
            if (this.delta == 0 && other.delta != 0) {
                return 1;
            }

            if (this.delta != 0 && other.delta == 0) {
                return -1;
            }

            if (this.hasAggressiveParameter() && !other.hasAggressiveParameter()) {
                return 1;
            }

            if (!this.hasAggressiveParameter() && other.hasAggressiveParameter()) {
                return -1;
            }

            if (this.hasAggressiveOther() && !other.hasAggressiveOther()) {
                return 1;
            }

            if (!this.hasAggressiveOther() && other.hasAggressiveOther()) {
                return -1;
            }

            int deltaLoc = this.liveLocationsCount - other.liveLocationsCount;

            if (deltaLoc != 0) {
                return -deltaLoc;
            }

            int deltaDelta = this.delta - other.delta;

            if (deltaDelta != 0) {
                return -delta;
            }

            // any other irregular order
            return im.getUniqueName().compareTo(other.im.getUniqueName());
        }

        public String toString() {
            return im.getUniqueName() + " with delta " + delta + " and live locations " + liveLocationsCount
                    + " and aggressive location " + aggressiveCount
                    + " and any value parameter "  + anyValueParameterCount;
        }
    }

    public Collection<InverseMutator> getInverseMutators() {
        if (NO_MDG) {
            return img.getInverseMutators();
        }

        List<InverseMutator> tail = img.getTails();
        LinkedList<InverseMutator> queue = new LinkedList<InverseMutator>(tail);

        if (debug) System.err.println("Tail: " + tail.size());

        List<InverseMutator> results = new ArrayList<InverseMutator>();
        for (int i=0; i<tail.size(); i++) {
            if (debug) System.err.format("%3d %s\n", i, tail.get(i));
            queue.add(tail.get(i));
            results.add(tail.get(i));
        }

        Set<InverseMutator> visited = new HashSet<InverseMutator>();
        visited.addAll(tail);

        InverseMutator imc = null;
        while (!queue.isEmpty()) {
            imc = queue.removeFirst();
            visited.add(imc);
            List<InverseMutator> preds = img.getPreds(imc.getUniqueName());
            for (InverseMutator p:preds) {
                if (visited.add(p)) {
                    results.add(p);
                    queue.addLast(p);
                }
            }
        }

        return results;
    }

    protected MethodSequenceElement applyMutator() {
        MethodSequenceElement mse = null;
        List<Result> results = new ArrayList<Result>();
        for (InverseMutator imc:getInverseMutators()) {
            if (!this.whiteLists.isEmpty() && !this.whiteLists.contains(imc.getUniqueName())) {
                continue;
            }
            //if (debug) System.err.println("Try to apply " + imc.getUniqueName());
            Result result = attemptApply(imc);

            if (result != null) {
                if (debug) System.err.println("Using " + imc.getUniqueName() + " with result " + result);
                results.add(result);
            }
        }

        if (results.isEmpty()) {
            return null;
        }

        Collections.sort(results, comparator);
        if (debug) printResults(results);
        Result best = results.get(0);
        if (best.delta == 0) {
            if (debug) System.err.println("Using " + best.im.getUniqueName() + " with delta " + best.delta);
            return null;
        }
        try {
            mse = apply(results.get(0).im);
        } catch (RuntimeException e) {
            throw new RuntimeException("sanity check failed, should has no exception", e);
        }

        return mse;
    }

    private void printResults(List<Result> results) {
        if (results.isEmpty()) {
            return;
        }
        System.out.println("=====================");
        int count = 1;
        for (Result r : results) {
           System.out.println("" + (count++) + r); 
        }
        System.out.println();
    }


    @SuppressWarnings("unused")
    protected Result attemptApply(InverseMutator im) {
        activeValueMap = new JavaValueMap(this);
        currentInverseMutator = im;
        Object [] params = new Object[] {activeObjectMap, activeValueMap, evalMap, this};

        try {
            activeObjectMap.begin();
            im.getMethod().invoke(null, params);
            boolean isDone = activeObjectMap.isDone();
            int delta = activeObjectMap.getDelta();
            int aggressiveCount = activeObjectMap.getAggressiveCount();
            int liveLocations = activeObjectMap.getLiveLocations().size();
            int anyValueParameterCount = getAnyValueParameterCount(activeValueMap, im);
            Object[] revertedParams = collectParameterValues(activeValueMap, im);
            if (checkParameterType(revertedParams, im) == false) {
                activeObjectMap.rollback();
                return null;
            }

            if (delta >= 0 && validate) {
                JavaInterpreter interpreter;
                if (im.isInit() || im.isClinit()) {
                    interpreter = new JavaInterpreter(this, (Constructor<?>)im.getMutator());
                } else {                
                    interpreter = new JavaInterpreter(this, (Method)im.getMutator());
                }

                interpreter.execute((Object[])revertedParams);

                activeValueMap = new JavaValueMap(this);
                currentInverseMutator = im;
                params = new Object[] {activeObjectMap, activeValueMap, evalMap, this};
                im.getMethod().invoke(null, params);
            }

            activeObjectMap.rollback();

            if (debug) System.err.println(im.getUniqueName() + " is applicable with dynamic delta " + delta);

            if (im.isInit()) {
                if (isDone) {
                    if (debug) System.err.println("An init " + im.getUniqueName() +" is applicable and all are done.");
                    delta = Integer.MAX_VALUE;
                    aggressiveCount = 0;
                } else {
                    if (debug) System.err.println("An init " + im.getUniqueName() + " is applicable and something is not done, make it unapplicable.");
                    return null;
                }
            }

            return new Result(delta, liveLocations, im, aggressiveCount, anyValueParameterCount);
        } catch (AbortTransformationException e) {
            if (debug) System.err.println(im.getUniqueName() + " results in " + e.getClass().getName() + "\n    " + e.getMessage());
            activeObjectMap.rollback();
            return null;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        } catch (RuntimeException e) {
            if (debug) {
                e.printStackTrace();
                System.err.println(im.getUniqueName() + " results in " + e.getClass().getName() + "\n    " + e.getMessage());
            }
            activeObjectMap.rollback();
            return null;
        } catch (InvocationTargetException e) {
            Throwable te = e.getTargetException();
            if (te instanceof AbortTransformationException) {
                if (debug) System.err.println(im.getUniqueName() + " results in " + te.getClass().getName() + "\n    " + te.getMessage());
                activeObjectMap.rollback();
                return null;
            } else if (te instanceof RuntimeException) {
                if (debug) {
                    te.printStackTrace();
                    System.err.println(im.getUniqueName() + " results in " + te.getClass().getName() + "\n    " + te.getMessage());
                }
                activeObjectMap.rollback();
                return null;
            } else {
                throw new RuntimeException(e);
            }
        } finally {
            currentInverseMutator = null;
        }
    }

}