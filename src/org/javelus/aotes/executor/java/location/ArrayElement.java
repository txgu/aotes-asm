package org.javelus.aotes.executor.java.location;

import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class ArrayElement extends HeapLocation {

    private final int index;
    public ArrayElement(String locationPath, Object array, int index) {
        super(locationPath, array);
        this.index = index;
        setValue(readValue());
    }

    public int getIndex() {
        return index;
    }

    @Override
    public void writeValue(Object value) {
        Object container = this.getContainer();
        if (container instanceof WrappedArray) {
            throw new RuntimeException("Sanity check failed");
        }
        ReflectionHelper.setArrayElement(container, index, JavaEvalMap.getValue(value));
    }

    @Override
    public Class<?> getType() {
        return ReflectionHelper.getObjectClass(getContainer()).getComponentType();
    }

    @Override
    public Object readValue() {        
        Object container = this.getContainer();
        if (container instanceof DummyObject) {
            throw new RuntimeException("sanity check failed");
        }
        if (container instanceof WrappedObject) {
            return ReflectionHelper.getDefaultValue(getType());
        }
        return ReflectionHelper.getArrayElement(container, index);
    }
}
