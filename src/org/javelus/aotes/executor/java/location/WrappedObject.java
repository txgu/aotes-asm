package org.javelus.aotes.executor.java.location;

public class WrappedObject {
    private Object id;
    private Class<?> type;

    public WrappedObject(Object id, Class<?> type) {
        super();
        this.id = id;
        this.type = type;
    }

    public Object getId() {
        return id;
    }

    public Class<?> getType() {
        return type;
    }

    public String toString() {
        return type.getSimpleName() + '@' + id.hashCode();
    }
}
