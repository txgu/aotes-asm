package org.javelus.aotes.executor.java.location;

public class WrappedArray extends WrappedObject {

    private int length;
    public WrappedArray(Object id, Class<?> type, int length) {
        super(id, type);
        this.length = length;
    }

    public int getLength() {
        return length;
    }

}
