package org.javelus.aotes.executor.java.location;

public class DummyObject {

    private Object id;
    private Class<?> type;

    public DummyObject(Object id, Class<?> type) {
        this.id = id;
        this.type = type;
    }

    public Object getId() {
        return id;
    }

    public Class<?> getType() {
        return type;
    }
}
