package org.javelus.aotes.executor.java.location;

public abstract class HeapLocation extends Location {
    Object container;

    public HeapLocation(String locationPath, Object container) {
        super(locationPath);
        this.container = container;
    }

    public Object getContainer() {
        return container;
    }

    public String toString() {
        if (getLocationPath() == null) {
            return this.getType().getName();
        }
        return super.toString();
    }
}
