package org.javelus.aotes.executor.java.location;

public class DummyArray extends DummyObject {

    private int length;
    public DummyArray(Object id, Class<?> type, int length) {
        super(id, type);
        this.length = length;
    }

    public int getLength() {
        return length;
    }

}
