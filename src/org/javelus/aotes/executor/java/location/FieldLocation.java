package org.javelus.aotes.executor.java.location;

import java.lang.reflect.Field;

import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class FieldLocation extends HeapLocation {
    private Field field;

    public FieldLocation(String locationPath, Object container, Field field) {
        super(locationPath, container);
        this.field = field;
        setValue(readValue());
    }

    public Field getField() {
        return this.field;
    }

    public Object getContainer() {
        return container;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((container == null) ? 0 : System.identityHashCode(container));
        result = prime * result + ((field == null) ? 0 : field.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FieldLocation other = (FieldLocation) obj;
        if (container == null) {
            if (other.container != null)
                return false;
        } else if (container == other.container)
            return false;
        if (field == null) {
            if (other.field != null)
                return false;
        } else if (!field.equals(other.field))
            return false;
        return true;
    }

    public void writeValue(Object value) {
        if (container instanceof WrappedObject) {
            throw new RuntimeException("Cannot write a value into a WrappedObject of " + getType());
        }
        try {
            field.set(container, JavaEvalMap.getValue(value));
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException("Write value of field " + field + " failed!", e);
        }
    }

    public Class<?> getType() {
        return field.getType();
    }

    @Override
    public Object readValue() {
        if (container instanceof DummyObject) {
            throw new RuntimeException("sanity check failed");
        }
        if (container instanceof WrappedObject) {
            return ReflectionHelper.getDefaultValue(getType());
        }
        try {
            return field.get(getContainer());
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException("Read value of field " + field + " failed!", e);
        }
    }
}
