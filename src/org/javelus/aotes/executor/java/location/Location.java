package org.javelus.aotes.executor.java.location;

public abstract class Location {
    private String locationPath;
    private Object value;

//    private Object saved;
//
//    public void save() {
//        saved = value;
//    }
//
//    public void clear() {
//        saved = null;
//    }
//
//    public boolean isChanged() {
//        Class<?> type = getType();
//        if (type.isPrimitive()) {
//            return !saved.equals(value);
//        }
//        return saved != value;
//    }

    public Location(String locationPath) {
        this.locationPath = locationPath;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return this.value;
    }

    public void writeBack() {
        writeValue(getValue());
    }

    public String getLocationPath() {
        return locationPath;
    }

    public abstract Class<?> getType();

    public abstract void writeValue(Object value);
    public abstract Object readValue();

    public boolean isModified() {
        Object readedValue = readValue();
        Class<?> type = getType();
        if (type.isPrimitive()) {
            return !readedValue.equals(value);
        }
        return readedValue != value;
    }

    public String toString() {
        return locationPath;
    }
}
