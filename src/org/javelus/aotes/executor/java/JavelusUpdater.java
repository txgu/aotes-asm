package org.javelus.aotes.executor.java;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.javelus.DeveloperInterface;
import org.javelus.aotes.executor.java.mov.MethodIDHelper;
import org.javelus.aotes.executor.java.mov.MethodSequenceElement;
import org.javelus.aotes.executor.java.mov.MethodSequenceObject;
import org.javelus.aotes.executor.java.mov.SimpleMethodSequenceObject;

public class JavelusUpdater extends JavaUpdater {
    static final boolean isJavelus;

    static {
        ClassLoader bootstrapOrExtension = ClassLoader.getSystemClassLoader().getParent();
        if (bootstrapOrExtension == null) {
            isJavelus = false;
        } else {
            Class<?> javelusDeveloperInterface = null;
            try {
                javelusDeveloperInterface = bootstrapOrExtension.loadClass("org.javelus.DeveloperInterface");
            } catch (ClassNotFoundException e) {
                //
                javelusDeveloperInterface = null;
            } finally {
                if (javelusDeveloperInterface != null) {
                    isJavelus = true;
                } else {
                    isJavelus = false;
                }
            }
        }
    }

    @Override
    public Object transform(MethodSequenceObject<?> imso) {
        if (!isJavelus) {
            return super.transform(imso);
        }

        if (imso == null) {
            System.out.println("JavelusUpdater: null imso");
            return null;
        }

        if (imso.size() == 0) {
            // make reflection work, do a default transformation
            DeveloperInterface.getMixThat(imso.getTarget());
            return imso.getTarget();
        }

        System.out.println("JavalusUpdater: try to update...");
        System.out.println(imso);

        SimpleMethodSequenceObject smso = (SimpleMethodSequenceObject) imso;
        MethodSequenceElement[] history = smso.getSequence();
        int length = history.length;
        int lastIndex = length - 1;
        MethodSequenceElement last = history[lastIndex];

        Object newObject = null;
        boolean lastIsInit = last.isInit(); 
        if (lastIsInit) {
            System.out.println("Last is an <init>, used it to allocate a new object.");
            Constructor<?> ctor = (Constructor<?>) MethodIDHelper.resolveMethodOrConstructor(last.getMethodID());
            try {
                newObject = ctor.newInstance(last.getArguments());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            lastIndex --;
        } else {
            newObject = imso.getTarget();
            DeveloperInterface.getMixThat(newObject);
        }

        if (newObject == null) {
            throw new RuntimeException("A null new object during updating.");
        }

        int count = 0;
        for (int i = lastIndex; i>=0; i--) {
            last = history[i];
            if (last.isInit()) {
                throw new RuntimeException("<init> is not allowed!");
            }
            Method method = (Method) MethodIDHelper.resolveMethodOrConstructor(last.getMethodID());
            try {
                System.out.println("Apply method " + (count++) + " " + method);
                method.invoke(newObject, last.getArguments());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        if (lastIsInit) {
            if (newObject == imso.getTarget()) {
                throw new RuntimeException("sanity check failed");
            }
            DeveloperInterface.replaceObject(imso.getTarget(), newObject);
        }

        return newObject;
    }

    public static void invokeDSU() {
        if (isJavelus) {
            DeveloperInterface.invokeDSU();
        }
    }
}
