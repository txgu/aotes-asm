package org.javelus.aotes.executor.java.any;

public abstract class AnyValue implements Cloneable {

    Class<?> type;


    AnyValue(Class<?> type) {
        this.type = type;
    }

    public abstract void commit();
    public abstract void save();
    public abstract void restore();

    public abstract Object getAnActualValue();

    public abstract boolean canBeDefault();
    public abstract void    toDefault();

    public abstract boolean eq(Object value);

    public abstract boolean ge(Object value);

    public abstract boolean gt(Object value);

    public abstract boolean le(Object value);

    public abstract boolean lt(Object value);

    public abstract boolean ne(Object value);

    static void checkSameClass(AnyValue v1, AnyValue v2) {
        if (v1.getClass() != v2.getClass()) {
            throw new RuntimeException("Different kinds of any value: v1:" + v1 + ", v2:" + v2);
        }
    }

    /**
     * TODO Currently, we cannot check the satisfiability with two free variables.
     * @param v1
     * @param v2
     * @return
     */
    public static boolean eqAnyValue(AnyValue v1, AnyValue v2) {
        if (v1 instanceof BooleanAnyValue) {
            if (v2 instanceof BooleanAnyValue) {
                return ((BooleanAnyValue)v1).eqBooleanAnyValue((BooleanAnyValue) v2);
            } else if (v2 instanceof IntegerAnyValue) {
                return ((BooleanAnyValue)v1).eqIntegerAnyValue((IntegerAnyValue)v2);
            }
        }
        if (v1 instanceof IntegerAnyValue) {
            return false;
        }
        if (v1 instanceof ReferenceAnyValue) {
            if (v2 instanceof ReferenceAnyValue) {
                return ((ReferenceAnyValue)v1).eqReferenceAnyValue((ReferenceAnyValue) v2);
            }
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        if (v1 instanceof FloatAnyValue) {
            return false;
        }
        throw new RuntimeException("Not implemented of comparing " + v1 + " " + v2);
    }

    public static boolean geAnyValue(AnyValue v1, AnyValue v2) {
        checkSameClass(v1, v2);
        if (v1 instanceof IntegerAnyValue) {
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        if (v1 instanceof FloatAnyValue) {
            return false;
        }
        throw new RuntimeException("Not implemented of comparing " + v1 + " " + v2);
    }

    public static boolean gtAnyValue(AnyValue v1, AnyValue v2) {
        checkSameClass(v1, v2);
        if (v1 instanceof IntegerAnyValue) {
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        throw new RuntimeException("Not implemented of comparing " + v1 + " " + v2);
    }

    public static boolean leAnyValue(AnyValue v1, AnyValue v2) {
        checkSameClass(v1, v2);
        if (v1 instanceof IntegerAnyValue) {
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        throw new RuntimeException("Not implemented of comparing " + v1 + " " + v2);
    }

    public static boolean ltAnyValue(AnyValue v1, AnyValue v2) {
        checkSameClass(v1, v2);
        if (v1 instanceof IntegerAnyValue) {
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        throw new RuntimeException("Not implemented of comparing " + v1 + " " + v2);
    }

    public static boolean neAnyValue(AnyValue v1, AnyValue v2) {
        if (v1 instanceof IntegerAnyValue) {
            if (v2 instanceof BooleanAnyValue) {
                return ((BooleanAnyValue)v2).neIntegerAnyValue((IntegerAnyValue)v1);
            } else if (v2 instanceof IntegerAnyValue) {
                return false;
            }
            throw new RuntimeException("Not implemented of comparing " + v1 + " " + v2);
        }
        if (v1 instanceof BooleanAnyValue) {
            if (v2 instanceof BooleanAnyValue) {
                return ((BooleanAnyValue)v1).neBooleanAnyValue((BooleanAnyValue) v2);
            } else if (v2 instanceof IntegerAnyValue) {
                return ((BooleanAnyValue)v1).neIntegerAnyValue((IntegerAnyValue)v2);
            }
            throw new RuntimeException("Not implemented of comparing " + v1 + " " + v2);
        }
        if (v1 instanceof ReferenceAnyValue) {
            return false;
        }
        if (v1 instanceof LongAnyValue) {
            return false;
        }
        if (v1 instanceof FloatAnyValue) {
            return false;
        }
        throw new RuntimeException("Not implemented of comparing " + v1 + " " + v2);
    }
}