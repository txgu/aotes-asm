package org.javelus.aotes.executor.java.any;

import java.util.HashSet;
import java.util.Set;

import org.javelus.aotes.executor.java.JavaEvalMap;

/*
 * TODO Overflow checking
 */
public class LongAnyValue extends AnyValue {

    /**
     * Inclusive
     */
    protected long maxValue;
    protected long minValue;
    protected Set<Long> notEqual = new HashSet<Long>();

    public LongAnyValue(Class<?> type) {
        super(type);
        if (type != long.class && type != Long.class) {
            throw new RuntimeException("Require integer get " + type);
        }
        maxValue = Long.MAX_VALUE;
        minValue = Long.MIN_VALUE;
    }

    static void assertNotOfThisClass(Object value) {
        if (value != null && value.getClass() == LongAnyValue.class) {
            throw new RuntimeException("" + value + " should not in " + IntegerAnyValue.class);
        }
    }

    public boolean eq(Object value) {
        assertNotOfThisClass(value);

        long longValue = JavaEvalMap.getLongValue(value);

        // invalid state, stop the interpretation
        if (minValue > maxValue) {
            return false;
        }

        // out of scope, we are always not equal
        if (longValue > maxValue || longValue < minValue) {
            return false;
        }

        if (notEqual.contains(longValue)) {
            return false;
        }

        if (maxValue == minValue && minValue == longValue) {
            return true;
        }

        this.maxValue = this.minValue = longValue;
        return true;
    }

    public boolean ge(Object value) {
        assertNotOfThisClass(value);
        long longValue = JavaEvalMap.getLongValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // all values in this AnyValue are larger-equal then the value.
        // maxValue >= minValue >= intValue
        if (minValue >= longValue) {
            return true;
        }

        // we are less than.
        // intValue > maxValue >= minValue
        if (longValue > maxValue) {
            return false;
        }

        // maxValue >= intValue > minValue
        minValue = longValue;
        // maxValue >= minValue >= intValue

        if (!assertEquaility()) { // inconsistent, return false
            return false;
        }

        return true;
    }

    public boolean gt(Object value) {
        assertNotOfThisClass(value);
        long longValue = JavaEvalMap.getLongValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // all values in this AnyValue are larger then the value.
        // maxValue >= minValue > intValue
        if (minValue > longValue) {
            return true;
        }

        // we are less-equal than intValue.
        // intValue >= maxValue >= minValue
        if (longValue >= maxValue) {
            return false;
        }

        // maxValue > intValue >= minValue
        minValue = longValue + 1;
        // maxValue >= minValue > intValue

        if (!assertEquaility()) { // inconsistent, return false
            return false;
        }

        return true;
    }

    public boolean le(Object value) {
        assertNotOfThisClass(value);
        int intValue = JavaEvalMap.getIntegerValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // all values in this AnyValue are larger-equal then the value.
        // intValue >= maxValue >= minValue
        if (maxValue <= intValue) {
            return true;
        }

        // we are great than.
        // maxValue >= minValue > intValue
        if (minValue > intValue) {
            return false;
        }

        // maxValue > intValue >= minValue 
        maxValue = intValue;
        // intValue >= maxValue >= minValue

        if (!assertEquaility()) { // inconsistent, return false
            return false;
        }

        return true;
    }

    public boolean assertEquaility() {
        // inconsistent state
        if (maxValue == minValue && notEqual.contains(maxValue)) {
            maxValue = Integer.MIN_VALUE;
            minValue = Integer.MAX_VALUE;
            return false;
        }
        return true;
    }

    public boolean lt(Object value) {
        assertNotOfThisClass(value);
        int intValue = JavaEvalMap.getIntegerValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // all values in this AnyValue are larger-equal then the value.
        // intValue > maxValue >= minValue
        if (maxValue < intValue) {
            return true;
        }

        // we are great-equal than.
        // maxValue >= minValue >= intValue
        if (intValue <= minValue) {
            return false;
        }

        // maxValue >= intValue > minValue 
        maxValue = intValue - 1;
        // intValue > maxValue >= minValue 

        if (!assertEquaility()) { // inconsistent, return false
            return false;
        }

        // intValue > maxValue >= minValue
        return true;
    }

    public boolean ne(Object value) {
        assertNotOfThisClass(value);
        long longValue = JavaEvalMap.getLongValue(value);

        // invalid states, return false to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // out of scope, we are always not equal
        if (longValue > maxValue || longValue < minValue) {
            return true;
        }

        // oops, we are equal
        if (maxValue == longValue && minValue == longValue) {
            return false;
        }

        notEqual.add(longValue);
        return true;
    }

    public LongAnyValue clone() {
        try {
            LongAnyValue value = (LongAnyValue) super.clone();
            value.notEqual = new HashSet<Long>();
            value.notEqual.addAll(notEqual);
            return value;
        } catch (CloneNotSupportedException e) {
        }

        return null;
    }

    LongAnyValue saved;

    public void commit() {
        // do nothing
        saved = null;
    }

    @Override
    public void save() {
        if (saved != null) {
            throw new RuntimeException("Snatiy check failed!");
        }
        saved = clone();
    }

    @Override
    public void restore() {
        this.maxValue = saved.maxValue;
        this.minValue = saved.minValue;
        this.notEqual = saved.notEqual;
        saved = null;
    }

    @Override
    public Object getAnActualValue() {
        if (minValue <= maxValue) {
            if (minValue <= 0) {
                minValue = maxValue = 0;
                return 0;
            }
            return minValue;
        }

        throw new RuntimeException("Should not reach here");
    }

    @Override
    public boolean canBeDefault() {
        if (minValue <= 0 && 0 <= maxValue) {
            return true;
        }
        return false;
    }

    @Override
    public void toDefault() {
        if (canBeDefault()) {
            this.maxValue = this.minValue = 0;
            return;
        }

        throw new RuntimeException("Should not reach here!");
    }
}