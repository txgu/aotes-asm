package org.javelus.aotes.executor.java.any;

import java.util.HashSet;
import java.util.Set;

import org.javelus.aotes.executor.java.JavaEvalMap;

/*
 * TODO Overflow checking
 */
public class FloatAnyValue extends AnyValue {

    static void assertNotOfThisClass(Object value) {
        if (value != null && value.getClass() == FloatAnyValue.class) {
            throw new RuntimeException("" + value + " should not in " + FloatAnyValue.class);
        }
    }

    /**
     * Inclusive
     */
    protected float theValue;
    protected Set<Float> notEqual = new HashSet<Float>();

    public FloatAnyValue(Class<?> type) {
        super(type);
        if (type != float.class && type != Float.class) {
            throw new RuntimeException("Require float get " + type);
        }
        theValue = Float.NaN;
    }

    /**
     * Narrow to ne and default value
     */
    public boolean eq(Object value) {
        assertNotOfThisClass(value);
        float floatValue = JavaEvalMap.getFloatValue(value);
        if (Float.isNaN(floatValue)) {
            return true; // fcmpg or fcmpq
        }

        if (notEqual.contains(floatValue)) {
            return false;
        }

        if (Float.isNaN(theValue)) {
            theValue = floatValue;
            return true;
        }

        if (theValue == floatValue) {
            return true;
        }

        return false;
    }

    /**
     * Narrow to lt, i.e., check whether is can be less than
     * if it can, return unsatisfied
     * if it must be greater than, then return satisified.
     */
    public boolean ge(Object value) {
        throw new RuntimeException("No if for float");
    }

    public boolean gt(Object value) {
        throw new RuntimeException("No if for float");
    }

    public boolean le(Object value) {
        throw new RuntimeException("No if for float");
    }

    public boolean lt(Object value) {
        throw new RuntimeException("No if for float");
    }

    /**
     * Narrow to eq
     */
    public boolean ne(Object value) {
        assertNotOfThisClass(value);
        float floatValue = JavaEvalMap.getIntegerValue(value);

        if (Float.isNaN(floatValue)) {
            return true; // fcmpg or fcmpq
        }

        if (Float.isNaN(theValue)) {
            notEqual.add(floatValue);
            return true;
        }

        if (theValue == floatValue) {
            return false;
        }

        notEqual.add(floatValue);
        return true;
    }

    public FloatAnyValue clone() {
        try {
            FloatAnyValue that = (FloatAnyValue) super.clone();
            that.notEqual = new HashSet<Float>();
            that.notEqual.addAll(notEqual);
            return that;
        } catch (CloneNotSupportedException e) {
        }

        return null;
    }

    FloatAnyValue saved;

    public void commit() {
        // do nothing
        saved = null;
    }

    @Override
    public void save() {
        if (saved != null) {
            throw new RuntimeException("Snatiy check failed!");
        }
        saved = clone();
    }

    @Override
    public void restore() {
        this.theValue = saved.theValue;
        this.notEqual = saved.notEqual;
        saved = null;
    }

    @Override
    public Object getAnActualValue() {
        if (Float.isNaN(theValue)) {
            return 0.0F;
        }

        return theValue;
    }

    @Override
    public boolean canBeDefault() {
        if (Float.isNaN(theValue) || theValue == 0.0F) {
            return true;
        }
        return false;
    }

    @Override
    public void toDefault() {
        if (canBeDefault()) {
            this.theValue = 0.0F;
            return;
        }

        throw new RuntimeException("Should not reach here!");
    }
}