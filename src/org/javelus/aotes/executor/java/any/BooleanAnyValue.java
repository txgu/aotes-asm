package org.javelus.aotes.executor.java.any;

import org.javelus.aotes.executor.java.JavaEvalMap;

public class BooleanAnyValue extends AnyValue {

    static final int UNKNOWN = -1;

    int booleanValue = UNKNOWN;

    public BooleanAnyValue(Class<?> type) {
        super(type);
    }

    /**
     * Narrow to return true
     */
    @Override
    public boolean eq(Object value) {
        int bv = JavaEvalMap.getBooleanValue(value);

        // haha, eq
        if (booleanValue == UNKNOWN) {
            booleanValue = bv;
            return true;
        }

        if (booleanValue != bv) {
            return false;
        } 

        return true;
    }

    @Override
    public boolean ge(Object value) {
        throw new RuntimeException();
    }

    @Override
    public boolean gt(Object value) {
        throw new RuntimeException();
    }

    @Override
    public boolean le(Object value) {
        throw new RuntimeException();
    }

    @Override
    public boolean lt(Object value) {
        throw new RuntimeException();
    }

    /**
     * Narrow to return false
     */
    @Override
    public boolean ne(Object value) {
        int bv = JavaEvalMap.getBooleanValue(value);

        if (booleanValue == UNKNOWN) {
            if (bv == JavaEvalMap.INT_TRUE) {
                booleanValue = JavaEvalMap.INT_FALSE;
            } else if (bv == JavaEvalMap.INT_FALSE) { // bv must be INT_FLASE
                booleanValue = JavaEvalMap.INT_TRUE;
            } else {
                throw new RuntimeException("Not implemented, get an unknown value!");
            }
            return true;
        }

        // oops, we are eq
        if (booleanValue == bv) {
            return false;
        }

        return true;
    }

    BooleanAnyValue saved;

    public BooleanAnyValue clone() {
        try {
            return (BooleanAnyValue) super.clone();
        } catch (CloneNotSupportedException e) {
        }

        return null;
    }

    public void commit() {
        // do nothing
        saved = null;
    }

    @Override
    public void save() {
        if (saved != null) {
            throw new RuntimeException("Snatiy check failed!");
        }
        saved = clone();
    }

    @Override
    public void restore() {
        this.booleanValue = saved.booleanValue;
        saved = null;
    }

    /**
     * Towards unsatified
     * @param that
     * @return
     */
    public boolean eqBooleanAnyValue(BooleanAnyValue that) {
        if (this.booleanValue == UNKNOWN) {
            if (that.booleanValue == UNKNOWN) {
                this.booleanValue = JavaEvalMap.INT_FALSE;
                this.booleanValue = JavaEvalMap.INT_FALSE;
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_TRUE) {
                this.booleanValue = JavaEvalMap.INT_TRUE;
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_FALSE) {
                this.booleanValue = JavaEvalMap.INT_FALSE;
                return true;
            }
        } else if (this.booleanValue == JavaEvalMap.INT_TRUE) {
            if (that.booleanValue == UNKNOWN) {
                that.booleanValue = JavaEvalMap.INT_TRUE;
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_TRUE) {
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_FALSE) {
                return false;
            }
        } else if (this.booleanValue == JavaEvalMap.INT_FALSE) {
            if (that.booleanValue == UNKNOWN) {
                that.booleanValue = JavaEvalMap.INT_FALSE;
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_TRUE) {
                return false;
            } else if (that.booleanValue == JavaEvalMap.INT_FALSE) {
                return true;
            }
        }

        throw new RuntimeException("Should not reach here..");
    }

    /**
     * return true to pass assertion
     * @param that
     * @return
     */
    public boolean neBooleanAnyValue(BooleanAnyValue that) {
        if (this.booleanValue == UNKNOWN) {
            if (that.booleanValue == UNKNOWN) {
                this.booleanValue = JavaEvalMap.INT_FALSE;
                that.booleanValue = JavaEvalMap.INT_TRUE;
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_TRUE) {
                this.booleanValue = JavaEvalMap.INT_FALSE;
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_FALSE) {
                this.booleanValue = JavaEvalMap.INT_TRUE;
                return true;
            }
        } else if (this.booleanValue == JavaEvalMap.INT_TRUE) {
            if (that.booleanValue == UNKNOWN) {
                that.booleanValue = JavaEvalMap.INT_FALSE;
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_TRUE) {
                return false;
            } else if (that.booleanValue == JavaEvalMap.INT_FALSE) {
                return true;
            }
        } else if (this.booleanValue == JavaEvalMap.INT_FALSE) {
            if (that.booleanValue == UNKNOWN) {
                that.booleanValue = JavaEvalMap.INT_TRUE;
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_TRUE) {
                return true;
            } else if (that.booleanValue == JavaEvalMap.INT_FALSE) {
                return false;
            }
        }

        throw new RuntimeException("Should not reach here..");
    }

    /**
     * Narrow to pass assertion
     * @param that
     * @return
     */
    public boolean eqIntegerAnyValue(IntegerAnyValue that) {
        if (this.booleanValue == UNKNOWN) {
            if (that.minValue == Integer.MIN_VALUE && that.maxValue == Integer.MAX_VALUE) {
                this.booleanValue = JavaEvalMap.INT_FALSE;
                that.minValue = that.maxValue = JavaEvalMap.INT_FALSE;
                return true;
            } else if (that.minValue == JavaEvalMap.INT_TRUE && that.maxValue == JavaEvalMap.INT_TRUE) {
                this.booleanValue = JavaEvalMap.INT_TRUE;
                return true;
            } else if (that.minValue == JavaEvalMap.INT_FALSE && that.maxValue == JavaEvalMap.INT_FALSE) {
                this.booleanValue = JavaEvalMap.INT_FALSE;
                return true;
            }
        } else if (this.booleanValue == JavaEvalMap.INT_TRUE) {
            if (that.minValue == Integer.MIN_VALUE && that.maxValue == Integer.MAX_VALUE) {
                that.minValue = that.maxValue = JavaEvalMap.INT_TRUE;
                return true;
            } else if (that.minValue == JavaEvalMap.INT_TRUE && that.maxValue == JavaEvalMap.INT_TRUE) {
                return true;
            } else if (that.minValue == JavaEvalMap.INT_FALSE && that.maxValue == JavaEvalMap.INT_FALSE) {
                return false;
            }
        } else if (this.booleanValue == JavaEvalMap.INT_FALSE) {
            if (that.minValue == Integer.MIN_VALUE && that.maxValue == Integer.MAX_VALUE) {
                that.minValue = that.maxValue = JavaEvalMap.INT_FALSE;
                return true;
            } else if (that.minValue == JavaEvalMap.INT_TRUE && that.maxValue == JavaEvalMap.INT_TRUE) {
                return false;
            } else if (that.minValue == JavaEvalMap.INT_FALSE && that.maxValue == JavaEvalMap.INT_FALSE) {
                return true;
            }
        }

        throw new RuntimeException("Should not reach here..");
    }

    public boolean neIntegerAnyValue(IntegerAnyValue that) {
        if (this.booleanValue == UNKNOWN) {
            if (that.minValue == Integer.MIN_VALUE && that.maxValue == Integer.MAX_VALUE) {
                this.booleanValue = JavaEvalMap.INT_FALSE;
                that.minValue = that.maxValue = JavaEvalMap.INT_FALSE;
                return false;
            } else if (that.minValue == JavaEvalMap.INT_TRUE && that.maxValue == JavaEvalMap.INT_TRUE) {
                this.booleanValue = JavaEvalMap.INT_FALSE;
                return true;
            } else if (that.minValue == JavaEvalMap.INT_FALSE && that.maxValue == JavaEvalMap.INT_FALSE) {
                this.booleanValue = JavaEvalMap.INT_TRUE;
                return true;
            }
        } else if (this.booleanValue == JavaEvalMap.INT_TRUE) {
            if (that.minValue == Integer.MIN_VALUE && that.maxValue == Integer.MAX_VALUE) {
                that.minValue = that.maxValue = JavaEvalMap.INT_FALSE;
                return true;
            } else if (that.minValue == JavaEvalMap.INT_TRUE && that.maxValue == JavaEvalMap.INT_TRUE) {
                return false;
            } else if (that.minValue == JavaEvalMap.INT_FALSE && that.maxValue == JavaEvalMap.INT_FALSE) {
                return true;
            }
        } else if (this.booleanValue == JavaEvalMap.INT_FALSE) {
            if (that.minValue == Integer.MIN_VALUE && that.maxValue == Integer.MAX_VALUE) {
                that.minValue = that.maxValue = JavaEvalMap.INT_TRUE;
                return true;
            } else if (that.minValue == JavaEvalMap.INT_TRUE && that.maxValue == JavaEvalMap.INT_TRUE) {
                return false;
            } else if (that.minValue == JavaEvalMap.INT_FALSE && that.maxValue == JavaEvalMap.INT_FALSE) {
                return true;
            }
        }

        throw new RuntimeException("Should not reach here..");
    }

    @Override
    public boolean canBeDefault() {
        if (this.booleanValue == JavaEvalMap.INT_TRUE) {
            return false;
        }

        if (this.booleanValue == JavaEvalMap.INT_FALSE) {
            return true;
        }

        if (this.booleanValue == UNKNOWN) {
            return true;
        }

        throw new RuntimeException("Should not reach here!");
    }

    public void toDefault() {
        if (canBeDefault()) {
            this.booleanValue = JavaEvalMap.INT_FALSE;
            return;
        }

        throw new RuntimeException("Should not reach here!");
    }

    /**
     * 
     */
    @Override
    public Object getAnActualValue() {
        if (this.booleanValue == JavaEvalMap.INT_TRUE) {
            return Boolean.TRUE;
        }

        if (this.booleanValue == JavaEvalMap.INT_FALSE) {
            return Boolean.FALSE;
        }

        if (this.booleanValue == UNKNOWN) {
            this.booleanValue = JavaEvalMap.INT_FALSE;
            return Boolean.FALSE;
        }

        throw new RuntimeException("Should not reach here!");
    }
}