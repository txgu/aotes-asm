package org.javelus.aotes.executor.java.any;

import java.util.HashSet;
import java.util.Set;

import org.javelus.aotes.executor.java.JavaEvalMap;

/*
 * TODO Overflow checking
 */
public class IntegerAnyValue extends AnyValue {

    /**
     * Inclusive
     */
    protected int maxValue;
    protected int minValue;
    protected Set<Integer> notEqual = new HashSet<Integer>();

    public IntegerAnyValue(Class<?> type) {
        super(type);
        if (type != int.class && type != Integer.class) {
            throw new RuntimeException("Require integer get " + type);
        }
        maxValue = Integer.MAX_VALUE;
        minValue = Integer.MIN_VALUE;
    }

    static void assertNotOfThisClass(Object value) {
        if (value != null && value.getClass() == IntegerAnyValue.class) {
            throw new RuntimeException("" + value + " should not in " + IntegerAnyValue.class);
        }
    }

    /**
     * Narrow to true and default value
     */
    public boolean eq(Object value) {
        assertNotOfThisClass(value);

        int intValue = JavaEvalMap.getIntegerValue(value);

        // invalid state, stop the interpretation
        if (minValue > maxValue) {
            return false;
        }

        // out of scope, we are always not equal
        if (intValue > maxValue || intValue < minValue) {
            return false;
        }

        if (notEqual.contains(intValue)) {
            return false;
        }

        if (maxValue == minValue && minValue == intValue) {
            return true;
        }

        this.maxValue = this.minValue = intValue;
        return true;
    }

    /**
     * Narrow to lt, i.e., check whether is can be less than
     * if it can, return unsatisfied
     * if it must be greater than, then return satisified.
     */
    public boolean ge(Object value) {
        assertNotOfThisClass(value);
        int intValue = JavaEvalMap.getIntegerValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // all values in this AnyValue are larger-equal then the value.
        // maxValue >= minValue >= intValue
        if (minValue >= intValue) {
            return true;
        }

        // we are less than.
        // intValue > maxValue >= minValue
        if (intValue > maxValue) {
            return false;
        }

        // maxValue >= intValue > minValue
        minValue = intValue;
        // maxValue >= minValue >= intValue

        if (!assertEquaility()) { // inconsistent, return false
            return false;
        }

        return true;
    }

    public boolean gt(Object value) {
        assertNotOfThisClass(value);
        int intValue = JavaEvalMap.getIntegerValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // all values in this AnyValue are larger then the value.
        // maxValue >= minValue > intValue
        if (minValue > intValue) {
            return true;
        }

        // we are less-equal than intValue.
        // intValue >= maxValue >= minValue
        if (intValue >= maxValue) {
            return false;
        }

        // maxValue > intValue >= minValue
        minValue = intValue + 1;
        // maxValue >= minValue > intValue

        if (!assertEquaility()) { // inconsistent, return false
            return false;
        }

        return true;
    }

    public boolean le(Object value) {
        assertNotOfThisClass(value);
        int intValue = JavaEvalMap.getIntegerValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // all values in this AnyValue are larger-equal then the value.
        // intValue >= maxValue >= minValue
        if (maxValue <= intValue) {
            return true;
        }

        // we are great than.
        // maxValue >= minValue > intValue
        if (minValue > intValue) {
            return false;
        }

        // maxValue > intValue >= minValue 
        maxValue = intValue;
        // intValue >= maxValue >= minValue

        if (!assertEquaility()) { // inconsistent, return false
            return false;
        }

        return true;
    }

    public boolean lt(Object value) {
        assertNotOfThisClass(value);
        int intValue = JavaEvalMap.getIntegerValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // all values in this AnyValue are larger-equal then the value.
        // intValue > maxValue >= minValue
        if (maxValue < intValue) {
            return true;
        }

        // we are great-equal than.
        // maxValue >= minValue >= intValue
        if (intValue <= minValue) {
            return false;
        }

        // maxValue >= intValue > minValue 
        maxValue = intValue - 1;
        // intValue > maxValue >= minValue 

        if (!assertEquaility()) { // inconsistent, return false
            return false;
        }

        // intValue > maxValue >= minValue
        return true;
    }

    public boolean assertEquaility() {
        // inconsistent state
        if (maxValue == minValue && notEqual.contains(maxValue)) {
            maxValue = Integer.MIN_VALUE;
            minValue = Integer.MAX_VALUE;
            return false;
        }
        return true;
    }

    /**
     * Narrow to eq
     */
    public boolean ne(Object value) {
        assertNotOfThisClass(value);
        int longValue = JavaEvalMap.getIntegerValue(value);

        // invalid states, return satisified to stop inverse program execution.
        if (maxValue < minValue) {
            return false;
        }

        // out of scope, we are always not equal
        if (longValue > maxValue || longValue < minValue) {
            return true;
        }

        // oops, we are equal
        if (maxValue == longValue && minValue == longValue) {
            return false;
        }

        notEqual.add(longValue);
        return true;
    }

    public IntegerAnyValue clone() {
        try {
            IntegerAnyValue that = (IntegerAnyValue) super.clone();
            that.notEqual = new HashSet<Integer>();
            that.notEqual.addAll(notEqual);
            return that;
        } catch (CloneNotSupportedException e) {
        }

        return null;
    }

    IntegerAnyValue saved;

    public void commit() {
        // do nothing
        saved = null;
    }

    @Override
    public void save() {
        if (saved != null) {
            throw new RuntimeException("Snatiy check failed!");
        }
        saved = clone();
    }

    @Override
    public void restore() {
        this.maxValue = saved.maxValue;
        this.minValue = saved.minValue;
        this.notEqual = saved.notEqual;
        saved = null;
    }

    @Override
    public Object getAnActualValue() {
        if (minValue <= maxValue) {
            if (0 >= minValue) {
                this.minValue = this.maxValue = 0;
                return 0;
            }
            return Integer.valueOf(minValue);
        }

        throw new RuntimeException("Should not reach here!");
    }

    @Override
    public boolean canBeDefault() {
        if (minValue <= 0 && 0 <= maxValue) {
            return true;
        }
        return false;
    }

    @Override
    public void toDefault() {
        if (canBeDefault()) {
            this.maxValue = this.minValue = 0;
            return;
        }

        throw new RuntimeException("Should not reach here!");
    }
}