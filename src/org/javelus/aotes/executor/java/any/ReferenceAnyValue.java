package org.javelus.aotes.executor.java.any;

import java.util.IdentityHashMap;

public class ReferenceAnyValue extends AnyValue {

    Object reference;

    static Object NULL = new Object(); 

    IdentityHashMap<Object, Object> blacklist = new IdentityHashMap<Object, Object>();

    public ReferenceAnyValue(Class<?> type) {
        super(type);
        if (type.isPrimitive()) {
            throw new RuntimeException("Require nont-primitive get " + type);
        }
        reference = null;
    }

    @Override
    public boolean eq(Object value) {
        Object nonNull = value == null ? NULL : value;

        if (blacklist.containsKey(nonNull)) {
            return false;
        }

        if (reference == null) {
            reference = nonNull;
            return true;
        }

        if (reference == nonNull) {
            return true;
        }

        return false;
    }

    @Override
    public boolean ge(Object value) {
        throw new RuntimeException();
    }

    @Override
    public boolean gt(Object value) {
        throw new RuntimeException();
    }

    @Override
    public boolean le(Object value) {
        throw new RuntimeException();
    }

    @Override
    public boolean lt(Object value) {
        throw new RuntimeException();
    }

    @Override
    public boolean ne(Object value) {
        Object nonNull = value == null ? NULL : value;

        if (reference != null) {
            if (reference != nonNull) {
                return true;
            } else {
                return false;
            }
        }

        blacklist.put(nonNull, nonNull);
        return true;
    }

    public ReferenceAnyValue clone() {
        try {
            ReferenceAnyValue c = (ReferenceAnyValue) super.clone();
            c.blacklist = new IdentityHashMap<Object, Object>(this.blacklist);
            return c;
        } catch (CloneNotSupportedException e) {
        }
        return null;
    }

    ReferenceAnyValue saved;

    public void commit() {
        // do nothing
        saved = null;
    }

    @Override
    public void save() {
        if (saved != null) {
            throw new RuntimeException("Snatiy check failed!");
        }
        saved = clone();
    }

    @Override
    public void restore() {
        this.reference = saved.reference;
        this.blacklist = saved.blacklist;
        saved = null;
    }

    @Override
    public Object getAnActualValue() {
        // Not narrowed
        if (reference == null && !blacklist.containsKey(NULL)) {
            reference = NULL;
            return null;
        }

        // narrowed to null
        if (reference == NULL) {
            return null;
        }

        if (!(reference instanceof AnyValue)) {
            return reference;
        }

        throw new RuntimeException("Not implemented yet getAnActualValue for ReferenceAnyValue");
    }

    @Override
    public boolean canBeDefault() {
        if (blacklist.containsKey(NULL)) {
            return false;
        }

        if (reference == null) {
            return true;
        }

        if (reference == NULL) {
            return true;
        }

        return false;
    }

    @Override
    public void toDefault() {
        if (canBeDefault()) {
            this.reference = NULL;
            return;
        }

        throw new RuntimeException("Should not reach here!");
    }

    /**
     * try to return true that pass the assertion
     * @param that
     * @return
     */
    public boolean eqReferenceAnyValue(ReferenceAnyValue that) {
        if (this.reference == null) {
            if (that.reference == null) {
                if (!this.blacklist.containsKey(NULL)
                        && !that.blacklist.containsKey(NULL)) {
                    this.reference = that.reference = NULL;
                    return true;
                }
            } else if (that.reference == NULL) {
                if (!this.blacklist.containsKey(NULL)) {
                    this.reference = NULL;
                    return true;
                }
            }
            return false;
        } else if (this.reference == NULL) {
            if (that.reference == null) {
                if (!that.blacklist.containsKey(NULL)) {
                    that.reference = NULL;
                    return true;
                }
            } else if (that.reference == NULL) {
                return true;
            }
        }
        return false;
    }
}