package org.javelus.aotes.executor.java;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.ObjectMap;
import org.javelus.aotes.executor.java.any.AnyValue;
import org.javelus.aotes.executor.java.location.ArrayElement;
import org.javelus.aotes.executor.java.location.DummyArray;
import org.javelus.aotes.executor.java.location.DummyObject;
import org.javelus.aotes.executor.java.location.FieldLocation;
import org.javelus.aotes.executor.java.location.Location;
import org.javelus.aotes.executor.java.location.WrappedArray;
import org.javelus.aotes.executor.java.location.WrappedObject;

public class JavaObjectMap implements ObjectMap {

    static interface Command {
        Object redo();

        void undo();
    }

    static final boolean UseAnyValue = true;

    class StoreLocationCommand implements Command {

        Object oldValue;
        Object newValue;
        Location location;
        boolean isResolvedBefore;
        StoreLocationCommand(Location location, Object newValue) {
            this.location = location;
            this.oldValue = location.getValue();
            this.newValue = newValue;
        }

        @Override
        public Object redo() {
            oldValue = location.getValue();
            if (isResolved(location)) {
                isResolvedBefore = true;
            }
            if (ReflectionHelper.isDefaultValue(oldValue)) {
                if (isDefaultAnyValue(newValue)) {
                    return oldValue;
                }
                if (ReflectionHelper.isDefaultValue(newValue)) {
                    return oldValue;
                }
                abort("Revert an default value into non-default at location " + location);
            } else if (isDefaultOrDefaultAnyValue(newValue)) {
                if (!isDefaultOrDefaultAnyValue(oldValue)) {
                    delta++;
                }
            } else if (getExecutor().getEvalMap().ne(oldValue, newValue)) {
                delta++;
            }
            setAndUpdateState(newValue);
            return oldValue;
        }

        private void setAndUpdateState(Object theValue) {
            location.setValue(theValue);
            if (isInInitialState(location)) {
                markResolved(location);
            } else {
                markUnresolved(location);
            }
        }

        @Override
        public void undo() {
            location.setValue(oldValue);
            if (isResolvedBefore) {
                markResolved(location);
            } else {
                markUnresolved(location);
            }
        }
    }

    private Set<AnyValue> anyValues = new HashSet<AnyValue>();

    /**
     * The current state Represent modified states of the target objects A root
     * map and other maps rooted at an object in the root map.
     */
    private Map<Object, Object> objectToLocations = new IdentityHashMap<Object, Object>();
    private Map<String, FieldLocation> fieldNameToStaticField =  new HashMap<String, FieldLocation>();
    private Map<Object, String> objectToLocationPath = new IdentityHashMap<Object, String>();

    /**
     * Path left to be reverted
     */
    private Set<Location> unresolved = new HashSet<Location>();
    private Set<Location> resolved = new HashSet<Location>();

    private Executor executor;
    private Object target;

    private int delta;
    private int aggressiveCount;

    /**
     * Defined location
     */
    private Set<Object> skeleton;

    private LinkedList<Command> commandStack = new LinkedList<Command>();

    public JavaObjectMap(Executor executor, Object target, Set<Object> skeleton) {
        this.executor = executor;
        this.target = target;
        this.skeleton = skeleton;
        collectHeapLocations("this", "this", target);
    }


    protected boolean isResolved(Location location) {
        boolean value = resolved.contains(location);
        if (value && unresolved.contains(location)) {
            throw new RuntimeException("Inconsistent location " + location);
        }
        return value;
    }

    protected boolean isUnesolved(Location location) {
        boolean value = unresolved.contains(location);
        if (value && resolved.contains(location)) {
            throw new RuntimeException("Inconsistent location " + location);
        }
        return value;
    }
    
    protected boolean markResolved(Location location) {
        if (!resolved.contains(location) && !unresolved.contains(location)) {
            throw new RuntimeException("Untracked location " + location + " when marking unresolved");
        }
        unresolved.remove(location);
        return resolved.add(location);
    }

    protected boolean markUnresolved(Location location) {
        if (!resolved.contains(location) && !unresolved.contains(location)) {
            throw new RuntimeException("Untracked location " + location + " when marking resolved");
        }
        resolved.remove(location);
        return unresolved.add(location);
    }

    protected boolean addUnresolved(Location location) {
        return this.unresolved.add(location);
    }
    
    protected boolean addResolved(Location location) {
        return this.resolved.add(location);
    }
    
    public boolean isDefaultOrDefaultAnyValue(Object value) {
        if (UseAnyValue) {
            // AnyValue is also a default value.
            if (isDefaultAnyValue(value)) {
                return true;
            }
        }
        return ReflectionHelper.isDefaultValue(value);
    }

    protected Object createAnyValue(Class<?> type) {
        AnyValue anyValue = JavaEvalMap.createAnyValue(type);
        addAnyValue(anyValue);
        return anyValue;
    }

    protected void addAnyValue(AnyValue av) {
        anyValues.add(av);
        av.save();
    }

    public Object getTarget() {
        return target;
    }

    private void collectInstanceFields(String accessPath, String locationPath, Object object) {
        if (objectToLocations.containsKey(object)) {
            throw new RuntimeException("Object " + object + " has been collected already.");
        }
        Class<?> objClass = object.getClass();
        Map<Object, FieldLocation> fieldLocations = new HashMap<Object, FieldLocation>();
        objectToLocations.put(object, fieldLocations);
        objectToLocationPath.put(object, locationPath);
        while (objClass != Object.class) {
            Field[] fields = objClass.getDeclaredFields();
            for (Field f : fields) {
                String fieldName = f.getName();
                String newAP = accessPath + '.' + fieldName;
                if (!inSkeleton(newAP)) {
                    continue;
                }
                String newLP = locationPath + '.' + fieldName;
                f.setAccessible(true);

                if (fieldLocations.containsKey(fieldName)) {
                    //throw new RuntimeException
                    System.err.println("Field overriding " + fieldName +" in class " + object.getClass().getName()
                            + " Not implemented yet!");
                    continue;
                }

                FieldLocation location = new FieldLocation(newLP, object, f);
                Object value = location.getValue();

                if (isInInitialState(location)) {
                    addResolved(location);
                } else {
                    addUnresolved(location);
                }

                if (f.getType().isPrimitive()) {
                    fieldLocations.put(fieldName, location);
                } else {
                    if (value == null) { // only put
                        fieldLocations.put(fieldName, location);
                    } else { // put and follow
                        fieldLocations.put(fieldName, location);
                        collectHeapLocations(newAP, newLP, value);
                    }
                }
            }
            objClass = objClass.getSuperclass();
        }
    }

    private void collectHeapLocations(String accessPath, String locationPath, Object object) {
        if (this.objectToLocations.containsKey(object)) {
            return;
        }
        Class<?> objClass = object.getClass();
        if (objClass.isArray()) {
            collectArrayElements(accessPath, locationPath, object);
        } else {
            collectInstanceFields(accessPath, locationPath, object);
        }
    }

    private void collectArrayElements(String accessPath, String locationPath, Object array) {
        if (objectToLocations.containsKey(array)) {
            throw new RuntimeException("sanity check failed");
        }
        String newAP = accessPath + "[*]";
        if (!inSkeleton(newAP)) {
            return;
        }
        int length = ReflectionHelper.getArrayLength(array);
        ArrayElement[] elements = new ArrayElement[length];
        objectToLocations.put(array, elements);
        objectToLocationPath.put(array, locationPath);
        for (int i = 0; i < length; i++) {
            elements[i] = new ArrayElement(locationPath + '[' + i + ']', array, i);
            if (isInInitialState(elements[i])) {
                addResolved(elements[i]);
            } else {
                addUnresolved(elements[i]);
            }
        }
        if (ReflectionHelper.getObjectClass(array).getComponentType().isPrimitive()) {
            return;
        }
        for (int i = 0; i < length; i++) {
            Object value = Array.get(array, i);
            if (value != null) {
                collectHeapLocations(newAP, elements[i].getLocationPath(), value);
            }
        }
    }

    private boolean inSkeleton(String newAp) {
        return this.skeleton.contains(newAp);
    }

    public final void abort(String message) {
        getExecutor().abort(message);
    }

    @Override
    public Set<Location> getTargets() {
        Object target = getTarget();
        IdentityHashMap<Object, Object> visited = new IdentityHashMap<Object, Object>();
        IdentityHashMap<Location, Location> liveLocations = new IdentityHashMap<Location, Location>();
        collectLiveLocations(target, visited, liveLocations);

        Set<Location> targets = new HashSet<Location>(this.unresolved.size());
        for (Location loc:unresolved) {
            if (liveLocations.containsKey(loc)) {
                targets.add(loc);
            }
        }
        return targets;
    }

    protected boolean isAnyValue(Object object) {
        return object instanceof AnyValue;
    }

    protected boolean isDefaultAnyValue(Object object) {
        if (object instanceof AnyValue) {
            AnyValue av = (AnyValue) object;
            return av.canBeDefault();
        }
        return false;
    }

    protected Object executeCommand(Command cmd) {
        commandStack.add(cmd);
        return cmd.redo();
    }

    @Override
    public Executor getExecutor() {
        return executor;
    }

    @Override
    public int size() {
        return unresolved.size();
    }

    @Override
    public void begin() {
        if (!commandStack.isEmpty()) {
            throw new RuntimeException("Inconsistent object map!");
        }

        for (AnyValue av : anyValues) {
            av.save();
        }

        delta = 0;
        aggressiveCount = 0;
        //this.liveLocations = new IdentityHashMap<>();
    }

    @Override
    public void commit() {
        this.commandStack.clear();
        for (AnyValue av : anyValues) {
            av.commit();
        }
    }

    @Override
    public void rollback() {
        for (AnyValue av : anyValues) {
            av.restore();
        }
        while (!commandStack.isEmpty()) {
            Command cmd = commandStack.removeLast();
            cmd.undo();
        }
    }

    @Override
    public boolean isStrictDone() {
        return isDone();
    }

    private IdentityHashMap<Location, Location> liveLocations = new IdentityHashMap<Location, Location>();

    @Override
    public boolean isDone() {
        Object target = getTarget();
        IdentityHashMap<Object, Object> visited = new IdentityHashMap<Object, Object>();
        liveLocations = new IdentityHashMap<Location, Location>();
        collectLiveLocations(target, visited, liveLocations);
        int count = 0;

        for (Location loc:unresolved) {
            if (liveLocations.containsKey(loc)) {
                count++;
            }
        }

        return count == 0;
    }

    @SuppressWarnings("unchecked")
    private void collectLiveLocations(Object object, Map<Object, Object> visited, Map<Location, Location> live) {
        if (object == null || visited.containsKey(object)) {
            return;
        }
        if (object instanceof DummyObject) {
            throw new RuntimeException("WrappedObject should not be reachable from DummyObject");
        }
        if (object instanceof WrappedObject) {
            throw new RuntimeException("WrappedObject should not be reachable from DummyObject");
        }
        if (object instanceof AnyValue) {
            throw new RuntimeException("Cannot follow an AnyValue");
        }
        Class<?> clazz = null;
        if (object instanceof WrappedObject) {
            clazz = ((WrappedObject) object).getType();
        } else {
            clazz = object.getClass();
        }
        visited.put(object, object);
        if (clazz.isArray()) {
            ArrayElement[] elements = (ArrayElement[]) this.objectToLocations.get(object);
            if (elements == null) {
                return;
            }
            for (ArrayElement ae : elements) {
                live.put(ae, ae);
            }
            if (!clazz.getComponentType().isPrimitive()) {
                for (ArrayElement ae : elements) {
                    collectLiveLocations(JavaEvalMap.getValue(ae.getValue()), visited, live);
                }
            }
        } else {
            Map<Object, FieldLocation> fields = (Map<Object, FieldLocation>) this.objectToLocations.get(object);
            if (fields == null) {
                return;
            }
            for (FieldLocation fl : fields.values()) {
                live.put(fl, fl);
                if (!fl.getType().isPrimitive()) {
                    collectLiveLocations(JavaEvalMap.getValue(fl.getValue()), visited, live);
                }
            }
        }
    }

    protected boolean isInInitialState(Location loc) {
        Object value = loc.getValue();
        return isDefaultOrDefaultAnyValue(value);
    }

    public int getDelta() {
        return delta;
    }

    int getAggressiveCount() {
        return aggressiveCount;
    }

    Set<Location> getLiveLocations() {
        return Collections.unmodifiableSet(this.liveLocations.keySet());
    }

    /**
     * For each value in active map, write its values.
     */
    public void writeBack() {
        Object target = getTarget();
        IdentityHashMap<Object, Object> visited = new IdentityHashMap<Object, Object>();
        writeBackObject(target, visited);
    }

    @SuppressWarnings("unchecked")
    protected void writeBackObject(Object object, Map<Object, Object> visited) {
        if (object == null || visited.containsKey(object)) {
            return;
        }
        if (object instanceof DummyObject || object instanceof WrappedObject) {
            throw new RuntimeException("Cannot write back into a warpped object");
        }
        if (object instanceof AnyValue) {
            throw new RuntimeException("Cannot write into an AnyValue");
        }
        Class<?> clazz = object.getClass();
        visited.put(object, object);
        if (clazz.isArray()) {
            ArrayElement[] elements = (ArrayElement[]) this.objectToLocations.get(object);
            if (elements == null) {
                return;
            }
            for (ArrayElement ae : elements) {
                ae.writeBack();
            }
            if (!clazz.getComponentType().isPrimitive()) {
                for (ArrayElement ae : elements) {
                    writeBackObject(ae.readValue(), visited);
                }
            }
        } else {
            Map<Object, FieldLocation> fields = (Map<Object, FieldLocation>) this.objectToLocations.get(object);
            if (fields == null) {
                return;
            }
            for (FieldLocation fl : fields.values()) {
                fl.writeBack();
                if (!fl.getType().isPrimitive()) {
                    writeBackObject(fl.readValue(), visited);
                }
            }
        }
    }

    private String toFieldName(Object field) {
        return field.toString();
    }

    protected Object checkObjectField(FieldLocation fl, Object value) {
        if (fl.getType().isPrimitive()) {
            return value;
        }
        if (!this.objectToLocationPath.containsKey(value)) {
            objectToLocationPath.put(value, fl.getLocationPath());
        }
        return value;
    }

    protected Object checkArrayElement(ArrayElement ae, Object value) {
        if (ae.getType().isPrimitive()) {
            return value;
        }
        if (!this.objectToLocationPath.containsKey(value)) {
            objectToLocationPath.put(value, ae.getLocationPath());
        }
        return value;
    }

    @Override
    public Object getField(Object object, Object field) {
        if (field.equals("java.lang.System.security")) {
            return System.getSecurityManager();
        }
        if (isPlainObject(object) && !hasFieldLocation(object, toFieldName(field))) {
            return ReflectionHelper.getFieldValue(object, toFieldName(field));
        }
        if (object == null || JavaEvalMap.getValue(object) == null) {
            abort("get field " + field + " on null receiver.");
            return null;
        }
        FieldLocation fl = getFieldLocation(object, toFieldName(field));
        return checkObjectField(fl, fl.getValue());
    }


    @Override
    public Object getStatic(Object field) {
        if (field.equals("java.lang.System.security")) {
            return System.getSecurityManager();
        }
        FieldLocation fl = getStaticFieldLocation(toFieldName(field));
        return checkObjectField(fl, fl.getValue());
    }
    
    @Override
    public Object popField(Object object, Object field) {
        if (object == null || JavaEvalMap.getValue(object) == null) {
            abort("pop field " + field + " on null receiver.");
            return null;
        }
        FieldLocation fl = getFieldLocation(object, toFieldName(field));
        if (fl == null) {
            abort("try to pop field " + field + " on null");
            return null;
        }
        return checkObjectField(fl, executeCommand(new StoreLocationCommand(fl, getDefaultOrAnyValue(fl.getType()))));
    }
    
    
    @Override
    public Object popStatic(Object field) {
        FieldLocation fl = getStaticFieldLocation(toFieldName(field));
        if (fl == null) {
            abort("try to pop field " + field + " on null");
            return null;
        }
        return checkObjectField(fl, executeCommand(new StoreLocationCommand(fl, getDefaultOrAnyValue(fl.getType()))));
    }

    private Object getDefaultOrAnyValue(Class<?> type) {
        if (UseAnyValue) {
            return createAnyValue(type);
        } else {
            return ReflectionHelper.getDefaultValue(type);
        }
    }

    @Override
    public void revertField(Object object, Object field) {
        if (object == null || JavaEvalMap.getValue(object) == null) {
            abort("revert field " + field + " on null receiver.");
            return;
        }
        FieldLocation fl = getFieldLocation(object, toFieldName(field));
        if (fl == null) {
            abort("try to revert field " + field + " on null with default value");
            return;
        }
        executeCommand(new StoreLocationCommand(fl, getDefaultOrAnyValue(fl.getType())));
        aggressiveCount++;
    }

    @Override
    public void revertField(Object object, Object field, Object value) {
        if (object == null || JavaEvalMap.getValue(object) == null) {
            abort("revert field " + field + " on null receiver.");
            return;
        }
        FieldLocation fl = getFieldLocation(object, toFieldName(field));
        if (fl == null) {
            abort("try to revert field " + field + " on null with " + value);
            return;
        }
        executeCommand(new StoreLocationCommand(fl, value));
    }
    
    @Override
    public void revertStatic(Object field) {
        FieldLocation fl = getStaticFieldLocation(toFieldName(field));
        if (fl == null) {
            abort("try to revert field " + field + " on null with default value");
            return;
        }
        executeCommand(new StoreLocationCommand(fl, getDefaultOrAnyValue(fl.getType())));
        aggressiveCount++;
    }

    @Override
    public void revertStatic(Object field, Object value) {
        FieldLocation fl = getStaticFieldLocation(toFieldName(field));
        if (fl == null) {
            abort("try to revert field " + field + " on null with " + value);
            return;
        }
        executeCommand(new StoreLocationCommand(fl, value));
    }

//    void setField(Object object, Object field, Object value) {
//        FieldLocation fl = getFieldLocation(object, toFieldName(field));
//        fl.setValue(value);
//    }
//    
//    void setStatic(Object field, Object value) {
//        FieldLocation fl = getStaticFieldLocation(toFieldName(field));
//        fl.setValue(value);
//    }

    @Override
    public Object getArrayElement(Object array, Object index) {
        int idx = JavaEvalMap.getIntegerValue(index);
        if (isPlainArray(array) && !hasArrayElement(array, idx)) {
            return ReflectionHelper.getArrayElement(array, idx);
        }
        ArrayElement ae = getArrayElementLocation(array, idx);
        return checkArrayElement(ae, ae.getValue());
    }

    private boolean isPlainArray(Object array) {
        return array != null && array.getClass().isArray();
    }

    private boolean isPlainObject(Object array) {
        return array != null && array.getClass().isArray();
    }

    @SuppressWarnings("unchecked")
    protected boolean hasFieldLocation(Object object, String field) {
        object = JavaEvalMap.getValue(object);
        Map<Object, FieldLocation> fieldLocations = (Map<Object, FieldLocation>) objectToLocations.get(object);
        if (fieldLocations == null) {
            return false;
        }
        FieldLocation fl = fieldLocations.get(field);
        return fl != null;
    }

    protected boolean hasArrayElement(Object array, int idx) {
        array = JavaEvalMap.getValue(array);
        ArrayElement[] elements = (ArrayElement[]) objectToLocations.get(array);
        if (elements == null) {
            return false;
        }
        if (idx < 0 || idx >= elements.length) {
            return false;
        }
        ArrayElement ae = elements[idx];
        return ae != null;
    }

    private ArrayElement[] getArrayElementLocations(Object array) {
        array = JavaEvalMap.getValue(array);
        if (array == null) {
            abort("No locations for null");
            return null;
        }
        Object locations = objectToLocations.get(array);
        if (locations == null) {
            String locationPath = objectToLocationPath.get(array);
            if (locationPath == null && !(array instanceof WrappedArray)) {
                abort("No locations for array " + array);
                return null;
            }
            int length = ReflectionHelper.getArrayLength(array);
            ArrayElement[] elements = new ArrayElement[length];
            objectToLocations.put(array, elements);
            for (int i = 0; i < length; i++) {
                elements[i] = new ArrayElement(locationPath + '[' + i + ']', array, i);
                trackNewLocation(elements[i]);
            }
            return elements;
        }
        return ((ArrayElement[]) locations);
    }

    private void trackNewLocation(Location location) {
        if (isInInitialState(location)) {
            addResolved(location);
        } else {
            addUnresolved(location);
        }
        //location.save();
    }
    
    private ArrayElement getArrayElementLocation(Object array, int index) {
        array = JavaEvalMap.getValue(array);
        if (array == null) {
            abort("No locations for null");
            return null;
        }
        Object locations = objectToLocations.get(array);
        if (locations == null) {
            String locationPath = objectToLocationPath.get(array);
            if (locationPath == null && !(array instanceof WrappedArray)) {
                abort("No locations for array " + array);
                return null;
            }
            int length = ReflectionHelper.getArrayLength(array);
            ArrayElement[] elements = new ArrayElement[length];
            objectToLocations.put(array, elements);
            for (int i = 0; i < length; i++) {
                elements[i] = new ArrayElement(locationPath + '[' + i + ']', array, i);
                trackNewLocation(elements[i]);
            }
            return elements[index];
        }
        ArrayElement ae = getArrayElementLocations(array)[index];
        if (ae == null) {
            throw new RuntimeException("Sanity check failed, null elements on object " + array + " at index " + index);
        }
        return ae;
    }

    @SuppressWarnings("unchecked")
    private FieldLocation getFieldLocation(Object object, String fieldName) {
        object = JavaEvalMap.getValue(object);
        FieldLocation fl = null;
        if (object == null) {
            // an instance field
            fl = getStaticFieldLocation(fieldName);
            if (fl != null) {
                return fl;
            }
            abort("No locations for null");
            return null;
        }
        Map<Object, FieldLocation> fieldLocations = (Map<Object, FieldLocation>) objectToLocations.get(object);
        if (fieldLocations == null) {
            String locationPath = objectToLocationPath.get(object);
            if (locationPath == null && !(object instanceof WrappedObject)) {
                abort("No locations for object " + object);
                return null;
            }
            fieldLocations = new HashMap<Object, FieldLocation>();
            objectToLocations.put(object, fieldLocations);
        }
        fl = fieldLocations.get(fieldName);
        if (fl == null) {
            String locationPath = objectToLocationPath.get(object);
            if (locationPath == null && !(object instanceof WrappedObject)) {
                throw new RuntimeException(
                        "Sanity check failed, try get a field " + fieldName + " on object " + object);
            }
            fl = createFieldLocation(locationPath, object, fieldName);
            trackNewLocation(fl);
            fieldLocations.put(fieldName, fl);
        }
        return fl;
    }

    private FieldLocation getStaticFieldLocation(String fieldName) {
        if (fieldName == null) {
            abort("Cannot find a static field with null name");
            return null;
        }
        FieldLocation fl = fieldNameToStaticField.get(fieldName);

        if (fl != null) {
            return fl; 
        }

        fl = createFieldLocation(null, null, fieldName);

        if (!Modifier.isStatic(fl.getField().getModifiers())) {
            return null;
        }

        trackNewLocation(fl);
        fieldNameToStaticField.put(fieldName, fl);
        return fl;
    }

    protected FieldLocation createFieldLocation(String locationPath, Object object, String fieldName) {
        Field field = ReflectionHelper.getField(object, fieldName);
        FieldLocation fl = new FieldLocation(locationPath + "." + fieldName, object, field);
        return fl;
    }

    @Override
    public Object popArrayElement(Object array, Object index) {
        int idx = JavaEvalMap.getIntegerValue(index);
        ArrayElement ae = getArrayElementLocation(array, idx);
        if (ae == null) {
            abort("try to pop array element " + index + " on null");
            return null;
        }
        return checkArrayElement(ae, executeCommand(new StoreLocationCommand(ae, getDefaultOrAnyValue(ae.getType()))));
    }

    @Override
    public void revertArrayElement(Object array, Object index) {
        int idx = JavaEvalMap.getIntegerValue(index);
        ArrayElement ae = getArrayElementLocation(array, idx);
        if (ae == null) {
            abort("try to revert array element " + index + " on null with default value");
            return;
        }
        executeCommand(new StoreLocationCommand(ae, getDefaultOrAnyValue(ae.getType())));
        aggressiveCount++;
    }

    @Override
    public void revertArrayElement(Object array, Object index, Object value) {
        int idx = JavaEvalMap.getIntegerValue(index);
        ArrayElement ae = getArrayElementLocation(array, idx);
        if (ae == null) {
            abort("try to revert array element " + index + " on null with " + value);
            return;
        }
        executeCommand(new StoreLocationCommand(ae, value));
    }

//    void setArrayElement(Object array, Object index, Object value) {
//        int idx = JavaEvalMap.getIntegerValue(index);
//        ArrayElement ae = getArrayElementLocation(array, idx);
//        ae.setValue(value);
//    }

    @Override
    public Object getArrayLength(Object array) {
        array = JavaEvalMap.getValue(array);
        if (array == null) {
            abort("Get Array length on null");
            return null;
        }
        return ReflectionHelper.getArrayLength(array);
    }

    @Override
    public Object guessArrayIndex(Object array) {
        ArrayElement[] elements = getArrayElementLocations(array);
        for (int i = 0; i < elements.length; i++) {
            if (!isInInitialState(elements[i])) {
                aggressiveCount++;
                return i;
            }
        }
        this.getExecutor().abort("No available index for guessing");
        return null;
    }

    @Override
    public Object newDefaultValue(Object cls) {
        Class<?> klass = null;
        if (cls instanceof Class) {
            klass = (Class<?>) cls;
        } else {
            String className = cls.toString();
            klass = ReflectionHelper.loadClassOrNull(className);
        }
        return getDefaultOrAnyValue(klass);
    }

    @Override
    public Object newObject(Object cls) {
        Class<?> klass = null;
        if (cls instanceof Class) {
            klass = (Class<?>) cls;
        } else {
            String className = cls.toString();
            klass = ReflectionHelper.loadClassOrNull(className);
        }
        if (klass == null) {
            this.getExecutor().abort("Cannot load class " + cls);
            return null;
        }
        if (cls instanceof Class) {
            return new WrappedObject(new Object(), klass);
        } else {
            return new DummyObject(new Object(), klass);
        }
    }

    @Override
    public Object newArray(Object cls, Object length) {
        Class<?> klass = null;
        if (cls instanceof Class) {
            klass = (Class<?>) cls;
        } else {
            String className = cls.toString();
            klass = ReflectionHelper.loadClassOrNull(className);
        }
        if (klass == null) {
            this.getExecutor().abort("Cannot load class " + cls);
            return null;
        }
        if (cls instanceof Class) {
            return new WrappedArray(new Object(), klass, JavaEvalMap.getIntegerValue(length));
        } else {
            return new DummyArray(new Object(), klass, JavaEvalMap.getIntegerValue(length));
        }
    }

    @Override
    public Object cloneObject(Object object) {
        if (object == null) {
            abort("Cannot clone null");
        }
        if (object instanceof WrappedArray) {
            WrappedArray cloned = new WrappedArray(new Object(), ReflectionHelper.getObjectClass(object),
                    ReflectionHelper.getArrayLength(object));
            cloneArrayElements((WrappedArray) object, cloned);
            return cloned;
        } else if (object instanceof WrappedObject) {
            WrappedObject cloned = new WrappedObject(new Object(), ReflectionHelper.getObjectClass(object));
            cloneFieldLocations((WrappedObject) object, cloned);
            return cloned;
        }
        return ReflectionHelper.cloneObject(object);
    }

    @SuppressWarnings("unchecked")
    private void cloneFieldLocations(WrappedObject source, WrappedObject target) {
        Map<Object, FieldLocation> sourceFields = (Map<Object, FieldLocation>) objectToLocations.get(source);
        if (sourceFields == null) {
            return;
        }

        Map<Object, FieldLocation> targetFields = new HashMap<Object, FieldLocation>();
        for (Entry<Object, FieldLocation> entry : sourceFields.entrySet()) {
            Object fieldName = entry.getKey();
            FieldLocation from = entry.getValue();
            FieldLocation to = new FieldLocation(null, target, from.getField());
            to.setValue(from.getValue());
            targetFields.put(fieldName, to);
            if (isInInitialState(to)) {
                addResolved(to);
            } else {
                addUnresolved(to);
                // this.beforeTargets ++;
                // elements[i].save();
            }
        }
        objectToLocations.put(target, targetFields);
    }

    private void cloneArrayElements(WrappedArray source, WrappedArray target) {
        if (source == null) {
            abort("Cannot clone from null");
            return;
        }
        ArrayElement[] sourceElements = (ArrayElement[]) objectToLocations.get(source);
        if (sourceElements == null) {
            return;
        }
        ArrayElement[] targetElements = new ArrayElement[sourceElements.length];
        for (int i = 0; i < targetElements.length; i++) {
            targetElements[i] = new ArrayElement(null, target, i);
            targetElements[i].setValue(sourceElements[i].getValue());
            if (isInInitialState(targetElements[i])) {
                addResolved(targetElements[i]);
            } else {
                addUnresolved(targetElements[i]);
                // this.beforeTargets ++;
                // elements[i].save();
            }
        }
        objectToLocations.put(target, targetElements);
    }

    boolean isArrayInInitialState(Object array) {
        ArrayElement[] locations = getArrayElementLocations(array);
        for (int i = 0; i < locations.length; i++) {
            if (!isInInitialState(locations[i])) {
                return false;
            }
        }
        return true;
    }
}
