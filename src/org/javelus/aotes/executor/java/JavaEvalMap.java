package org.javelus.aotes.executor.java;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.Eval;
import org.javelus.aotes.executor.EvalMap;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.any.AnyValue;
import org.javelus.aotes.executor.java.any.BooleanAnyValue;
import org.javelus.aotes.executor.java.any.FloatAnyValue;
import org.javelus.aotes.executor.java.any.IntegerAnyValue;
import org.javelus.aotes.executor.java.any.LongAnyValue;
import org.javelus.aotes.executor.java.any.ReferenceAnyValue;
import org.javelus.aotes.executor.java.eval.ARRAYLENGTH;
import org.javelus.aotes.executor.java.eval.F2I;
import org.javelus.aotes.executor.java.eval.FADD;
import org.javelus.aotes.executor.java.eval.FCMPG;
import org.javelus.aotes.executor.java.eval.FCMPL;
import org.javelus.aotes.executor.java.eval.FDIV;
import org.javelus.aotes.executor.java.eval.FMUL;
import org.javelus.aotes.executor.java.eval.FNEG;
import org.javelus.aotes.executor.java.eval.FSUB;
import org.javelus.aotes.executor.java.eval.I2F;
import org.javelus.aotes.executor.java.eval.I2L;
import org.javelus.aotes.executor.java.eval.IADD;
import org.javelus.aotes.executor.java.eval.IAND;
import org.javelus.aotes.executor.java.eval.IDIV;
import org.javelus.aotes.executor.java.eval.IINC;
import org.javelus.aotes.executor.java.eval.IMUL;
import org.javelus.aotes.executor.java.eval.INEG;
import org.javelus.aotes.executor.java.eval.INSTANCEOF;
import org.javelus.aotes.executor.java.eval.IOR;
import org.javelus.aotes.executor.java.eval.IREM;
import org.javelus.aotes.executor.java.eval.ISHL;
import org.javelus.aotes.executor.java.eval.ISHR;
import org.javelus.aotes.executor.java.eval.ISUB;
import org.javelus.aotes.executor.java.eval.IUSHR;
import org.javelus.aotes.executor.java.eval.IXOR;
import org.javelus.aotes.executor.java.eval.L2I;
import org.javelus.aotes.executor.java.eval.LADD;
import org.javelus.aotes.executor.java.eval.LCMP;
import org.javelus.aotes.executor.java.eval.LSHR;
import org.javelus.aotes.executor.java.eval.LSUB;
import org.javelus.aotes.executor.java.eval.LXOR;
import org.javelus.aotes.executor.java.eval.MethodEval;
import org.javelus.aotes.executor.java.eval.XALOAD;
import org.javelus.aotes.executor.java.eval.XASTORE;

public class JavaEvalMap implements EvalMap {

    static enum Comparator {
        EQ, GE, GT, LE, LT, NE;
    }

    public static AnyValue createAnyValue(Class<?> type) {
        if (type == Integer.class || type == int.class) {
            return new IntegerAnyValue(type);
        }

        if (type == Boolean.class || type == boolean.class) {
            return new BooleanAnyValue(type);
        }

        if (type == Long.class || type == long.class) {
            return new LongAnyValue(type);
        }

        if (type == Float.class || type == float.class) {
            return new FloatAnyValue(type);
        }

        if (type.isPrimitive()) {
            throw new RuntimeException("AnyValue is not implemented " + type);
        }

        return new ReferenceAnyValue(type);
    }

    public static Object getValue (Object value) {
        if (value instanceof AnyValue) {
            return ((AnyValue) value).getAnActualValue();
        }
        return value;
    }

    public static int getBooleanValue(Object value) {
        if (value instanceof Boolean) {
            return (Boolean)value ? INT_TRUE : INT_FALSE;
        }

        if (value instanceof Integer) {
            int intValue = ((Integer)value).intValue();

            if (!(intValue == INT_TRUE || intValue == INT_FALSE)) {
                throw new RuntimeException("Invalid boolean value " + value);
            }

            return intValue;
        }

        // boxed value
        if (value == null) {
            return INT_FALSE;
        }

        throw new RuntimeException("Invalid boolean value " + value);
    }

    public static int getIntegerValue(Object value) {
        if (value instanceof Integer) {
            return ((Integer)value).intValue();
        }

        if (value instanceof Boolean) {
            return ((Boolean) value).booleanValue() == true ? INT_TRUE : INT_FALSE;
        }

        // boxed value
        if (value == null) {
            return 0;
        }

        if (value instanceof IntegerAnyValue) {
            return (Integer)((IntegerAnyValue) value).getAnActualValue();
        }

        throw new RuntimeException("Invalid integer value " + value);
    }

    public static float getFloatValue(Object value) {
        if (value instanceof Float) {
            return ((Float)value).floatValue();
        }

        // boxed value
        if (value == null) {
            return 0;
        }

        if (value instanceof FloatAnyValue) {
            return (Float)((FloatAnyValue) value).getAnActualValue();
        }

        throw new RuntimeException("Invalid integer value " + value);
    }

    public static long getLongValue(Object value) {
        if (value instanceof Long) {
            return ((Long)value).longValue();
        }

        // boxed value
        if (value == null) {
            return 0;
        }

        throw new RuntimeException("Invalid long value " + value);
    }

    /**
     * Assert the value to be true.
     */
    public static boolean AnyValuePassAssertion = true;

    public static int INT_TRUE = 1;

    public static int INT_FALSE = 0;

    private Executor executor;

    private Map<Object, Eval> idToEval = new HashMap<Object, Eval>();

    public JavaEvalMap(Executor executor) {
        this.executor = executor;
        initialize();
    }

    private void initialize() {
        this.idToEval.put("f2i", new F2I(executor));
        this.idToEval.put("fadd", new FADD(executor));
        this.idToEval.put("fcmpl", new FCMPL(executor));
        this.idToEval.put("fcmpg", new FCMPG(executor));
        this.idToEval.put("fdiv", new FDIV(executor));
        this.idToEval.put("fmul", new FMUL(executor));
        this.idToEval.put("fneg", new FNEG(executor));
        this.idToEval.put("fsub", new FSUB(executor));
        this.idToEval.put("i2f", new I2F(executor));
        this.idToEval.put("i2l", new I2L(executor));
        this.idToEval.put("iadd", new IADD(executor));
        this.idToEval.put("iand", new IAND(executor));
        this.idToEval.put("idiv", new IDIV(executor));
        this.idToEval.put("iinc", new IINC(executor));
        this.idToEval.put("imul", new IMUL(executor));
        this.idToEval.put("ineg", new INEG(executor));
        this.idToEval.put("instanceof", new INSTANCEOF(executor));
        this.idToEval.put("ior", new IOR(executor));
        this.idToEval.put("irem", new IREM(executor));
        this.idToEval.put("ishl", new ISHL(executor));
        this.idToEval.put("ishr", new ISHR(executor));
        this.idToEval.put("isub", new ISUB(executor));
        this.idToEval.put("iushr", new IUSHR(executor));
        this.idToEval.put("ixor", new IXOR(executor));
        this.idToEval.put("l2i", new L2I(executor));
        this.idToEval.put("ladd", new LADD(executor));
        this.idToEval.put("lcmp", new LCMP(executor));
        this.idToEval.put("lshr", new LSHR(executor));
        this.idToEval.put("lsub", new LSUB(executor));
        this.idToEval.put("lxor", new LXOR(executor));

        this.idToEval.put("arraylength", new ARRAYLENGTH(executor));
        this.idToEval.put("xaload", new XALOAD(executor));
        this.idToEval.put("xastore", new XASTORE(executor));
    }

    @Override
    public Executor getExecutor() {
        return executor;
    }

    @Override
    public Eval op(Object opType) {
        Eval eval = idToEval.get(opType);

        if (eval != null) {
            return eval;
        }

        if (isMethodOperator(opType)) {
            eval = new MethodEval(executor, opType);
            this.idToEval.put(opType, eval);
        } else {
            System.err.println("Operator has not been implemented " + opType);
            System.exit(1);
            throw new RuntimeException("Operator has not been implemented " + opType);
        }

        return eval;
    }

    private boolean isMethodOperator(Object opType) {
        return opType.toString().contains("(");
    }

    @Override
    public int size() {
        return idToEval.size();
    }

    @Override
    public boolean eq(Object v1, Object v2) {
        if (v1 == v2) { // fast path
            return true;
        }
        if (v1 instanceof AnyValue) {
            AnyValue av = (AnyValue) v1;
            if (v2 instanceof AnyValue) {
                return AnyValue.eqAnyValue(av, (AnyValue)v2);
            }
            return av.eq(v2);
        }

        if (v2 instanceof AnyValue) {
            AnyValue av = (AnyValue) v2;
            return av.eq(v1);
        }

        if (v1 == null) {
            if (v2 != null) {
                return false;
            } else {
                return true;
            }
        } else {
            if (v2 == null) {
                return false;
            } else {
                if (v1 != v2 && areTwoEmptyArrays(v1, v2)) {
                    return true;
                }
                return equivalent(v1, v2);
            }
        }
    }

    boolean equivalent(Object v1, Object v2) {
        if (compare(v1, v2) == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean lt(Object v1, Object v2) {
        if (v1 instanceof AnyValue) {
            AnyValue av = (AnyValue) v1;
            if (v2 instanceof AnyValue) {
                return AnyValue.ltAnyValue(av, (AnyValue)v2);
            }
            return av.lt(v2);
        }

        if (v2 instanceof AnyValue) {
            AnyValue av = (AnyValue) v2;
            return av.ge(v1);
        }

        return compare(v1, v2) < 0;
    }

    @Override
    public boolean le(Object v1, Object v2) {
        if (v1 instanceof AnyValue) {
            AnyValue av = (AnyValue) v1;
            if (v2 instanceof AnyValue) {
                return AnyValue.leAnyValue(av, (AnyValue)v2);
            }
            return av.le(v2);
        }

        if (v2 instanceof AnyValue) {
            AnyValue av = (AnyValue) v2;
            return av.gt(v1);
        }

        return compare(v1, v2) <= 0;
    }

    @Override
    public boolean gt(Object v1, Object v2) {
        if (v1 instanceof AnyValue) {
            AnyValue av = (AnyValue) v1;
            if (v2 instanceof AnyValue) {
                return AnyValue.gtAnyValue(av, (AnyValue)v2);
            }
            return av.gt(v2);
        }

        if (v2 instanceof AnyValue) {
            AnyValue av = (AnyValue) v2;
            return av.le(v1);
        }

        return compare(v1, v2) > 0;
    }

    @Override
    public boolean ge(Object v1, Object v2) {
        if (v1 instanceof AnyValue) {
            AnyValue av = (AnyValue) v1;
            if (v2 instanceof AnyValue) {
                return AnyValue.geAnyValue(av, (AnyValue)v2);
            }
            return av.ge(v2);
        }

        if (v2 instanceof AnyValue) {
            AnyValue av = (AnyValue) v2;
            return av.lt(v1);
        }

        return compare(v1, v2) >= 0;
    }


    /**
     * 
     * @param v1
     * @param v2
     * @return 0 equal
     */
    protected int compare(Object v1, Object v2) {
        Class<?> c1 = ReflectionHelper.getObjectClass(v1); //v1.getClass();
        Class<?> c2 = ReflectionHelper.getObjectClass(v2); //v2.getClass();

        if (c1 == Boolean.class) {
            if (c2 == Integer.class) {
                return compareBoolAndInt((Boolean)v1, (Integer)v2);
            } else if (c2 == Boolean.class) {
                return c1.equals(c2) ? 0 : 1;
            } else {
                throw new RuntimeException("Type error! " + c2);
            }
        } else if (c1 == Integer.class) {
            if (c2 == Boolean.class) {
                return -compareBoolAndInt((Boolean)v2, (Integer)v1);
            } else if (c2 == Byte.class
                    || c2 == Short.class
                    || c2 == Integer.class
                    || c2 == Long.class) {
                Number n1 = (Number) v1;
                Number n2 = (Number) v2;
                return compareIntegerNumber(n1, n2);
            } else if (c2 == Character.class) {
                Character c = (Character) v2;
                int i1 = ((Integer)v1).intValue();
                int i2 = c.charValue();
                return i1 - i2;
            } else {
                throw new RuntimeException("Comparing integer and other types" + c2 + " is not implemented");
            }
        } else if (c1 == Byte.class) {
            if (c2 == Boolean.class) {
                throw new RuntimeException("Comparing byte and boolean is not implemented");
            } else if (c2 == Byte.class
                    || c2 == Short.class
                    || c2 == Integer.class
                    || c2 == Long.class) {
                Number n1 = (Number) v1;
                Number n2 = (Number) v2;
                return compareIntegerNumber(n1, n2);
            }
        } else if (c1 == Short.class) {
            throw new RuntimeException("Comparing short is not implemented");
        } else if (c1 == Long.class) {
            if (c2 == Long.class) {
                return ((Long)v1).compareTo((Long)v2);
            } else {
                throw new RuntimeException("Not implemented");
            }
        } else if (c1 == Float.class) {
            if (c2 == Float.class) {
                return ((Float)v1).compareTo((Float)v2);
            } else {
                throw new RuntimeException("Comparing float with other types is not implemented");
            }
        } else if (c1 == Double.class) {
            if (c2 == Double.class) {
                return ((Double)v1).compareTo((Double)v2);
            } else {
                throw new RuntimeException("Comparing double with other types is not implemented");
            }
        } else if (c1 == Character.class) {
            if (c2 == Integer.class) {
                Character c = (Character) v1;
                int i2 = ((Integer)v2).intValue();
                int i1 = c.charValue();
                return i1 - i2;
            } else if (c2 == Character.class) {
                Character ch1 = (Character) v1;
                Character ch2 = (Character) v2;
                return ch1.compareTo(ch2);
            } else {
                throw new RuntimeException("Comparing char and other types" + c2 + " is not implemented");
            }
        } else if (c1 == String.class) {
            if (c2 == String.class) {
                return v1.equals(v2) ? 0 : 1;
            } else {
                throw new RuntimeException("Comparing string with other types is not implemented");
            }
        }

        if (v1 instanceof Comparable) {
            //return ((Comparable) v1).compareTo(v2);
            throw new RuntimeException("TODO: compare Comparable is not implemented yet, values are " + v1 + " and " + v2);
        }

        // identity equivalent
        if (v1 == v2) return 0;

        // Array are not implemented yet!
        return 1;
    }

    private boolean areTwoEmptyArrays(Object v1, Object v2) {
        Class<?> c1 = ReflectionHelper.getObjectClass(v1);
        Class<?> c2 = ReflectionHelper.getObjectClass(v2);
        if (!c1.isArray() || !c2.isArray()) {
            return false;
        }
        int l1 = Array.getLength(v1);
        int l2 = Array.getLength(v2);
        if (l1 != 0 && l2 != 0) {
            return false;
        }
        if (l1 == 0 && l2 == 0) {
            return true;
        }

        if (l1 == 0) {
            return isArrayInInitialState(v1);
        }

        if (l2 == 0) {
            return isArrayInInitialState(v2);
        }
        throw new RuntimeException("Should not reach here");
    }

    private boolean isArrayInInitialState(Object array) {
        JavaObjectMap objectMap = (JavaObjectMap) executor.getObjectMap();
        return objectMap.isArrayInInitialState(array);
    }

    private int compareIntegerNumber(Number n1, Number n2) {
        return n1.intValue() - n2.intValue();
    }

    /**
     *  true, 0    => 0
     *  true, !0   => 1
     *  false, 0   => 1
     *  false,!0   => 0
     * @param b
     * @param i
     * @return
     */
    private int compareBoolAndInt(Boolean b, Integer i) {
        if (b) {
            if (i == INT_TRUE) {
                return 0;
            } else {
                return 1;
            }
        } else {
            if (i == INT_TRUE) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    protected int compareTwoPrimitives(Object v1, Object v2) {
        if (v1.getClass() == Boolean.class) {
            if (v2.getClass() == Integer.class) {
                return compareBoolAndInt((Boolean)v1, (Integer)v2);
            } else {
                throw new RuntimeException("Type error!");
            }
        } else if (v1.getClass() == Integer.class) {
            if (v2.getClass() == Boolean.class) {
                return -compareBoolAndInt((Boolean)v2, (Integer)v1);
            } else if (v2.getClass() == Byte.class
                    || v2.getClass() == Short.class
                    || v2.getClass() == Long.class) {
                Number n1 = (Number) v1;
                Number n2 = (Number) v2;
                return n1.intValue() - n2.intValue();
            }
        }

        throw new RuntimeException("Not implemented comparison of " + v1 + " and " + v2);
    }

    @Override
    public boolean ne(Object v1, Object v2) {
        if (v1 instanceof AnyValue) {
            AnyValue av = (AnyValue) v1;
            if (v2 instanceof AnyValue) {
                return AnyValue.neAnyValue(av, (AnyValue)v2);
            }
            return av.ne(v2);
        }

        if (v2 instanceof AnyValue) {
            AnyValue av = (AnyValue) v2;
            return av.ne(v1);
        }

        if (v1 == null) {
            if (v2 != null) {
                return true;
            } else {
                return false;
            }
        } else {
            if (v2 == null) {
                return true;
            } else {
                return !equivalent(v1, v2);
            }
        }
    }

}
