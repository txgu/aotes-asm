package org.javelus.aotes.executor.java;

import org.javelus.aotes.executor.java.mov.MethodSequenceObject;

public class JavaUpdater {

    /**
     * We cannot create the actual new object as standard Java may not support the updating
     * @param imso
     * @return
     */
    public Object transform(MethodSequenceObject<?> imso) {
        if (imso == null) {
            System.out.println("Null imso");
            return null;
        }
        System.out.println("Inversed MSO has " + imso.size() + " inverse mutators.");
        System.out.println(imso);
        return imso.getTarget();
    }
}
