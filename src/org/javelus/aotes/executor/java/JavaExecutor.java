package org.javelus.aotes.executor.java;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.javelus.aotes.asm.Options;
import org.javelus.aotes.executor.AbortTransformationException;
import org.javelus.aotes.executor.Defined;
import org.javelus.aotes.executor.EvalMap;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.InverseMutator;
import org.javelus.aotes.executor.InverseMutatorGraph;
import org.javelus.aotes.executor.MutatedReceiver;
import org.javelus.aotes.executor.ObjectMap;
import org.javelus.aotes.executor.ValueMap;
import org.javelus.aotes.executor.java.any.AnyValue;
import org.javelus.aotes.executor.java.img.InverseMutatorGraphImpl;
import org.javelus.aotes.executor.java.location.Location;
import org.javelus.aotes.executor.java.mov.InversedMethodSequenceElement;
import org.javelus.aotes.executor.java.mov.MethodIDHelper;
import org.javelus.aotes.executor.java.mov.MethodSequenceElement;
import org.javelus.aotes.executor.java.mov.MethodSequenceObject;
import org.javelus.aotes.executor.java.mov.SimpleMethodSequenceObject;

public class JavaExecutor implements Executor {

    protected boolean debug = Options.debug("executor");
    Class<?> mutatorClass;
    Set<Object> defined = new HashSet<Object>();
    Set<Object> mutatedReceivers = new HashSet<Object>();
    InverseMutatorGraph img;

    JavaValueMap activeValueMap;
    JavaObjectMap activeObjectMap;
    final JavaEvalMap evalMap;

    protected Object currentInverseMutator;
    protected boolean writeBack;

    public JavaExecutor(Class<?> mutatorClass, boolean writeBack) {
        this.mutatorClass = mutatorClass;
        this.writeBack = writeBack;
        img = InverseMutatorGraphImpl.build(mutatorClass.getDeclaredMethods());
        Defined s = mutatorClass.getAnnotation(Defined.class);
        if (s != null) {
            for (Object o:s.value()) {
                defined.add(o);
            }
        }

        MutatedReceiver m = mutatorClass.getAnnotation(MutatedReceiver.class);
        if (m != null) {
            for (Object o:m.value()) {
                mutatedReceivers.add(o);
            }
        }

        
        this.evalMap = new JavaEvalMap(this);
    }


    @Override
    public ValueMap getValueMap() {
        return activeValueMap;
    }

    @Override
    public ObjectMap getObjectMap() {
        return activeObjectMap;
    }

    @Override
    public EvalMap getEvalMap() {
        return evalMap;
    }

    @Override
    public Object transformClass(Class<?> old) {
        throw new RuntimeException("Not implemented");
    }

    public void printTargetMap() {
        System.out.println("Target object map:");
        int count = 0;
        for (Object l : activeObjectMap.getTargets()) {
            Location location = (Location) l;
            System.out.format("%3d %s@%s\n", count++, location, location.isModified()? "modified" : "unmodified");
        }
    }

    public boolean hasModifiedTarget() {
        for (Object l : activeObjectMap.getTargets()) {
            Location location = (Location) l;
            if (location.isModified()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object transform(Object old) {
        activeObjectMap = new JavaObjectMap(this, old, getDefined());

        MethodSequenceObject<?> imso = synthesize();

        if (imso == null) {
            //targetObjectMap.isDone();
            System.out.println("Fail to synthesize a sequence for " + activeObjectMap.getTarget());
            printTargetMap();
            return null;
        }

        if (imso.size() > 0 && writeBack) {
            this.activeObjectMap.writeBack();
        }

        return imso;
    }

    protected MethodSequenceElement applyMutator() {
        List<InverseMutator> tail = img.getTails();
        LinkedList<InverseMutator> queue = new LinkedList<InverseMutator>(tail);
        Set<InverseMutator> visited = new HashSet<InverseMutator>();

        if (debug) System.err.println("Tail: " + tail.size());

        for (int i=0; i<tail.size(); i++) {
            if (debug) System.err.format("%3d %s\n", i, tail.get(i));
        }

        MethodSequenceElement mse = null;

        while (!queue.isEmpty()) {
            InverseMutator imc = queue.removeFirst();
            visited.add(imc);
            if (debug) System.err.println("Try to apply " + imc.getUniqueName());
            mse = apply(imc);
            if (mse != null) {
                //im = imc;
                if (debug) System.err.println(imc.getUniqueName() + " is applicable!");
                break;
            }

            List<InverseMutator> preds = img.getPreds(imc.getUniqueName());
            for (InverseMutator p:preds) {
                if (visited.add(p)) {
                    queue.addLast(p);
                }
            }
        }

        return mse;
    }

    protected Class<?>[] getParameterTypes(InverseMutator im) {
        if (im.isInit() || im.isClinit()) {
            Constructor<?> c = (Constructor<?>)im.getMutator();
            if (c == null) {
                abort("Cannot resolve mutator for " + im.getUniqueName());
            }
            return c.getParameterTypes();
        }

        Method m = (Method)im.getMutator();
        if (m == null) {
            abort("Cannot resolve mutator for " + im.getUniqueName());
        }
        return m.getParameterTypes();
    }

    public Object getCurrentInverseMutator() {
        return currentInverseMutator;
    }

    protected boolean checkParameterType(Object[] params, InverseMutator im) {
        Class<?>[] pts = getParameterTypes(im);
        final int pl = pts.length;

        if (pl != params.length) {
            throw new RuntimeException("Sanity check failed!");
        }

        // type check in case type narrowing
        for (int index=0; index<pl; index++) {
            Object parami = params[index];
            Class<?> expectedType = pts[index]; 
            if (parami != null && !(parami instanceof AnyValue)) {
                // A real value;
                if (expectedType.isPrimitive()) {
                    continue;
                }

                if (!expectedType.isInstance(parami)) {
                    System.err.println("WARNING: Type error caused by Reverse Execution: required=" + pts[index] + ", get=" + parami.getClass());
                    return false;
                }
            }
        }

        return true;
    }

    protected int getAnyValueParameterCount(ValueMap valueMap, InverseMutator im) {
        Class<?>[] pts = getParameterTypes(im);
        final int pl = pts.length;
        int count = 0;
        if (pl > 0) {
            for (int index=0; index<pl; index++) {
                Object value = valueMap.get("param" + index);
                if (value instanceof AnyValue) {
                    count++;
                }
            }
        }
        return count;
    }
    
    protected Object[] collectParameterValues(ValueMap valueMap, InverseMutator im) {
        Class<?>[] pts = getParameterTypes(im);
        final int pl = pts.length;
        Object[] revertedParams = new Object[pl];
        if (pl > 0) {
            revertedParams = new Object[pl];
            for (int index=0; index<pl; index++) {
                Object value = valueMap.get("param" + index);
                if (value instanceof AnyValue) {
                    revertedParams[index] = ((AnyValue)value).getAnActualValue();
                } else {
                    revertedParams[index] = value;
                }
            }
        }
        return revertedParams;
    }

    protected MethodSequenceElement apply(InverseMutator im) {
        activeValueMap = new JavaValueMap(this);
        currentInverseMutator = im;
        Object [] params = new Object[] {activeObjectMap, activeValueMap, evalMap, this};

        try {
            activeObjectMap.begin();
            im.getMethod().invoke(null, params);
            if (im.isInit()) {
                if (!activeObjectMap.isDone()) {
                    activeObjectMap.rollback();
                    return null;
                }
            }
            Object[] revertedParams = collectParameterValues(activeValueMap, im);
            if (checkParameterType(revertedParams, im) == false) {
                activeObjectMap.rollback();
                return null;
            }
            activeObjectMap.commit();
            return new InversedMethodSequenceElement(im, MethodIDHelper.getMethodID(im.im()), null, revertedParams);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            Throwable te = e.getTargetException();
            if (te instanceof AbortTransformationException) {
                if (debug) System.err.println(im.getUniqueName() + " results in " + te.getClass().getName() + "\n    " + te.getMessage());
                activeObjectMap.rollback();
                return null;
            } else if (te instanceof RuntimeException) {
                if (debug) {
                    te.printStackTrace();
                    System.err.println(im.getUniqueName() + " results in " + te.getClass().getName() + "\n    " + te.getMessage());
                }
                activeObjectMap.rollback();
                return null;
            } else {
                throw new RuntimeException(e);
            }
        } finally {
            currentInverseMutator = null;
        }
    }

    protected MethodSequenceObject<?> synthesize() {
        List<MethodSequenceElement> mses = new ArrayList<MethodSequenceElement>();

        final int upperBound = Integer.getInteger("aotes.synthesis.bound", 100);
        int bound = 0;

        while (true) {
            bound++;

            if (bound > upperBound) {
                System.out.println("Out of bounds " + bound + " ...");
                System.out.println(new SimpleMethodSequenceObject(this, activeObjectMap.getTarget(), mses));
                return null;
            }

            if (debug) System.err.println("Round " + bound);

            MethodSequenceElement mse = applyMutator();

            if (mse == null) {
                if (debug) {
                    printTargetMap();
                    if (!mses.isEmpty()) {
                        System.out.println("Partial history:");
                        int i = 0;
                        for (MethodSequenceElement m:mses) {
                            System.out.format("%3d %s\n", i++, m);
                        }
                    }
                }
                break;
            } else {
                if (debug) System.err.println("Adding " + mse + " to the history.");
                mses.add(mse);
            }

            if (activeObjectMap.isDone()) {
                if (!activeObjectMap.isStrictDone()) {
                    System.err.println("WARNING: is not strict done");
                }
                break;
            }
        };

        boolean hasInit = false;
        for (int i=0; i<mses.size(); i++) {
            MethodSequenceElement e = mses.get(i);
            if (e.isInit()) {
                if (hasInit) {
                    throw new RuntimeException("Should be only one constructor!");
                }

                // Only one <init> at the end of the history?
                if (i != mses.size() - 1) {
                    throw new RuntimeException("Constructor should be the last element in the reverse sequence.");
                }
                hasInit = true;
            }
        }

        if (!hasInit) {
            for(InverseMutator im:img.getInverseMutators()) {
                if (im.isInit()) {
                    MethodSequenceElement mse = apply(im);
                    if (mse != null) {
                        mses.add(mse);
                        break;
                    }
                    if (debug) System.err.println("WARNING: there is an init " + im.getUniqueName() + " that cannot be applied!");
                }
            }
        }

        MethodSequenceObject<?> mso = new SimpleMethodSequenceObject(this, activeObjectMap.getTarget(), mses);
        return mso;
    }


    protected Set<Object> getDefined() {
        return defined;
    }

    protected Set<Object> getMutatedReceivers() {
        return mutatedReceivers;
    }

    @Override
    public void abort(String message) {
        throw new AbortTransformationException(message);
    }

}
