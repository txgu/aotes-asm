package org.javelus.aotes.executor.java;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.ValueMap;

public class JavaValueMap implements ValueMap {

    private Map<Object, Object> keyToValue = new HashMap<Object, Object>();

    private Executor executor;

    public JavaValueMap(Executor executor) {
        this.executor = executor;
    }

    @Override
    public Executor getExecutor() {
        return executor;
    }

    @Override
    public void put(Object key, Object value) {
        if (keyToValue.containsKey(key)) {
            Object oldValue = keyToValue.get(key);
            getExecutor().abort(String.format("Inconsistent value for key %s, old value is %s, new value is %s", key, oldValue, value));
            return;
        }
        keyToValue.put(key, value); 
    }

    @Override
    public Object get(Object key) {
        if (!keyToValue.containsKey(key)) {
            getExecutor().abort("Uninitialized value for key " + key);
            return null;
        }
        return keyToValue.get(key);
    }

    @Override
    public void checkOrFail(Object key1, Object key2, Object comparator) {
        throw new RuntimeException("checkOrFail: not implemented");
    }

    @Override
    public Map<Object, Set<Object>> getConflictValues() {
        throw new RuntimeException("getConflictValues: not implemented");
    }

    @Override
    public String toString() {
        return this.keyToValue.toString();
    }

    @Override
    public int size() {
        return keyToValue.size();
    }
}
