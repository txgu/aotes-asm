package org.javelus.aotes.executor.java;

public final class PrimitiveWrapper {
    Object value;

    public PrimitiveWrapper(Object value) {
        this.value = value; 
    }

    public boolean isint() {
        return value instanceof Integer;
    }

    public boolean isbyte() {
        return value instanceof Byte;
    }

    public boolean isboolean() {
        return value instanceof Boolean;
    }

    public boolean ischar() {
        return value instanceof Character;
    }

    public boolean isshort() {
        return value instanceof Short;
    }

    public boolean isfloat() {
        return value instanceof Float;
    }

    public boolean islong() {
        return value instanceof Long;
    }

    public boolean isdouble() {
        return value instanceof Double;
    }

    public int asint() {
        return (Integer)value;
    }

    public byte asbyte() {
        return (Byte)value;
    }

    public boolean asboolean() {
        return (Boolean)value;
    }

    public char aschar() {
        return (Character)value;
    }

    public short asshort() {
        return (Short)value;
    }

    public float asfloat() {
        return (Float)value;
    }

    public long aslong() {
        return (Long)value;
    }

    public double asdouble() {
        return (Double)value;
    }
}
