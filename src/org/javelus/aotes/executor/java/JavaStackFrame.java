package org.javelus.aotes.executor.java;

import org.javelus.aotes.asm.vm.MethodInfo;
import org.javelus.aotes.asm.vm.StackFrame;

public class JavaStackFrame extends StackFrame<Object> {

    public JavaStackFrame(MethodInfo mi) {
        super(mi);
    }

    @Override
    protected Object[] createLocalsAndExpressionStack(int length) {
        return new Object[length];
    }

}
