package org.javelus.aotes.executor.java;

import static org.objectweb.asm.Opcodes.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.javelus.aotes.asm.value.constant.AddressValue;
import org.javelus.aotes.asm.value.operator.MethodOperatorFactory;
import org.javelus.aotes.asm.value.operator.OpcodeOperator;
import org.javelus.aotes.asm.value.operator.OpcodeOperatorFactory;
import org.javelus.aotes.asm.value.operator.RelationalOperator;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ClassLoaderInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.Instruction;
import org.javelus.aotes.asm.vm.MethodInfo;
import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.asm.vm.StackFrame;
import org.javelus.aotes.asm.vm.ThreadInfo;
import org.javelus.aotes.executor.EvalMap;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.location.WrappedObject;
import org.objectweb.asm.Type;

public class JavaInterpreter {
    private static boolean debug = false;

    MethodInfo mi;
    Executor executor;
    JavaObjectMap objectMap;
    EvalMap evalMap;
    ThreadInfo<Object> threadInfo;

    public JavaInterpreter(Executor executor, Method method) {
        this(executor, ReflectionHelper.methodToMethodInfo(method));
    }

    public JavaInterpreter(Executor executor, Constructor<?> ctor) {
        this(executor, ReflectionHelper.constructorToMethodInfo(ctor));
    }

    public JavaInterpreter(Executor executor, MethodInfo mi) {
        this.executor = executor;
        this.threadInfo = new ThreadInfo<Object>();
        this.objectMap = (JavaObjectMap)executor.getObjectMap();
        this.evalMap = executor.getEvalMap();
        this.mi = mi;
    }

    public void interpret() {
        StackFrame<Object> frame = null;
        long count = 0;
        while ((frame = threadInfo.getTopFrame())!= null) {
            count ++ ;
            if (count > 100000L) {
                System.out.println("We have executed too many instructions " + count + ", and we have to interrupt the execution");
                threadInfo.printStackTrace();
                //throw new RuntimeException("Potential dead loop...");
                exitWithException(frame, "java/lang/InterruptedException");
                return;
            }
            Instruction insn = frame.getPC();
            int opcode = insn.getOpcode();
            if (opcode > 0 && debug) System.out.println("Executing " + insn + " in " + frame);
            switch(opcode) {
            case -1: // LableNode, Frame
            case NOP:
                frame.advance();
                break;
            case ACONST_NULL:
                frame.push(null);
                frame.advance();
                break;
            case ICONST_M1:
                frame.push(Integer.valueOf(-1));
                frame.advance();
                break;
            case ICONST_0:
                frame.push(Integer.valueOf(0));
                frame.advance();
                break;
            case ICONST_1:
                frame.push(Integer.valueOf(1));
                frame.advance();
                break;
            case ICONST_2:
                frame.push(Integer.valueOf(2));
                frame.advance();
                break;
            case ICONST_3:
                frame.push(Integer.valueOf(3));
                frame.advance();
                break;
            case ICONST_4:
                frame.push(Integer.valueOf(4));
                frame.advance();
                break;
            case ICONST_5:
                frame.push(Integer.valueOf(5));
                frame.advance();
                break;
            case LCONST_0:
                frame.pushLong(Long.valueOf(1L));
                frame.advance();
                break;
            case LCONST_1:
                frame.pushLong(Long.valueOf(1L));
                frame.advance();
                break;
            case FCONST_0:
                frame.push(Float.valueOf(0F));
                frame.advance();
                break;
            case FCONST_1:
                frame.push(Float.valueOf(1F));
                frame.advance();
                break;
            case FCONST_2:
                frame.push(Float.valueOf(2F));
                frame.advance();
                break;
            case DCONST_0:
                frame.pushLong(Double.valueOf(0D));
                frame.advance();
                break;
            case DCONST_1:
                frame.pushLong(Double.valueOf(1D));
                frame.advance();
                break;
            case IALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case LALOAD:
                arrayLoad(frame, true);
                frame.advance();
                break;
            case FALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case DALOAD:
                arrayLoad(frame, true);
                frame.advance();
                break;
            case AALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case BALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case CALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case SALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case IASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case LASTORE:
                arrayStore(frame, true);
                frame.advance();
                break;
            case FASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case DASTORE:
                arrayStore(frame, true);
                frame.advance();
                break;
            case AASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case BASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case CASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case SASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case POP:
                frame.pop();
                frame.advance();
                break;
            case POP2:
                frame.popLong();
                frame.advance();
                break;
            case DUP:
                frame.dup();
                frame.advance();
                break;
            case DUP_X1:
                frame.dup_x1();
                frame.advance();
                break;
            case DUP_X2:
                frame.dup_x2();
                frame.advance();
                break;
            case DUP2:
                frame.dup2();
                frame.advance();
                break;
            case DUP2_X1:
                frame.dup2_x1();
                frame.advance();
                break;
            case DUP2_X2:
                frame.dup2_x2();
                frame.advance();
                break;
            case SWAP:
                frame.swap();
                frame.advance();
                break;
            case IADD:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LADD:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FADD:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DADD:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case ISUB:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSUB:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FSUB:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DSUB:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IMUL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LMUL:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FMUL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DMUL:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IDIV:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LDIV:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FDIV:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DDIV:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IREM:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LREM:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FREM:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DREM:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case INEG:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LNEG:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FNEG:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DNEG:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case ISHL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSHL:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case ISHR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSHR:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case IUSHR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LUSHR:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case IAND:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LAND:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IOR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LOR:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IXOR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LXOR:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case I2L:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case I2F:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2D:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case L2I:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case L2F:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case L2D:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case F2I:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case F2L:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case F2D:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case D2I:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case D2L:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case D2F:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case I2B:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2C:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2S:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LCMP:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case FCMPL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case FCMPG:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DCMPL:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case DCMPG:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case IRETURN:
                _return(frame, 1);
                break;
            case LRETURN:
                _return(frame, 2);
                break;
            case FRETURN:
                _return(frame, 1);
                break;
            case DRETURN:
                _return(frame, 2);
                break;
            case ARETURN:
                _return(frame, 1);
                break;
            case RETURN:
                _return(frame, 0);
                break;
            case ARRAYLENGTH:
                arrayLength(frame, insn);
                frame.advance();
                break;
            case ATHROW:
                _throw(frame);
                break;
            case MONITORENTER:
                frame.pop();
                frame.advance();
                break;
            case MONITOREXIT:
                frame.pop();
                frame.advance();
                break;
            case ILOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case LLOAD:
                frame.pushLong(frame.getLongLocal(insn.getVar()));
                frame.advance();
                break;
            case FLOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case DLOAD:
                frame.pushLong(frame.getLongLocal(insn.getVar()));
                frame.advance();
                break;
            case ALOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case ISTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case LSTORE:
                frame.setLongLocal(insn.getVar(), frame.popLong());
                frame.advance();
                break;
            case FSTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case DSTORE:
                frame.setLongLocal(insn.getVar(), frame.popLong());
                frame.advance();
                break;
            case ASTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case IINC:
                iinc(frame, insn);
                frame.advance();
                break;
            case IFEQ:
            case IFNE:
            case IFLT:
            case IFGE:
            case IFGT:
            case IFLE:
                _if(frame, insn, Integer.valueOf(0));
                break;
            case IF_ICMPEQ:
            case IF_ICMPNE:
            case IF_ICMPLT:
            case IF_ICMPGE:
            case IF_ICMPGT:
            case IF_ICMPLE:
            case IF_ACMPEQ:
            case IF_ACMPNE:
                _if(frame, insn, frame.pop());
                break;
            case IFNULL:
            case IFNONNULL:
                _if(frame, insn, null);
                break;
            case GOTO:
                frame.setPC(insn.getTarget());
                break;
            case BIPUSH:
            case SIPUSH:
                frame.push(Integer.valueOf(insn.getInt()));
                frame.advance();
                break;
            case NEWARRAY:
                frame.push(newarray(frame, insn));
                frame.advance();
                break;
            case NEW:
                frame.push(_new(frame, insn));
                frame.advance();
                break;
            case ANEWARRAY:
                frame.push(anewarray(frame, insn));
                frame.advance();
                break;
            case CHECKCAST:
                checkcast(frame, insn);
                frame.advance();
                break;
            case INSTANCEOF:
                _instanceof(frame, insn);
                frame.advance();
                break;
            case GETSTATIC:
                getfield_or_static(frame, insn, true);
                frame.advance();
                break;
            case PUTSTATIC:
                putfield_or_static(frame, insn, true);
                frame.advance();
                break;
            case GETFIELD:
                getfield_or_static(frame, insn, false);
                frame.advance();
                break;
            case PUTFIELD:
                putfield_or_static(frame, insn, false);
                frame.advance();
                break;
            case INVOKEVIRTUAL:
                invoke(frame, insn, true, false);
                break;
            case INVOKESPECIAL:
                invoke(frame, insn, false, false);
                break;
            case INVOKESTATIC:
                invoke(frame, insn, false, true);
                break;
            case INVOKEINTERFACE:
                invoke(frame, insn, true, false);
                break;
            case INVOKEDYNAMIC: {
                System.out.println("invokedynamic is not supported now.");
                threadInfo.printStackTrace();
                exitWithException(frame, "java/lang/InterruptedException");
                return;}
            case LDC:
                ldc(frame, insn);
                frame.advance();
                break;
            case LOOKUPSWITCH:
                lookupswitch(frame, insn);
                break;
            case TABLESWITCH:
                tableswitch(frame, insn);
                break;
            case MULTIANEWARRAY:
                multianewarray(frame, insn);
                frame.advance();
                break;
            case RET:
                AddressValue saved = (AddressValue) frame.getLocal(insn.getVar());
                frame.setPC((Instruction)saved.getAddress());
                break;
            case JSR:
                AddressValue save = new AddressValue(frame.getPC());
                frame.push(save);
                frame.setPC(insn.getTarget());
                break;
            default:
                throw new RuntimeException("Should not reach here");
            }
        }
    }

    private void exitWithException(StackFrame<Object> frame, String className) {
        ClassInfo ci = ClassLoaderInfo.getResolvedClassInfo(className);
        if (debug) System.out.println("Throw an exception " + className);
        Class<?> clazz = ReflectionHelper.classInfoToClass(ci);
        Object exception = newObject(clazz);
        threadInfo.setPendingException(exception);
        if (frame != threadInfo.getTopFrame()) {
            throw new RuntimeException("sanity check failed");
        }
        while (frame != null) {
            frame = threadInfo.popFrame();
        }
    }

    protected void lookupswitch(StackFrame<Object> frame, Instruction insn) {
        Object key = frame.pop();
        int branch = evaluateLookupSwitch(insn, key);
        Instruction target;
        if (branch == -1) {
            target = insn.getLookupSwitchDefault();
        } else {
            target = insn.getLookupSwitchBranch(branch);
        }

        frame.setPC(target);
    }


    private int evaluateLookupSwitch(Instruction insn, Object key) {
        throw new RuntimeException("Not implemented");
    }

    protected void tableswitch(StackFrame<Object> frame, Instruction insn) {
        Object index = frame.pop();
        int branch = evaluateTableSwitch(insn, index);
        Instruction target;
        if (branch == -1) {
            target = insn.getTableSwitchDefault();
        } else {
            target = insn.getTableSwitchBranch(branch);
        }

        frame.setPC(target);
    }


    private int evaluateTableSwitch(Instruction insn, Object index) {
        throw new RuntimeException("Not implemented");
    }

    protected void multianewarray(StackFrame<Object> frame, Instruction insn) {
        int dims = insn.getMultiDims();
        Object[] arrayLengths = new Object[dims];
        for (int i = dims - 1; i >= 0; i--) {
            arrayLengths[i] = frame.pop();
        }
        ClassInfo target = frame.resolveClassInfoOrNull(insn.getMultiArrayDesc());
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return;
        }
        Object array = newMultiArray(insn, target, arrayLengths, 0);
        frame.push(array);
    }

    private Object newMultiArray(Instruction insn, ClassInfo target, Object[] arrayLengths, int i) {
        throw new RuntimeException("Not implemented");
    }

    protected void ldc(StackFrame<Object> frame, Instruction insn) {
        Object cst = insn.getConstant();

        if (cst instanceof Integer) {
            frame.push(Integer.valueOf((Integer)cst));
        } else if (cst instanceof Float) {
            frame.push(Float.valueOf((Float)cst));
        } else if (cst instanceof Double) {
            frame.pushLong(Double.valueOf((Double)cst));
        } else if (cst instanceof Long) {
            frame.pushLong(Long.valueOf((Long)cst));
        } else if (cst instanceof String) {
            frame.push(cst);
        } else if (cst instanceof Type) {
            Type type = (Type) cst;
            ClassInfo target = frame.resolveClassInfoOrNull(type.getInternalName());
            if (target == null) {
                createAndThrowClassNotFoundException(frame);
                return;
            }
            frame.push(ReflectionHelper.classInfoToClass(target));
        } else {
            throw new RuntimeException("Should not reach here");
        }
    }

    protected boolean checkAndHandleMethodOperatorForLogger(StackFrame<Object> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isLoggerMethodOperator(callee)) {
            if (checkMethodArgs(frame, callee)) {
                createAndPushMethodOperatorValue(frame, callee);
                return true;
            }
        }

        return false;
    }

    protected void createAndPushMethodOperatorValue(StackFrame<Object> frame, MethodInfo callee) {
        Method method = ReflectionHelper.methodInfoToMethod(callee);
        int returnSize = callee.getReturnSize();
        Object[] argv = popMethodArgs(callee, frame);
        Object result = null;
        Object recv = null;
        if (!callee.isStatic()) {
            recv = frame.pop();
        }
        try {
            result = method.invoke(recv, (Object[])argv);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException("Invoke method " + mi + " failed.", e);
        }
        if (returnSize == 1) {
            frame.push(result);
        } else if (returnSize == 2) {
            frame.pushLong(result);
        }
        frame.advance();
    }

    private boolean checkMethodArgs(StackFrame<Object> frame, MethodInfo callee) {
        int argOffset = callee.getArgumentsSize() - 1;
        Object value = null;

        if (!callee.isStatic()) {
            value = frame.peek(argOffset--);
            if (value instanceof WrappedObject) {
                return false;
            }
        }
        Type[] types = callee.getArgumentTypes();
        for (int i=0; i<types.length; i++) {
            int sort = types[i].getSort();

            switch (sort) {
            case Type.DOUBLE:
            case Type.LONG:
                value = frame.peek(argOffset);
                argOffset -= 2;
                if (value instanceof WrappedObject) {
                    return false;
                }
                break;
            case Type.FLOAT:
            case Type.INT:
            case Type.CHAR:
            case Type.BYTE:
            case Type.BOOLEAN:
            case Type.SHORT:
            default:
                value = frame.peek(argOffset--);
                if (value instanceof WrappedObject) {
                    return false;
                }
            }
        }
        return true;

    }

    private Object[] popMethodArgs(MethodInfo callee, StackFrame<Object> frame) {
        Object[] argv = new Object[callee.getNumOfArguments()];
        int argIndex = argv.length - 1;
        Type[] types = callee.getArgumentTypes();
        for (int i=0; i<types.length; i++) {
            int sort = types[i].getSort();

            switch (sort) {
            case Type.DOUBLE:
            case Type.LONG:
                argv[argIndex--] = JavaEvalMap.getValue(frame.popLong());
                break;
            case Type.FLOAT:
            case Type.INT:
            case Type.CHAR:
            case Type.BYTE:
            case Type.BOOLEAN:
            case Type.SHORT:
            default:
                argv[argIndex--] = JavaEvalMap.getValue(frame.pop());
            }
        }
        return argv;
    }

    protected boolean checkAndHandleMethodOperatorForNative(StackFrame<Object> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isNativeMethodOperator(callee)) {
            if (checkMethodArgs(frame, callee)) {
                createAndPushMethodOperatorValue(frame, callee);
                return true;
            }
        }

        return false;
    }

    protected void invoke(StackFrame<Object> frame, Instruction insn, boolean isDynamic, boolean isStatic) {
        Object recv = null;
        String owner = insn.getMethodOwner();
        String name = insn.getMethodName();
        String desc = insn.getMethodDesc();
        MethodInfo staticCallee = frame.resolveMethod(owner, name, desc);
        if (staticCallee == null) {
            // infeasible path of instanceof
            System.err.println("Cannot find method " + owner + "." + name + desc);
            frame.resolveMethod(owner, name, desc);
            createAndThrowException(frame, "java/lang/NoSuchMethodError");
            return;
        }

        MethodInfo callee = null;
        int argumentSize = staticCallee.getArgumentsSize();
        if (!isStatic) {
            recv = frame.peek(argumentSize-1);
            if (recv == null) {
                createAndThrowNullPointerException(frame);
                return;
            }
        }

        if (checkAndHandleMethodOperatorForLogger(frame, staticCallee)) {
            return;
        }

        if (isDynamic) {
            callee = resolveMethodVirtual(frame, recv, name, desc);
            if (callee == null) {
                // infeasible path of instanceof
                createAndThrowException(frame, "java/lang/IncompatibleClassChangeError");
                return;
            }
        } else {
            callee = staticCallee;
        }

        if (callee.isAbstract()) {
            createAndThrowException(frame, "java/lang/AbstractMethodError");
            return;
        }

        if (callee.isNative()) {
            if (checkAndCloneObject(frame, callee)) {
                return;
            }
            if (checkAndHandleMethodOperatorForNative(frame, callee)) {
                return;
            }
            createAndThrowException(frame, "java/lang/UnsatisfiedLinkError");
            return;
        }

        StackFrame<Object> calleeFrame = new JavaStackFrame(callee);
        threadInfo.pushFrame(calleeFrame);
        calleeFrame.setupArguments(argumentSize);
    }

    private MethodInfo resolveMethodVirtual(StackFrame<Object> frame, Object recv, String name, String desc) {
        ClassInfo ci = ReflectionHelper.classToClassInfo(ReflectionHelper.getObjectClass(recv));
        return ci.getMethod(name, desc);
    }

    protected boolean checkAndCloneObject(StackFrame<Object> frame, MethodInfo callee) {
        ClassInfo ci = callee.getClassInfo();
        if (!ci.getName().equals("java.lang.Object")
                || !callee.getName().equals("clone")) {
            return false;
        }
        Object recv = frame.peek();
        Object cloned = objectMap.cloneObject(recv);
        if (cloned == null) {
            return false;
        }
        frame.pop();
        frame.push(cloned);
        frame.advance();
        return true;
    }

    protected void getfield_or_static(StackFrame<Object> frame, Instruction insn, boolean isStatic) {
        FieldInfo fi = frame.resolveField(insn.getFieldOwner(), insn.getFieldName(), insn.getFieldDesc());
        if (fi == null) {
            createAndThrowException(frame, "java/lang/NoSuchFieldError");
            return;
        }
        Object ret;
        if (isStatic) {
            ret = getFieldLocation(null, fi);
        } else {
            Object recv = frame.peek();
            if (recv == null) {
                createAndThrowNullPointerException(frame);
                return;
            }
            recv = frame.pop();
            ret = getFieldLocation(recv, fi);
        }

        if (fi.getSize() == 1) {
            frame.push(ret);
        } else {
            frame.pushLong(ret);
        }
    }

    private Object getFieldLocation(Object object, FieldInfo fi) {
        ClassInfo ci = fi.getClassInfo();
        ClassInfo sci = ci.getSuperClass();
        if (fi.isStatic()) {
            return objectMap.getStatic(fi.getClassInfo().getName() + '.' + fi.getName());
        }
        if ((sci != null && sci.getField(fi.getName()) != null)) {
            return objectMap.getField(object, fi.getClassInfo().getName() + '.' + fi.getName());
        }
        return objectMap.getField(object, fi.getName());
    }

    protected void putfield_or_static(StackFrame<Object> frame, Instruction insn, boolean isStatic) {
        FieldInfo fi = frame.resolveField(insn.getFieldOwner(), insn.getFieldName(), insn.getFieldDesc());
        if (fi == null) {
            createAndThrowException(frame, "java/lang/NoSuchFieldError");
            return;
        }
        Object recv = null;
        if (!isStatic) {
            recv = frame.peek(fi.getSize());
            if (recv == null) {
                createAndThrowNullPointerException(frame);
                return;
            }
        }

        Object value;
        if (fi.getSize() == 1) {
            value = frame.pop();
        } else {
            value = frame.popLong();
        }

        if (isStatic) {
            setFieldLocation(null, fi, value);
        } else {
            frame.pop(); // recv;
            setFieldLocation(recv, fi, value);
        }
    }

    private void setFieldLocation(Object object, FieldInfo fi, Object value) {
        ClassInfo ci = fi.getClassInfo();
        ClassInfo sci = ci.getSuperClass();
        if (fi.isStatic()) {
            objectMap.revertStatic(fi.getClassInfo().getName() + '.' + fi.getName(), value);
            return;
        }
        if ((sci != null && sci.getField(fi.getName()) != null)) {
            objectMap.revertField(object, fi.getClassInfo().getName() + '.' + fi.getName(), value);
        } else {
            objectMap.revertField(object, fi.getName(), value);
        }
    }

    protected void checkcast(StackFrame<Object> frame, Instruction insn) {
        Object object = frame.peek();
        if (object == null) {
            return;
        }

        ClassInfo target = frame.resolveClassInfoOrNull(insn.getDesc());
        Class<?> targetClass = ReflectionHelper.classInfoToClass(target);
        Class<?> objectClass = ReflectionHelper.getObjectClass(object);
        if (targetClass.isAssignableFrom(objectClass)) {
            return;
        }
        this.createAndThrowException(frame, "java/lang/ClassCastException");
    }

    protected void _instanceof(StackFrame<Object> frame, Instruction insn) {
        Object object = frame.peek();
        ClassInfo target = frame.resolveClassInfoOrNull(insn.getDesc());
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return;
        }
        if (object == null) {
            frame.pop();
            frame.push(Integer.valueOf(0));
            return;
        }

        frame.pop();
        Class<?> targetClass = ReflectionHelper.classInfoToClass(target);
        Class<?> objectClass = ReflectionHelper.getObjectClass(object);
        if (targetClass.isAssignableFrom(objectClass)) {
            frame.push(Integer.valueOf(1));
        } else {
            frame.push(Integer.valueOf(0));
        }
    }


    protected void createAndThrowClassNotFoundException(StackFrame<Object> frame) {
        createAndThrowException(frame, "java/lang/ClassNotFoundException");
    }

    /**
     * 
     * @param className internal name
     * @return
     */
    protected Object _new(StackFrame<Object> frame, Instruction insn) {
        ClassInfo target = frame.resolveClassInfoOrNull(insn.getDesc());
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return null;
        }
        Class<?> clazz = ReflectionHelper.classInfoToClass(target);
        return objectMap.newObject(clazz);
    }

    protected Object anewarray(StackFrame<Object> frame, Instruction insn) {
        return newarrayCommon(frame, insn, arrayClassNameForClass(insn.getDesc()), frame.pop());
    }

    protected Object newarray(StackFrame<Object> frame, Instruction insn) {
        return newarrayCommon(frame, insn, arrayClassNameForPrimitiveType(insn.getInt()), frame.pop());
    }

    protected Object newarrayCommon(StackFrame<Object> frame, Instruction insn, String arrayClassName, Object length) {
        ClassInfo target = frame.resolveClassInfoOrNull(arrayClassName);
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return null;
        }
        Class<?> clazz = ReflectionHelper.classInfoToClass(target);
        return objectMap.newArray(clazz, length);
    }

    static String arrayClassNameForClass(String desc) {
        if (desc.charAt(0) == '[') {
            return "[" + desc;
        }
        return "[L" + desc + ";";
    }

    static String arrayClassNameForPrimitiveType(int type) {
        switch(type) {
        case T_BOOLEAN:
            return "[Z";
        case T_CHAR:
            return "[C";
        case T_FLOAT:
            return "[F";
        case T_DOUBLE:
            return "[D";
        case T_BYTE:
            return "[B";
        case T_SHORT:
            return "[S";
        case T_INT:
            return "[I";
        case T_LONG:
            return "[J";
        }
        throw new RuntimeException("invalid primitive type code " + type);
    }

    protected void iinc(StackFrame<Object> frame, Instruction insn) {
        Object v = frame.getLocal(insn.getIincVar());
        Object incr = Integer.valueOf(insn.getIincIncr());
        frame.setLocal(insn.getIincVar(), evaluateOperator(insn, v, incr));
    }


    protected void _throw(StackFrame<Object> frame) {
        Object exception = frame.pop();
        if (exception == null) {
            createAndThrowNullPointerException(frame);
            return;
        }
        threadInfo.setPendingException(exception);
        ClassInfo ci = ReflectionHelper.classToClassInfo(ReflectionHelper.getObjectClass(exception));
        threadInfo.handleException(frame, ci);
    }


    protected void arrayLength(StackFrame<Object> frame, Instruction insn) {
        Object array = frame.peek();
        if (array == null) {
            createAndThrowNullPointerException(frame);
            return;
        }

        frame.pop();
        frame.push(ReflectionHelper.getArrayLength(array));
    }



    protected void createAndThrowException(StackFrame<Object> frame, String className) {
        ClassInfo ci = ClassLoaderInfo.getResolvedClassInfo(className);
        Class<?> clazz = ReflectionHelper.classInfoToClass(ci);
        Object exception = newObject(clazz);
        if (debug) System.out.println("Throw an exception " + className);
        threadInfo.setPendingException(exception);
        threadInfo.handleException(frame, ci);
    }

    private Object newObject(Class<?> clazz) {
        return objectMap.newObject(clazz);
    }

    protected void createAndThrowNullPointerException(StackFrame<Object> frame) {
        createAndThrowException(frame, "java/lang/NullPointerException");
    }



    /**
     * ...,array, index, Object -> ...,
     * @param frame
     * @param longOrDouble
     */
    protected void arrayStore(StackFrame<Object> frame, boolean longOrDouble) {
        Object array = frame.peek(longOrDouble?3:2);

        if (array == null) {
            createAndThrowNullPointerException(frame);
            return;
        }

        Object value = null;
        if (longOrDouble) {
            value = frame.popLong();
        } else {
            value = frame.pop();
        }
        Object index = frame.pop();
        array = frame.pop();

        setArrayElement(array, index, value);
    }

    private void setArrayElement(Object array, Object index, Object value) {
        objectMap.revertArrayElement(array, index, value);
    }

    protected void _if(StackFrame<Object> frame, Instruction insn, Object v2) {
        Object v1 = frame.pop();
        if (evaluateIf(insn, v1, v2)) {
            frame.setPC(insn.getTarget());
        } else {
            frame.setPC(insn.getNext());
        }
    }

    private boolean evaluateIf(Instruction insn, Object v1, Object v2) {
        RelationalOperator cp = OpcodeOperatorFactory.getComparator(insn);
        switch (cp) {
        case EQ:
            return evalMap.eq(v1, v2);
        case NE:
            return evalMap.ne(v1, v2);
        case GT:
            return evalMap.gt(v1, v2);
        case GE:
            return evalMap.ge(v1, v2);
        case LT:
            return evalMap.lt(v1, v2);
        case LE:
            return evalMap.le(v1, v2);
        }

        throw new RuntimeException("Unknown branch instruction " + insn);
    }

    /**
     * ...,array, index -> ..., array[index] 
     * @param frame
     * @param longOrDouble
     */
    protected void arrayLoad(StackFrame<Object> frame, boolean longOrDouble) {
        Object array = frame.peek(1);
        if (array == null) {
            createAndThrowNullPointerException(frame);
            return;
        }
        Object index = frame.pop();
        frame.pop();

        Object value = getArrayElement(array, index);

        if (longOrDouble) {
            frame.pushLong(value);
        } else {
            frame.push(value);
        }
    }

    private Object getArrayElement(Object array, Object index) {
        return objectMap.getArrayElement(array, index);
    }

    static ClassInfo getArrayClassInfo(Instruction insn) {
        int opcode = insn.getOpcode();
        switch (opcode) {
        case IALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[I");
        case LALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[J");
        case FALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[F");
        case DALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[D");
        case AALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[Ljava/lang/Object;");
        case BALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[B");
        case CALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[C");
        case SALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[S");
        }
        throw new RuntimeException("Invalid insn " + insn);
    }

    protected void binaryOperator(StackFrame<Object> frame, Instruction insn, boolean longOrDoubleOperand) {
        binaryOperator(frame, insn, longOrDoubleOperand, longOrDoubleOperand, longOrDoubleOperand);
    }

    /**
     * v1, v2 -> ret
     * v1 is left operand and v2 is right operand.
     * @param frame
     * @param insn
     * @param longOrDoubleOperandLeft
     * @param longOrDoubleOperandRight
     * @param longOrDoubleResult
     */
    protected void binaryOperator(StackFrame<Object> frame, Instruction insn, boolean longOrDoubleOperandLeft, boolean longOrDoubleOperandRight, boolean longOrDoubleResult) {
        Object v1, v2, result;
        if (longOrDoubleOperandRight) {
            v2 = frame.popLong();
        } else {
            v2 = frame.pop();
        }

        if (longOrDoubleOperandLeft) {
            v1 = frame.popLong();
        } else {
            v1 = frame.pop();
        }

        result = evaluateOperator(insn, v1, v2);

        if (longOrDoubleResult) {
            frame.pushLong(result);
        } else {
            frame.push(result);
        }
    }

    protected void unaryOperator(StackFrame<Object> frame, Instruction insn, boolean longOrDoubleOperand) {
        unaryOperator(frame, insn, longOrDoubleOperand, longOrDoubleOperand);
    }

    protected void unaryOperator(StackFrame<Object> frame, Instruction insn, boolean longOrDoubleOperand, boolean longOrDoubleResult) {
        Object v, result;
        if (longOrDoubleOperand) {
            v = frame.popLong();
        } else {
            v = frame.pop();
        }

        result = evaluateOperator(insn, v);

        if (longOrDoubleResult) {
            frame.pushLong(result);
        } else {
            frame.push(result);
        }
    }

    private Object evaluateOperator(Instruction insn, Object ... v) {
        OpcodeOperator op = OpcodeOperatorFactory.getOpcodeOperator(insn.getOpcode());
        String type = op.type;
        return evalMap.op(type).eval(v);
    }

    protected void _return(StackFrame<Object> frame, int returnSize) {
        Object ret = null;
        if (returnSize == 1) {
            ret = frame.peek(); // do not pop, we may use this value during framePop event
        } else if (returnSize == 2) {
            ret = frame.peekLong();
        }

        MethodInfo mi = frame.getMethodInfo();
        StackFrame<Object> caller = threadInfo.popFrame();

        if (caller == null) {
            return;
        }

        caller.pop(mi.getArgumentsSize());

        if (returnSize == 1) {
            caller.push(ret);
        } else if (returnSize == 2) {
            caller.pushLong(ret);
        }

        caller.advance();
    }


    public void execute(Object ... argv) {
        StackFrame<Object> frame = new JavaStackFrame(mi);

        int argOffset = 0;
        int argIndex = 0;
        if (!mi.isStatic()) {
            argOffset = frame.setLocal(argOffset, objectMap.getTarget());
        }

        Type[] types = mi.getArgumentTypes();
        for (int i=0; i<types.length; i++) {
            int sort = types[i].getSort();

            switch (sort) {
            case Type.DOUBLE:
            case Type.LONG:
                argOffset = frame.setLongLocal(argOffset, argv[argIndex++]);
                break;
            case Type.FLOAT:
            case Type.INT:
            case Type.CHAR:
            case Type.BYTE:
            case Type.BOOLEAN:
            case Type.SHORT:
            default:
                argOffset = frame.setLocal(argOffset, argv[argIndex++]);
            }
        }

        threadInfo.pushFrame(frame);

        interpret();

        Object exception = threadInfo.getPendingException();
        if (exception != null) {
            executor.abort("An exception " + ReflectionHelper.getObjectClass(exception).getName());
        }
    }
}
