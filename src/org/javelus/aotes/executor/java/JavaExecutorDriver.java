package org.javelus.aotes.executor.java;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.javelus.DeveloperInterface;
import org.javelus.aotes.executor.java.mov.MethodIDHelper;
import org.javelus.aotes.executor.java.mov.MethodSequenceElement;
import org.javelus.aotes.executor.java.mov.MethodSequenceObject;
import org.javelus.aotes.executor.java.mov.SimpleMethodSequenceObject;

public class JavaExecutorDriver {

    private static final boolean useDynamicDeltaExecutor = true;

    public static Object replay(SimpleMethodSequenceObject mso) throws Exception {
        if (mso == null) {
            return null;
        }
        MethodSequenceElement[] mses = mso.getSequence();
        if (mses.length == 0) {
            return null;
        }
        System.out.println("Replay history length: " + mses.length);
        System.out.println(mso);
        int index = mses.length - 1;
        MethodSequenceElement last = mses[index];
        Object result = null;
        if (last.isInit()) {
            Constructor<?> ctor = (Constructor<?>) MethodIDHelper.resolveMethodOrConstructor(last.getMethodID());
            ctor.setAccessible(true);
            result = ctor.newInstance(last.getArguments());
        } else {
            Method method = (Method) MethodIDHelper.resolveMethodOrConstructor(last.getMethodID());
            result = mso.getTarget();
            method.setAccessible(true);
            method.invoke(result, last.getArguments());
        }
        for (int i = index - 1; i >= 0; i--) {
            MethodSequenceElement mse = mses[i];
            Method method = (Method) MethodIDHelper.resolveMethodOrConstructor(mse.getMethodID());
            method.setAccessible(true);
            method.invoke(result, mse.getArguments());
        }
        return result;
    }

    public static void defaultTransformation(Object stale) {
        // touch the object and trigger and update
        DeveloperInterface.getMixThat(stale);
    }

    public static boolean isDefaultMode() {
        return mode("default");
    }

    public static boolean isTOSMode() {
        return mode("tos");
    }

    public static boolean isJavelusMode() {
        return mode("javelus");
    }

    public static boolean mode(String mode) {
        return System.getProperty("aotes.mode", "javelus").equals(mode) ;
    }

    public static void doAll(List<Object> staleObjects, String fileName) {
        // 1) Dump heap for TOS
        if (isTOSMode()) {
            File file = new File(".");
            if (!file.exists()) {
                file.mkdirs();
            }
            dumpHeap(new File(file, fileName).getAbsolutePath());
            return;
        }

        if (isDefaultMode()) {
            invokeDSU();

            for (Object stale:staleObjects) {
                defaultTransformation(stale);
            }

            return;
        }

        // 2) Build history
        List<Object> transformed = transformAll(staleObjects, true);

        // 3) Update classes
        invokeDSU();

        // 4) Update objects
        updateAll(transformed);
    }

    public static void invokeDSU() {
        JavelusUpdater.invokeDSU();
    }

    public static void dumpHeap(String fileName) {
        HeapDumper.dumpHeap(fileName, true);
    }

    public static List<Object> transformAll(List<Object> staleObjects) {
        return transformAll(staleObjects, false);
    }

    public static List<Object> transformAll(List<Object> staleObjects, boolean writeBack) {
        if (isTOSMode() || isDefaultMode()) {
            return null;
        }

        List<Object> transformed = new ArrayList<Object>();

        for (Object stale : staleObjects) {
            transformed.add(transform(stale, writeBack));
        }

        return transformed;
    }

    public static void assertTrue(boolean value) {
        if (value == false) {
            //            StackTraceElement[] ste = Thread.currentThread().getStackTrace();
            //            System.out.println(ste[2].getMethodName() + " in " + ste[2].getClassName() + " FAILED");
            throw new RuntimeException("Assertion failed!");
        } else {
            //            StackTraceElement[] ste = Thread.currentThread().getStackTrace();
            //            System.out.println(ste[2].getMethodName() + " in " + ste[2].getClassName() + " PASSED");
        }
    }

    public static List<Object> updateAll(List<Object> transformed) {
        List<Object> updated = new ArrayList<Object>();

        JavaUpdater updater = null;

        if (isJavelusMode()) {
            updater = new JavelusUpdater();
        } else {
            updater = new JavaUpdater();
        }

        for (Object mso : transformed) {
            updated.add(updater.transform((MethodSequenceObject<?>) mso));
        }

        return updated;
    }

    public static boolean isJavelus() {
        return JavelusUpdater.isJavelus && !isTOSMode();
    }

    public static Object transform(Object stale) {
        return transform(stale, false);
    }

    public static Object transform(Object stale, boolean writeBack) {
        if (isTOSMode() || isDefaultMode()) {
            return null;
        }
        Class<?> clazz = stale.getClass();

        String mutatorName = "AOTES_" + clazz.getName().replace('.', '_');

        Class<?> mutatorClass = null;

        try {
            mutatorClass = Class.forName(mutatorName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Fail to load class " + mutatorName);
        }

        JavaExecutor je = null;
        if (useDynamicDeltaExecutor) {
            je = new DynamicDeltaJavaExecutor(mutatorClass, writeBack);
        } else {
            je = new JavaExecutor(mutatorClass, writeBack);
        }

        long beginTime = System.nanoTime();
        try {
            return je.transform(stale);
        } finally {
            long endTime = System.nanoTime();
            System.out.println("Total transform time is " + TimeUnit.NANOSECONDS.toMillis(endTime-beginTime) + "ms");
        }
    }
}
