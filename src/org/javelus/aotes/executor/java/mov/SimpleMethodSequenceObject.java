package org.javelus.aotes.executor.java.mov;

import java.util.List;

import org.javelus.aotes.executor.Executor;

public class SimpleMethodSequenceObject implements MethodSequenceObject<Object> {

    Object actualObject;
    List<MethodSequenceElement> sequences;
    int top;
    Executor executor;
    public SimpleMethodSequenceObject(Executor executor, Object actualObject, List<MethodSequenceElement> sequences) {
        this.actualObject = actualObject;
        this.sequences = sequences;
        this.top = sequences.size() - 1;
        this.executor = executor;
    }

    public MethodSequenceElement pop(String methodID) {
        if (top < 1 || top > sequences.size()) {
            throw new RuntimeException("Inconsistent state: cannot pop");
        }
        return this.sequences.get(top--);
    }

    public int size() {
        return top  + 1;
    }

    public void push(MethodSequenceElement mse) {
        MethodSequenceElement in = this.sequences.get(top + 1);
        if (in != mse) {
            throw new RuntimeException("Inconsistent method sequence: push " + mse + " but get " + in);
        }
        top++;
    }

    public boolean isDone() {
        return top < 0;
    }

    public String toString() {
        if (top == -1) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<=top; i++) {
            sb.append(String.format("%3d %s\n", i, sequences.get(i)));
        }
        return sb.toString();
    }

    @Override
    public Executor getExecutor() {
        return executor;
    }

    @Override
    public Object getTarget() {
        return actualObject;
    }

    public MethodSequenceElement[] getSequence() {
        return this.sequences.toArray(new MethodSequenceElement[this.sequences.size()]);
    }

    @Override
    public void writeBack() {
        // We need a map to write back.
        //((JavaObjectMap)getExecutor().getObjectMap()).writeBack();
    }
}
