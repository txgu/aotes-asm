package org.javelus.aotes.executor.java.mov;

import org.javelus.aotes.executor.Executor;

public interface MethodSequenceObject<T> {
    T getTarget();
    Executor getExecutor();
    MethodSequenceElement pop(String methodID);
    void push(MethodSequenceElement mse);
    boolean isDone();
    int size();
    void writeBack();
}
