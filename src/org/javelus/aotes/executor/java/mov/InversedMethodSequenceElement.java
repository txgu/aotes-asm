package org.javelus.aotes.executor.java.mov;

import org.javelus.aotes.executor.InverseMutator;

public class InversedMethodSequenceElement extends MethodSequenceElement {

    InverseMutator inverseMutator;

    public InversedMethodSequenceElement(InverseMutator inverseMutator, String methodID, Object result,
            Object[] arguments) {
        super(methodID, result, arguments);
        this.inverseMutator = inverseMutator;
    }

    public InverseMutator getInverseMutator() {
        return inverseMutator;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(inverseMutator.getUniqueName());
        sb.append('@');
        sb.append(methodID);
        sb.append(", params=(");
        if (this.arguments.length > 0) {
            sb.append(arguments[0]);
            for (int i=1; i<arguments.length; i++) {
                sb.append(',');
                sb.append(arguments[i]);
            }
        }
        sb.append(')');

        return sb.toString();
    }
}
