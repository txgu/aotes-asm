package org.javelus.aotes.executor.java.mov;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.javelus.aotes.executor.IM;

public class MethodIDHelper {

    public static String getMethodID(IM im) {
        return String.format("%s.%s%s", im.clazz(), im.name(), im.desc());
    }

    public static String getMethodID(String clazz, String name, String desc) {
        return String.format("%s.%s%s", clazz, name, desc);
    }

    public static Class<?> resolveClass(String className) {
        if (className.equals("byte")) {
            return byte.class;
        }
        if (className.equals("char")) {
            return char.class;
        }
        if (className.equals("boolean")) {
            return boolean.class;
        }
        if (className.equals("short")) {
            return short.class;
        }
        if (className.equals("int")) {
            return int.class;
        }
        if (className.equals("long")) {
            return long.class;
        }
        if (className.equals("float")) {
            return float.class;
        }
        if (className.equals("double")) {
            return double.class;
        }

        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static Constructor<?> resolveConstructor(String className, String desc) {
        Class<?> c = resolveClass(className);

        String [] typeNames = getArgumentTypeNames(desc);
        Class<?>[] paramTypes = new Class<?>[typeNames.length];
        for (int i=0; i<typeNames.length; i++) {
            paramTypes[i] = resolveClass(typeNames[i]);
            if (paramTypes[i] == null) {
                return null;
            }
        }

        try {
            Constructor<?> ctor = c.getDeclaredConstructor(paramTypes);
            ctor.setAccessible(true);
            return ctor;
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static Object resolveMethodOrConstructor(String className, String methodName, String desc) {
        if (methodName.equals("<init>")) {
            return resolveConstructor(className, desc);
        }
        return resolveMethod(className, methodName, desc);
    }

    public static Method resolveMethod(String className, String methodName, String desc) {
        Class<?> c = resolveClass(className);

        String [] typeNames = getArgumentTypeNames(desc);
        Class<?>[] paramTypes = new Class<?>[typeNames.length];
        for (int i=0; i<typeNames.length; i++) {
            paramTypes[i] = resolveClass(typeNames[i]);
            if (paramTypes[i] == null) {
                return null;
            }
        }

        try {
            Method method = c.getDeclaredMethod(methodName, paramTypes);
            method.setAccessible(true);
            return method;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        return null;
    } 

    public static Object resolveMethodOrConstructor(Object id) {
        String strID = id.toString();

        int leftBracket = strID.indexOf('(');
        int dot = strID.lastIndexOf('.', leftBracket);

        if (dot == -1) {
            return null;
        }

        String className = strID.substring(0, dot);
        String methodName = strID.substring(dot+1, leftBracket);
        String args = strID.substring(leftBracket);

        return resolveMethodOrConstructor(className, methodName, args);
    }

    public static String getType(Class<?> c) {
        if (c.isArray()) {
            return c.getName();
        }

        if (c.isPrimitive()) {
            String name = c.getName();
            if (name.equals("byte")) {
                return "B";
            }
            if (name.equals("char")) {
                return "C";
            }
            if (name.equals("boolean")) {
                return "Z";
            }
            if (name.equals("short")) {
                return "S";
            }
            if (name.equals("int")) {
                return "I";
            }
            if (name.equals("long")) {
                return "J";
            }
            if (name.equals("float")) {
                return "F";
            }
            if (name.equals("double")) {
                return "D";
            }
            throw new RuntimeException("Unknown primitive type " + name);
        }

        return "L" + c.getName().replace('.', '/') + ";";
    }

    public static String[] getArgumentTypeNames (String signature) {
        int len = signature.length();

        if ((len > 1) && (signature.charAt(1) == ')')) {
            return new String[0]; // 'no args' shortcut
        }

        ArrayList<String> a = new ArrayList<String>();

        for (int i = 1; signature.charAt(i) != ')';) {
            int end = i + getTypeLength(signature,i);
            String arg = signature.substring(i, end);
            i = end;

            a.add(getTypeName(arg));
        }

        String[] typeNames = new String[a.size()];
        a.toArray(typeNames);

        return typeNames;
    }

    private static int getTypeLength (String signature, int idx) {
        switch (signature.charAt(idx)) {
        case 'B':
        case 'C':
        case 'D':
        case 'F':
        case 'I':
        case 'J':
        case 'S':
        case 'V':
        case 'Z':
            return 1;

        case '[':
            return 1 + getTypeLength(signature, idx + 1);

        case 'L':

            int semicolon = signature.indexOf(';', idx);

            if (semicolon == -1) {
                throw new RuntimeException("invalid type signature: " +
                        signature);
            }

            return semicolon - idx + 1;
        }

        throw new RuntimeException("invalid type signature " + signature);
    }

    public static String getTypeName (String signature) {
        int  len = signature.length();
        char c = signature.charAt(0);

        if (len == 1) {
            switch (c) {
            case 'B':
                return "byte";

            case 'C':
                return "char";

            case 'D':
                return "double";

            case 'F':
                return "float";

            case 'I':
                return "int";

            case 'J':
                return "long";

            case 'S':
                return "short";

            case 'V':
                return "void";

            case 'Z':
                return "boolean";
            }
        }

        if (c == '[') {
            return signature.replace('/', '.');
        }

        int len1 = len-1;
        if (signature.charAt(len1) == ';') {
            return signature.substring(1, len1).replace('/', '.');
        }

        throw new RuntimeException("invalid type string: " + signature);
    }

    public static boolean checkDesc(Class<?>[] pts, String desc) {
        return false;
    }

    //	public static boolean checkArgs(Class<?>[] pts, String args) {
    //		int offset = 0;
    //		for (Class<?> pt:pts) {
    //			String type = getType(pt);
    //			if (!args.startsWith(type, offset)) {
    //				return false;
    //			}
    //			offset += type.length();
    //		}
    //		return offset == args.length();
    //	}
}
