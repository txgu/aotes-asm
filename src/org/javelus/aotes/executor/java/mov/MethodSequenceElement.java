package org.javelus.aotes.executor.java.mov;

public class MethodSequenceElement {

    String methodID;
    Object result;
    Object[] arguments;

    public MethodSequenceElement(String methodID, Object[] arguments) {
        this(methodID, null, arguments);
    }

    public MethodSequenceElement(String methodID, Object result,
            Object[] arguments) {
        this.methodID = methodID;
        this.result = result;
        this.arguments = arguments;
    }

    public boolean isInit() {
        return this.methodID.contains("<init>");
    }

    public String getMethodID () {
        return this.methodID;
    }

    public Object getResult() {
        return result;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public Object getArgument(int index) {
        return arguments[index];
    }

    public boolean is(Object id) {
        return methodID.equals(id);
    }



    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(methodID);
        sb.append(", params=(");
        if (this.arguments.length > 0) {
            sb.append(arguments[0]);
            for (int i=1; i<arguments.length; i++) {
                sb.append(',');
                sb.append(arguments[i]);
            }
        }
        sb.append(')');

        return sb.toString();
    }
}
