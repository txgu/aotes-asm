package org.javelus.aotes.executor.java.img;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.javelus.aotes.executor.InverseMutator;
import org.javelus.aotes.executor.InverseMutatorGraph;

public class InverseMutatorGraphImpl implements InverseMutatorGraph {

    private static final boolean debug = false;

    public static InverseMutatorGraph build(Method[] methods) {
        return new InverseMutatorGraphImpl(methods);
    }

    private Map<String, InverseMutator> nodes;

    private List<InverseMutator> heads;
    private List<InverseMutator> tails;

    private Map<String, List<InverseMutator>> nodeToPreds;
    private Map<String, List<InverseMutator>> nodeToSuccs;

    InverseMutatorGraphImpl(Method[] methods) {
        nodes = new HashMap<String, InverseMutator>();
        for (Method m:methods) {
            InverseMutatorImpl im = new InverseMutatorImpl(m);
            nodes.put(im.getUniqueName(), im);
        }

        heads = new ArrayList<InverseMutator>();
        tails = new ArrayList<InverseMutator>();
        nodeToPreds = new HashMap<String, List<InverseMutator>>();
        nodeToSuccs = new HashMap<String, List<InverseMutator>>();

        for (InverseMutator im:nodes.values()) {
            String[] ps = im.getPredecessors();
            String[] ss = im.getSuccessors();
            if (ps.length == 0) {
                nodeToPreds.put(im.getUniqueName(), Collections.<InverseMutator>emptyList());
            } else {
                for (String p:ps) {
                    if (nodes.get(p) == null) {
                        if (debug) System.err.println("WARNING: missing inverse method " + p);
                        continue;
                    }
                    addToMapList(nodeToPreds, im.getUniqueName(), nodes.get(p));
                }
                if (nodeToPreds.get(im.getUniqueName()) == null) {
                    heads.add(im);
                    nodeToPreds.put(im.getUniqueName(), Collections.<InverseMutator>emptyList());
                } else {
                    Collections.sort(nodeToPreds.get(im.getUniqueName()), initComparator);
                }
            }
            if (ss.length == 0) {
                tails.add(im);
                nodeToSuccs.put(im.getUniqueName(), Collections.<InverseMutator>emptyList());
            } else {
                for (String s:ss) {
                    if (nodes.get(s) == null) {
                        if (debug) System.err.println("WARNING: missing inverse method " + s);
                        continue;
                    }
                    addToMapList(nodeToSuccs, im.getUniqueName(), nodes.get(s));
                }
                if (nodeToSuccs.get(im.getUniqueName()) == null) {
                    tails.add(im);
                    nodeToSuccs.put(im.getUniqueName(), Collections.<InverseMutator>emptyList());
                } else {
                    Collections.sort(nodeToSuccs.get(im.getUniqueName()), initComparator);
                }
            }
        }

        // detect circle and put all nodes in a circle in to the tails
        for (Entry<String, InverseMutator> entry:nodes.entrySet()) {
            List<InverseMutator> preds = nodeToPreds.get(entry.getKey());
            if (!preds.isEmpty()) {
                // Check whether this is in a circle
                if (isInCircle(entry.getKey())) {
                    tails.add(entry.getValue());
                }
            }
        }

        Collections.sort(heads, initComparator);
        Collections.sort(tails, initComparator);
    }

    private boolean isInCircle(String key) {

        LinkedList<String> queue = new LinkedList<String>();
        Set<String> visited = new HashSet<String>();
        queue.add(key);
        visited.add(key);

        while (!queue.isEmpty()) {
            String uniqueName = queue.removeFirst();
            List<InverseMutator> succs = this.getSuccs(uniqueName);
            for (InverseMutator im:succs) {
                if (im.getUniqueName().equals(key)) {
                    return true;
                }
                if (visited.add(im.getUniqueName())) {
                    queue.add(im.getUniqueName());
                }
            }
        }

        return false;
    }

    static Comparator<InverseMutator> deltaComparator = new Comparator<InverseMutator>(){

        @Override
        public int compare(InverseMutator o1, InverseMutator o2) {
            return o2.getDelta().size() - o1.getDelta().size();
        }

    };

    static Comparator<InverseMutator> initComparator = new Comparator<InverseMutator>(){

        @Override
        public int compare(InverseMutator o1, InverseMutator o2) {
            if (o1.isInit()) {
                if (o2.isInit()) {
                    return o2.getDelta().size() - o1.getDelta().size();
                } else {
                    return Integer.MAX_VALUE;
                }
            } else {
                if (o2.isInit()) {
                    return Integer.MIN_VALUE;
                } else {
                    return o2.getDelta().size() - o1.getDelta().size();
                }
            }
        }

    };

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutatorGraph#getHead()
     */
    @Override
    public List<InverseMutator> getHeads() {
        return heads;
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutatorGraph#getTail()
     */
    @Override
    public List<InverseMutator> getTails() {
        return tails;
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutatorGraph#getPred(java.lang.String)
     */
    @Override
    public List<InverseMutator> getPreds(String uniqueName) {
        return this.nodeToPreds.get(uniqueName);
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutatorGraph#getSucc(java.lang.String)
     */
    @Override
    public List<InverseMutator> getSuccs(String uniqueName) {
        return this.nodeToSuccs.get(uniqueName);
    }

    public static <K, V> boolean addToMapList(Map<K, List<V>> mapSet, K key, V value) {
        List<V> values = mapSet.get(key);
        if (values == null) {
            values = new ArrayList<V>();
            mapSet.put(key, values);
        }

        return values.add(value);
    }

    @Override
    public Collection<InverseMutator> getInverseMutators() {
        return nodes.values();
    }

    @Override
    public boolean isPredOf(InverseMutator im, InverseMutator pred) {
        List<InverseMutator> preds = getPreds(im.getUniqueName());
        if (preds == null) {
            return false;
        }
        return preds.contains(pred);
    }
}
