package org.javelus.aotes.executor.java.img;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

import org.javelus.aotes.executor.DG;
import org.javelus.aotes.executor.IM;
import org.javelus.aotes.executor.InverseMutator;
import org.javelus.aotes.executor.java.mov.MethodIDHelper;

public class InverseMutatorImpl implements InverseMutator {
    Method method;

    IM im;
    DG dg;
    Set<Object> definedOutput = new HashSet<Object>();
    Set<Object> revertedInput = new HashSet<Object>();
    Set<Object> mutatedReceivers = new HashSet<Object>();
    Set<Object> delta = new HashSet<Object>();

    Object mutator;

    public InverseMutatorImpl(Method method) {
        this.method = method;
        method.setAccessible(true);
        im = method.getAnnotation(IM.class);
        dg = method.getAnnotation(DG.class);

        for (Object i:im.revertedInput()) {
            revertedInput.add(i);
        }

        for (Object o:im.definedOutput()) {
            definedOutput.add(o);
        }

        for (Object o:im.mutatedReceivers()) {
            mutatedReceivers.add(o);
        }

        for (Object o:im.delta()) {
            delta.add(o);
        }
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#im()
     */
    @Override
    public IM im() {
        return this.im;
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getUniqueName()
     */
    @Override
    public String getUniqueName() {
        return method.getName();
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getDefinedOutput()
     */
    @Override
    public Set<Object> getDefinedOutput() {
        return definedOutput;
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getMutatedReceivers()
     */
    @Override
    public Set<Object> getMutatedReceivers() {
        return mutatedReceivers;
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getTotalMutated()
     */
    @Override
    public int getTotalMutated() {
        return getDefinedOutput().size() + getMutatedReceivers().size();
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getRevertedInput()
     */
    @Override
    public Set<Object> getRevertedInput() {
        return revertedInput;
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getDelta()
     */
    @Override
    public Set<Object> getDelta() {
        return delta;
    }

    String[] EMPTY_STRING_ARRAY = new String[0];

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getPredecessors()
     */
    @Override
    public String[] getPredecessors() {
        if (dg == null) {
            return EMPTY_STRING_ARRAY;
        }
        return dg.pred();
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getSuccessors()
     */
    @Override
    public String[] getSuccessors() {
        if (dg == null) {
            return EMPTY_STRING_ARRAY;
        }
        return dg.succ();
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getMethod()
     */
    @Override
    public Method getMethod() {
        return method;
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#getMutator()
     */
    @Override
    public Object getMutator() {
        if (mutator != null) {
            return mutator;
        }

        if (isInit()) {
            mutator = MethodIDHelper.resolveConstructor(im.clazz(), im.desc());
        } else if (isClinit()) {
            mutator = MethodIDHelper.resolveConstructor(im.clazz(), im.desc());
        } else {
            mutator = MethodIDHelper.resolveMethod(im.clazz(), im.name(), im.desc());
        }
        if (mutator == null) {
            throw new RuntimeException("Cannot resolve mutator for " + im);
        }
        return mutator;
    }

    @Override
    public boolean isStatic() {
        if (isInit()) {
            Constructor<?> ctor = (Constructor<?>) getMutator();
            return Modifier.isStatic(ctor.getModifiers());
        }
        if (isClinit()) {
            return true;
        }
        Method mth = (Method) getMutator();
        return Modifier.isStatic(mth.getModifiers());
    }

    @Override
    public Class<?> getMutatorClass() {
        return MethodIDHelper.resolveClass(im.clazz());
    }

    /* (non-Javadoc)
     * @see org.javelus.aotes.executor.java.img.InverseMutator#isInit()
     */
    @Override
    public boolean isInit() {
        return im.name().equals("<init>");
    }

    @Override
    public boolean isClinit() {
        return im.name().equals("<clinit>");
    }

    public String toString() {
        return getUniqueName();
    }
}
