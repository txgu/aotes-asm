package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class ARRAYLENGTH extends AbstractEval {

    public ARRAYLENGTH(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 1);

        Object array = JavaEvalMap.getValue(args[0]);

        return ReflectionHelper.getArrayLength(array);
    }

}
