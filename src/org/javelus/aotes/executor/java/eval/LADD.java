package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class LADD extends AbstractEval {

    public LADD(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 2);

        long v1 = JavaEvalMap.getLongValue(args[0]);
        long v2 = JavaEvalMap.getLongValue(args[1]);

        return v1 + v2;
    }

}
