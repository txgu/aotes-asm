package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class I2F extends AbstractEval {

    public I2F(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 1);

        int v1 = JavaEvalMap.getIntegerValue(args[0]);

        return Float.valueOf(v1);
    }

}
