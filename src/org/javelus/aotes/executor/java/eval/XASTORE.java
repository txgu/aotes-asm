package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class XASTORE extends AbstractEval {

    public XASTORE(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 3);

        Object array = JavaEvalMap.getValue(args[0]);
        int v2 = JavaEvalMap.getIntegerValue(args[1]);
        Object value = JavaEvalMap.getValue(args[2]);

        ReflectionHelper.setArrayElement(array, v2, value);
        return null;
    }

}
