package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class FCMPL extends AbstractEval {

    public FCMPL(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 2);

        float v1 = JavaEvalMap.getFloatValue(args[0]);
        float v2 = JavaEvalMap.getFloatValue(args[1]);
        if (Float.isNaN(v1) || Float.isNaN(v2)) {
            return -1;
        }
        
        if (v1 > v2) {
            return 1;
        } else if (v1 < v2) {
            return -1;
        }
        return 0;
    }

}
