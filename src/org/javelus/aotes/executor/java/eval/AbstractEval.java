package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Eval;
import org.javelus.aotes.executor.Executor;

public abstract class AbstractEval implements Eval {

    protected Executor executor;

    public AbstractEval(Executor executor) {
        this.executor = executor;
    }

    protected void abortIfNotInLength(Object[] array, int length) {
        if (array.length != length) {
            getExecutor().abort("Inconsistent array length, require " + length + ", get " + array.length);
        }
    }

    @Override
    public Executor getExecutor() {
        return executor;
    }

}
