package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class IDIV extends AbstractEval {

    public IDIV(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 2);

        int v1 = JavaEvalMap.getIntegerValue(args[0]);
        int v2 = JavaEvalMap.getIntegerValue(args[1]);

        return v1 / v2;
    }

}
