package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class L2I extends AbstractEval {

    public L2I(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 1);

        long v1 = JavaEvalMap.getLongValue(args[0]);

        return (int) v1;
    }

}
