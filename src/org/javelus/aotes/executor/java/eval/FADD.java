package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class FADD extends AbstractEval {

    public FADD(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 2);

        float v1 = JavaEvalMap.getFloatValue(args[0]);
        float v2 = JavaEvalMap.getFloatValue(args[1]);
        return v1 + v2;
    }

}
