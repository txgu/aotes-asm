package org.javelus.aotes.executor.java.eval;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;
import org.javelus.aotes.executor.java.mov.MethodIDHelper;

public class MethodEval extends AbstractEval {

    private Object method;

    public MethodEval(Executor executor, Object id) {
        super(executor);
        method = MethodIDHelper.resolveMethodOrConstructor(id);
        if (method == null) {
            getExecutor().abort("Cannot find method " + id);
        }
    }

    public static Object[] emptyArgs = new Object[0];

    public boolean isStatic() {
        if (method instanceof Method) {
            return Modifier.isStatic(((Method) method).getModifiers());
        }

        return Modifier.isStatic(((Constructor<?>) method).getModifiers());
    }

    @Override
    public Object eval(Object... args) {
        Object receiver = null;
        if (!isStatic() && !(method instanceof Constructor<?>)) {
            receiver = JavaEvalMap.getValue(args[0]);
            Object oldArgs = args;
            if (args.length == 1) {
                args = emptyArgs;
            } else {
                args = new Object[args.length - 1];
                System.arraycopy(oldArgs, 1, args, 0, args.length);
            }
        }

        for (int i=0; i<args.length; i++) {
            args[i] = JavaEvalMap.getValue(args[i]);
        }

        Object ret = null;
        try {
            if (method instanceof Constructor<?>) {
                ret = ((Constructor<?>)method).newInstance(args);
            } else {
                ret = ((Method)method).invoke(receiver, args);
            }
        } catch (IllegalAccessException e) {
            this.getExecutor().abort(e.getMessage());
        } catch (IllegalArgumentException e) {
            this.getExecutor().abort(e.getMessage());
        } catch (InvocationTargetException e) {
            this.getExecutor().abort(e.getMessage());
        } catch (InstantiationException e) {
            this.getExecutor().abort(e.getMessage());
        } catch (NullPointerException e) {
            this.getExecutor().abort("Receiver is null to invoke method " + method);
        }
        return ret;
    }

}
