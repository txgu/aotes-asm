package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class FNEG extends AbstractEval {

    public FNEG(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 1);

        float v1 = JavaEvalMap.getFloatValue(args[0]);

        return -v1;
    }

}
