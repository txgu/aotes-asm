package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class XALOAD extends AbstractEval {

    public XALOAD(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 2);

        Object array = JavaEvalMap.getValue(args[0]);
        int v2 = JavaEvalMap.getIntegerValue(args[1]);

        return ReflectionHelper.getArrayElement(array, v2);
    }

}
