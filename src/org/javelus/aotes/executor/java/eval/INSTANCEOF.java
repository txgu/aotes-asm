package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.asm.vm.ReflectionHelper;
import org.javelus.aotes.executor.Executor;

public class INSTANCEOF extends AbstractEval {

    public INSTANCEOF(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 2);
        Object object = args[0];
        if (object == null) {
            return false;
        }

        String className = (String)args[1];
        Class<?> cls = ReflectionHelper.loadClassOrNull(className);

        return cls.isAssignableFrom(object.getClass());
    }

}
