package org.javelus.aotes.executor.java.eval;

import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.java.JavaEvalMap;

public class LCMP extends AbstractEval {

    public LCMP(Executor executor) {
        super(executor);
    }

    @Override
    public Object eval(Object... args) {
        abortIfNotInLength(args, 2);

        long v1 = JavaEvalMap.getLongValue(args[0]);
        long v2 = JavaEvalMap.getLongValue(args[1]);

        if (v1 > v2) {
            return 1;
        } else if (v1 == v2) {
            return 0;
        } else {
            return -1;
        }
    }

}
