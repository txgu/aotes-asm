package org.javelus.aotes.executor;

public class AbortTransformationException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -3268597466739484359L;

    public AbortTransformationException(String message, Throwable cause) {
        super(message, cause, false, false);
    }

    public AbortTransformationException(String message) {
        super(message, null, false, false);
    }

}
