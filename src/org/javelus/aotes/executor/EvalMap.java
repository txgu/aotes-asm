package org.javelus.aotes.executor;



public interface EvalMap {
    Executor getExecutor();
    Eval op(Object opType);
    int size();

    boolean eq(Object v1, Object v2);
    boolean lt(Object v1, Object v2);
    boolean le(Object v1, Object v2);
    boolean gt(Object v1, Object v2);
    boolean ge(Object v1, Object v2);
    boolean ne(Object v1, Object v2);
}
