package org.javelus.aotes.asm.summary;

import java.util.HashSet;
import java.util.Set;

public class AbstractDependenceNode <K extends AbstractDependenceNode<K>> implements DependenceNode <K>{

    Set<K> predecessors;
    Set<K> successors;

    public AbstractDependenceNode(){
        predecessors = new HashSet<K>(5);
        successors = new HashSet<K>(5);
    }

    @Override
    public boolean addPredecessor(K s) {
        return predecessors.add(s);
    }

    @Override
    public final boolean addSuccessor(K t) {
        return successors.add(t);
    }

    @Override
    public Set<K> getPredecessors() {
        return predecessors;
    }

    @Override
    public Set<K> getSuccessors() {
        return successors;
    }

}
