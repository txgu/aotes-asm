package org.javelus.aotes.asm.summary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.javelus.aotes.asm.CallGraph;
import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.accesspath.FieldAccessPath;
import org.javelus.aotes.asm.accesspath.MethodParameterAccessPath;
import org.javelus.aotes.asm.im.InverseFragment;
import org.javelus.aotes.asm.im.InverseMutator;
import org.javelus.aotes.asm.trace.TraceKey;
import org.javelus.aotes.asm.utils.JVMUtils;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.Instruction;
import org.javelus.aotes.asm.vm.MethodInfo;



public class MethodSummary extends AbstractDependenceNode<MethodSummary> {

    private ClassSummary classSummary;
    private MethodInfo methodInfo;

    private Map<TraceKey, ExecutionSummary> keyToCompletedExecutionSummry;
    private Map<TraceKey, ExecutionSummary> keyToExceptionalExecutionSummry;
    private Map<TraceKey, ExecutionSummary> keyToInterruptedExecutionSummry;

    private InverseMutator inverseMutator;

    private Map<Object, Integer> visitedBranch;
    
    public MethodSummary (ClassSummary cs, MethodInfo mi) {
        this.methodInfo = mi;
        this.classSummary = cs;
        this.inverseMutator = new InverseMutator(mi);

        if (mi.isNative() || mi.isAbstract()) {
            throw new RuntimeException("Cannot create a method summary for a native method or an abstract method " + mi);
        }

        keyToCompletedExecutionSummry = new HashMap<TraceKey, ExecutionSummary>();
        keyToExceptionalExecutionSummry = new HashMap<TraceKey, ExecutionSummary>();
        keyToInterruptedExecutionSummry =  new IdentityHashMap<TraceKey, ExecutionSummary>();
        visitedBranch =  new HashMap<Object, Integer>();
    }

    private static final boolean dynamicallyAddingClasses = true;

    public boolean isInherited() {
        return this.classSummary.getClassInfo() != methodInfo.getClassInfo();
    }
    
    /**
     * Update the execution summary
     * @param es
     */
    public void addExecutionSummary(ExecutionSummary es) {
        TraceKey key = es.getTrace().getKey();
        MDG mdg = classSummary.getMDG();
        addKey(es);
        if (es.isCompleted()) {
            keyToInterruptedExecutionSummry.remove(key);
            keyToCompletedExecutionSummry.put(key, es);

            mdg.getPartialHeapSkeleton().merge(es.getAllocatedAccessPaths());
        } else if (es.isExceptional()) {
            keyToInterruptedExecutionSummry.remove(key);
            keyToExceptionalExecutionSummry.put(key, es);

            mdg.getPartialHeapSkeleton().merge(es.getAllocatedAccessPaths());
        } else {
            keyToInterruptedExecutionSummry.put(key, es);

            if (dynamicallyAddingClasses) {
                for (AccessPath ap : es.getUnallocatedAccessPaths()) {
                    if (ap instanceof FieldAccessPath) {
                        FieldInfo fi = ((FieldAccessPath) ap).getFieldInfo();
                        ClassInfo ci = fi.getClassInfo();
                        if (JVMUtils.isImmutable(ci.getName())) {
                            continue;
                        }
                        ClassSummary cs = mdg.getOrCreateClassSummary(ci);
                        if (fi.isStatic()) {
                            for (MethodInfo method : ci.getDeclaredMethodInfos()) {
                                if (method.isAbstract() || method.isNative()) {
                                    continue;
                                }
                                if ((method.isStatic() || method.isClinit())) {
                                    cs.getOrCreateMethodSummary(method);
                                }
                            }
                        }
                    } else if (ap instanceof MethodParameterAccessPath) {
                        MethodInfo mi = ((MethodParameterAccessPath) ap).getMethodInfo();
                        while (mi != null) {
                            ClassInfo ci = mi.getClassInfo();

                            Collection<MethodInfo> callers = CallGraph.getCaller(mi);
                            if (callers != null) {
                                for (MethodInfo caller:callers) {
                                    if (JVMUtils.isImmutable(caller.getClassInfo().getName())) {
                                        continue;
                                    }
                                    //mdg.addClassSummary(caller.getClassInfo());
                                    mdg.addMethodSummary(caller.getClassInfo(), caller);
                                }
                                break;
                            } else {
                                ci = ci.getSuperClass();
                                if (ci == null) {
                                    break;
                                }
                                if (ci.isObjectClassInfo()) {
                                    break;
                                }
                                mi = ci.getMethod(mi.getName(), mi.getDescriptor());
                            }
                        }
                    }
                }
            }

            if (es.isInterrupted() && es.getUnallocatedAccessPaths().isEmpty()) {
                throw new RuntimeException("Sanity check failed");
            }
        }

        es.clearExtraHeapUpdated();
    }

    private void addKey(ExecutionSummary es) {
        MDG mdg = classSummary.getMDG();
        mdg.getProfiler().addTrace(es);
        TraceKey key = es.getTrace().getKey();
        int numOfCond = key.getNumOfConditions();
        for (int i=0; i<numOfCond; i++) {
            int branch = key.getBranch(i);
            Instruction insn = key.getInstruction(i);
            if (insn.isIf()) {
                this.visitIf(insn, branch);
            }
        }
    }

    public Collection<ExecutionSummary> getCompletedExecutionSummaries() {
        return this.keyToCompletedExecutionSummry.values();
    }

    public Collection<ExecutionSummary> getExceptionalExecutionSummaries() {
        return this.keyToExceptionalExecutionSummry.values();
    }

    public Collection<ExecutionSummary> getInterruptedExecutionSummaries() {
        return this.keyToInterruptedExecutionSummry.values();
    }

    public int countSummaries() {
        Set<TraceKey> keys = new HashSet<TraceKey>();
        for (TraceKey key : keyToInterruptedExecutionSummry.keySet()) {
            keys.add(key);
        }
        return this.keyToCompletedExecutionSummry.size() + this.keyToExceptionalExecutionSummry.size() + keys.size();
    }
    
    public ClassInfo getTargetClassInfo() {
        return classSummary.getClassInfo();
    }

    public MethodInfo getMethodInfo() {
        return methodInfo;
    }

    public void addInverseFragment(InverseFragment fragment) {
        this.inverseMutator.addFragment(fragment);
    }

    public boolean hasInverseFragment() {
        return this.inverseMutator.hasInverseFragment();
    }

    public InverseMutator getInverseMutator() {
        return this.inverseMutator;
    }

    @Override
    public boolean addPredecessor(MethodSummary ms) {
        if (this.getMethodInfo().isInit() || this.getMethodInfo().isClinit()) {
            return false;
        }
        return super.addPredecessor(ms) && ms.addSuccessor(this);
    }

    /**
     * For all reversible execution summary, find a predecessor for it.
     */
    public void updateExecutionSummaryGraph(MethodSummary ams) {
        for (ExecutionSummary es:getCompletedExecutionSummaries()) {
            if (es.isReversible()) {
                es.addPredecessorForExecutionSummary(ams);
            }
        }
    }

    public void printList() {
        System.out.println("  " + getMethodInfo().getUniqueName());

        List<ExecutionSummary> summaries = new ArrayList<ExecutionSummary>();
        summaries.addAll(getCompletedExecutionSummaries());
        summaries.addAll(getExceptionalExecutionSummaries());

        Map<TraceKey, ExecutionSummary> unresolvedExecutionSummry = new HashMap<TraceKey, ExecutionSummary>(this.keyToInterruptedExecutionSummry);
        summaries.addAll(unresolvedExecutionSummry.values());

        Collections.sort(summaries, new Comparator<ExecutionSummary>() {

            @Override
            public int compare(ExecutionSummary o1, ExecutionSummary o2) {
                String k1 = o1.getTrace().getKey().getKeyString();
                String k2 = o2.getTrace().getKey().getKeyString();
                return k1.compareTo(k2);
            }

        });

        for (ExecutionSummary es:summaries) {
            es.printList();
        }
    }

    public ClassSummary getClassSummary() {
        return classSummary;
    }

    public String toString() {
        return classSummary.toString() + "." + methodInfo.getUniqueName();
    }

    public boolean requireExtraHeap() {
        for (ExecutionSummary es:keyToInterruptedExecutionSummry.values()) {
            if (es.requireExtraHeap()) {
                return true;
            } 
        }
        return false;
    }

    public void updateExtraHeap(MDG mdg) {
        for (ExecutionSummary es:keyToInterruptedExecutionSummry.values()) {
            if (es.requireExtraHeap()) {
                es.updateExtraHeap(mdg);
            }
        }
    }

    public boolean extraHeapUpdated() {
        if (keyToInterruptedExecutionSummry.isEmpty() 
                && keyToCompletedExecutionSummry.isEmpty()
                && keyToExceptionalExecutionSummry.isEmpty()) {
            return true;
        }
        for (ExecutionSummary es:keyToInterruptedExecutionSummry.values()) {
            if (es.extraHeapUpdated()) {
                return true; 
            }
        }
        return false;
    }
    
    static Integer ALL_VISITED = Integer.MAX_VALUE;
    
    public boolean isBranchVisited(Object insn, int branch) {
        Integer b = visitedBranch.get(insn);
        if (b == null) {
            return false;
        }
        if (b.equals(branch)) {
            return true;
        }
        return b == ALL_VISITED;
    }
    
    public void visitIf(Object insn, int branch) {
        Integer b = visitedBranch.get(insn);
        if (b == null) {
            visitedBranch.put(insn, branch);
            return;
        }
        if (b.equals(branch)) {
            return;
        }
        if (b == ALL_VISITED) {
            return;
        }
        if (branch + b != 1) {
            throw new RuntimeException("they must be 1 and 0");
        }
        visitedBranch.put(insn, ALL_VISITED);
    }
    
    public boolean allVisited() {
        for (Entry<Object,Integer> e:visitedBranch.entrySet()) {
            Object insn = e.getKey();
            Integer branch = e.getValue();
            if (branch != ALL_VISITED) {
                insn.getClass(); // let eclipse happy
                return false;
            }
        }
        return true;
    }
}
