package org.javelus.aotes.asm.summary;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.accesspath.TargetAccessPath;
import org.javelus.aotes.asm.utils.MapUtils;
import org.javelus.aotes.asm.vm.ClassInfo;



/**
 * A PartialHeapSkeleton is used to initialize a PrePartialHeap.
 * @author tianxiao
 *
 */
public class PartialHeapSkeleton {

    private Map<AccessPath, ClassInfo> accessPathToBasicType = new HashMap<AccessPath, ClassInfo>();
    private Set<AccessPath> roots = new HashSet<AccessPath>();
    private Map<AccessPath, Set<ClassInfo>> accessPathToSubTypes = new HashMap<AccessPath, Set<ClassInfo>>();

    private Map<ClassInfo, Set<AccessPath>> classInfoToAccessPaths = new HashMap<ClassInfo, Set<AccessPath>>(); 

    private final Object scope;

    public PartialHeapSkeleton(Object scope) {
        this.scope = scope;
    }

    public PartialHeapSkeleton(Object scope, TargetAccessPath targetAp) {
        this(scope);
        add(targetAp, targetAp.getClassInfo());
    }

    public Object getScope() {
        return scope;
    }

    protected boolean add(AccessPath accessPath, ClassInfo ci) {
        if (ci.isInterface()) {
            // ci may be abstract here and we can perform downcast to a concrete sub class.
            //throw new RuntimeException("ci is used to allocate object and should not be interface.");
        }
        boolean added = false;
        ClassInfo basicType = accessPathToBasicType.get(accessPath);
        if (basicType == null) {
            added = accessPathToBasicType.put(accessPath, ci) == null;
            //added = true;
        } else if (basicType == ci) {
            return false;
        } else {
            if (basicType.isInstanceOf(ci)) {
                // ci is the basic type
                if (containsSubType(accessPath, ci)) { // TODO we only support single type heirarchy here!
                    throw new RuntimeException("ci is a basic type and cannot be a sub type");
                }
                // move basicType to subType
                // add ci to basicType
                accessPathToBasicType.put(accessPath, ci);
                MapUtils.addToMapSet(accessPathToSubTypes, accessPath, basicType);
                added = true;
            } else if (ci.isInstanceOf(basicType)) {
                added = MapUtils.addToMapSet(accessPathToSubTypes, accessPath, ci);
            } else {
                //throw new RuntimeException("invalid type hierarchy!");
                added = MapUtils.addToMapSet(accessPathToSubTypes, accessPath, ci);
            }
        }

        if (added && accessPath.isRoot()) {
            roots.add(accessPath);
        }

        if (added) {
            MapUtils.addToMapSet(classInfoToAccessPaths, ci, accessPath);
        }

        return added;
    }

    public Set<AccessPath> getAccessPaths() {
        return accessPathToBasicType.keySet();
    }

    public ClassInfo getBasicType(AccessPath accessPath) {
        return accessPathToBasicType.get(accessPath);
    }

    public boolean isEmpty() {
        return this.accessPathToBasicType.isEmpty();
    }

    public boolean contains(AccessPath accessPath) {
        return accessPathToBasicType.containsKey(accessPath);
    }

    public boolean containsSubType(AccessPath accessPath, ClassInfo subType) {
        Set<ClassInfo> subTypes = accessPathToSubTypes.get(accessPath);
        if (subTypes == null) {
            return false;
        }
        return subTypes.contains(subType);
    }

    public Set<AccessPath> getRoots() {
        return roots;
    }

    public boolean merge(AccessPath ap, ClassInfo ci) {
        return add(ap, ci);
    }

    public boolean merge(Map<AccessPath, ClassInfo> extraHeap) {
        boolean updated = false;
        for (Entry<AccessPath, ClassInfo> entry:extraHeap.entrySet()) {
            AccessPath ap = entry.getKey();
            ClassInfo  ci = entry.getValue();
            updated |= add(ap, ci);
            updated |= add(ap.getCanonical(), ci);
        }

        return updated;
    }

    public boolean merge(PartialHeapSkeleton that) {
        boolean updated = false;
        for (Entry<AccessPath, ClassInfo> entry:that.accessPathToBasicType.entrySet()) {
            AccessPath ap = entry.getKey();
            ClassInfo  ci = entry.getValue();
            updated |= add(ap, ci);
        }

        // An access path with sub types must have a basic type
        for (Entry<AccessPath, Set<ClassInfo>> entry:that.accessPathToSubTypes.entrySet()) {
            AccessPath ap = entry.getKey();
            Set<ClassInfo>  subTypes = entry.getValue();
            if (subTypes == null || subTypes.isEmpty()) {
                throw new RuntimeException("Sub types should not be empty.");
            }
            for (ClassInfo ci:subTypes) {
                updated |= add(ap, ci);
            }
        }

        return updated;
    }

    public boolean mergeThatAt(PartialHeapSkeleton that, AccessPath thatAp) {
        return mergeThatAt(that, thatAp, thatAp);
    }

    /**
     * Merge access path at that ap, no need to recursively add.
     * @param that
     * @param thatAp
     * @param thisAp
     * @return
     */
    public boolean mergeThatAt(PartialHeapSkeleton that, AccessPath thatAp, AccessPath thisAp) {
        boolean updated = false;
        ClassInfo basicType = that.accessPathToBasicType.get(thatAp);
        if (basicType == null) {
            return false;
        }

        updated = add(thisAp, basicType);

        Set<ClassInfo> subTypes = that.accessPathToSubTypes.get(thatAp);

        if (subTypes == null) {
            return updated;
        }

        for (ClassInfo subType:subTypes) {
            updated |= add(thisAp, subType);
        }

        return updated;
    }
}
