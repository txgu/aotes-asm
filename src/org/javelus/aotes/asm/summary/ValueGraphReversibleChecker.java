package org.javelus.aotes.asm.summary;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.javelus.aotes.asm.Options;
import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.accesspath.ArrayLengthAccessPath;
import org.javelus.aotes.asm.im.InverseFragment;
import org.javelus.aotes.asm.im.Predicate;
import org.javelus.aotes.asm.im.Statement;
import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.ArrayLength;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.InstanceField;
import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.MethodParameterLocation;
import org.javelus.aotes.asm.location.StaticField;
import org.javelus.aotes.asm.trace.IfTraceNode;
import org.javelus.aotes.asm.trace.LookupSwitchTraceNode;
import org.javelus.aotes.asm.trace.SwitchTraceNode;
import org.javelus.aotes.asm.trace.TableSwitchTraceNode;
import org.javelus.aotes.asm.trace.Trace;
import org.javelus.aotes.asm.trace.TraceEngine;
import org.javelus.aotes.asm.trace.TraceNode;
import org.javelus.aotes.asm.utils.MapUtils;
import org.javelus.aotes.asm.value.BinopExpr;
import org.javelus.aotes.asm.value.ClonedRef;
import org.javelus.aotes.asm.value.Constant;
import org.javelus.aotes.asm.value.GuessIndexValue;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.NewRef;
import org.javelus.aotes.asm.value.OperatorValue;
import org.javelus.aotes.asm.value.Output;
import org.javelus.aotes.asm.value.TargetRef;
import org.javelus.aotes.asm.value.UnopExpr;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.value.constant.IntegerValue;
import org.javelus.aotes.asm.value.operator.OpcodeOperatorFactory;
import org.javelus.aotes.asm.value.operator.RelationalOperator;
import org.javelus.aotes.asm.vm.ArrayElementInfo;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.Instruction;


public class ValueGraphReversibleChecker implements ReversibleChecker {


    /**
     * We have no need do backward check.
     * During synthesis, we have made a check that if IM(s), then IM(M(IM(s)))
     */
    private static final boolean doCheckForwardValue = true;
    /*private */static final boolean doCheckConcrete = false;

    private static final boolean doCheckBackwardValues = true;

    private Set<Value> resolvedValues;
    private Set<Value> unresolvedValues;
    private Set<IfTraceNode> ifTraceNodes;
    private Set<SwitchTraceNode> switchTraceNodes;

    private Map<Value, Set<Value>> valueToBackwardValues;
    private Set<Value> forwardResolvedValues;

    // Inverse Fragment
    private InverseFragment inverseFragment;
    private List<Statement> resolvedStatement;
    private List<Predicate> resolvedPredicates;
    private Set<AccessPath> definedOutput;
    private Set<AccessPath> revertedInput;
    private Set<AccessPath> mutatedReceivers;

    private ExecutionSummary es;

    private final boolean debug = Options.debug("resolution");

    private static boolean printWhyInreversible = false;

    public ValueGraphReversibleChecker(ExecutionSummary es) {
        this.es = es;
        check();
        this.es.setChecker(this);
    }

    public InverseFragment getInverseFragment() {
        return inverseFragment;
    }

    protected void check() {
        if (debug) System.out.println(">>>>>>>>> Begin: check " + es.getEntryMethod().getUniqueName());
        initialize();
        iterate();

        if (isReversible()) {
            buildInverseFragment();
        }

        if (debug) System.out.println(">>>>>>>>> End: check " + es.getEntryMethod().getUniqueName() + ", result: " + isReversible());
    }

    protected void buildInverseFragment() {
        if (doCheckConcrete) {
            checkConcreteValues();
        }
        buildPredicates();
        inverseFragment = new InverseFragment(es, resolvedStatement, resolvedPredicates, revertedInput, definedOutput, mutatedReceivers);
    }

    protected void checkConcreteValues() {
        for (Value v:resolvedValues) {
            if (v instanceof BinopExpr) {
                BinopExpr be = (BinopExpr) v;
                Value v1 = be.getLeftOperand();
                Value v2 = be.getRightOperand();
                if (v1 instanceof Constant && !isBackwardResolvedBy(v1, v)) {
                    checkBackwardResolvedValues(v1, v, v2);
                }
                if (v2 instanceof Constant && !isBackwardResolvedBy(v2, v)) {
                    checkBackwardResolvedValues(v2, v, v1);
                }
            } else if (v instanceof UnopExpr) {
                Value v1 = ((UnopExpr) v).getOperand();
                if (v1 instanceof Constant && !isBackwardResolvedBy(v1, v)) {
                    checkBackwardResolvedValues(v1, v);
                }
            } else 
                if (v instanceof Output) {
                Value v1 = ((Output<?>) v).getValue();
                if (!isBackwardResolvedBy(v1, v)) {
                    checkBackwardResolvedValues(v1, v);
                }
            }
        }
    }

    protected void buildPredicates() {
        for (IfTraceNode ifTN:ifTraceNodes) {
            Value v1 = ifTN.getLeftValue();
            Value v2 = ifTN.getRightValue();

            if (!(isResolved(v1) && isResolved(v2))) {
                throw new RuntimeException("Values of a predicate should be resolved!");
            }

            RelationalOperator comp = null;
            if (ifTN.isIfTrue()) {
                comp = OpcodeOperatorFactory.getComparator(ifTN.getInstruction());
            } else {
                comp = OpcodeOperatorFactory.getOppositeComparator(ifTN.getInstruction());
            }
            Predicate pred = new Predicate(v1, v2, comp);
            resolvedPredicates.add(pred);
        }

        for (SwitchTraceNode node:switchTraceNodes) {
            Value v = node.getValue();
            if (!isResolved(v)) {
                throw new RuntimeException("Values of a switch should be resolved!");
            }
            int branch = node.getBranch();
            if (node instanceof TableSwitchTraceNode) {
                TableSwitchTraceNode tstn = (TableSwitchTraceNode) node;
                int min = tstn.getMin();
                int max = tstn.getMax();
                if (branch == -1) {
                    for (int i=min ; i<max; i++) {
                        Constant m = IntegerValue.valueOf(i);
                        checkAndResolveConstant(m);
                        Predicate pred = new Predicate(v, m, RelationalOperator.NE);
                        resolvedPredicates.add(pred);
                    }
                } else {
                    Constant m = IntegerValue.valueOf(min + branch);
                    checkAndResolveConstant(m);
                    Predicate pred = new Predicate(v, m, RelationalOperator.EQ);
                    resolvedPredicates.add(pred);
                }
            } else if (node instanceof LookupSwitchTraceNode) {
                LookupSwitchTraceNode lstn = (LookupSwitchTraceNode) node;
                List<Integer> keys = lstn.getKeys();
                if (branch == -1) {
                    for (int k : keys) {
                        Constant m = IntegerValue.valueOf(k);
                        checkAndResolveConstant(m);
                        Predicate pred = new Predicate(v, m, RelationalOperator.NE);
                        resolvedPredicates.add(pred);
                    }
                } else {
                    Constant m = IntegerValue.valueOf(keys.get(branch));
                    checkAndResolveConstant(m);
                    Predicate pred = new Predicate(v, m, RelationalOperator.EQ);
                    resolvedPredicates.add(pred);
                }
            }
        }
    }

    protected void checkAndResolveConstant(Constant c) {
        if (isResolved(c)) {
            return;
        }
        logForwardResolved(c);
        addToResolved(c);
    }

    protected void initialize() {
        resolvedValues = new HashSet<Value>();
        unresolvedValues = new HashSet<Value>();
        valueToBackwardValues = new HashMap<Value, Set<Value>>();
        forwardResolvedValues = new HashSet<Value>();

        resolvedStatement = new ArrayList<Statement>();
        resolvedPredicates = new ArrayList<Predicate>();
        revertedInput = new HashSet<AccessPath>();
        definedOutput = new HashSet<AccessPath>();
        mutatedReceivers = new HashSet<AccessPath>();

        ifTraceNodes = new HashSet<IfTraceNode>();
        switchTraceNodes = new HashSet<SwitchTraceNode>();

        collectUsedUnknowns();
        collectPostHeapValues();
        collectPathConditions();
    }

    protected void collectUsedUnknowns() {
        for (Input<?> input:es.getUsedUnknownValues()) {
            collectPreHeapValue(input);
        }
    }

    protected void collectPreHeapValue(Input<?> input) {
        Location l = input.getLocation();
        if (l instanceof HeapLocation) {
            addUnresolvedValue(input);
            HeapLocation hl = (HeapLocation) l;
            Value ref = hl.getContainer().getReference();

            if (hl instanceof ArrayElement) {
                addUnresolvedValue(ref);
                Value index = ((ArrayElement) hl).getIndex();
                addUnresolvedValue(index);
                addUnresolvedValue(input);
            } else if (hl instanceof StaticField) {
                addUnresolvedValue(input);
            } else {
                addUnresolvedValue(ref);
                addUnresolvedValue(input);
            }
        } else if (l instanceof MethodParameterLocation) {
            addUnresolvedValue(input);
        } else {
            throw new RuntimeException("Not implemented yet");
        }
    }

    protected boolean addToResolved(Value v) {
        unresolvedValues.remove(v);
        return resolvedValues.add(v);
    }

    protected boolean addToUnresolved(Value v) {
        if (resolvedValues.contains(v)) {
            throw new RuntimeException("Sanity check failed!");
        }
        return unresolvedValues.add(v);
    }

    protected void addResolvedValue(Value v) {
        if (v == null) {
            throw new RuntimeException("v should not be null");
        }


        // There may be more than one paths that a value can be resolved.
        // But we only mark it as resolved once.
        // Be carefully to handle multiple backward resolution as there should be no conflicts.
        if (isResolved(v)) {
            return;
        }

        if (v instanceof Input) {
            Input<?> i = (Input<?>) v;
            Location loc = i.getLocation();

            if (loc instanceof MethodParameterLocation) {
                addToResolved(v);
                return;
            }

            if (loc instanceof StaticField) {
                addToResolved(v);
                return;
            }

            if (loc instanceof ArrayElement) {
                ArrayElement ae = (ArrayElement) loc;
                Value ref = ae.getContainer().getReference();
                Value index = ae.getIndex();
                if (!isResolved(ref) || !isResolved(index)) {
                    throw new RuntimeException("sanity check failed");
                }
                addToResolved(v);
                return;
            }

            if (loc instanceof InstanceField) {
                InstanceField inf = (InstanceField) loc;
                Value ref = inf.getContainer().getReference();
                if (!isResolved(ref)) {
                    throw new RuntimeException("sanity check failed");
                }
                addToResolved(v);
                return;
            }

            if (loc instanceof ArrayLength) {
                ArrayLength al = (ArrayLength) loc;
                Value ref = al.getContainer().getReference();
                if (!isResolved(ref)) {
                    throw new RuntimeException("sanity check failed");
                }
                addToResolved(v);
                return;
            }

            throw new RuntimeException("Should not reach here");
        }

        addToResolved(v);

        if (v instanceof UnopExpr) {
            Value r = ((UnopExpr) v).getOperand();
            addUnresolvedValue(r);
            return;
        }

        if (v instanceof BinopExpr) {
            Value r1 = ((BinopExpr) v).getLeftOperand();
            Value r2 = ((BinopExpr) v).getRightOperand();
            addUnresolvedValue(r1);
            addUnresolvedValue(r2);
            return;
        }

        if (v instanceof Output) {
            Output<?> output = (Output<?>) v;
            Location loc = output.getLocation();
            if (loc instanceof ArrayLength) {
                Value al = output.getValue();
                if (isUnresolved(al) && notLocationUnresolvedInput(al)) {
                    logBackwardResolved(al, v);
                    addResolvedValue(al);
                } else {
                    addUnresolvedValue(al);
                }
                return;
            }

            if (loc instanceof InstanceField) {
                Value iv = output.getValue();
                if (isUnresolved(iv) && notLocationUnresolvedInput(iv)) {
                    logBackwardResolved(iv, v);
                    addResolvedValue(iv);
                } else {
                    addUnresolvedValue(iv);
                }
                return;
            }

            if (loc instanceof ArrayElement) {
                Value ei = ((ArrayElement) loc).getIndex();
                Value ev = output.getValue();
                if (!isResolved(ei)) {
                    throw new RuntimeException("The index value should be resovled first!");
                }
                if (isUnresolved(ev) && notLocationUnresolvedInput(ev)) {
                    logBackwardResolved(ev, v);
                    addResolvedValue(ev);
                } else {
                    addUnresolvedValue(ev);
                }
                return;
            }

            if (loc instanceof StaticField) {
                Value iv = output.getValue();
                if (isUnresolved(iv) && notLocationUnresolvedInput(iv)) {
                    logBackwardResolved(iv, v);
                    addResolvedValue(iv);
                } else {
                    addUnresolvedValue(iv);
                }
                return;
            }
        }

        if (v instanceof Constant) {
            return;
        }

        if (v instanceof NewRef) {
            return;
        }

        if (v instanceof TargetRef) {
            return;
        }

        if (v instanceof ClonedRef) {
            Value source = ((ClonedRef) v).getSource();
            addUnresolvedValue(source);
            return;
        }

        if (v instanceof OperatorValue) {
            OperatorValue ov = ((OperatorValue) v);
            Value previous = ov.getPreviousMutator();
            if (previous != null) {
                addUnresolvedValue(previous);
            }
            for (Value op : ov.getOperands()) {
                addUnresolvedValue(op);
            }
            return;
        }

        if (v instanceof GuessIndexValue) {
            return;
        }

        throw new RuntimeException("Unknown value type " + v);
    }

    protected boolean isResolved(Value v) {
        return resolvedValues.contains(v);
    }

    protected boolean isUnresolved(Value v) {
        return unresolvedValues.contains(v);
    }

    protected void addUnresolvedValues(List<Value> uvs) {
        for (Value uv:uvs) {
            addUnresolvedValue(uv);
        }
    }

    protected void addUnresolvedValue(Value uv) {
        if (uv == null) {
            throw new RuntimeException("Unresolved value should not be null");
        }

        if (isResolved(uv)) {
            return;
        }

        if (uv instanceof Constant) {
            logForwardResolved(uv);
            addToResolved(uv);
            return;
        }

        if (uv instanceof TargetRef) {
            logForwardResolved(uv);
            addToResolved(uv);
            return;
        }

        if (uv instanceof NewRef) {
            if (es.getReceiverInPostObject(uv) == null) {
                ElementInfo ei = Value.getElementInfo(uv);
                ClassInfo ci = ei.getClassInfo();
                if (ci.isArray()) {
                    ArrayElementInfo aei = (ArrayElementInfo) ei;
                    Value length = aei.arrayLength().value();
                    addUnresolvedValue(length);
                }
            } else {
                // ArrayLength are added during collect post heap values
            }
            addToUnresolved(uv);
            return;
        }

        if (addToUnresolved(uv)) {
            if (uv instanceof BinopExpr) {
                BinopExpr be = (BinopExpr) uv;
                addUnresolvedValue(be.getLeftOperand());
                addUnresolvedValue(be.getRightOperand());
                return;
            }

            if (uv instanceof UnopExpr) {
                addUnresolvedValue(((UnopExpr) uv).getOperand());
                return;
            }


            if (uv instanceof OperatorValue) {
                OperatorValue ov = ((OperatorValue) uv);
                Value previous = ov.getPreviousMutator();
                if (previous != null) {
                    addUnresolvedValue(previous);
                }
                for (Value op : ov.getOperands()) {
                    addUnresolvedValue(op);
                }
                return;
            }

            if (uv instanceof Input) {
                Input<?> i = (Input<?>) uv;
                Location loc = i.getLocation();

                if (loc instanceof StaticField) {
                    return;
                }

                if (loc instanceof ArrayElement) {
                    ArrayElement ae = (ArrayElement) loc;
                    Value ref = ae.getContainer().getReference();
                    Value index = ae.getIndex();
                    addUnresolvedValue(ref);
                    addUnresolvedValue(index);
                    return;
                }

                if (loc instanceof InstanceField) {
                    InstanceField inf = (InstanceField) loc;
                    Value ref = inf.getContainer().getReference();
                    addUnresolvedValue(ref);
                    return;
                }

                if (loc instanceof ArrayLength) {
                    ArrayLength al = (ArrayLength) loc;
                    Value ref = al.getContainer().getReference();
                    addUnresolvedValue(ref);
                    return;
                }

                if (loc instanceof MethodParameterLocation) {
                    return;
                }

                throw new RuntimeException("Not implemented");
            }

            if (uv instanceof Output) {
                Output<?> output = (Output<?>) uv;
                Location loc = output.getLocation();
                if (loc instanceof ArrayElement) {
                    addUnresolvedValue(((ArrayElement) loc).getIndex());
                }
                addUnresolvedValue(output.getValue());
                return;
            }

            // For debugging
            throw new RuntimeException("Not implemented " + uv);
        }

    }

    protected void collectUnknownParameters() {
        if (es.getUnknownParameters() == null) {
            return;
        }

        for (Input<MethodParameterLocation> uv:es.getUnknownParameters()) {
            addUnresolvedValue(uv);
        }
    }

    protected void collectPostHeapValue(Output<? extends HeapLocation> phv) {
        HeapLocation hl = phv.getLocation();
        AccessPath ap = phv.getLocationPath().getAccessPath();
        Value ref = hl.getContainer().getReference();

        if (hl instanceof ArrayElement) {
            addUnresolvedValue(ref);
            Value index = ((ArrayElement) hl).getIndex();
            if (isResolved(ref) && isResolved(index) ) {
                logForwardResolved(phv, ref, index);
                addResolvedValue(phv);
            } else {
                addUnresolvedValue(index);
                addUnresolvedValue(phv);
            }
        } else if (hl instanceof StaticField) {
            logBackwardResolved(phv);
            addResolvedValue(phv);
        } else {
            addUnresolvedValue(ref);
            if (isResolved(ref)) {
                logBackwardResolved(phv, ref);
                addResolvedValue(phv);
            } else {
                addUnresolvedValue(phv);
            }
        }
        if (es.isDefined(hl)) {
            if (ap instanceof ArrayLengthAccessPath) {
                throw new RuntimeException("sanity check failed");
            }
            definedOutput.add(ap);
            if (es.isUsedUnknown(hl)) {
                // Define after use, so we need to revert its value,
                // which may not be a wild-card.
                revertedInput.add(ap);
            } else {
                // Define before use, so we need to revert its value to a wild-card.
                // Thus, this location is not in revertedInput 
                // as it may be a wild-card and thus a default value.
            }
        }
    }

    /**
     * 
     */
    protected void collectPostHeapValues() {
        for (Output<? extends HeapLocation> output:es.getPostHeapValues()) {
            collectPostHeapValue(output);
        }
    }

    protected void collectPathConditions() {
        Trace trace = es.getTrace();
        TraceNode tn = trace.getEnd();
        while (tn != null) {
            if (tn instanceof IfTraceNode) {
                addIfTraceNode((IfTraceNode)tn);
            }
            if (tn instanceof SwitchTraceNode) {
                addSwitchTraceNode((SwitchTraceNode)tn);
            }
            tn = tn.getPrevious();
        }
    }

    protected void addSwitchTraceNode(SwitchTraceNode tn) {
        switchTraceNodes.add(tn);
        addUnresolvedValue(tn.getValue());
    }

    protected void addIfTraceNode(IfTraceNode tn) {
        Instruction insn = tn.getInstruction();
        Value v1 = tn.getLeftValue();
        Value v2 = tn.getRightValue();
        int res = TraceEngine.evaluateConstantIf(insn, v1, v2);
        if (res == TraceEngine.CONSTANT_IF_TRUE
                || res == TraceEngine.CONSTANT_IF_FALSE) {
            return;
        }
        ifTraceNodes.add(tn);
        addUnresolvedValue(tn.getLeftValue());
        addUnresolvedValue(tn.getRightValue());
    }

    protected void iterate() {
        while (true) {
            while (resolve());
            if (!aggressivelyResolve()) {
                break;
            }
        }
        // TODO: every value is only resolved once.
        // If there is multiple resolution method, we select only one and assume that all other values are the same.
        if (doCheckForwardValue) checkForwardResolvedValues();
    }

    protected void checkForwardResolvedValues() {
        for (Value bv:this.valueToBackwardValues.keySet()) {
            if (!isForwardResolved(bv)) {
                checkIfHasForwardValue(bv);
            }
        }
    }

    private void checkIfHasForwardValue(Value bv) {
        if (bv instanceof BinopExpr) {
            BinopExpr be = (BinopExpr) bv;
            Value l1 = be.getLeftOperand();
            Value l2 = be.getRightOperand();

            if (isResolved(l1) && isResolved(l2) 
                    && !isBackwardResolvedBy(l1, bv) 
                    && !isBackwardResolvedBy(l2, bv)) {
                logForwardResolved(false, bv, l1, l2);
                addResolvedValue(bv);
            }
            return;
        }

        if (bv instanceof UnopExpr) {
            UnopExpr ue = (UnopExpr) bv;
            Value l = ue.getOperand();

            if (isResolved(l)
                    && !isBackwardResolvedBy(l, bv)) {
                logForwardResolved(false, bv, l);
                addResolvedValue(bv);
            }

            return;
        }

        if (bv instanceof OperatorValue) {
            OperatorValue ov = (OperatorValue) bv;
            Value previous = ov.getPreviousMutator();
            if (!isResolved(previous)) {
                return;
            }
            Value[] operands = ov.getOperands();
            for (Value o:operands) {
                if (!isResolved(o)) {
                    return;
                }
            }
            logForwardResolved(false, bv, operands);
            addResolvedValue(bv);
            return;
        }
    }

    private boolean isBackwardResolvedBy(Value v, Value by) {
        Set<Value> bys = this.valueToBackwardValues.get(v);
        if (bys == null) {
            return false;
        }
        return bys.contains(by);
    }

    /**
     * A aggressive resolved value may be a wild-card.
     * A wild-card would be assign to an object during a assertion(v == obj).
     * If it cannot be non-null value, the online synthesis may definitely failed.
     * @return
     */
    protected boolean aggressivelyResolve() {
        List<Value> tempResolved = new ArrayList<Value>();
        List<Value> tempUnresolved = new ArrayList<Value>();
        boolean resolved = false;
        for (Value uv : unresolvedValues) {
            if (resolveOneAggressively(uv, tempResolved, tempUnresolved)) {
                resolved = true;
                break;
            };
        }
        for (Value rv:tempResolved) {
            addResolvedValue(rv);
        }
        // As we collected unresolved at the beginning
        for (Value uv:tempUnresolved) {
            addUnresolvedValue(uv);
        }
        return resolved;
    }

    protected boolean resolve() {
        List<Value> tempResolved = new ArrayList<Value>();
        List<Value> tempUnresolved = new ArrayList<Value>();

        boolean resolved = false;

        for (Value uv : unresolvedValues) {
            if (resolveOneForward(uv, tempResolved, tempUnresolved)) {
                resolved = true;
                break;
            };
        }

        if (!resolved) {
            for (Value rv : resolvedValues) {
                if (resolveOneBackward(rv, tempResolved, tempUnresolved)) {
                    resolved = true;
                    break;
                };
            }
        }

        for (Value rv:tempResolved) {
            addResolvedValue(rv);
        }
        // As we collected unresolved at the beginning
        for (Value uv:tempUnresolved) {
            addUnresolvedValue(uv);
        }

        return resolved;
    }

    protected boolean resolveOneAggressively(Value uv, List<Value> tempResolved,
            List<Value> tempUnresolved) {
        if (uv instanceof Output) {
            Output<?> output = (Output<?>) uv;
            Location loc = output.getLocation();
            if (loc instanceof ArrayElement) {
                ArrayElement ae = (ArrayElement) loc;
                Value index = ae.getIndex();
                Value ref = ae.getContainer().getReference();
                if (isResolved(ref)) {
                    if (isResolved(index)) {
                        throw new RuntimeException("Sanity check failed.");
                    }

                    if (notLocationUnresolvedInput(index)) {
                        GuessIndexValue giv = new GuessIndexValue(ref);
                        logForwardResolved(giv);
                        tempResolved.add(giv);
                        logBackwardResolved(index, giv);
                        tempResolved.add(index);
                        logForwardResolved(uv, ref, index);
                        tempResolved.add(uv);
                        return true;
                    }
                }
            }
            return false;
        }

        if (uv instanceof Input) {
            boolean onlyInputUnresolved = true;
            for (Value u : this.unresolvedValues) {
                if (!(u instanceof Input)) {
                    onlyInputUnresolved = false;
                }
            }
            if (onlyInputUnresolved == false) {
                return false;
            }
            Input<?> input = (Input<?>) uv;
            Location loc = input.getLocation();
            if (loc instanceof StaticField) {
                if (doesNotPointToObject(uv)) {
                    logForwardResolved(uv);
                    tempResolved.add(uv);
                    return true;
                }
                return false;
            } else if (loc instanceof ArrayElement) {
                ArrayElement ae = (ArrayElement) loc;
                Value ref = ae.getContainer().getReference();
                Value index = ae.getIndex();
                if (isResolved(ref)) {
                    if (isResolved(index)) {
                        if (doesNotPointToObject(uv)) {
                            logForwardResolved(uv);
                            tempResolved.add(uv);
                            return true;
                        }
                        return false;
                    } else {

                        if (notLocationUnresolvedInput(index)) {
                            GuessIndexValue giv = new GuessIndexValue(ref);
                            logForwardResolved(giv);
                            tempResolved.add(giv);
                            logBackwardResolved(index, giv);
                            tempResolved.add(index);
                            return true;
                        }
                        return false;
                    }
                }
            } else if (loc instanceof HeapLocation) { // instance field or array length
                HeapLocation hl = (HeapLocation) loc;
                Value ref = hl.getContainer().getReference();
                if (isResolved(ref)) {
                    if (doesNotPointToObject(uv)) {
                        logForwardResolved(uv);
                        tempResolved.add(uv);
                        return true;
                    }
                    return false;
                }
            } else if (loc instanceof MethodParameterLocation) {
                // Aggressively resolve a method parameter that does not point to an object.
                // only when there is no any other unresolved but input
                if (doesNotPointToObject(input)) {
                    logForwardResolved(input);
                    tempResolved.add(input);
                    return true;
                }
                return false;
            }
            return false;
        }

        return false;
    }

    protected boolean resolveOneBackward(Value rv, List<Value> tempResolved,
            List<Value> tempUnresolved) {

        checkConsistency(rv);

        if (!isResolved(rv)) {
            throw new RuntimeException("Must be resolved");
        }

        if (rv instanceof BinopExpr) {
            BinopExpr be = (BinopExpr) rv;
            Value o1 = be.getLeftOperand();
            Value o2 = be.getRightOperand();

            checkConsistency(o1);
            checkConsistency(o2);

            if (!isOperatorReversible(be.getInstruction())) {
                return false;
            }

            if (isResolved(o1)) {
                if (isUnresolved(o2)) {
                    if (notLocationUnresolvedInput(o2)) {
                        logBackwardResolved(o2, rv, o1);
                        tempResolved.add(o2);
                        return true;
                    }
                    return false;
                }

                checkBackwardResolvedValues(o1, rv, o2);
                checkBackwardResolvedValues(o2, rv, o1);
            } else if (isUnresolved(o1)) {
                if (isResolved(o2)) {
                    if (notLocationUnresolvedInput(o1)) {
                        logBackwardResolved(o1, rv, o2);
                        tempResolved.add(o1);
                        return true;
                    }
                }
            } else {
                throw new RuntimeException("Should not reach here!");
            }

            return false;
        }

        if (rv instanceof UnopExpr) {
            UnopExpr ue = (UnopExpr) rv;
            Value o = ue.getOperand();

            checkConsistency(o);

            if (!isOperatorReversible(ue.getInstruction())) {
                return false;
            }

            if (isUnresolved(o)) {
                if (notLocationUnresolvedInput(o)) {
                    logBackwardResolved(o, rv);
                    tempResolved.add(o);
                    return true;
                }
            } else {
                checkBackwardResolvedValues(o, rv);
            }
            return false;
        }

        if (rv instanceof Output) {
            Output<?> output = (Output<?>) rv;
            Location loc = output.getLocation();
            Value v = output.getValue();
            checkConsistency(v);
            if (loc instanceof InstanceField) {
                if (isUnresolved(v)) {
                    if (notLocationUnresolvedInput(v)) {
                        logBackwardResolved(v, rv);
                        tempResolved.add(v);
                        return true;
                    }
                } else {
                    checkBackwardResolvedValues(v, rv);
                }
                // no change
                return false;
            }

            if (loc instanceof StaticField) {
                if (isUnresolved(v)) {
                    if (notLocationUnresolvedInput(v)) {
                        logBackwardResolved(v, rv);
                        tempResolved.add(v);
                        return true;
                    }
                } else {
                    checkBackwardResolvedValues(v, rv);
                }

                // no change
                return false;
            }

            if (loc instanceof ArrayLength) {
                if (isUnresolved(v)) {
                    if (notLocationUnresolvedInput(v)) {
                        logBackwardResolved(v, rv);
                        tempResolved.add(v);
                        return true;
                    }
                } else {
                    checkBackwardResolvedValues(v, rv);
                }

                return false;
            }

            if (loc instanceof ArrayElement) {
                ArrayElement aae = (ArrayElement) loc;
                Value index = aae.getIndex();
                if (isUnresolved(index)) {
                    throw new RuntimeException("Sanity check failed!");
                }
                if (isUnresolved(v)) {
                    if (notLocationUnresolvedInput(v)) {
                        logBackwardResolved(v, rv);
                        tempResolved.add(v);
                        return true;
                    }
                } else {
                    checkBackwardResolvedValues(v, rv);
                }
                return false;
            }
        }

        if (rv instanceof OperatorValue) {
            return false;
        }

        if (rv instanceof Constant) {
            return false;
        }

        if (rv instanceof NewRef) {
            return false;
        }

        if (rv instanceof ClonedRef) {
            return false;
        }

        if (rv instanceof TargetRef) {
            return false;
        }

        if (rv instanceof Input) {
            Input<?> input = (Input<?>)rv;
            Location loc = input.getLocation();
            if (loc instanceof ArrayElement) {
                ArrayElement ae = (ArrayElement) loc;
                Value index = ae.getIndex();
                if (!isResolved(index)) {
                    tempUnresolved.add(index);
                }
            }
            return false;
        }

        if (rv instanceof GuessIndexValue) {
            return false;
        }

        throw new RuntimeException("Unknown value type " + rv);
    }

    protected void checkBackwardResolvedValues(Value v, Value ... by) {
        if (!doCheckBackwardValues) {
            return;
        }
        if (!isResolved(v)) {
            throw new RuntimeException("v must be resovled");
        }
        for (Value b:by) {
            if (!isResolved(b)) {
                throw new RuntimeException("v must be resovled");
            }
        }
        if (isBackwardResolved(v) && isBackwardResolvedBy(v, by[0])) {
            return;
        }
        if (!isForwardResolved(by[0])) {
            if (by.length == 1) {
                logBackwardResolved(false, v, by);
            } else if (by.length == 2) {
                Value v1 = by[1];
                if (!isBackwardResolvedBy(v1, by[0])) {
                    logBackwardResolved(false, v, by);
                }
            } else {
                throw new RuntimeException("More values are not implemented.");
            }
        }
    }

    private boolean notLocationUnresolvedInput(Value v) {
        if (v instanceof Input) {
            Input<?> input = (Input<?>) v;
            Location l = input.getLocation();
            if (l instanceof MethodParameterLocation) {
                return true;
            } else if (l instanceof StaticField) {
                return true;
            } else if (l instanceof InstanceField) {
                InstanceField inf = (InstanceField) l;
                Value ref = inf.getContainer().getReference();
                return isResolved(ref);
            } else if (l instanceof ArrayLength) {
                ArrayLength al = (ArrayLength) l;
                Value ref = al.getContainer().getReference();
                return isResolved(ref);
            } else if (l instanceof ArrayElement) {
                ArrayElement ae = (ArrayElement) l;
                Value ref = ae.getContainer().getReference();
                Value index = ae.getIndex();
                return isResolved(ref) && isResolved(index);
            } else {
                throw new RuntimeException("Should not reach here");
            }
        }
        return true;
    }

    private boolean isOperatorReversible(Instruction instruction) {
        if (instruction == null) {
            throw new RuntimeException("instruction  should not be null.");
        }
        return OpcodeOperatorFactory.getOpcodeOperator(instruction.getOpcode()).reversible;
    }


    protected void logBackwardResolved(Value v, Value... by) {
        logBackwardResolved(true, v, by);
    }

    protected void logBackwardResolved(boolean checkResolved, Value v, Value... by) {
        if (checkResolved && isResolved(v)) {
            throw new RuntimeException("We must first log and then add the node to resolved set.");
        }
        boolean added;
        // TODO, use the first by[0] as the id of backward value
        if (by.length > 0) {
            added = MapUtils.addToMapSet(valueToBackwardValues, v, by[0]);
        } else {
            added = MapUtils.addToMapSet(valueToBackwardValues, v, v);
        }

        if (added) {
            if (debug) {
                logResolved("Backward Resolve ", v, by);
            }
            resolvedStatement.add(new Statement(v, by, false));
        }

        if (v instanceof Constant && !isForwardResolved(v)) {
            logForwardResolved(v);
        }
    }

    protected void logResolved(String type, Value v, Value... by) {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(es.getEntryMethod().getName()).append("] ");
        sb.append(type).append(v).append(" by ").append(by.length > 0 ? by[0] : "itself");

        for (int i=1; i<by.length; i++) {
            Value b = by[i];
            sb.append(" and ").append(b);
        }

        sb.append(".");
        System.out.println(sb.toString());
    }

    protected void logForwardResolved(Value v, Value... by) {
        logForwardResolved(true, v, by);
    }
    protected void logForwardResolved(boolean checkResolved, Value v, Value... by) {
        if (checkResolved && isResolved(v)) {
            throw new RuntimeException("We must first log and then add the node to resolved set.");
        }

        if (debug) {
            logResolved("Forward  Resolve ", v, by);
        }

        if (v instanceof OperatorValue) {
            OperatorValue mov = (OperatorValue) v;
            OperatorValue previous = mov.getPreviousMutator();
            if (previous != null && !isResolved(previous)) {
                throw new RuntimeException("The previous mutator of a forward resolved method operator value should be resolved first! previous is " + previous + " and mov is " + mov);
            }
        }

        resolvedStatement.add(new Statement(v, by, true));

        this.forwardResolvedValues.add(v);
    }

    void checkConsistency(Value v) {
        if (resolvedValues.contains(v)) {
            if (unresolvedValues.contains(v)) {
                throw new RuntimeException("Inconsistent value " + v);
            }
        } else if (!unresolvedValues.contains(v)) {
            throw new RuntimeException("Untracked value " + v);
        }
    }

    protected boolean resolveOneForward(Value uv, List<Value> tempResolved,
            List<Value> tempUnresolved) {
        checkConsistency(uv);

        if (!isUnresolved(uv)) {
            throw new RuntimeException("Must not be resolved");
        }

        if (uv instanceof BinopExpr) {
            BinopExpr be = (BinopExpr) uv;
            Value l1 = be.getLeftOperand();
            Value l2 = be.getRightOperand();

            checkConsistency(l1);
            checkConsistency(l2);

            if (isResolved(l1) && isResolved(l2)) {
                logForwardResolved(uv, l1, l2);
                tempResolved.add(uv);
                return true;
            }

            return false;
        }

        if (uv instanceof UnopExpr) {
            UnopExpr ue = (UnopExpr) uv;
            Value l = ue.getOperand();

            checkConsistency(l);

            if (isResolved(l)) {
                logForwardResolved(uv, l);
                tempResolved.add(uv);
                return true;
            }

            return false;
        }

        if (uv instanceof Input) {
            return false;
        }

        if (uv instanceof OperatorValue) {
            OperatorValue ov = ((OperatorValue) uv);

            // There may be a lot of MOV associated with the same receiver
            // but only mutator are related to state.
            OperatorValue previous = ov.getPreviousMutator();
            if (previous != null && !isResolved(previous)) {
                return false;
            }

            Value[] ops = ov.getOperands();
            for (Value op: ops) {
                if (!isResolved(op)) {
                    return false;
                }
            }

            logForwardResolved(uv, ops);
            tempResolved.add(uv);
            return true;
        }

        if (uv instanceof Output) {
            Output<?> output = (Output<?>) uv;
            Location loc = output.getLocation();
            if (loc instanceof ArrayElement) {
                ArrayElement ae = (ArrayElement) loc;
                Value ref = ae.getContainer().getReference();
                Value aei = ae.getIndex();
                Value aev = output.getValue();
                if (isResolved(ref) && isResolved(aei)) {
                    logForwardResolved(uv, ref, aei);
                    tempResolved.add(uv);
                    if (isUnresolved(aev) && notLocationUnresolvedInput(aev)) {
                        logBackwardResolved(aev, uv);
                        tempResolved.add(aev);
                    }
                    return true;
                }
            } else if (loc instanceof HeapLocation) {
                // other heap locations
                HeapLocation hl = (HeapLocation) loc;
                Value ref = hl.getContainer().getReference();
                if (isResolved(ref)) {
                    if (notLocationUnresolvedInput(ref)) {
                        logBackwardResolved(uv, ref);
                        tempResolved.add(uv);
                        return true;
                    }
                }
            }

            return false;
        }

        if (uv instanceof NewRef) {
            // check whether it is in post heap
            if (es.getReceiverInPostObject(uv) == null) {
                ElementInfo ei = Value.getElementInfo(uv);
                ClassInfo ci = ei.getClassInfo();
                if (ci.isArray()) {
                    ArrayElementInfo aei = (ArrayElementInfo) ei;
                    Value length = aei.arrayLength().value();
                    if (isResolved(length)) {
                        logForwardResolved(uv, length);
                        tempResolved.add(uv);
                        return true; 
                    }
                    return false;
                } else {
                    logForwardResolved(uv);
                    tempResolved.add(uv);
                    return true;
                }
            }
            // let post heap to resolve it
            return false;
        }

        throw new RuntimeException("Unknown value type " + uv);
    }

    public boolean isForwardResolved(Value v) {
        return forwardResolvedValues.contains(v);
    }

    public boolean isBackwardResolved(Value v) {
        return valueToBackwardValues.containsKey(v);
    }

    protected boolean doesNotPointToObject(Value ref) {
        return Value.getElementInfo(ref) == null;
    }

    public boolean isReversible() {
        boolean ret = checkReversible();

        if (ret && !unresolvedValues.isEmpty()) {
            for (Value v:unresolvedValues) {
                System.out.println(v);
            }
            throw new RuntimeException("A reversible method has unresovled values");
        }

        return ret;
    }

    public boolean checkReversible() {
        if (unresolvedValues.isEmpty()) {
            return true;
        }

        for (Value v:resolvedValues) {
            // we must also resolve the location of the input.
            if (v instanceof Input) {
                Location loc = ((Input<?>) v).getLocation();
                if (loc instanceof ArrayElement) {
                    ArrayElement ae = (ArrayElement) loc;
                    Value ref = ae.getContainer().getReference();
                    Value index = ae.getIndex();
                    if (!(isResolved(ref) && isResolved(index))) {
                        if (printWhyInreversible) System.out.println("Cannot resolve ref or index for an array element input " + v);
                        return false;
                    }
                } else if (loc instanceof InstanceField) {
                    InstanceField inf = (InstanceField) loc;
                    Value ref = inf.getContainer().getReference();
                    if (!isResolved(ref)) {
                        if (printWhyInreversible) System.out.println("Cannot resolve ref for an instance field input " + v);
                        return false;
                    }
                } else if (loc instanceof ArrayLength) {
                    ArrayLength al = (ArrayLength) loc;
                    Value ref = al.getContainer().getReference();
                    if (!isResolved(ref)) {
                        if (printWhyInreversible) System.out.println("Cannot resolve ref for an array length input " + v);
                        return false;
                    }
                }
            }
        }

        for (Value v:unresolvedValues) {
            if (v instanceof Input) {
                Location loc = ((Input<?>) v).getLocation();
                if (loc instanceof MethodParameterLocation) {
                    if (printWhyInreversible) System.out.println("Cannot resolve a parameter " + v);
                    return false;
                }

                if (loc instanceof HeapLocation) {
                    if (printWhyInreversible) System.out.println("Cannot resolve a heap location " + v);
                    return false;
                }

                throw new RuntimeException("unknown location type " + loc);
            }

            if (v instanceof Output) {
                // we have no idea about the index.
                // Thus, we have to conservatively make this irreversible.
                if (printWhyInreversible) System.out.println("Cannot resolve an output " + v);
                return false;
            }

            if (v instanceof OperatorValue) {
                if (printWhyInreversible) System.out.println("Cannot resolve an operator " + v);
                return false;
            }



            if (v instanceof NewRef) {
                // throw new RuntimeException("Not implemented yet");
                // We cannot resolve an dangling object ref.
                if (printWhyInreversible) System.out.println("Cannot resolve an output " + v);
                return false;
            }

            if (v instanceof BinopExpr || v instanceof UnopExpr) {
                continue;
            }

            throw new RuntimeException("Sanity check failed! Unknown unresolved value " + v);
        }

        for (IfTraceNode ifTN:ifTraceNodes) {
            Value v1 = ifTN.getLeftValue();
            Value v2 = ifTN.getRightValue();

            if (!(isResolved(v1) && isResolved(v2))) {
                if (printWhyInreversible) System.out.println("Cannot resolve a predicate " + ifTN);
                return false;
            }
        }

        for (SwitchTraceNode switchTraceNode:switchTraceNodes) {
            Value v = switchTraceNode.getValue();

            if (!(isResolved(v))) {
                if (printWhyInreversible) System.out.println("Cannot resolve a switch " + switchTraceNode);
                return false;
            }
        }

        return true;
    }

    public void toString(PrintWriter pw) {
        int count = 0;
        pw.println("Predicates:");
        for (IfTraceNode node:ifTraceNodes) {
            pw.format("%2d: %s ({%s} {%s})\n", count++, node, node.getLeftValue(), node.getRightValue());
        }

        count = 0;
        pw.println("Forward Resolved:");
        for (Value v:resolvedValues) {
            pw.format("%2d: %s\n", count++, v);
        }

        count = 0;
        pw.println("Unresolved:");
        for (Value v:unresolvedValues) {
            pw.format("%2d: %s\n", count++, v);
        }

        // do nothing
        pw.close();
    }

    public String toString() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(baos);

        toString(pw);

        return baos.toString();
    }

    public static boolean checkReversible(ExecutionSummary es) {
        if (!es.getUnallocatedAccessPaths().isEmpty()) {
            return false;
        }

        return new ValueGraphReversibleChecker(es).isReversible();
    }
}
