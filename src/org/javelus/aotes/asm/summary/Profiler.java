package org.javelus.aotes.asm.summary;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.javelus.aotes.asm.trace.TraceKey;
import org.javelus.aotes.asm.utils.MapUtils;
import org.javelus.aotes.asm.vm.Instruction;
import org.javelus.aotes.asm.vm.MethodInfo;

public class Profiler {


    Map<MethodInfo, Map<Instruction, Set<MethodInfo>>> callGraph = new HashMap<>();
    public void addCallGraphEdge(MethodInfo caller, Instruction insn, MethodInfo callee) {
        MapUtils.addToMapMapSet(callGraph, caller, insn, callee);
    }

    Map<MethodSummary, SymbolicExecutionTree> symbolicExecutionTrees = new HashMap<MethodSummary, SymbolicExecutionTree>();
    public void addTrace(ExecutionSummary es) {
        TraceKey key = es.getTrace().getKey();
        SymbolicExecutionTree set = getSymbolicExecutionTree(es.getMethodSummary());
        set.addTrace(key, es.isCompleted() || es.isExceptional());
    }

    public SymbolicExecutionTree getSymbolicExecutionTree(MethodSummary ms) {
        SymbolicExecutionTree set = symbolicExecutionTrees.get(ms);
        if (set == null) {
            set = new SymbolicExecutionTree(ms);
            symbolicExecutionTrees.put(ms, set);
        }
        return set;
    }

    public boolean isBranchVisited(MethodSummary ms, TraceKey key, Instruction insn, int branch) {
        SymbolicExecutionTree set = getSymbolicExecutionTree(ms);
        return set.isBranchVisisted(key, insn, branch);
    }

    public boolean isNodeVisited(MethodSummary mi, TraceKey key, Instruction insn, int branch) {
        SymbolicExecutionTree set = getSymbolicExecutionTree(mi);
        return set.isNodeVisited(key, insn, branch);
    }

    public boolean hasMorePaths(MethodSummary mi) {
        SymbolicExecutionTree set = getSymbolicExecutionTree(mi);
        return set.hasMorePaths();
    }


    enum IfStatus{
        TRUE,
        FALSE,
        ANY,
    }

    Map<Instruction, IfStatus> blacklistIf = new HashMap<Instruction, IfStatus>();
    public boolean blacklistIf(Instruction insn, boolean value) {
        IfStatus status = value ? IfStatus.TRUE : IfStatus.FALSE;
        IfStatus old = blacklistIf.put(insn, status);

        if (old == null) {
            return true;
        }

        if (old == status) {
            return true;
        }

        // conflict, so we can take either
        blacklistIf.put(insn, IfStatus.ANY);
        return false;
    }

    public boolean takenIf(Instruction insn, boolean value) {
        IfStatus old = this.blacklistIf.get(insn);

        if (old == null || old == IfStatus.ANY) {
            return true;
        }
        IfStatus status = value ? IfStatus.TRUE : IfStatus.FALSE;
        return old != status;
    }


    static class SwitchStatus {
        private int branch;

        public SwitchStatus(int branch) {
            this.branch = branch;
        }

        public int getBranch() {
            return branch;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + branch;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            SwitchStatus other = (SwitchStatus) obj;
            if (branch != other.branch)
                return false;
            return true;
        }

        static Map<Integer, SwitchStatus> statuses = new HashMap<Integer, SwitchStatus>();

        static SwitchStatus status(int branch) {
            SwitchStatus status = statuses.get(branch);
            if (status == null) {
                status = new SwitchStatus(branch);
                MapUtils.addToMap(statuses, branch, status);
            }
            return status;
        }
    }

    Map<Instruction, Set<SwitchStatus>> blacklistSwitch = new HashMap<Instruction, Set<SwitchStatus>>();
    public void blacklistSwitch(Instruction insn, int branch) {
        MapUtils.addToMapSet(blacklistSwitch, insn, SwitchStatus.status(branch));
    }

    public boolean takenSwitch(Instruction insn, int branch) {
        Set<SwitchStatus> switches = blacklistSwitch.get(insn);
        return !switches.contains(SwitchStatus.status(branch));
    }


}
