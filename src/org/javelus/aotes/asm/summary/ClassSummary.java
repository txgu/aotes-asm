package org.javelus.aotes.asm.summary;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.im.InverseFragment;
import org.javelus.aotes.asm.im.InverseFragmentPrinter;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.MethodInfo;
import org.javelus.aotes.executor.ValueMap;


public class ClassSummary {

    private MDG mdg;
    private ClassInfo classInfo;

    /**
     * like a vtable
     */
    private Map<String, MethodSummary> keyToMethodSummary;

    public ClassSummary(MDG mdg, ClassInfo ci) {
        this.mdg = mdg;
        classInfo = ci;
        keyToMethodSummary = new HashMap<String, MethodSummary>();
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }

    public MDG getMDG() {
        return mdg;
    }

    public void printInverseFragment(PrintWriter pw) {
        List<InverseFragment> ifs = new ArrayList<InverseFragment>();
        for (MethodSummary ms:getMethodSummaries()) {
            if (ms.hasInverseFragment()) {
                ifs.addAll(ms.getInverseMutator().getFragments());
            }
        }
        printInverseFragment(pw, ifs);
    }

    public void printInverseFragment(PrintWriter pw, Collection<InverseFragment> inverseFragments) {
        if (classInfo.isArray() || classInfo.isPrimitive() || classInfo.isInterface()) {
            return;
        }

        Set<AccessPath> defined = new HashSet<>();
        Set<AccessPath> mutatedReceivers = new HashSet<>();

        for (InverseFragment ift:inverseFragments) {
            ExecutionSummary es = ift.getExecutionSummary();
            defined.addAll(es.getDefinedAccessPaths());
        }

        pw.print("import ");
        pw.print(ValueMap.class.getPackage().getName());
        pw.println(".*;");
        if (!defined.isEmpty()) {
            Iterator<AccessPath> it = defined.iterator();
            AccessPath ap = it.next();
            pw.print("@Defined({\"");
            pw.print(ap.toString());
            pw.print("\"");
            while (it.hasNext()) {
                ap = it.next();
                pw.print(",\"");
                pw.print(ap.toString());
                pw.print("\"");
            }
            pw.print("})\n");
        }

        if (!mutatedReceivers.isEmpty()) {
            Iterator<AccessPath> it = mutatedReceivers.iterator();
            AccessPath ap = it.next();
            pw.print("@MutatedReceiver({\"");
            pw.print(ap.toString());
            pw.print("\"");
            while (it.hasNext()) {
                ap = it.next();
                pw.print(",\"");
                pw.print(ap.toString());
                pw.print("\"");
            }
            pw.print("})\n");
        }

        pw.print("public class ");
        pw.print(getInverseClassName());
        pw.print(" {\n");
        printInverseFragments(pw, inverseFragments);
        pw.print("}\n");
        pw.flush();
    }

    void printInverseFragments(PrintWriter pw, Collection<InverseFragment> inverseFragments) {
        for (InverseFragment ift:inverseFragments) {
            InverseFragmentPrinter.print(ift, pw);
        }
    }

    public String getInverseClassName() {
        return "AOTES_" + getPrintClassName(classInfo);
    }

    public static String getPrintClassName(ClassInfo ci) {
        if (ci.getEnclosingClassInfo() != null) {
            return getPrintClassName(ci.getEnclosingClassInfo()) + "$" + ci.getSimpleName();
        }

        if (ci.getEnclosingMethodInfo() != null) {
            ClassInfo emici = ci.getEnclosingMethodInfo().getClassInfo();
            return getPrintClassName(emici) + "$" + ci.getSimpleName();
        }

        return ci.getName().replace('.', '_');
    }

    public void printList() {
        System.out.println(getPrintClassName(classInfo));
        for (MethodSummary ms:keyToMethodSummary.values()) {
            ms.printList();
        }
    }

    public MethodSummary getOrCreateMethodSummary(MethodInfo mi) {
        String key = mi.getUniqueName();
        MethodSummary ms = keyToMethodSummary.get(key);;
        if (ms == null) {
            ms = new MethodSummary(this, mi);
            keyToMethodSummary.put(key, ms);
            mdg.methodSummaries.add(ms);
        }
        return ms;
    }

    public MethodSummary getMethodSummary(MethodInfo mi) {
        String key = mi.getUniqueName();
        return keyToMethodSummary.get(key);
    }

    public Collection<MethodSummary> getMethodSummaries() {
        return keyToMethodSummary.values();
    }

    void addAllMethods() {
        for (MethodInfo mi:classInfo.getDeclaredMethodInfos()) {
            if (mi.isNative() || mi.isAbstract()) {
                continue;
            }
            getOrCreateMethodSummary(mi);
        }
        addAllMethodsFromSuper(classInfo.getSuperClass());
    }

    private void addAllMethodsFromSuper(ClassInfo ci) {
        if (ci == null || ci.isObjectClassInfo()) {
            return;
        }
        for (MethodInfo mi:ci.getDeclaredMethodInfos()) {
            if (mi.isNative() || mi.isAbstract() || mi.isStatic()) {
                continue;
            }
            if (mi.isPrivate() || mi.isInit() || mi.isClinit()) {
                continue;
            }
            getOrCreateMethodSummary(mi);
        }
        addAllMethodsFromSuper(ci.getSuperClass());
    }

    public String toString() {
        return classInfo.getName();
    }

    public void updateExecutionSummaryGraph() {
        for (MethodSummary ms:getMethodSummaries()) {
            for (MethodSummary ams:getMethodSummaries()) {
                ms.updateExecutionSummaryGraph(ams);
            }
        }
    }

    public boolean hasInverseMethods() {       
        for (MethodSummary ms:getMethodSummaries()) {
            if (ms.hasInverseFragment()) {
                return true;
            }
        }
        return false;
    }
}
