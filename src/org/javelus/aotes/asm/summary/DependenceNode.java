package org.javelus.aotes.asm.summary;

import java.util.Set;

public interface DependenceNode <K extends DependenceNode<K>>{

    boolean addPredecessor(K s);
    boolean addSuccessor(K t);

    Set<K> getPredecessors();
    Set<K> getSuccessors();
}
