package org.javelus.aotes.asm.summary;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.ArrayLength;
import org.javelus.aotes.asm.location.FieldLocation;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.LocationPath;
import org.javelus.aotes.asm.trace.TraceEngine;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.Output;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.ArrayElementInfo;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.FieldInfo;



public class PostPartialHeap extends PartialHeap {

    private Map<LocationPath, Output<? extends HeapLocation>> postHeapValues = new HashMap<LocationPath, Output<? extends HeapLocation>>();

    /**
     * Create a post heap based on the root in the PrePartialHeap
     * We need visit all objects reachable from root and add all non-unknown values into 
     * @param prePartialHeap
     * @param traceEngine
     * @return
     */
    public static PostPartialHeap createdFromPreHeap(TraceEngine traceEngine, PrePartialHeap pre) {
        PostPartialHeap postPartialHeap = new PostPartialHeap();
        List<LocationPath> rootRefs = pre.getRootRefs();
        // 1). Collect values reachable from roots in PrePartialHeap
        for (LocationPath lp:rootRefs) {
            postPartialHeap.buildPostAccessPathMap(traceEngine, lp, pre.getPointedObject(lp));
        }
        return postPartialHeap;
    }

    /**
     * 
     * @param traceEngine
     * @param accessPath: accesspath to this object
     * @param objectRef: this object ref
     */
    private void buildPostAccessPathMap(TraceEngine traceEngine, LocationPath objectLocationPath, ElementInfo object) {
        if (objectToAccessPath.containsKey(object)) {
            return;
        }

        registerObject(objectLocationPath, object);

        if (object.isArray()) {
            ArrayElementInfo aei = (ArrayElementInfo) object;
            {
                ArrayLength al = aei.arrayLength();
                LocationPath locationPath = getLocationPath(al);
                if (al.value() !=  Input.INPUT_PLACEHOLDER) {
                    postHeapValues.put(locationPath, traceEngine.createPostHeapOutput(locationPath, al));
                }
            }

            Iterator<HeapLocation> it = aei.locations().iterator();
            if (it.hasNext()) {
                while(it.hasNext()) {
                    ArrayElement e = (ArrayElement) it.next();
                    LocationPath locationPath = getLocationPath(e);
                    if (e.value() == Input.INPUT_PLACEHOLDER) {
                        throw new RuntimeException("Placeholder should be substituted");
                    }
                    postHeapValues.put(locationPath, traceEngine.createPostHeapOutput(locationPath, e));

                    if (e.isReference()) {
                        Value value = e.value();
                        ElementInfo ei = Value.getElementInfo(value);
                        if (ei == null) {
                            continue;
                        }
                        buildPostAccessPathMap(traceEngine, locationPath, ei);
                    }
                }
            }
        } else {
            Iterator<HeapLocation> it = object.locations().iterator();

            while (it.hasNext()) {
                FieldLocation loc = (FieldLocation) it.next();
                FieldInfo fi = loc.getFieldInfo();
                Value value = loc.value();

                if (value == null) {
                    throw new RuntimeException("Sanity check failed!");
                }

                // An untouched field.
                if (value == Input.INPUT_PLACEHOLDER) {
                    throw new RuntimeException("Sanity check failed");
                }

                LocationPath locationPath = getLocationPath(loc);
                postHeapValues.put(locationPath, traceEngine.createPostHeapOutput(locationPath, loc));

                if (fi.isReference()) {
                    ElementInfo ei = Value.getElementInfo(loc.value());
                    if (ei != null) {
                        buildPostAccessPathMap(traceEngine, locationPath, ei);
                    }
                }
            }
        }
    }

    public Output<?  extends HeapLocation> getPostHeapValue(LocationPath locationPath) {
        return postHeapValues.get(locationPath);
    }

    public Collection<Output<? extends HeapLocation>> getPostHeapValues() {
        return postHeapValues.values();
    }
}
