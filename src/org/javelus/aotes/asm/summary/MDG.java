package org.javelus.aotes.asm.summary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.javelus.aotes.asm.CallGraph;
import org.javelus.aotes.asm.DynamicPatch;
import org.javelus.aotes.asm.Options;
import org.javelus.aotes.asm.im.InverseFragment;
import org.javelus.aotes.asm.trace.TraceEngine;
import org.javelus.aotes.asm.utils.JVMUtils;
import org.javelus.aotes.asm.utils.MapUtils;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.MethodInfo;
import org.javelus.aotes.executor.DirectedGraph;
import org.javelus.aotes.executor.MethodMeta;


public class MDG {

    Set<MethodSummary> methodSummaries;

    private Map<ClassInfo, ClassSummary> classToClassSummary;

    private List<MethodSummary> head;
    private List<MethodSummary> tail;

    /**
     * Partial Heap that reachable from static fields
     */
    private PartialHeapSkeleton partialHeapSkeleton;

    final int executionBound;

    private final DynamicPatch dynamicPatch;

    private boolean useProfiler = false;
    /**
     * Unmatched methods and methods that do not depend on changed methods are pruned.
     */
    final boolean pruneStaticMethods;
    boolean pruneNoAddedField;
    private final boolean pruneUnmatchedMethods;
    /*private*/ final boolean pruneUnreachableMethods;

    private Profiler profiler;

    public MDG(DynamicPatch dynamicPatch) {
        this.dynamicPatch = dynamicPatch;
        this.executionBound = Options.executionBound();
        this.pruneNoAddedField = Options.pruneNoAddedFieldClasses();
        this.pruneStaticMethods = Options.pruneStaticMethods();
        this.pruneUnmatchedMethods = Options.pruneUnmatchedMethods();
        this.pruneUnreachableMethods = Options.pruneUnmatchedMethods();
        methodSummaries = new HashSet<MethodSummary>();
        classToClassSummary = new HashMap<ClassInfo, ClassSummary>();
        partialHeapSkeleton = new PartialHeapSkeleton(this);
        profiler = new Profiler();

        initializeMDG();
    }

    public void setPruneNoAddedField(boolean value) {
        this.pruneNoAddedField = value;
    }

    public Profiler getProfiler() {
        return profiler;
    }

    protected void initializeMDG() {
        for(ClassInfo ci:this.dynamicPatch.getChangedClasses()) {
            addAllMethods(ci);
        }
    }

    private void addAllMethods(ClassInfo ci) {
        if (ci.isObjectClassInfo()) {
            return;
        }
        ClassSummary cs = getOrCreateClassSummary(ci);
        cs.addAllMethods();
    }

    public void updateExecutionSummaryGraph() {
        for (ClassSummary cs:getClassSummaries()) {
            cs.updateExecutionSummaryGraph();
        }
    }

    public void addSummary(MethodSummary ms, ExecutionSummary es) {
        ms.addExecutionSummary(es);
    }

    public ClassSummary getClassSummary(ClassInfo ci) {
        return classToClassSummary.get(ci);
    }

    protected ClassSummary getOrCreateClassSummary(ClassInfo ci) {
        if (JVMUtils.isImmutable(ci.getName())) {
            throw new RuntimeException("Cannot add class summary for immutable class " + ci);
        }
        ClassSummary cs = classToClassSummary.get(ci);
        if (cs == null) {
            cs = new ClassSummary(this, ci);
            classToClassSummary.put(ci, cs);
        }
        return cs;
    }

    protected MethodSummary getOrCreateMethodSummary (ClassSummary cs, MethodInfo mi) {
        return cs.getOrCreateMethodSummary(mi);
    }

    void buildLeafNodes() {
        head = new ArrayList<MethodSummary>();
        tail = new ArrayList<MethodSummary>();

        for (MethodSummary ms:getMethodSummaries()) {
            if (ms.getPredecessors().isEmpty()) {
                head.add(ms);
            }
            if (ms.getSuccessors().isEmpty()) {
                tail.add(ms);
            }
        }
    }

    public List<MethodSummary> getHead() {
        return head;
    }

    public List<MethodSummary> getTail() {
        return tail;
    }

    public boolean checkReversibleConstructors() {
        boolean hasConstructor = false;
        boolean hasReversible = false;
        for (MethodSummary ms:this.methodSummaries) {
            if (!dynamicPatch.isChangedClass(ms.getTargetClassInfo())) {
                continue;
            }
            for (ExecutionSummary es:ms.getCompletedExecutionSummaries()) {
                MethodInfo mi = es.getEntryMethod();
                if (mi.isInit() && es.isMutator()) {
                    hasConstructor = true;
                    if (ValueGraphReversibleChecker.checkReversible(es)) {
                        hasReversible = true;
                        ms.addInverseFragment(es.getChecker().getInverseFragment());;
                    }
                }
            }
        }

        return hasConstructor && hasReversible;
    }

    public boolean checkReversibleMethods() {
        boolean has = false;
        for (MethodSummary ms:this.methodSummaries) {
            if (!dynamicPatch.isChangedClass(ms.getTargetClassInfo())) {
                continue;
            }
            for (ExecutionSummary es:ms.getCompletedExecutionSummaries()) {
                MethodInfo mi = es.getEntryMethod();
                if (!mi.isInit() && es.isMutator()) {
                    if (ValueGraphReversibleChecker.checkReversible(es)) {
                        has = true;
                        ms.addInverseFragment(es.getChecker().getInverseFragment());;
                    }
                }
            }
        }
        return has;
    }

    public static String getMethodNameAndSignature(ExecutionSummary es) {
        MethodInfo mi = es.getEntryMethod();
        return String.format("%s.%s", mi.getClassName(), mi.getUniqueName());
    }

    public Collection<MethodSummary> getMethodSummaries() {
        return methodSummaries;
    }

    public void printList() {
        for (ClassSummary cs:classToClassSummary.values()) {
            if (!dynamicPatch.isChangedClass(cs.getClassInfo())) {
                continue;
            }
            cs.printList();
        }
    }

    public void printInverseFragments(PrintWriter pw) {
        for (ClassSummary cs:classToClassSummary.values()) {
            if (shouldPrintThisClass(cs)) {
                cs.printInverseFragment(pw);
            }
        }
    }

    public void checkReversibility() {
        checkReversibleConstructors();
        checkReversibleMethods();
    }

    public void process() {
        process(getMethodSummaries());
    }

    public void process(MethodInfo mi) {
        ClassSummary cs = getOrCreateClassSummary(mi.getClassInfo());
        MethodSummary ms = getOrCreateMethodSummary(cs, mi);
        process(Collections.singleton(ms));
    }

    public void addClassSummary(ClassInfo ci) {
        ClassSummary cs = getOrCreateClassSummary(ci);
        cs.addAllMethods();
    }

    public void addMethodSummary(ClassInfo ci, MethodInfo mi) {
        ClassSummary cs = getOrCreateClassSummary(ci);
        cs.getOrCreateMethodSummary(mi);
    }

    public void process(Collection<MethodSummary> mss) {
        List<MethodSummary> unprocessed = new ArrayList<MethodSummary>(mss);

        int bounds = 0;

        final int UPPER_BOUND = Integer.getInteger("aotes.process.bound", 100);

        while (!unprocessed.isEmpty()) {
            bounds ++;
            if (bounds > UPPER_BOUND) {
                for (int i=0; i<unprocessed.size(); i++) {
                    MethodSummary ms = unprocessed.get(i);
                    MethodInfo mi = ms.getMethodInfo();
                    System.out.format("%4d: %s.%s%s\n", i, ms.getTargetClassInfo(), mi.getName(), mi.getDescriptor());
                }
                return;
            }
            System.out.format(">>> Round (%d): %d methods need to be processed.\n", bounds, unprocessed.size());
            for(MethodSummary ms:unprocessed) {
                //System.err.format(">>> Round(%d) Begin to update method summary for %s\n", bounds, ms.getMethodInfo().getFullName());
                updateMethodSummary(ms);
                //System.err.format(">>> Round(%d) End to update method summary for %s\n", bounds, ms.getMethodInfo().getFullName());
            }

            unprocessed.clear();

            for (MethodSummary ms: getMethodSummaries()) {
                if (ms.requireExtraHeap()) {
                    ms.updateExtraHeap(this);
                }
                if (ms.extraHeapUpdated()) {
                    // System.out.format("Round (%d): Add summary for %s \n", bounds, ms);
                    unprocessed.add(ms);
                }
            }
        }
    }

    public void printStatistics(String title) {
        printStatistics(title, false);
    }

    public void printStatistics(String title, boolean onlyPrintable) {
        List<ClassSummary> classes = new ArrayList<ClassSummary>();
        for (ClassInfo cls:dynamicPatch.getChangedClasses()) {
            classes.add(getClassSummary(cls));
        }
        printStatistics(title, onlyPrintable, classes);
    }

    public void printStatistics(String title, boolean onlyPrintable, Collection<ClassSummary> classes) {
        System.out.println(">>> Statistics of " + title);
        System.out.println("CC: Changed Classes;\n"
                + "CCI: Changed Classes with at least an Inverse method;\n"
                + "M: Methods in changed classes;\n"
                + "MI: Methods with at least an Inverse method;\n"
                + "IM: Inverse Methods;\n"
                + "MM: Matched methods;\n"
                + "MCM: Matched Changed Methods;\n"
                + "MMI: Matched Methods with at least an Inverse method;\n"
                + "MIM: Matched Inverse Methods;\n");
        int count = 0;
        System.out.println("INDEX\tM.\tM.I.\tI.M.\tM.M.\tM.C.M.\tM.M.I.\tM.I.M.\tCLASS");
        int totalNumOfMethodSummary = 0;
        int totalNumOfMatchedMethodSummary = 0;
        int totalNumOfMatchedChangedMethodSummary = 0;
        int totalNumOfInversedMethodSummary = 0;
        int totalNumOfInverseFragments = 0;
        int totalNumOfMatchedInversedMethodSummary = 0;
        int totalNumOfMatchedInverseFragments = 0;
        int totalNumOfInversedClass = 0;
        for (ClassSummary cs : classes) {
            if (onlyPrintable && !shouldPrintThisClass(cs)) {
                continue;
            }
            int numOfMethodSummary = 0;
            int numOfMatchedMethodSummary = 0;
            int numOfMatchedChangedMethodSummary = 0;
            int numOfInversedMethodSummary = 0;
            int numOfInverseFragments = 0;
            int numOfMatchedInversedMethodSummary = 0;
            int numOfMatchedInverseFragments = 0;
            for (MethodSummary ms:cs.getMethodSummaries()) {
                if (ms.isInherited()) {
                    continue;
                }
                if (onlyPrintable && !shouldPrintThisMethod(ms)) {
                    continue;
                }
                numOfMethodSummary ++;
                if (ms.hasInverseFragment()) {
                    numOfInversedMethodSummary ++;
                }
                numOfInverseFragments += ms.getInverseMutator().getFragments().size();
                if (dynamicPatch.isUnmatchedChangedMethods(ms.getMethodInfo())) {
                    continue;
                }
                numOfMatchedMethodSummary ++;
                if (dynamicPatch.isMatchedChangedMethods(ms.getMethodInfo())) {
                    numOfMatchedChangedMethodSummary++;
                }
                if (ms.hasInverseFragment()) {
                    numOfMatchedInversedMethodSummary ++;
                }
                numOfMatchedInverseFragments += ms.getInverseMutator().getFragments().size();
            }
            System.out.format("%5d). %d\t%d\t%d\t%d\t%d\t%d\t%d\t%s\n",
                    count++, numOfMethodSummary, numOfInversedMethodSummary, numOfInverseFragments,
                    numOfMatchedMethodSummary, numOfMatchedChangedMethodSummary,
                    numOfMatchedInversedMethodSummary, numOfMatchedInverseFragments,
                    cs.getClassInfo().getName());
            if (numOfInversedMethodSummary > 0) {
                totalNumOfInversedClass++;
            }
            totalNumOfMethodSummary += numOfMethodSummary;
            totalNumOfMatchedMethodSummary += numOfMatchedMethodSummary;
            totalNumOfMatchedChangedMethodSummary += numOfMatchedChangedMethodSummary;
            totalNumOfInversedMethodSummary += numOfInversedMethodSummary;
            totalNumOfInverseFragments += numOfInverseFragments;
            totalNumOfMatchedInversedMethodSummary += numOfMatchedInversedMethodSummary;
            totalNumOfMatchedInverseFragments += numOfMatchedInverseFragments;
        }
        System.out.println("C.C.\tC.I.\tM.\tM.I.\tI.M.\tM.M.\tM.C.M.\tM.M.I.\tM.I.M.");
        System.out.format("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",
                count++, totalNumOfInversedClass, totalNumOfMethodSummary, totalNumOfInversedMethodSummary, totalNumOfInverseFragments,
                totalNumOfMatchedMethodSummary, totalNumOfMatchedChangedMethodSummary,
                totalNumOfMatchedInversedMethodSummary, totalNumOfMatchedInverseFragments);
        System.out.println(">>> End of Statistics");
    }

    static Pattern ANONYMOUS_CLASS = Pattern.compile("[$][0-9]+");


    private boolean shouldPrintThisClass(ClassSummary cs) {
        if (pruneNoAddedField) {
            ClassInfo ci = cs.getClassInfo();
            if (!dynamicPatch.hasNewFields(ci)) {
                return false;
            }
            if (ci.isAbstract()) {
                return false;
            }
            String name = ci.getName();
            Matcher m = ANONYMOUS_CLASS.matcher(name);
            if (m.find()) {
                return false;
            }
            return true;
        }
        return true;
    }

    /**
     * 
     */
    public void writeDirectedGraph(File directory) {
        Map<MethodSummary, String> msToID = new HashMap<MethodSummary, String>();
        for (MethodSummary ms:methodSummaries) {
            MethodInfo mi = ms.getMethodInfo();
            String id = MethodMeta.toLine(mi.getClassInfo().getName(), mi.getName(), mi.getDescriptor());
            msToID.put(ms, id);
        }

        Set<String> nodes = new HashSet<String>();
        Set<String> heads = new HashSet<String>();
        Set<String> tails = new HashSet<String>();
        Map<String, Set<String>> nodeToPreds = new HashMap<String, Set<String>>();
        Map<String, Set<String>> nodeToSuccs = new HashMap<String, Set<String>>();

        for (MethodSummary ms:methodSummaries) {
            String id = msToID.get(ms); 
            nodes.add(id);
            Set<MethodSummary> preds = ms.getPredecessors();
            Set<MethodSummary> succs = ms.getSuccessors();
            if (preds.isEmpty()) {
                heads.add(id);
            } else {
                for (MethodSummary p:preds) {
                    MapUtils.addToMapSet(nodeToPreds, id, msToID.get(p));
                }
            }
            if (succs.isEmpty()) {
                tails.add(id);
            } else {
                for (MethodSummary s:succs) {
                    MapUtils.addToMapSet(nodeToSuccs, id, msToID.get(s));
                }
            }
        }

        DirectedGraph<String> dg = new DirectedGraph<String>(nodes, nodeToPreds, nodeToSuccs, heads, tails);
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(new File(directory, "directedgraph.mdg")));
            oos.writeObject(dg);
        } catch(IOException e){
            System.err.println("Write DirectedGraph error!");
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void updateMethodSummary(MethodSummary ms) {
        MethodInfo mi = ms.getMethodInfo();
        if (mi.isAbstract()) {
            return;
        }

        if (mi.isNative()) {
            return;
        }

        boolean requireNew = true;

        if (!ms.getInterruptedExecutionSummaries().isEmpty()) {
            requireNew = false;
            List<ExecutionSummary> unresolvedExecutionSummaries = new ArrayList<ExecutionSummary>(ms.getInterruptedExecutionSummaries());
            for (ExecutionSummary es:unresolvedExecutionSummaries) {
                if (es.extraHeapUpdated() && es.isInterrupted()) {
                    es.resume();
                    requireNew = true;
                }
            }
        }

        int count = ms.countSummaries();

        if (!useProfiler || requireNew) {

            while(true) {
                count++;
                if (count > executionBound) {
                    break;
                }

                if (useProfiler && !profiler.hasMorePaths(ms)) {
                    break;
                }

                TraceEngine traceEngine = new TraceEngine(this, ms);
                traceEngine.execute();
            }
        }
    }

    public boolean isChangedClass(ClassInfo ci) {
        return this.dynamicPatch.isChangedClass(ci);
    }

    public PartialHeapSkeleton getPartialHeapSkeleton() {
        return partialHeapSkeleton;
    }

    public Collection<ClassSummary> getClassSummaries() {
        return this.classToClassSummary.values();
    }

    /**
     * Matched methods contains unchanged methods and matched changed methods except unmatched changed methods
     * @param ms
     * @return
     */
    public boolean isMatched(MethodSummary ms) {
        return !dynamicPatch.isUnmatchedChangedMethods(ms.getMethodInfo());
    }

    public boolean isMatched(ExecutionSummary es) {
        return isMatched(es.getMethodSummary());
    }

    public boolean isMatchedChanged(MethodSummary ms) {
        return dynamicPatch.isMatchedChangedMethods(ms.getMethodInfo());
    }

    private boolean shouldPrintThisMethod(MethodSummary ms) {
        if (!shouldPrintThisClass(ms.getClassSummary())) {
            return false;
        }
        if (!ms.hasInverseFragment()) {
            return false;
        }
        if (pruneStaticMethods
                && (ms.getMethodInfo().isStatic() || ms.getMethodInfo().isClinit())) {
            return false;
        }
        if (pruneUnmatchedMethods && !isMatched(ms)) {
            return false;
        }
        return true;
    }

    protected Map<ClassSummary, Set<InverseFragment>> pruneInverseFragments() {
        Set<MethodSummary> visited = new HashSet<MethodSummary>();

        if (pruneUnmatchedMethods == false
                && pruneUnreachableMethods == false) {
            for (ClassSummary cs:getClassSummaries()) {
                for (MethodSummary ms:cs.getMethodSummaries()) {
                    if (ms.hasInverseFragment()) {
                        visited.add(ms);
                    }
                }
            }
        } else if (pruneUnmatchedMethods && !pruneUnreachableMethods) {
            for (ClassSummary cs:getClassSummaries()) {
                for (MethodSummary ms:cs.getMethodSummaries()) {
                    if (shouldPrintThisMethod(ms)) {
                        visited.add(ms);
                    }
                }
            }
        } else if (pruneUnmatchedMethods && pruneUnreachableMethods){
            // 1). collect roots, i.e., matched changed methods
            for (ClassSummary cs:getClassSummaries()) {
                if (shouldPrintThisClass(cs)) {
                    boolean added = false;
                    for (MethodSummary ms:cs.getMethodSummaries()) {
                        if (shouldPrintThisMethod(ms) 
                                && (isMatchedChanged(ms))) {
                            visited.add(ms);
                            added = true;
                        }
                    }
                    if (added) {
                        for (MethodSummary ms:cs.getMethodSummaries()) {
                            if (shouldPrintThisMethod(ms) 
                                    && (ms.getMethodInfo().isInit())) {
                                visited.add(ms);
                            }
                        }
                    }
                }
            }


            Set<MethodSummary> added = new HashSet<MethodSummary>();
            while (true) {
                added.clear();
                for (MethodSummary ms:visited) {
                    // add predecessors
                    for (MethodSummary p:ms.getPredecessors()) {
                        if (visited.contains(p)) {
                            continue;
                        }
                        if (shouldPrintThisMethod(p)) {
                            added.add(p);
                        }
                    }

                    // add callers
                    Collection<MethodInfo> callers = CallGraph.getCaller(ms.getMethodInfo());
                    if (callers == null || callers.isEmpty()) {
                        continue;
                    }
                    for (MethodInfo caller:callers) {
                        ClassSummary cs = getClassSummary(caller.getClassInfo());
                        if (cs == null) {
                            continue;
                        }
                        MethodSummary callerMS = cs.getMethodSummary(caller);
                        if (callerMS == null) {
                            continue;
                        }
                        if (visited.contains(callerMS)) {
                            continue;
                        }
                        if (shouldPrintThisMethod(callerMS)) {
                            added.add(callerMS);
                        }
                    }
                }
                if (added.isEmpty()) {
                    break;
                }
                visited.addAll(added);
            }

        } else {
            throw new RuntimeException("Should not reach here");
        }
        Map<ClassSummary, Set<InverseFragment>> pruned = new HashMap<ClassSummary, Set<InverseFragment>>();
        for (MethodSummary ms:visited) {
            MapUtils.addAllToMapSet(pruned, ms.getClassSummary(), ms.getInverseMutator().getFragments());
        }

        return pruned;
    }

    public void pruneAndPrintInverseFragments(String statisticsTitle, File directory) {
        Map<ClassSummary, Set<InverseFragment>> pruned = pruneInverseFragments();
        if (Options.verbose()) {
            PrintWriter pw = new PrintWriter(System.out);
            for (Entry<ClassSummary, Set<InverseFragment>> entry:pruned.entrySet()) {
                entry.getKey().printInverseFragment(pw, entry.getValue());
            }
        }

        printStatistics(statisticsTitle, false, pruned.keySet());

        PrintWriter pw = null;
        for (Entry<ClassSummary, Set<InverseFragment>> entry:pruned.entrySet()) {
            try {
                ClassSummary cs = entry.getKey();
                String fileName = cs.getInverseClassName() + ".java";
                pw = new PrintWriter(new FileOutputStream(new File(directory, fileName)));
                cs.printInverseFragment(pw, entry.getValue());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (pw != null) {
                    pw.close();
                }
            }
        }
    }

    public boolean isDeleted(MethodSummary ms) {
        return dynamicPatch.getMethodUpdateType(ms.getMethodInfo()).isDeleted();
    }

    public void printInverseFragments() {
        PrintWriter pw = new PrintWriter(System.out);
        printInverseFragments(pw);
    }

    public void printInverseFragments(File directory) {
        PrintWriter pw = null;
        for (ClassSummary cs:getClassSummaries()) {
            try {
                List<InverseFragment> fragments = new ArrayList<InverseFragment>();
                for (MethodSummary ms:cs.getMethodSummaries()) {
                    if (ms.hasInverseFragment()) {
                        fragments.addAll(ms.getInverseMutator().getFragments());
                    }
                }
                if (fragments.isEmpty()) {
                    continue;
                }
                String fileName = cs.getInverseClassName() + ".java";
                pw = new PrintWriter(new FileOutputStream(new File(directory, fileName)));
                cs.printInverseFragment(pw, fragments);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (pw != null) {
                    pw.close();
                }
            }
        }
    }
}
