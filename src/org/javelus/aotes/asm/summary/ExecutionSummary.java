package org.javelus.aotes.asm.summary;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.im.InverseFragment;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.LocationPath;
import org.javelus.aotes.asm.location.MethodParameterLocation;
import org.javelus.aotes.asm.trace.Trace;
import org.javelus.aotes.asm.trace.TraceEngine;
import org.javelus.aotes.asm.trace.TraceKey;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.NewRef;
import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.value.OperatorValue;
import org.javelus.aotes.asm.value.Output;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.value.operator.MethodOperator;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.MethodInfo;




/**
 * We should know 
 * <ol>
 *   <li> resolvable, reversible
 *   <li> used input unknown heap values
 *   <li> defined output heap values
 * </ol>
 * Many of these properties are direclty copied from trace engine.
 * @author tianxiao
 *
 */
public class ExecutionSummary extends AbstractDependenceNode<ExecutionSummary> {

    /**
     * Calculated from post heap.
     */
    private Map<AccessPath, ClassInfo> allocatedAccessPaths;

    /**
     * accessed unknown pointers which not in pre-heap.
     * TODO: in this version, we only have a single unallocated access path.
     */
    private Set<AccessPath> unallocatedAccessPaths;

    /**
     * Used locations in the PrePartialHeap
     */
    private Set<AccessPath> usedUnknownAccessPaths;
    private Set<Location> usedUnknownLocations;
    /**
     * Defined locations in the PostPartialHeap
     */
    private Set<AccessPath> definedAccessPaths;

    private MethodSummary methodSummary;

    private PostPartialHeap postPartialHeap;
    private Trace trace;

    private ReversibleChecker checker;

    private InverseFragment inverseFragment;

    private String uniqueName;

    private boolean extraHeapUpdated = true;

    private TraceEngine traceEngine;


    private static AtomicInteger id = new AtomicInteger();
    
    public ExecutionSummary(MethodSummary ms, TraceEngine traceEngine){
        this.methodSummary = ms;
        this.traceEngine = traceEngine;
        MethodInfo mi = ms.getMethodInfo();
        int postfix = id.getAndIncrement();
        if (mi.isInit()) {
            uniqueName = "_init_" + postfix;
        } else if (mi.isClinit()) {
            uniqueName = "_clinit_" + postfix;
        } else {
            uniqueName = mi.getName() + postfix;
        }
    }

    public boolean requireExtraHeap() {
        return unallocatedAccessPaths != null && !unallocatedAccessPaths.isEmpty();
    }

    /**
     * Update the input skeleton.
     * @param mdg
     * @return
     */
    public boolean updateExtraHeap(MDG mdg) {
        if (unallocatedAccessPaths == null || 
                unallocatedAccessPaths.isEmpty()) {
            return false;
        }
        extraHeapUpdated = false;

        Set<AccessPath> toBeRemoved = new HashSet<AccessPath>();
        PartialHeapSkeleton skeleton = mdg.getPartialHeapSkeleton();
        for (AccessPath uap:unallocatedAccessPaths) {
            AccessPath cuap = uap.getCanonical();
            if (skeleton.contains(cuap)) {
                toBeRemoved.add(uap);
            }
        }

        if (toBeRemoved.size() == unallocatedAccessPaths.size()) {
            extraHeapUpdated = true;
            unallocatedAccessPaths.removeAll(toBeRemoved);
            return true;
        }

        unallocatedAccessPaths.removeAll(toBeRemoved);
        return false;
    }

    public boolean extraHeapUpdated() {
        return extraHeapUpdated;
    }

    public void clearExtraHeapUpdated() {
        extraHeapUpdated = false;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public boolean isReversible() {
        return checker != null && checker.isReversible();
    }

    public Map<AccessPath, ClassInfo> getAllocatedAccessPaths() {
        if (allocatedAccessPaths == null) {
            return Collections.emptyMap();
        }
        return allocatedAccessPaths;
    }

    public void addAllocatedAccessPath(AccessPath ap, ClassInfo ci) {
        if (allocatedAccessPaths == null) {
            allocatedAccessPaths = new HashMap<AccessPath, ClassInfo>();
        }
        allocatedAccessPaths.put(ap, ci);
    }

    public Set<AccessPath> getUsedUnknownAccessPaths() {
        if (usedUnknownAccessPaths == null) {
            return Collections.emptySet();
        }
        return usedUnknownAccessPaths;
    }

    public void addUsedUnknownAccessPath(AccessPath ap) {
        if (usedUnknownAccessPaths == null) {
            usedUnknownAccessPaths = new HashSet<AccessPath>();
        }
        usedUnknownAccessPaths.add(ap);
    }

    public void addUsedUnknownLocation(Location loc) {
        if (usedUnknownLocations == null) {
            usedUnknownLocations = new HashSet<Location>();
        }
        usedUnknownLocations.add(loc);
    }

    public Set<Location> getUsedUnknownLocations() {
        if (usedUnknownLocations == null) {
            return Collections.emptySet();
        }
        return usedUnknownLocations;
    }

    public Set<AccessPath> getDefinedAccessPaths() {
        if (definedAccessPaths == null) {
            return Collections.emptySet();
        }
        return definedAccessPaths;
    }

    public void addDefinedAccessPaths(AccessPath ap) {
        if (definedAccessPaths == null) {
            definedAccessPaths = new HashSet<AccessPath>();
        }
        definedAccessPaths.add(ap);
    }

    public Set<AccessPath> getUnallocatedAccessPaths() {
        if (unallocatedAccessPaths == null) {
            return Collections.emptySet();
        }
        return unallocatedAccessPaths;
    }

    public void addUnallocatedAccessPaths(AccessPath ap) {
        if (unallocatedAccessPaths == null) {
            unallocatedAccessPaths = new HashSet<AccessPath>();
        }
        unallocatedAccessPaths.add(ap);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("SimpleExecutionSummary for ");
        sb.append(methodSummary.getMethodInfo());

        sb.append("\n\nAllocated:\n");
        int count = 0;
        for(Entry<AccessPath, ClassInfo> entry : getAllocatedAccessPaths().entrySet()){
            AccessPath ap = entry.getKey();
            ClassInfo ci = entry.getValue();
            sb.append('[');
            sb.append(count++);
            sb.append("]\t");
            sb.append(ap);
            sb.append(",");
            sb.append(ci);
            sb.append('\n');
        }

        sb.append('\n');
        sb.append("Unallocated:\n");
        count = 0;
        for(AccessPath ap : getUnallocatedAccessPaths()){
            sb.append('[');
            sb.append(count++);
            sb.append("]\t");
            sb.append(ap);
            sb.append('\n');
        }

        sb.append('\n');
        sb.append("Used unknown:\n");
        count = 0;
        for(AccessPath ap : getUsedUnknownAccessPaths()){
            sb.append('[');
            sb.append(count++);
            sb.append("]\t");
            sb.append(ap);
            sb.append('\n');
        }

        sb.append('\n');
        sb.append("Defined:\n");
        count = 0;
        for(AccessPath ap : getDefinedAccessPaths()){
            sb.append('[');
            sb.append(count++);
            sb.append("]\t");
            sb.append(ap);
            sb.append('\n');
        }

        return sb.toString();
    }

    public MethodSummary getMethodSummary() {
        return methodSummary;
    }

    public MethodInfo getEntryMethod() {
        return methodSummary.getMethodInfo();
    }

    public void setTrace(Trace trace) {
        this.trace = trace;
    }

    public Trace getTrace() {
        return trace;
    }

    public Value getEntryReturnValue() {
        return traceEngine.getEntryReturnValue();
    }

    public Set<HeapLocation> getDefinedLocations() {
        return traceEngine.getDefinedHeapLocations();
    }

    public Set<HeapLocation> getUsedLocations() {
        return traceEngine.getUsedHeapLocations();
    }

    public Set<Input<?>> getUsedUnknownValues() {
        return traceEngine.getUsedUnknownValues();
    }

    public boolean isUsedUnknown(Location loc) {
        return getUsedUnknownLocations().contains(loc);
    }

    public boolean isUsed(HeapLocation loc) {
        return getUsedLocations().contains(loc);
    }

    public boolean isAccessed(Object loc) {
        return getUsedLocations().contains(loc) || getDefinedLocations().contains(loc);
    }

    public boolean isDefined(HeapLocation loc) {
        return getDefinedLocations().contains(loc);
    }

    public ReversibleChecker getChecker() {
        return checker;
    }

    public void setChecker(ReversibleChecker checker) {
        this.checker = checker;
    }

    public LocationPath getReceiverInPostObject(Value receiver) {
        // Check if it is in post heap
        for (Output<?> v: getPostHeapValues()) {
            if (v.getValue() == receiver) {
                return v.getLocationPath();
            }
        }

        return null;
    }

    public Input<MethodParameterLocation>[] getUnknownParameters() {
        return traceEngine.getUnknownParameters();
    }

    public Collection<Output<? extends HeapLocation>> getPostHeapValues() {
        return postPartialHeap.getPostHeapValues();
    }

    public boolean isMutator() {
        return  isCompleted() && !getDefinedAccessPaths().isEmpty();
    }

    public void printList() {
        System.out.println("    " + getEntryMethod().getUniqueName()
                + ", termination=" + traceEngine.getTerminationStatusLine()
                + ", reversible=" + isReversible());
        TraceKey key = getTrace().getKey();
        System.out.format("        key:(0x%08x) %s\n", key.hashCode(), key.getKeyString());
        for (AccessPath ap:getUnallocatedAccessPaths()) {
            System.out.println("        uap: " + ap);
        }
    }

    public OperatorValue getInitOfMethodOperatorValueReceiver(NewRef nor) {
        OperatorValue mov = getReceiverToOperatorValues().get(nor);
        if (mov == null) {
            return null;
        }

        while (mov.getPrevious() != null) {
            mov = mov.getPrevious();
        }

        Operator mo = mov.getOperator();

        if (mo instanceof MethodOperator) {
            MethodOperator smo = (MethodOperator) mo;
            if (smo.getMethodInfo().isInit()) {
                if (smo.getMethodInfo().getClassInfo() == nor.getClassInfo()) {
                    return mov;
                }
            }
        }

        return null;
    }

    public Map<Value, OperatorValue> getReceiverToOperatorValues() {
        return traceEngine.getReceiverToOperatorValues();
    }

    /**
     * Find a predecessor from one of resolved es of ms
     * @param that
     */
    public void addPredecessorForExecutionSummary(
            MethodSummary that) {
        boolean doAddMS = false;
        Set<AccessPath> thisUsedUnknown = getUsedUnknownAccessPaths();
        Set<AccessPath> thisDefinedAP = getDefinedAccessPaths();
        for (ExecutionSummary thatES:that.getCompletedExecutionSummaries()) {
            if (!thatES.isReversible()) {
                continue;
            }
            if (thatES == this) {
                continue;
            }
            // A execution summary may depend on itself.
            Set<AccessPath> thatDefinedAP = thatES.getDefinedAccessPaths();

            // used must depend on defined
            for (AccessPath ap:thisUsedUnknown) {
                if (ap.isRoot()) {
                    continue;
                }
                AccessPath apc = ap.getCanonical();
                AccessPath parent = ap.getParent();
                AccessPath parentc = parent.getCanonical();
                if ((!thisDefinedAP.contains(parent) && !thisDefinedAP.contains(parentc))
                        && (thatDefinedAP.contains(parent) || thatDefinedAP.contains(parentc))) {
                    if (addPredecessor(thatES)) doAddMS = true;
                }

                if ((thatDefinedAP.contains(ap) || thatDefinedAP.contains(apc))
                        || (thatDefinedAP.contains(parent) || thatDefinedAP.contains(parentc))) {
                    doAddMS = true;
                }
            }

            // defined must depend on defined
            for (AccessPath ap:thisDefinedAP) {
                if (ap.isRoot()) {
                    continue;
                }
                AccessPath apc = ap.getCanonical();
                AccessPath parent = ap.getParent();
                AccessPath parentc = parent.getCanonical();
                if ((!thisDefinedAP.contains(parent) && !thisDefinedAP.contains(parentc))
                        && (thatDefinedAP.contains(parent) || thatDefinedAP.contains(parentc))) {
                    if (addPredecessor(thatES)) doAddMS = true;
                }
                if ((thatDefinedAP.contains(ap) || thatDefinedAP.contains(apc))
                        || (thatDefinedAP.contains(parent) || thatDefinedAP.contains(parentc))) {
                    doAddMS = true;
                }
            }
        }
        if (doAddMS) {
            this.methodSummary.addPredecessor(that);
        }
    }

    @Override
    public boolean addPredecessor(ExecutionSummary es) {
        if (this.getEntryMethod().isInit() || this.getEntryMethod().isClinit()) {
            return false;
        }
        return super.addPredecessor(es) && es.addSuccessor(this);
    }

    public boolean isExceptional() {
        return traceEngine.getTerminationStatus().isException();
    }

    public boolean isCompleted() {
        return traceEngine.getTerminationStatus().isReturn();
    }

    public boolean isInterrupted() {
        return traceEngine.getTerminationStatus().isInterrupted();
    }

    public InverseFragment getInverseFragment() {
        return inverseFragment;
    }

    public void setInverseFragment(InverseFragment inverseFragment) {
        if (this.inverseFragment != null) {
            throw new RuntimeException("Sanity check failed!");
        }
        this.inverseFragment = inverseFragment;
    }

    public void resume() {
        traceEngine.resume();
    }

    public void setPostPartialHeap(PostPartialHeap postPartialHeap) {
        this.postPartialHeap = postPartialHeap;
    }

    public void clearUnallocatedAccessPath() {
        if (this.unallocatedAccessPaths != null) {
            this.unallocatedAccessPaths.clear();
        }
    }
}
