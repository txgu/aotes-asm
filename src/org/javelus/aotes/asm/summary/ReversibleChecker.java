package org.javelus.aotes.asm.summary;

import org.javelus.aotes.asm.im.InverseFragment;

public interface ReversibleChecker {
    boolean isReversible();
    InverseFragment getInverseFragment();
}
