package org.javelus.aotes.asm.summary;

import java.util.ArrayList;
import java.util.List;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.location.ClassLocation;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.LocationPath;
import org.javelus.aotes.asm.location.NamedLocation;
import org.javelus.aotes.asm.location.TargetLocation;
import org.javelus.aotes.asm.trace.TraceEngine;
import org.javelus.aotes.asm.value.ClonedRef;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.TargetRef;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ClassLoaderInfo;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.StaticElementInfo;
import org.objectweb.asm.Type;



/**
 * A pre-partial heap is created during access to an unknown pointer.  
 * @author tianxiao
 *
 */
public class PrePartialHeap extends PartialHeap {

    protected PartialHeapSkeleton skeleton;

    List<LocationPath> rootRefs = new ArrayList<LocationPath>();

    public PrePartialHeap(PartialHeapSkeleton skeleton) {
        this.skeleton = skeleton;
    }

    public TargetRef createTargetRef(TraceEngine traceEngine, ClassInfo ci) {
        TargetLocation targetLocation = new TargetLocation(ci);
        ElementInfo ei = traceEngine.allocateSkeletonObject(ci);
        TargetRef targetRef = traceEngine.createTargetRef(ei);
        LocationPath lp = new LocationPath(targetLocation);
        registerObject(lp, ei);
        addRootRef(lp);
        return targetRef;
    }


    public void registerStaticElementInfo(StaticElementInfo sei) {
        ClassLocation cl = new ClassLocation(sei.getClassInfo());
        LocationPath lp = new LocationPath(cl);
        registerObject(lp, sei);
        addRootRef(lp);
    }

    private void addRootRef(LocationPath lp) {
        this.rootRefs.add(lp);
    }

    public List<LocationPath> getRootRefs() {
        return rootRefs;
    }

    public ElementInfo allocateSkeletonObject(TraceEngine traceEngine, Input<?> unknownNullPointer) {
        Location location = unknownNullPointer.getLocation();
        LocationPath locationPath;
        if (location instanceof HeapLocation) {
            locationPath = getLocationPath((HeapLocation)location);
        } else if (location instanceof NamedLocation) {
            locationPath = getLocationPath((NamedLocation)location);
        } else {
            throw new RuntimeException("Not implemented");
        }

        AccessPath accessPath = locationPath.getAccessPath();

        ClassInfo ci = null;
        String typeDescriptor = location.getTypeDescriptor();
        Type type = Type.getType(typeDescriptor);
        if (type.getSort() == Type.OBJECT) {
            ci = ClassLoaderInfo.getResolvedClassInfoOrNull(type.getInternalName());
            if (ci != null && !ci.isFinalInCHA()) {
                ci = null; // disable this class if it is not final
            }
        } else if (type.getSort() == Type.ARRAY) {
            ci = ClassLoaderInfo.getResolvedClassInfo(typeDescriptor);
        }

        if (ci == null) {
            ci = skeleton.getBasicType(locationPath.getAccessPath());
            if (ci == null) {
                ci = skeleton.getBasicType(accessPath.getCanonical());
            }
        }

        if (ci == null) {
            return null;
        }

        ElementInfo ei = traceEngine.allocateSkeletonObject(ci); 
        unknownNullPointer.setPreallocated(ei);
        registerObject(locationPath, ei);

        return ei;
    }

    public <T extends HeapLocation> Value checkHeapLocationPlaceholder(TraceEngine traceEngine, T loc) {
        Value value = loc.value();
        if (value != Input.INPUT_PLACEHOLDER) {
            return value;
        }

        LocationPath locationPath = getLocationPath(loc);
        if (locationPath == null) {
            ElementInfo container = loc.getContainer();
            Value ref = container.getReference();
            if (!(ref instanceof ClonedRef)) {
                throw new RuntimeException("An unknown location should be in pre-heap");
            }
            ClonedRef cr = (ClonedRef) ref;
            HeapLocation sourceLoc = cr.getSourceLocation(loc);
            Value v = checkHeapLocationPlaceholder(traceEngine, sourceLoc); 
            loc.value(v);
            return v;
        }
        Input<T> input = traceEngine.createUnknownInput(locationPath, loc);
        loc.value(input);
        traceEngine.useUnknownValue(input);
        return input;
    }
}
