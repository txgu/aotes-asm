package org.javelus.aotes.asm.summary;

import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.LocationPath;
import org.javelus.aotes.asm.location.NamedLocation;
import org.javelus.aotes.asm.vm.ElementInfo;

/**
 * A partial heap can be created before and after execution.
 * An important role of partial heap is used to create query access path of an object.
 * @author tianxiao
 *
 */
public class PartialHeap {

    /**
     * Only one access path for an object is enough.
     */
    protected Map<ElementInfo, LocationPath> objectToAccessPath;

    /**
     * A precise access path points to only one object given a real heap.
     */
    protected Map<LocationPath, ElementInfo> accessPathToObject;

    protected Map<Location, LocationPath> locationToLocationPath;

    protected Map<AccessPath, LocationPath> nameToLocationPath;

    public PartialHeap() {
        objectToAccessPath = new HashMap<ElementInfo, LocationPath>();
        accessPathToObject = new HashMap<LocationPath, ElementInfo>();
        locationToLocationPath = new HashMap<Location, LocationPath>();
        nameToLocationPath = new HashMap<AccessPath, LocationPath>();
    }

    public LocationPath getLocationPath(ElementInfo container) {
        return objectToAccessPath.get(container);
    }

    public LocationPath getLocationPath(HeapLocation location) {
        LocationPath locationPath = locationToLocationPath.get(location);

        if (locationPath == null) {
            LocationPath parent = getLocationPath(location.getContainer());
            if (parent == null) {
                return null;
            }
            locationPath = parent.dot(location);
            registerLocation(locationPath, location);
        }

        return locationPath;
    }

    public LocationPath getLocationPath(NamedLocation location) {
        LocationPath locationPath = locationToLocationPath.get(location);

        if (locationPath == null) {
            locationPath = new LocationPath(location);
            registerLocation(locationPath, location);
        }

        return locationPath;
    }

    public ElementInfo getPointedObject(LocationPath locationPath) {
        return accessPathToObject.get(locationPath);
    }


    public HeapLocation getHeapLocation(LocationPath locationPath) {
        if (locationPath.getParent() == null) {
            throw new IllegalArgumentException("Only non-root access path can refer to heap locations.");
        }
        return (HeapLocation) locationPath.getLocation();
    }

    protected void registerObject(LocationPath locationPath, ElementInfo object) {
        this.accessPathToObject.put(locationPath, object);
        this.objectToAccessPath.put(object, locationPath);
        registerLocation(locationPath, locationPath.getLocation());
    }

    protected void registerLocation(LocationPath locationPath, Location location) {
        this.locationToLocationPath.put(location, locationPath);
        if (locationPath.getParent() == null) {
            this.nameToLocationPath.put(locationPath.getAccessPath(), locationPath);
        }
    }
}
