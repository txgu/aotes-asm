package org.javelus.aotes.asm.summary;

import org.javelus.aotes.asm.trace.TraceKey;
import org.javelus.aotes.asm.vm.Instruction;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LookupSwitchInsnNode;
import org.objectweb.asm.tree.TableSwitchInsnNode;

public class SymbolicExecutionTree {

    static abstract class Node {
        Instruction instruction;

        Node sibling;
        Node[] children;

        Node(Instruction insn) {
            this.instruction = insn;
            int count = getBranchCount();
            this.children = new Node[count];
        }

        public Instruction getInstruction() {
            return instruction;
        }

        /**
         * add child at the branch
         * @param branch, the parent branch
         * @param child
         */
        public void addChild(int branch, Node child) {
            Node exist = getChild(branch, child.getInstruction());
            if (exist != null) {
                return;
            }

            child.sibling = children[branchToIndex(branch)];
            children[branchToIndex(branch)] = child;
        }

        /**
         * 
         * @param branch, the parent branch
         * @param insn, the child instruction
         * @return
         */
        public Node getChild(int branch, Instruction insn) {
            Node sibling = children[branchToIndex(branch)];

            while (sibling != null && sibling.getInstruction() != insn) {
                if (sibling.getInstruction().getMethodInfo() == insn.getMethodInfo()) {
                    throw new RuntimeException("sanity check failed, " + insn.getMethodInfo() + " has two different root: "
                            + sibling.getInstruction() + " and " + insn);
                }
                sibling = sibling.sibling;
            }

            return sibling;
        }

        /**
         * get the first node
         * @param branch
         * @return
         */
        public Node getFirstChild(int branch) {
            return children[branchToIndex(branch)];
        }

        /**
         * Branch to array index
         * @param branch
         * @return
         */
        protected int branchToIndex(int branch) {
            return branch;
        }

        /**
         * total branch count
         * @return
         */
        protected abstract int getBranchCount();

        /**
         * if there is any next branch, this branch is visited.
         * @param branch
         * @return
         */
        public boolean isBranchVisited(int branch) {
            return getFirstChild(branch) != null;
        }

        /**
         * if the branch and the next instruction is insn is visited
         * @param branch
         * @param insn
         * @return
         */
        public boolean isBranchVisited(int branch, Instruction insn) {
            return getChild(branch, insn) != null;
        }

        public boolean isNodeVisited() {
            for (Node child:children) {
                // if any branch is not visited
                // return false;
                if (child == null) {
                    return false;
                }

                // if any existing child is not node-visited
                // return false;
                while (child != null) {
                    if (!child.isNodeVisited()) {
                        return false;
                    }
                    child = child.sibling;
                }
            }

            return true;
        }
    }

    static class FinishNode extends Node {

        FinishNode() {
            super(null);
        }

        @Override
        protected int getBranchCount() {
            return 0;
        }

        public final boolean isNodeVisited() {
            return true;
        }

    }

    static final Node FINISHED_NODE = new FinishNode();

    static class IfNode extends Node {

        IfNode(Instruction insn) {
            super(insn);
        }

        @Override
        protected int getBranchCount() {
            return 2;
        }
    }

    static abstract class SwitchNode extends Node {

        SwitchNode(Instruction insn) {
            super(insn);
        }

        protected int branchToIndex(int branch) {
            return (branch + children.length) % children.length;
        }
    }

    static class TableSwitchNode extends SwitchNode {

        TableSwitchNode(Instruction insn) {
            super(insn);
        }

        @Override
        protected int getBranchCount() {
            return instruction.getTableSwitchBranchCount();
        }
    }

    static class LookupSwitchNode extends SwitchNode {

        LookupSwitchNode(Instruction insn) {
            super(insn);
        }

        @Override
        protected int getBranchCount() {
            return instruction.getLookupSwitchBranchCount();
        }
    }

    public static Node createNode(Instruction instruction) {
        AbstractInsnNode node = instruction.getNode();

        if (node instanceof JumpInsnNode) {
            return new IfNode(instruction);
        }

        if (node instanceof TableSwitchInsnNode) {
            return new TableSwitchNode(instruction);
        }

        if (node instanceof LookupSwitchInsnNode) {
            return new LookupSwitchNode(instruction);
        }

        throw new RuntimeException("Unsupported instruction " + instruction);
    }

    MethodSummary methodSummary;

    Node root;

    public SymbolicExecutionTree(MethodSummary methodSummary) {
        this.methodSummary = methodSummary;
    }

    public MethodSummary getMethodInfo() {
        return methodSummary;
    }

    public Node getRoot(Instruction insn) {
        Node cur = this.root;
        while (cur != null && cur.getInstruction() != insn) {
            if (cur.getInstruction().getMethodInfo() == insn.getMethodInfo()) {
                throw new RuntimeException("sanity check failed, " + insn.getMethodInfo() + " has two different root: "
                        + cur.getInstruction() + " and " + insn);
            }
            cur = cur.sibling;
        }
        return cur;
    }

    public Node addRoot(Instruction insn) {
        Node cur = getRoot(insn);
        if (cur != null) {
            return cur;
        }

        cur = createNode(insn);
        cur.sibling = root;
        root = cur;
        return cur;
    }

    public void addTrace(TraceKey key, boolean isTerminated) {
        int count = key.getNumOfConditions();
        if (count == 0) {
            if (isTerminated) {
                if (root != null && root != FINISHED_NODE) {
                    throw new RuntimeException("Not implemented, a empty key but there is a root " + root.getInstruction());
                }
                root = FINISHED_NODE;
            }
            return;
        }
        Instruction insn = key.getInstruction(0);
        Node parent = addRoot(insn);
        int parentBranch = key.getBranch(0);
        for (int i = 1; i < count; i++) {
            insn = key.getInstruction(i);
            Node child = createNode(insn);
            parent.addChild(parentBranch, child);
            parent = child;
            parentBranch = key.getBranch(i);
        }
    }

    public Node getNode(TraceKey key, Instruction childInsn) {
        if (root == null) {
            throw new RuntimeException("Empty symbolic execution tree");
        }

        int count = key.getNumOfConditions();
        if (count == 0) {
            return null;
        }

        Instruction insn = key.getInstruction(0);
        Node parent = getRoot(insn);
        if (parent == null) {
            return null;
        }
        Node child = null;
        int parentBranch = key.getBranch(0);
        for (int i = 1; i < count; i++) {
            insn = key.getInstruction(i);
            child = parent.getChild(parentBranch, insn);
            if (child == null) {
                return null;
            }
            parentBranch = key.getBranch(i);
            parent = child;
        }
        return parent.getChild(parentBranch, childInsn);
    }

    public boolean isBranchVisisted(TraceKey key, Instruction insn, int branch) {
        if (root == null) {
            return false;
        }

        if (root == FINISHED_NODE) {
            throw new RuntimeException("Sanity check failed");
        }

        Node node = getNode(key, insn);
        if (node == null) {
            return false;
        }

        if (node.getInstruction() != insn) {
            if (node.getInstruction().getMethodInfo() != insn.getMethodInfo()) {
                throw new RuntimeException("Not implemented: unmatched method info: node is "
                        + node.getInstruction().getMethodInfo() + " and insn is " + insn.getMethodInfo());
            }
            throw new RuntimeException("Not implemented: unmatched instruction: node is "
                    + node.getInstruction() + " and insn is " + insn);
        }

        return node.isBranchVisited(branch);
    }

    public boolean isNodeVisited(TraceKey key, Instruction insn, int branch) {
        if (root == null) {
            return false;
        }

        if (root == FINISHED_NODE) {
            throw new RuntimeException("Sanity check failed");
        }

        Node node = getNode(key, insn);
        if (node == null) {
            return false;
        }

        if (node.getInstruction() != insn) {
            if (node.getInstruction().getMethodInfo() != insn.getMethodInfo()) {
                throw new RuntimeException("Not implemented: unmatched method info: node is "
                        + node.getInstruction().getMethodInfo() + " and insn is " + insn.getMethodInfo());
            }
            throw new RuntimeException("Not implemented: unmatched instruction: root is "
                    + node.getInstruction() + " and insn is " + insn);
        }
        return node.isNodeVisited();
    }

    public boolean hasMorePaths() {
        return root == null || !root.isNodeVisited();
    }
}
