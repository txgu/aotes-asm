package org.javelus.aotes.asm.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapUtils {

    public static <K, V> boolean addToMapSet(Map<K, Set<V>> mapSet, K key, V value) {
        Set<V> values = mapSet.get(key);
        if (values == null) {
            values = new HashSet<V>();
            mapSet.put(key, values);
        }

        return values.add(value);
    }

    public static <K1, K2, V> boolean addToMapMapSet(Map<K1, Map<K2, Set<V>>> mapSet, K1 key1, K2 key2, V value) {
        Map<K2, Set<V>> map = mapSet.get(key1);
        if (map == null) {
            map = new HashMap<K2, Set<V>>();
            mapSet.put(key1, map);
        }

        return addToMapSet(map, key2, value);
    }
    
    public static <K1, K2, V> boolean addToMapMap(Map<K1, Map<K2, V>> mapSet, K1 key1, K2 key2, V value) {
        Map<K2, V> map = mapSet.get(key1);
        if (map == null) {
            map = new HashMap<K2, V>();
            mapSet.put(key1, map);
        }

        return addToMap(map, key2, value);
    }
    
    public static <K1, K2, V> V getValueFromMapMap(Map<K1, Map<K2, V>> mapSet, K1 key1, K2 key2) {
        Map<K2, V> map = mapSet.get(key1);
        if (map == null) {
            return null;
        }
        return map.get(key2);
    }

    public static <K, V> boolean addAllToMapSet(Map<K, Set<V>> mapSet, K key, Collection<V> add) {
        Set<V> values = mapSet.get(key);
        if (values == null) {
            values = new HashSet<V>();
            mapSet.put(key, values);
        }

        return values.addAll(add);
    }

    public static <K, V> boolean addToMapList(Map<K, List<V>> mapSet, K key, V value) {
        List<V> values = mapSet.get(key);
        if (values == null) {
            values = new ArrayList<V>();
            mapSet.put(key, values);
        }

        return values.add(value);
    }

    public static <K, V> boolean addToMap(Map<K, V> map, K key, V value) {
        V oldValue = map.put(key, value);
        if (oldValue != null && !oldValue.equals(value)) {
            throw new RuntimeException(String.format("Duplicated map key value: key=%s, value=%s, oldValue=%s",
                    key, value, oldValue));
        }
        return true;
    }
}
