package org.javelus.aotes.asm.utils;

import org.javelus.aotes.asm.vm.ClassInfo;
import org.objectweb.asm.Type;

public class JVMUtils {

    public static boolean isAtomic(ClassInfo ci) {
        return ci.getName().equals("java.lang.String");
    }


    public static boolean isImmutable(String className) {
        return className.equals("java.lang.String")
                || className.equals("java.lang.Class")
                || className.equals("java.lang.Integer")
                || className.equals("java.lang.Byte")
                || className.equals("java.lang.Character")
                || className.equals("java.lang.Boolean")
                || className.equals("java.lang.Short")
                || className.equals("java.lang.Float")
                || className.equals("java.lang.Long")
                || className.equals("java.lang.Double")
                || className.equals("java.math.BigInteger");
    }

    public static boolean isPrimitive(String internalName) {
        if (internalName.length() != 1) {
            return false;
        }

        char c = internalName.charAt(0);

        if (c == 'V'
                || c == 'Z'
                || c == 'B'
                || c == 'S'
                || c == 'C'
                || c == 'I'
                || c == 'F'
                || c == 'D'
                || c == 'J') {
            return true;
        }

        return false;
    }

    public static boolean isPrimitiveArray(String descriptor) {
        Type type = Type.getType(descriptor);
        if (type.getSort() == Type.ARRAY) {
            int elementSort = type.getElementType().getSort();
            switch (elementSort) {
            case Type.BOOLEAN:
            case Type.BYTE:
            case Type.CHAR:
            case Type.SHORT:
            case Type.INT:
            case Type.FLOAT:
            case Type.LONG:
            case Type.DOUBLE:
                return true;
            }
            return false;
        }
        return false;
    }

    public static boolean isArray(String internalName) {
        return internalName.charAt(0) == '[';
    }
}
