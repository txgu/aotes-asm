package org.javelus.aotes.asm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.javelus.aotes.asm.summary.MDG;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ClassLoaderInfo;

public class Main {

    static String classPath;
    static String mainClass;
    static String classesList;

    static String dynamicPatchPath;
    static String outputDirectory = ".";

    public static void main(String[] args) throws IOException {
        processCommandline(args);

        ClassLoaderInfo.initializeClassLoaders(classPath);
        if (classPath != null) {
            CallGraph.initialize(classPath);
        }

        DynamicPatch dp = null;
        if (dynamicPatchPath != null) {
            dp = DynamicPatch.createFromXMLDynamicPatch(dynamicPatchPath, true);
        } else if (mainClass != null) {
            dp = DynamicPatch.createFromClassInfo(Collections.singleton(ClassLoaderInfo.getResolvedClassInfo(mainClass.replace('.', '/'))));
        } else {
            dp = DynamicPatch.createFromClassInfo(readClassInfos(classesList));
        }

        MDG mdg = new MDG(dp);
        {
            long beginTime = System.nanoTime();
            try {
                mdg.process();
            } finally {
                long endTime = System.nanoTime();
                System.out.println("Total symbolic execution time is " + TimeUnit.NANOSECONDS.toMillis(endTime-beginTime) + "ms");
            }
        }

        {   
            long beginTime = System.nanoTime();
            try {
                mdg.checkReversibility();
            } finally {
                long endTime = System.nanoTime();
                System.out.println("Total resolution time is " + TimeUnit.NANOSECONDS.toMillis(endTime-beginTime) + "ms");
            }
        }

        mdg.updateExecutionSummaryGraph();

        if (Options.oopsla()) {
            File directory = new File(outputDirectory);
            mdg.printStatistics("only field-added classes", true);
            mdg.pruneAndPrintInverseFragments("only field-added classes (with an inverse method)", directory);
            if (Options.pruneNoAddedFieldClasses()) {
                directory = new File(directory.getParentFile(), "noprune");
                deleteDirectory(directory);
                directory.mkdir();
                mdg.setPruneNoAddedField(false);
                mdg.printStatistics("all changed", false);
                mdg.pruneAndPrintInverseFragments("all changed (with an inverse method)", directory);
            }
        } else if (Options.pruneNoAddedFieldClasses()
                || Options.pruneStaticMethods()
                || Options.pruneUnmatchedMethods()
                || Options.pruneUnreachableMethods()) {
            File directory = new File(outputDirectory);
            mdg.pruneAndPrintInverseFragments("pruned", directory);
            directory = new File(directory.getParentFile(), "noprune");
            deleteDirectory(directory);
            directory.mkdir();
            mdg.printInverseFragments(directory);
        } else {
            File directory = new File(outputDirectory);
            mdg.printInverseFragments(directory);
        }


        if (Options.verbose()) {
            mdg.printList();
            CallGraph.printUnsatisfiedNativeMethods();
        }
    }

    private static Collection<ClassInfo> readClassInfos(String classesList) {
        BufferedReader reader = null;
        List<ClassInfo> classes = new ArrayList<ClassInfo>();
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(classesList)));
            String line = null;
            while ((line = reader.readLine()) != null) {
                classes.add(ClassLoaderInfo.getResolvedClassInfo(line.replace('.', '/')));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return classes;
    }

    public static boolean deleteDirectory(File directory) {
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (null != files) {
                for( int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    } else {
                        files[i].delete();
                    }
                }
            }
        }
        return (directory.delete());
    }

    private static void processCommandline(String[] args) throws IOException {
        int index = 0;
        while (index < args.length) {
            String arg = args[index++];
            if (arg.equals("-cp")) {
                classPath = args[index++];
            } else if (arg.equals("-dp")) {
                dynamicPatchPath = args[index++];
            } else if (arg.equals("-main")) {
                mainClass = args[index++];
                Options.singleClassMode();
            } else if (arg.equals("-o")) {
                outputDirectory = args[index++];
            } else if (arg.equals("-v")) {
                Options.setVerbose(Integer.valueOf(args[index++]));
            } else if (arg.equals("-classes")) {
                classesList = args[index++];
            }
        }


        if (dynamicPatchPath == null && mainClass == null) {
            System.err.println("Please specify a dynamic patch (via -dp) or a main class (via -main)");
            System.exit(1);
        }

        if (classPath == null) {
            System.out.println("WARNING: using bootstrap class path");
        }
    }
}
