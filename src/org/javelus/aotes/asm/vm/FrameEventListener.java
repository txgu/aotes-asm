package org.javelus.aotes.asm.vm;

public interface FrameEventListener<E> {
    /**
     * After the frame has been pushed
     * @param ti
     */
    void framePush(ThreadInfo<E> ti);

    /**
     * Before the frame has been popped
     * @param ti
     */
    void framePop(ThreadInfo<E> ti);
}
