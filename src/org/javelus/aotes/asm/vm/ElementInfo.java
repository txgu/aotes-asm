package org.javelus.aotes.asm.vm;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.ArrayLength;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.InstanceField;
import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.StaticField;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.Value;

public abstract class ElementInfo implements Cloneable {

    enum AllocType {
        NEW,
        PRE,;

        public boolean isNew() {
            return this == NEW;
        }

        public boolean isPre() {
            return this == PRE;
        }
    }


    private Value reference;

    private AllocType allocationType = AllocType.NEW;

    private ClassInfo classInfo;

    private Map<Object, HeapLocation> heapLocations = new HashMap<Object, HeapLocation>();


    public ElementInfo(ClassInfo classInfo) {
        this.classInfo = classInfo;
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }

    public ElementInfo clone() {
        try {
            ElementInfo c = (ElementInfo) super.clone();
            c.reference = null;
            c.heapLocations = new HashMap<Object, HeapLocation>();
            return c;
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    protected HeapLocation findHeapLocation(Object accessor) {
        return heapLocations.get(accessor);
    }

    protected void addHeapLocation(Object accessor, HeapLocation location) {
        heapLocations.put(accessor, location);
        newValue(location);
    }

    public StaticField staticField(FieldInfo field) {
        throw new RuntimeException();
    }

    public InstanceField instanceField(FieldInfo field) {
        throw new RuntimeException();
    }

    public ArrayLength arrayLength() {
        throw new RuntimeException();
    }

    public ArrayElement arrayElement(Value index) {
        throw new RuntimeException();
    }

    public boolean isArray() {
        return false;
    }

    public boolean isStatic() {
        return false;
    }

    public boolean isInstance() {
        return false;
    }

    public Collection<HeapLocation> locations() {
        return heapLocations.values();
    }

    public AllocType getAllocationType() {
        return allocationType;
    }

    public void setAllocPre() {
        this.allocationType = AllocType.PRE;
    }

    public void setAllocNew() {
        this.allocationType = AllocType.NEW;
    }

    protected void newValue(Location loc) {
        if (getAllocationType().isNew()) {
            loc.value(Value.getDefaultValue(loc.getTypeDescriptor()));
        } else {
            // As we have no idea about the access path, we use a placeholder here.
            loc.value(Input.INPUT_PLACEHOLDER);
        }
    }

    public Value getReference() {
        return reference;
    }

    public void setReference(Value reference) {
        this.reference = reference;
    }

    public String toString() {
        return "(" + classInfo.getSimpleName() + ")";
    }
}
