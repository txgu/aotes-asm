package org.javelus.aotes.asm.vm;

import java.util.ArrayList;
import java.util.List;

public class ThreadInfo<E> {

    StackFrame<E> top;

    private E pendingException;

    private List<FrameEventListener<E>> frameListeners = new ArrayList<FrameEventListener<E>>();

    public E getPendingException() {
        return pendingException;
    }

    public StackFrame<E> getTopFrame() {
        return top;
    }

    public void addFrameListener(FrameEventListener<E> listener) {
        frameListeners.add(listener);
    }

    public void removeFrameListener(FrameEventListener<E> listener) {
        frameListeners.remove(listener);
    }

    public void setPendingException(E pendingException) {
        this.pendingException = pendingException;
    }

    public StackFrame<E> popFrame() {
        notifyFramePop();
        StackFrame<E> callee = top;
        top = top.getPrevious();
        callee.setPrevious(null);
        return top;
    }

    public void pushFrame(StackFrame<E> frame) {
        frame.setPrevious(top);
        top = frame;
        notifyFramePush();
    }

    public void handleException(StackFrame<E> frame, ClassInfo ci) {
        while(frame != null) {
            MethodInfo mi = frame.getMethodInfo();
            Instruction handler = null;
            try {
                handler = mi.exceptionHandler(frame, ci);
            } catch (RuntimeException e) {
                while (frame != null) {
                    frame = popFrame();
                }
                return;
            }
            if (handler != null) {
                frame.clearExpressionStack();
                frame.push(pendingException); 
                frame.setPC(handler);
                clearPendingException();
                return;
            }
            frame = popFrame();
        }
    }



    /**
     * After the frame has been pushed
     * @param ti
     */
    void notifyFramePush() {
        for (FrameEventListener<E> l:frameListeners) {
            l.framePush(this);
        }

    }

    /**
     * Before the frame has been popped
     * @param ti
     */
    void notifyFramePop() {
        for (FrameEventListener<E> l:frameListeners) {
            l.framePop(this);
        }
    }

    public void clearPendingException() {
        this.pendingException = null;
    }

    public void printStackTrace() {
        if (top == null) {
            System.out.println("Empty stack");
        }
        for (StackFrame<E> frame = top; frame != null; frame = frame.getPrevious()) {
            System.out.format("    %s.%s @ %s\n", frame.getMethodInfo().getClassName(), frame.getMethodInfo().getFullName(), frame.getPC());
        }
    }

}
