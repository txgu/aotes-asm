package org.javelus.aotes.asm.vm;

import static org.objectweb.asm.Opcodes.*;

import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.InstanceField;
import org.javelus.aotes.asm.location.StaticField;
import org.javelus.aotes.asm.trace.TraceEngine;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.OperatorValue;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.value.constant.AddressValue;
import org.javelus.aotes.asm.value.constant.ClassInfoValue;
import org.javelus.aotes.asm.value.constant.DoubleValue;
import org.javelus.aotes.asm.value.constant.FloatValue;
import org.javelus.aotes.asm.value.constant.IntegerValue;
import org.javelus.aotes.asm.value.constant.LongValue;
import org.javelus.aotes.asm.value.constant.StringValue;
import org.javelus.aotes.asm.value.operator.MethodOperatorFactory;
import org.objectweb.asm.Type;


public class Interpreter {

    private static final boolean debug = false;
    private static final long DEFAULT_MAX_STEP = 100000L;

    ThreadInfo<Value> threadInfo;
    TraceEngine traceEngine;

    public Interpreter(TraceEngine traceEngine) {
        this.threadInfo = new ThreadInfo<Value>();
        this.traceEngine = traceEngine;
        if (traceEngine != null) {
            traceEngine.attachThreadInfo(this.threadInfo);
        }
    }



    protected boolean stepGuard(long step) {
        if (step > DEFAULT_MAX_STEP) {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unused")
    public void interpret() {
        StackFrame<Value> frame = null;
        long count = 0;
        while ((frame = threadInfo.getTopFrame())!= null) {
            count ++ ;
            if (stepGuard(count)) {
                System.out.println("We have executed too many instructions " + count + ", and we have to interrupt the execution");
                exit(frame);
                return;
            }
            Instruction insn = frame.getPC();
            int opcode = insn.getOpcode();
            if (opcode > 0 && debug) System.out.println("Executing " + insn + " in " + frame);
            switch(opcode) {
            case -1: // LableNode, Frame
            case NOP:
                frame.advance();
                break;
            case ACONST_NULL:
                frame.push(Value.NULL);
                frame.advance();
                break;
            case ICONST_M1:
                frame.push(IntegerValue.m1);
                frame.advance();
                break;
            case ICONST_0:
                frame.push(IntegerValue.zero);
                frame.advance();
                break;
            case ICONST_1:
                frame.push(IntegerValue.one);
                frame.advance();
                break;
            case ICONST_2:
                frame.push(IntegerValue.two);
                frame.advance();
                break;
            case ICONST_3:
                frame.push(IntegerValue.three);
                frame.advance();
                break;
            case ICONST_4:
                frame.push(IntegerValue.four);
                frame.advance();
                break;
            case ICONST_5:
                frame.push(IntegerValue.five);
                frame.advance();
                break;
            case LCONST_0:
                frame.pushLong(LongValue.zero);
                frame.advance();
                break;
            case LCONST_1:
                frame.pushLong(LongValue.one);
                frame.advance();
                break;
            case FCONST_0:
                frame.push(FloatValue.zero);
                frame.advance();
                break;
            case FCONST_1:
                frame.push(FloatValue.one);
                frame.advance();
                break;
            case FCONST_2:
                frame.push(FloatValue.two);
                frame.advance();
                break;
            case DCONST_0:
                frame.pushLong(DoubleValue.zero);
                frame.advance();
                break;
            case DCONST_1:
                frame.pushLong(DoubleValue.one);
                frame.advance();
                break;
            case IALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case LALOAD:
                arrayLoad(frame, true);
                frame.advance();
                break;
            case FALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case DALOAD:
                arrayLoad(frame, true);
                frame.advance();
                break;
            case AALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case BALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case CALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case SALOAD:
                arrayLoad(frame, false);
                frame.advance();
                break;
            case IASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case LASTORE:
                arrayStore(frame, true);
                frame.advance();
                break;
            case FASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case DASTORE:
                arrayStore(frame, true);
                frame.advance();
                break;
            case AASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case BASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case CASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case SASTORE:
                arrayStore(frame, false);
                frame.advance();
                break;
            case POP:
                frame.pop();
                frame.advance();
                break;
            case POP2:
                frame.popLong();
                frame.advance();
                break;
            case DUP:
                frame.dup();
                frame.advance();
                break;
            case DUP_X1:
                frame.dup_x1();
                frame.advance();
                break;
            case DUP_X2:
                frame.dup_x2();
                frame.advance();
                break;
            case DUP2:
                frame.dup2();
                frame.advance();
                break;
            case DUP2_X1:
                frame.dup2_x1();
                frame.advance();
                break;
            case DUP2_X2:
                frame.dup2_x2();
                frame.advance();
                break;
            case SWAP:
                frame.swap();
                frame.advance();
                break;
            case IADD:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LADD:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FADD:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DADD:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case ISUB:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSUB:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FSUB:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DSUB:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IMUL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LMUL:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FMUL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DMUL:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IDIV:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LDIV:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FDIV:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DDIV:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IREM:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LREM:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FREM:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DREM:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case INEG:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LNEG:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case FNEG:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DNEG:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case ISHL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSHL:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case ISHR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LSHR:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case IUSHR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LUSHR:
                binaryOperator(frame, insn, true, false, true);
                frame.advance();
                break;
            case IAND:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LAND:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IOR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LOR:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case IXOR:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LXOR:
                binaryOperator(frame, insn, true);
                frame.advance();
                break;
            case I2L:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case I2F:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2D:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case L2I:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case L2F:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case L2D:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case F2I:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case F2L:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case F2D:
                unaryOperator(frame, insn, false, true);
                frame.advance();
                break;
            case D2I:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case D2L:
                unaryOperator(frame, insn, true);
                frame.advance();
                break;
            case D2F:
                unaryOperator(frame, insn, true, false);
                frame.advance();
                break;
            case I2B:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2C:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case I2S:
                unaryOperator(frame, insn, false);
                frame.advance();
                break;
            case LCMP:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case FCMPL:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case FCMPG:
                binaryOperator(frame, insn, false);
                frame.advance();
                break;
            case DCMPL:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case DCMPG:
                binaryOperator(frame, insn, true, true, false);
                frame.advance();
                break;
            case IRETURN:
                _return(frame, 1);
                break;
            case LRETURN:
                _return(frame, 2);
                break;
            case FRETURN:
                _return(frame, 1);
                break;
            case DRETURN:
                _return(frame, 2);
                break;
            case ARETURN:
                _return(frame, 1);
                break;
            case RETURN:
                _return(frame, 0);
                break;
            case ARRAYLENGTH:
                arrayLength(frame, insn);
                frame.advance();
                break;
            case ATHROW:
                _throw(frame);
                break;
            case MONITORENTER:
                frame.pop();
                frame.advance();
                break;
            case MONITOREXIT:
                frame.pop();
                frame.advance();
                break;
            case ILOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case LLOAD:
                frame.pushLong(frame.getLongLocal(insn.getVar()));
                frame.advance();
                break;
            case FLOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case DLOAD:
                frame.pushLong(frame.getLongLocal(insn.getVar()));
                frame.advance();
                break;
            case ALOAD:
                frame.push(frame.getLocal(insn.getVar()));
                frame.advance();
                break;
            case ISTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case LSTORE:
                frame.setLongLocal(insn.getVar(), frame.popLong());
                frame.advance();
                break;
            case FSTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case DSTORE:
                frame.setLongLocal(insn.getVar(), frame.popLong());
                frame.advance();
                break;
            case ASTORE:
                frame.setLocal(insn.getVar(), frame.pop());
                frame.advance();
                break;
            case IINC:
                iinc(frame, insn);
                frame.advance();
                break;
            case IFEQ:
            case IFNE:
            case IFLT:
            case IFGE:
            case IFGT:
            case IFLE:
                _if(frame, insn, IntegerValue.zero);
                break;
            case IF_ICMPEQ:
            case IF_ICMPNE:
            case IF_ICMPLT:
            case IF_ICMPGE:
            case IF_ICMPGT:
            case IF_ICMPLE:
            case IF_ACMPEQ:
            case IF_ACMPNE:
                _if(frame, insn, frame.pop());
                break;
            case IFNULL:
            case IFNONNULL:
                _if(frame, insn, Value.NULL);
                break;
            case GOTO:
                frame.setPC(insn.getTarget());
                break;
            case BIPUSH:
            case SIPUSH:
                frame.push(IntegerValue.valueOf(insn.getInt()));
                frame.advance();
                break;
            case NEWARRAY:
                frame.push(newarray(frame, insn));
                frame.advance();
                break;
            case NEW:
                frame.push(_new(frame, insn));
                frame.advance();
                break;
            case ANEWARRAY:
                frame.push(anewarray(frame, insn));
                frame.advance();
                break;
            case CHECKCAST:
                checkcast(frame, insn);
                frame.advance();
                break;
            case INSTANCEOF:
                _instanceof(frame, insn);
                frame.advance();
                break;
            case GETSTATIC:
                getfield_or_static(frame, insn, true);
                frame.advance();
                break;
            case PUTSTATIC:
                putfield_or_static(frame, insn, true);
                frame.advance();
                break;
            case GETFIELD:
                getfield_or_static(frame, insn, false);
                frame.advance();
                break;
            case PUTFIELD:
                putfield_or_static(frame, insn, false);
                frame.advance();
                break;
            case INVOKEVIRTUAL:
                invoke(frame, insn, true, false);
                break;
            case INVOKESPECIAL:
                invoke(frame, insn, false, false);
                break;
            case INVOKESTATIC:
                invoke(frame, insn, false, true);
                break;
            case INVOKEINTERFACE:
                invoke(frame, insn, true, false);
                break;
            case INVOKEDYNAMIC:{
                System.out.println("invokedynamic is not supported now.");
                exit(frame);
                return;}
            case LDC:
                ldc(frame, insn);
                frame.advance();
                break;
            case LOOKUPSWITCH:
                lookupswitch(frame, insn);
                break;
            case TABLESWITCH:
                tableswitch(frame, insn);
                break;
            case MULTIANEWARRAY:
                multianewarray(frame, insn);
                frame.advance();
                break;
            case RET:
                AddressValue saved = (AddressValue) frame.getLocal(insn.getVar());
                frame.setPC((Instruction)saved.getAddress());
                break;
            case JSR:
                AddressValue save = new AddressValue(frame.getPC());
                frame.push(save);
                frame.setPC(insn.getTarget());
                break;
            default:
                throw new RuntimeException("Should not reach here: " + opcode);
            }
        }
    }

    private void exitWithException(StackFrame<Value> frame, String className) {
        ClassInfo ci = ClassLoaderInfo.getResolvedClassInfo(className);
        ElementInfo exception = newException(ci);
        if (debug) System.out.println("Throw an exception " + className);
        threadInfo.setPendingException(exception.getReference());
        if (frame != threadInfo.getTopFrame()) {
            throw new RuntimeException("sanity check failed");
        }
        while (frame != null) {
            frame = threadInfo.popFrame();
        }
    }

    protected void lookupswitch(StackFrame<Value> frame, Instruction insn) {
        Value key = frame.pop();
        int branch = traceEngine.evaluateLookupSwitch(insn);
        Instruction target;
        if (branch == -1) {
            target = insn.getLookupSwitchDefault();
        } else {
            target = insn.getLookupSwitchBranch(branch);
        }

        traceEngine.traceLookupSwitch(insn, key, insn.getLookupSwitchKeys(), branch);

        frame.setPC(target);
    }


    protected void tableswitch(StackFrame<Value> frame, Instruction insn) {
        Value index = frame.pop();
        int branch = traceEngine.evaluateTableSwitch(insn);
        Instruction target;
        if (branch == -1) {
            target = insn.getTableSwitchDefault();
        } else {
            target = insn.getTableSwitchBranch(branch);
        }

        traceEngine.traceTableSwitch(insn, index, insn.getTableSwitchMin(), insn.getTableSwitchMax(), branch);

        frame.setPC(target);
    }


    protected void multianewarray(StackFrame<Value> frame, Instruction insn) {
        int dims = insn.getMultiDims();
        Value[] arrayLengths = new Value[dims];
        for (int i = dims - 1; i >= 0; i--) {
            arrayLengths[i] = frame.pop();
        }
        ClassInfo target = frame.resolveClassInfoOrNull(insn.getMultiArrayDesc());
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return;
        }
        Value array = traceEngine.newMultiArray(insn, target, arrayLengths, 0);
        frame.push(array);
    }


    protected void ldc(StackFrame<Value> frame, Instruction insn) {
        Object cst = insn.getConstant();

        if (cst instanceof Integer) {
            frame.push(IntegerValue.valueOf((Integer)cst));
        } else if (cst instanceof Float) {
            frame.push(FloatValue.valueOf((Float)cst));
        } else if (cst instanceof Double) {
            frame.pushLong(DoubleValue.valueOf((Double)cst));
        } else if (cst instanceof Long) {
            frame.pushLong(LongValue.valueOf((Long)cst));
        } else if (cst instanceof String) {
            frame.push(StringValue.valueOf((String)cst));
        } else if (cst instanceof Type) {
            Type type = (Type) cst;
            ClassInfo target = frame.resolveClassInfoOrNull(type.getInternalName());
            if (target == null) {
                createAndThrowClassNotFoundException(frame);
                return;
            }
            frame.push(ClassInfoValue.valueOf(target));
        } else {
            throw new RuntimeException("Should not reach here");
        }
    }

    /**
     * The receiver can only be java.lang.String or java.lang.Class
     * @param frame
     * @param callee
     * @return
     */
    protected boolean checkAndHandleMethodOperatorForNullReceiver(StackFrame<Value> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isNullReceiverMethodOperator(callee)) {
            createAndPushMethodOperatorValue(frame, callee);
            return true;
        }

        return false;
    }

    protected boolean checkAndHandleMethodOperatorForLogger(StackFrame<Value> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isLoggerMethodOperator(callee)) {
            createAndPushMethodOperatorValue(frame, callee);
            return true;
        }

        return false;
    }

    protected void createAndPushMethodOperatorValue(StackFrame<Value> frame, MethodInfo callee) {
        int returnSize = callee.getReturnSize();
        Value[] args = traceEngine.popMethodArgs(callee, frame);
        if (returnSize == 1) {
            frame.push(traceEngine.createMethodOperatorValue(callee, args));
        } else if (returnSize == 2) {
            frame.pushLong(traceEngine.createMethodOperatorValue(callee, args));
        }
        frame.advance();
    }

    protected boolean checkAndHandleMethodOperatorForHashCodeAndEquals(StackFrame<Value> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isHashCodeOrEqualsMethodOperator(callee)) {
            createAndPushMethodOperatorValue(frame, callee);
            return true;
        }

        return false;
    }


    protected boolean checkAndHandleMethodOperatorForNative(StackFrame<Value> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isNativeMethodOperator(callee)) {
            createAndPushMethodOperatorValue(frame, callee);
            return true;
        }

        return false;
    }


    protected void invoke(StackFrame<Value> frame, Instruction insn, boolean isDynamic, boolean isStatic) {
        Value recv = null;
        String owner = insn.getMethodOwner();
        String name = insn.getMethodName();
        String desc = insn.getMethodDesc();
        MethodInfo staticCallee = frame.resolveMethod(owner, name, desc);
        if (staticCallee == null) {
            // infeasible path of instanceof
            System.err.println("Cannot find method " + owner + "." + name + desc);
            frame.resolveMethod(owner, name, desc);
            createAndThrowException(frame, "java/lang/NoSuchMethodError");
            return;
        }
        if (checkAndHandleMethodOperatorForLogger(frame, staticCallee)) {
            return;
        }
        if (checkAndHandleMethodOperatorForHashCodeAndEquals(frame, staticCallee)) {
            return;
        }
        //        if (checkAndHandleMethodOperatorForStringBuilder(frame, staticCallee)) {
        //            return;
        //        }
        //        if (checkAndHandleMethodOperatorForSecurity(frame, staticCallee)) {
        //            return;
        //        }
        MethodInfo callee = null;
        int argumentSize = staticCallee.getArgumentsSize();
        ElementInfo ei = null;
        if (!isStatic) {
            recv = frame.peek(argumentSize-1);
            if (recv == Value.NULL) {
                createAndThrowNullPointerException(frame);
                return;
            }
            ei = getAndTryPreallocate(recv);
            if (ei == null) {
                if (checkAndHandleMethodOperatorForNullReceiver(frame, staticCallee)) {
                    return;
                }
                createAndThrowNullPointerException(frame);
                return;
            }
        }

        if (isDynamic) {
            callee = frame.resolveMethodVirtual(ei, name, desc);
            if (callee == null) {
                // infeasible path of instanceof
                createAndThrowException(frame, "java/lang/IncompatibleClassChangeError");
                return;
            }
        } else {
            callee = staticCallee;
        }

        if (callee.isAbstract()) {
            createAndThrowException(frame, "java/lang/AbstractMethodError");
            return;
        }

        if (callee.isNative()) {
            if (checkAndCloneObject(frame, callee)) {
                return;
            }
            if (checkAndHandleMethodOperatorForNative(frame, callee)) {
                return;
            }
            traceEngine.registerUnsatisfiedMethod(callee);
            createAndThrowException(frame, "java/lang/UnsatisfiedLinkError");
            return;
        }

        // Do not execute method of String
        if (checkAndHandleMethodOperatorForNullReceiver(frame, callee)) {
            return;
        }

        Value[] args = traceEngine.peekMethodArgs(callee, frame);
        traceEngine.traceInvoke(frame.getMethodInfo(), insn, callee, args);
        traceEngine.traceMethodEntry(insn, callee);
        StackFrame<Value> calleeFrame = new ValueStackFrame(callee);
        threadInfo.pushFrame(calleeFrame);
        calleeFrame.setupArguments(argumentSize);
    }

    protected boolean checkAndHandleMethodOperatorForSecurity(StackFrame<Value> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isSecurityMethodOperator(callee)) {
            createAndPushMethodOperatorValue(frame, callee);
            return true;
        }
        return false;
    }

    protected boolean checkAndHandleMethodOperatorForStringBuilder(StackFrame<Value> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isStringBuilderMethodOperator(callee)) {
            createAndPushMethodOperatorValue(frame, callee);
            return true;
        }
        return false;
    }

    protected boolean checkAndHandleMethodOperatorForImmutable(StackFrame<Value> frame, MethodInfo callee) {
        if (MethodOperatorFactory.isImmutableMethodOperator(callee)) {
            createAndPushMethodOperatorValue(frame, callee);
            return true;
        }
        return false;
    }

    protected boolean checkAndCloneObject(StackFrame<Value> frame, MethodInfo callee) {
        ClassInfo ci = callee.getClassInfo();
        if (!ci.getName().equals("java.lang.Object")
                || !callee.getName().equals("clone")) {
            return false;
        }
        Value recv = frame.pop();
        Value cloned = traceEngine.cloneObject(recv);
        frame.push(cloned);
        frame.advance();
        return true;
    }

    protected void getfield_or_static(StackFrame<Value> frame, Instruction insn, boolean isStatic) {
        FieldInfo fi = frame.resolveField(insn.getFieldOwner(), insn.getFieldName(), insn.getFieldDesc());
        if (fi == null) {
            fi = frame.resolveField(insn.getFieldOwner(), insn.getFieldName(), insn.getFieldDesc());
            createAndThrowException(frame, "java/lang/NoSuchFieldError");
            return;
        }
        ElementInfo ei;
        Value ret;
        if (isStatic) {
            ei = traceEngine.getStaticElementInfo(fi.getClassInfo());
            StaticField sf = ei.staticField(fi);
            ret = traceEngine.checkHeapLocationPlaceholder(sf);
            traceEngine.traceUseStaticField(insn, sf);
        } else {
            Value recv = frame.peek();
            if (recv == Value.NULL) {
                createAndThrowNullPointerException(frame);
                return;
            }
            ei = getAndTryPreallocate(recv);
            recv = frame.pop();
            if (ei != null) {
                InstanceField inf = ei.instanceField(fi);
                ret = traceEngine.checkHeapLocationPlaceholder(inf);
                traceEngine.traceUseInstanceField(insn, inf);
            } else {
                ret = traceEngine.createGetFieldOperator(fi, recv);
            }
        }

        if (fi.getSize() == 1) {
            frame.push(ret);
        } else {
            frame.pushLong(ret);
        }
    }

    protected void putfield_or_static(StackFrame<Value> frame, Instruction insn, boolean isStatic) {
        FieldInfo fi = frame.resolveField(insn.getFieldOwner(), insn.getFieldName(), insn.getFieldDesc());
        if (fi == null) {
            createAndThrowException(frame, "java/lang/NoSuchFieldError");
            return;
        }
        Value recv = null;
        ElementInfo ei = null;
        if (!isStatic) {
            recv = frame.peek(fi.getSize());
            if (recv == Value.NULL) {
                createAndThrowNullPointerException(frame);
                return;
            }
            ei = getAndTryPreallocate(recv);
        }

        Value value;
        if (fi.getSize() == 1) {
            value = frame.pop();
        } else {
            value = frame.popLong();
        }

        if (isStatic) {
            ei = traceEngine.getStaticElementInfo(fi.getClassInfo());
            StaticField sf = ei.staticField(fi);
            sf.value(value);
            traceEngine.traceDefineStaticField(insn, sf);
        } else {
            frame.pop(); // recv;
            if (ei != null) {
                InstanceField inf = ei.instanceField(fi);
                inf.value(value);
                traceEngine.traceDefineInstanceField(insn, inf);
            } else {
                traceEngine.createPutFieldOperator(fi, recv, value);
            }
        }
    }

    protected void checkcast(StackFrame<Value> frame, Instruction insn) {
        Value obj = frame.peek();
        if (obj == Value.NULL) {
            return;
        }
        ElementInfo ei = getAndTryPreallocate(obj);

        if (ei != null) {
            ClassInfo eici = ei.getClassInfo();
            ClassInfo target = frame.resolveClassInfoOrNull(insn.getDesc());
            if (target == null) {
                createAndThrowClassNotFoundException(frame);
                return;
            }
            if (!eici.isInstanceOf(target)) {
                if (debug) System.out.println("WARNING: " + eici  + " is not a subtype of " + target + ", thrown a CCE");
                createAndThrowException(frame, "java/lang/ClassCastException");
                return;
            }
        } else {
            // 
        }

    }

    protected void _instanceof(StackFrame<Value> frame, Instruction insn) {
        Value object = frame.peek();
        ClassInfo target = frame.resolveClassInfoOrNull(insn.getDesc());
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return;
        }
        Value className = StringValue.valueOf(target.getName());
        //Value result = traceEngine.createAndTraceBinopExpr(insn, object, className);
        if (object == Value.NULL) {
            frame.pop();
            frame.push(IntegerValue.zero);
            //frame.push(result);
            return;
        }

        ElementInfo ei = getAndTryPreallocate(object);
        frame.pop();
        if (ei != null) {
            ClassInfo eici = ei.getClassInfo();
            if (eici.isInstanceOf(target)) {
                frame.push(IntegerValue.one);
                //frame.push(result);
            } else {
                frame.push(IntegerValue.zero);
                //frame.push(result);
            }
        } else {
            frame.push(traceEngine.createInstanceOfOperator(target, object, className));
        }
    }


    protected void createAndThrowClassNotFoundException(StackFrame<Value> frame) {
        createAndThrowException(frame, "java/lang/ClassNotFoundException");
    }

    /**
     * 
     * @param className internal name
     * @return
     */
    protected Value _new(StackFrame<Value> frame, Instruction insn) {
        ClassInfo target = frame.resolveClassInfoOrNull(insn.getDesc());
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return null;
        }
        return traceEngine.newObject(insn, target);
    }

    protected Value anewarray(StackFrame<Value> frame, Instruction insn) {
        return newarrayCommon(frame, insn, arrayClassNameForClass(insn.getDesc()), frame.pop());
    }

    protected Value newarray(StackFrame<Value> frame, Instruction insn) {
        return newarrayCommon(frame, insn, arrayClassNameForPrimitiveType(insn.getInt()), frame.pop());
    }

    protected Value newarrayCommon(StackFrame<Value> frame, Instruction insn, String arrayClassName, Value length) {
        ClassInfo target = frame.resolveClassInfoOrNull(arrayClassName);
        if (target == null) {
            createAndThrowClassNotFoundException(frame);
            return null;
        }
        return traceEngine.newArray(insn, target, length);
    }

    static String arrayClassNameForClass(String desc) {
        if (desc.charAt(0) == '[') {
            return "[" + desc;
        }
        return "[L" + desc + ";";
    }

    static String arrayClassNameForPrimitiveType(int type) {
        switch(type) {
        case T_BOOLEAN:
            return "[Z";
        case T_CHAR:
            return "[C";
        case T_FLOAT:
            return "[F";
        case T_DOUBLE:
            return "[D";
        case T_BYTE:
            return "[B";
        case T_SHORT:
            return "[S";
        case T_INT:
            return "[I";
        case T_LONG:
            return "[J";
        }
        throw new RuntimeException("invalid primitive type code " + type);
    }

    protected void iinc(StackFrame<Value> frame, Instruction insn) {
        Value v = frame.getLocal(insn.getIincVar());
        Value incr = IntegerValue.valueOf(insn.getIincIncr());
        frame.setLocal(insn.getIincVar(), traceEngine.createAndTraceBinopExpr(insn, v, incr));
    }


    protected void _throw(StackFrame<Value> frame) {
        Value exception = frame.pop();
        ElementInfo ei = Value.getElementInfo(exception);
        if (ei == null) {
            createAndThrowNullPointerException(frame);
            return;
        }
        threadInfo.setPendingException(ei.getReference());
        ClassInfo ci = ei.getClassInfo();
        threadInfo.handleException(frame, ci);
    }


    protected void arrayLength(StackFrame<Value> frame, Instruction insn) {
        Value array = frame.peek();
        if (array == Value.NULL) {
            createAndThrowNullPointerException(frame);
            return;
        }

        ElementInfo ei = getAndTryPreallocate(array);
        frame.pop();
        if (ei != null) {
            frame.push(traceEngine.checkHeapLocationPlaceholder(ei.arrayLength()));
            traceEngine.useArrayLength(ei.arrayLength());
        } else {
            frame.push(traceEngine.createArrayLengthOperator(array));
        }
    }

    private ElementInfo getAndTryPreallocate(Value v) {
        ElementInfo ei = Value.getElementInfo(v);
        if (ei == null) {
            ei = tryPreallocate(v);
        }
        return ei;
    }

    private ElementInfo tryPreallocate(Value v) {
        if (v instanceof Input) {
            String type = v.getTypeDescriptor();
            if (type.equals("Ljava/lang/String;") || type.equals("Ljava/lang/Class;")) {
                return null;
            }

            ElementInfo ei = traceEngine.allocateSkeletonObjectAndUpdateInput((Input<?>)v);
            if (ei == null) {
                traceEngine.accessUnknownNullPointer(v);
                throw new ExecutionInterruptedException();
            }
            return ei;
        }
        if (v instanceof OperatorValue) {
            return null;
        }
        if (v instanceof StringValue || v instanceof ClassInfoValue) {
            return null;
        }
        throw new RuntimeException("Should not reach here");
    }

    protected ElementInfo newException(ClassInfo ci) {
        return traceEngine.newObject(null, ci).getElementInfo();
    }

    protected void createAndThrowException(StackFrame<Value> frame, String className) {
        ClassInfo ci = ClassLoaderInfo.getResolvedClassInfo(className);
        ElementInfo exception = newException(ci);
        if (debug) System.out.println("Throw an exception " + className);
        threadInfo.setPendingException(exception.getReference());
        threadInfo.handleException(frame, ci);
    }

    protected void createAndThrowNullPointerException(StackFrame<Value> frame) {
        createAndThrowException(frame, "java/lang/NullPointerException");
    }



    /**
     * ...,array, index, value -> ...,
     * @param frame
     * @param longOrDouble
     */
    protected void arrayStore(StackFrame<Value> frame, boolean longOrDouble) {
        Value array = frame.peek(longOrDouble?3:2);

        if (array == Value.NULL) {
            createAndThrowNullPointerException(frame);
            return;
        }

        ElementInfo ei = getAndTryPreallocate(array);

        Value value = null;
        if (longOrDouble) {
            value = frame.popLong();
        } else {
            value = frame.pop();
        }
        Value index = frame.pop();
        array = frame.pop();

        if (ei != null) {
            ArrayElement ae = ei.arrayElement(index);
            ae.value(value);
            traceEngine.traceDefineArrayElement(frame.getPC(), ae);
        } else {
            traceEngine.createPutElementOperator(array, index, value);
        }
    }

    protected void _if(StackFrame<Value> frame, Instruction insn, Value v2) {
        Value v1 = frame.pop();
        frame.setPC(traceEngine.evaluateIf(insn, v1, v2));
    }

    /**
     * ...,array, index -> ..., array[index] 
     * @param frame
     * @param longOrDouble
     */
    protected void arrayLoad(StackFrame<Value> frame, boolean longOrDouble) {
        Value array = frame.peek(1);
        if (array == Value.NULL) {
            createAndThrowNullPointerException(frame);
            return;
        }
        ElementInfo ei = getAndTryPreallocate(array);
        Value index = frame.pop();
        frame.pop();

        Value value;
        if (ei != null) {
            value = traceEngine.checkHeapLocationPlaceholder(ei.arrayElement(index));
        } else {
            value = traceEngine.createGetElementOperator(array, getArrayClassInfo(frame.getPC()), index);
        }

        if (longOrDouble) {
            frame.pushLong(value);
        } else {
            frame.push(value);
        }
    }

    static ClassInfo getArrayClassInfo(Instruction insn) {
        int opcode = insn.getOpcode();
        switch (opcode) {
        case IALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[I");
        case LALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[J");
        case FALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[F");
        case DALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[D");
        case AALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[Ljava/lang/Object;");
        case BALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[B");
        case CALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[C");
        case SALOAD:
            return ClassLoaderInfo.getResolvedClassInfo("[S");
        }
        throw new RuntimeException("Invalid insn " + insn);
    }

    protected void binaryOperator(StackFrame<Value> frame, Instruction insn, boolean longOrDoubleOperand) {
        binaryOperator(frame, insn, longOrDoubleOperand, longOrDoubleOperand, longOrDoubleOperand);
    }

    /**
     * v1, v2 -> ret
     * v1 is left operand and v2 is right operand.
     * @param frame
     * @param insn
     * @param longOrDoubleOperandLeft
     * @param longOrDoubleOperandRight
     * @param longOrDoubleResult
     */
    protected void binaryOperator(StackFrame<Value> frame, Instruction insn, boolean longOrDoubleOperandLeft, boolean longOrDoubleOperandRight, boolean longOrDoubleResult) {
        Value v1, v2, result = null;
        if (longOrDoubleOperandRight) {
            v2 = frame.popLong();
        } else {
            v2 = frame.pop();
        }

        if (longOrDoubleOperandLeft) {
            v1 = frame.popLong();
        } else {
            v1 = frame.pop();
        }

        try {
            result = traceEngine.createAndTraceBinopExpr(insn, v1, v2);
        } catch (java.lang.ArithmeticException e) {
            createAndThrowException(frame, "java.lang.ArithmeticException");
            return;
        }
        if (longOrDoubleResult) {
            frame.pushLong(result);
        } else {
            frame.push(result);
        }
    }

    protected void unaryOperator(StackFrame<Value> frame, Instruction insn, boolean longOrDoubleOperand) {
        unaryOperator(frame, insn, longOrDoubleOperand, longOrDoubleOperand);
    }

    protected void unaryOperator(StackFrame<Value> frame, Instruction insn, boolean longOrDoubleOperand, boolean longOrDoubleResult) {
        Value v, result;
        if (longOrDoubleOperand) {
            v = frame.popLong();
        } else {
            v = frame.pop();
        }

        result = traceEngine.createAndTraceUnopExpr(insn, v);

        if (longOrDoubleResult) {
            frame.pushLong(result);
        } else {
            frame.push(result);
        }
    }

    protected void _return(StackFrame<Value> frame, int returnSize) {
        Value ret = null;
        if (returnSize == 1) {
            ret = frame.peek(); // do not pop, we may use this value during framePop event
        } else if (returnSize == 2) {
            ret = frame.peekLong();
        }

        MethodInfo mi = frame.getMethodInfo();
        StackFrame<Value> caller = threadInfo.popFrame();

        if (caller == null) {
            return;
        }

        caller.pop(mi.getArgumentsSize());

        if (returnSize == 1) {
            caller.push(ret);
        } else if (returnSize == 2) {
            caller.pushLong(ret);
        }

        caller.advance();
    }


    public void execute() {
        MethodInfo mi = traceEngine.getEntryMethodInfo();
        Value recv = null;
        if (!mi.isStatic()) {
            recv = traceEngine.allocateTarget();
        }

        traceEngine.traceMethodEntry(null, mi);
        StackFrame<Value> frame = new ValueStackFrame(mi);
        traceEngine.pushMethodArgs(mi, frame, recv);
        threadInfo.pushFrame(frame);

        try {
            interpret();
        } catch (MaxConditionException e) {
            exitWithException(threadInfo.getTopFrame(), "java/lang/InterruptedException");
        }
    }

    protected void exit(StackFrame<Value> frame) {
        threadInfo.printStackTrace();
        //throw new RuntimeException("Potential dead loop...");
        exitWithException(frame, "java/lang/InterruptedException");
    }

    public void resume() {
        try {
            interpret();
        } catch (MaxConditionException e) {
            exitWithException(threadInfo.getTopFrame(), "java/lang/InterruptedException");
        }
    }

}
