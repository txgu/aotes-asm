package org.javelus.aotes.asm.vm;

import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.InstanceField;

public class InstanceElementInfo extends ElementInfo implements Cloneable {

    public InstanceElementInfo(ClassInfo classInfo) {
        super(classInfo);
    }

    public InstanceField instanceField(FieldInfo field) {
        InstanceField location = (InstanceField) findHeapLocation(field);
        if (location == null) {
            location = new InstanceField(this, field);
            addHeapLocation(field, location);
        }
        return (InstanceField) location;
    }

    public final boolean isInstance() {
        return true;
    }

    public InstanceElementInfo clone() {
        InstanceElementInfo iei = (InstanceElementInfo) super.clone();
        for (HeapLocation hl:locations()) {
            InstanceField inf = (InstanceField) hl;
            iei.instanceField(inf.getFieldInfo()).value(inf.value());
        }
        return iei;
    }
}
