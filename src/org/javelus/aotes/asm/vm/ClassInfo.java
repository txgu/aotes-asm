package org.javelus.aotes.asm.vm;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

public class ClassInfo {

    private ClassInfo superClass;

    private List<ClassInfo> declaredInterfaces;

    private ClassInfo componentClassInfo;

    private Type type;

    private int modifier;

    private ClassNode classNode;

    private boolean isPrimitive;

    private final boolean isLinked;

    private Map<String, MethodInfo> methods;

    private Map<String, FieldInfo> fields;

    private static final Map<String, MethodInfo> EMPTY_METHODS = Collections.emptyMap();

    private static final Map<String, FieldInfo> EMPTY_FIELDS = Collections.emptyMap();

    private Set<ClassInfo> subclasses;

    private String className;

    private String typeDescriptor;

    public ClassInfo(ClassInfo superClass, List<ClassInfo> interfaces, ClassNode classNode) { 
        this.classNode = classNode;
        this.type = Type.getType("L" + classNode.name + ";");
        this.declaredInterfaces = interfaces;
        this.modifier = classNode.access;
        this.superClass = superClass;
        if (superClass != null) {
            superClass.addSubClass(this);
        }
        for (ClassInfo itfc:declaredInterfaces) {
            itfc.addSubClass(this);
        }
        this.methods = new HashMap<String, MethodInfo>();
        this.fields = new HashMap<String, FieldInfo>();
        this.isLinked = linkClass();
    }

    public ClassInfo(ClassInfo componentClassInfo, String descriptor) {
        this(descriptor);
        this.superClass = JavaLangObject;
        this.declaredInterfaces = ArrayInterfaces;
        this.componentClassInfo = componentClassInfo;
    }

    public ClassInfo(String descriptor) {
        this.declaredInterfaces = Collections.emptyList();
        this.methods = EMPTY_METHODS;
        this.fields = EMPTY_FIELDS;
        this.type = Type.getType(descriptor);
        this.isLinked = true;
    }

    private void addSubClass(ClassInfo sub) {
        if (this.subclasses == null) {
            subclasses = new HashSet<ClassInfo>();
        }
        subclasses.add(sub);
    }

    private boolean linkClass() {
        if (superClass != null && !superClass.isLinked()) {
            return false;
        }

        for (ClassInfo i:declaredInterfaces) {
            if (!i.isLinked()) {
                return false;
            }
        }

        for (MethodNode mn:(List<MethodNode>)classNode.methods) {
            //            if ((mn.access & Opcodes.ACC_SYNTHETIC) != 0) {
            //                continue;
            //            }
            MethodInfo mi = new MethodInfo(this, mn);
            if(methods.put(mi.getUniqueName(), mi) != null) {
                throw new RuntimeException("Duplicated method");
            }
        }

        for (FieldNode fn:(List<FieldNode>)classNode.fields) {
            FieldInfo fi = new FieldInfo(this, fn);
            fields.put(fi.getName(), fi);
        }

        return true;
    }

    public Map<String, MethodInfo> getMethods() {
        return methods;
    }

    public String getTypeDescriptor() {
        if (typeDescriptor == null) {
            typeDescriptor = type.getDescriptor();
        }
        return typeDescriptor;
    }

    public boolean isArray() {
        return componentClassInfo != null;
    }

    public ClassInfo getComponentClassInfo() {
        return componentClassInfo;
    }

    public String getName() {
        if (className == null) {
            className = type.getClassName();
        }
        return className;
    }

    public boolean isPrimitive() {
        return isPrimitive;
    }

    public boolean isLinked() {
        return isLinked;
    }

    public boolean isAbstract() {
        return Modifier.isAbstract(modifier);
    }

    public boolean isInterface() {
        return Modifier.isInterface(modifier);
    }

    public boolean isObjectClassInfo() {
        return classNode != null && classNode.superName == null;
    }

    public MethodInfo getDeclaredMethod(String uniqueName) {
        return methods.get(uniqueName);
    }

    public MethodInfo getDeclaredMethod(String name, String descriptor) {
        return getDeclaredMethod(MethodInfo.getUniqueName(name, descriptor));
    }

    public Collection<MethodInfo> getDeclaredMethods() {
        return methods.values();
    }

    public MethodInfo getMethod(String uniqueName) {
        MethodInfo mi = getDeclaredMethod(uniqueName);

        if (mi == null && superClass != null) {
            mi = superClass.getMethod(uniqueName);
        }

        if (mi == null) {
            for (ClassInfo itfc:this.declaredInterfaces) {
                mi = itfc.getMethod(uniqueName);
                if (mi != null) {
                    if (!isAbstract()) {
                        throw new RuntimeException("Resolve an abstract method for a non-abstract class");
                    }
                    return mi;
                }
            }
        }

        return mi;
    }

    public MethodInfo getMethod(String name, String descriptor) {
        return getMethod(MethodInfo.getUniqueName(name, descriptor));
    }

    public String toString() {
        return getName();
    }

    public boolean isInstanceOf(ClassInfo ci) {
        if (this == ci) {
            return true;
        }


        /**
         * JLS 4.10.1 
         *  double >1 float
         *  float >1 long
         *  long >1 int
         *  int >1 char
         *  int >1 short
         *  short >1 byte
         */
        if (isPrimitive()) {
            if (ci.isPrimitive()) {
                int thisSort = type.getSort();
                int thatSort = ci.type.getSort();
                switch(thisSort) {
                case Type.BYTE:
                    return thatSort == Type.SHORT ||
                    thatSort == Type.INT ||
                    thatSort == Type.LONG ||
                    thatSort == Type.FLOAT ||
                    thatSort == Type.DOUBLE;
                case Type.CHAR:
                case Type.SHORT:
                    return thatSort == Type.INT ||
                    thatSort == Type.LONG ||
                    thatSort == Type.FLOAT ||
                    thatSort == Type.DOUBLE;
                case Type.INT:
                    return thatSort == Type.LONG ||
                    thatSort == Type.FLOAT ||
                    thatSort == Type.DOUBLE;
                case Type.LONG:
                    return thatSort == Type.FLOAT ||
                    thatSort == Type.DOUBLE;
                case Type.FLOAT:
                    return thatSort == Type.DOUBLE;
                }
            }
            return false;
        }

        if (isArray()) {
            if (ci.isPrimitive()) {
                return false;
            }
            if (ci.isArray()) {
                ClassInfo thisCT = this.componentClassInfo;
                ClassInfo thatCT = ci.componentClassInfo;
                if (thisCT.isPrimitive() || thatCT.isPrimitive()) {
                    return false;
                }

                return thisCT.isInstanceOf(thatCT);
            }
            if (ci.isObjectClassInfo()) {
                return true;
            }
            String className = ci.getName();
            return className.equals("java.lang.Cloneable")
                    || className.equals("java.io.Serializable");
        }

        if (ci.isObjectClassInfo()) {
            return true;
        }

        if (superClass != null && superClass.isInstanceOf(ci)) {
            return true;
        }

        for (ClassInfo i:this.declaredInterfaces) {
            if (i.isInstanceOf(ci)) {
                return true;
            }
        }
        return false;
    }

    public String getEnclosingClassName() {
        if (classNode == null) {
            return null;
        }
        return classNode.outerClass;
    }

    public ClassInfo getEnclosingClassInfo() {
        String enclosingClass = getEnclosingClassName();
        return enclosingClass == null ? null : ClassLoaderInfo.getResolvedClassInfo(enclosingClass);
    }

    public String getEnclosingMethodName() {
        if (this.classNode == null) {
            return null;
        }
        return this.classNode.outerMethod;
    }

    public String getEnclosingMethodDescriptor() {
        if (this.classNode == null) {
            return null;
        }
        return this.classNode.outerMethodDesc;
    }

    public MethodInfo getEnclosingMethodInfo() {
        ClassInfo enclosingClassInfo = getEnclosingClassInfo();
        if (enclosingClassInfo == null) {
            return null;
        }
        String name = getEnclosingMethodName();
        String desc = getEnclosingMethodDescriptor();
        if (name == null || desc == null) {
            return null;
        }
        return enclosingClassInfo.getDeclaredMethod(name, desc);
    }

    public String getSimpleName() {
        String name = getName();
        int index = name.lastIndexOf('.');
        if (index == -1) {
            return name;
        }
        return name.substring(index + 1);
    }

    public Collection<MethodInfo> getDeclaredMethodInfos() {
        return methods.values();
    }

    public ClassInfo getSuperClass() {
        return superClass;
    }

    public FieldInfo getDeclaredField(String name) {
        return fields.get(name);
    }

    public FieldInfo getField(String name) {
        FieldInfo fi = getDeclaredField(name);

        if (fi == null && superClass != null) {
            fi = superClass.getField(name);
        }

        if (fi == null) {
            for (ClassInfo itfc:this.declaredInterfaces) {
                fi = itfc.getField(name);
                if (fi != null) {
                    return fi;
                }
            }
        }

        return fi;
    }

    public boolean isFinalInCHA() {
        return this.subclasses == null;
    }

    public boolean isPublic() {
        if (classNode == null) {
            return true;
        }
        return Modifier.isPublic(classNode.access);
    }

    private static ClassInfo JavaLangObject;
    private static List<ClassInfo> ArrayInterfaces; 
    public static void createArrayInterfaces() {
        JavaLangObject = ClassLoaderInfo.getResolvedClassInfo("java/lang/Object");
        ArrayInterfaces = Arrays.asList(ClassLoaderInfo.getResolvedClassInfo("java/lang/Cloneable"),
                ClassLoaderInfo.getResolvedClassInfo("java/io/Serializable"));
    }

    public Type getType() {
        return type;
    }
}
