package org.javelus.aotes.asm.vm;

public interface BranchChoice {
    boolean taken(Instruction insn);
}
