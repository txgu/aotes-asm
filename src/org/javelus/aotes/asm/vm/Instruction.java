package org.javelus.aotes.asm.vm;

import static org.javelus.aotes.asm.utils.Opcodes._if_acmpeq;
import static org.javelus.aotes.asm.utils.Opcodes._if_acmpne;
import static org.javelus.aotes.asm.utils.Opcodes._if_icmpeq;
import static org.javelus.aotes.asm.utils.Opcodes._if_icmpge;
import static org.javelus.aotes.asm.utils.Opcodes._if_icmpgt;
import static org.javelus.aotes.asm.utils.Opcodes._if_icmple;
import static org.javelus.aotes.asm.utils.Opcodes._if_icmplt;
import static org.javelus.aotes.asm.utils.Opcodes._if_icmpne;
import static org.javelus.aotes.asm.utils.Opcodes._ifeq;
import static org.javelus.aotes.asm.utils.Opcodes._ifge;
import static org.javelus.aotes.asm.utils.Opcodes._ifgt;
import static org.javelus.aotes.asm.utils.Opcodes._ifle;
import static org.javelus.aotes.asm.utils.Opcodes._iflt;
import static org.javelus.aotes.asm.utils.Opcodes._ifne;
import static org.javelus.aotes.asm.utils.Opcodes._ifnonnull;
import static org.javelus.aotes.asm.utils.Opcodes._ifnull;

import java.util.List;

import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.IincInsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.LookupSwitchInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MultiANewArrayInsnNode;
import org.objectweb.asm.tree.TableSwitchInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.util.Printer;

public class Instruction {
    MethodInfo methodInfo;

    AbstractInsnNode node;

    public Instruction(MethodInfo methodInfo, AbstractInsnNode node) {
        this.methodInfo = methodInfo;
        this.node = node;
    }

    public MethodInfo getMethodInfo() {
        return methodInfo;
    }

    public int getOpcode() {
        return node.getOpcode();
    }

    private Instruction toInstruction(AbstractInsnNode node) {
        return methodInfo.getInstruction(node);
    }

    public Instruction getNext() {
        return toInstruction(node.getNext());
    }

    public int getVar() {
        return ((VarInsnNode)node).var;
    }

    public int getIincVar() {
        return ((IincInsnNode)node).var;
    }

    public int getIincIncr() {
        return ((IincInsnNode)node).incr;
    }

    public Instruction getTarget() {
        return toInstruction(((JumpInsnNode)node).label);
    }

    public int getInt() {
        return ((IntInsnNode)node).operand;
    }

    public String getDesc() {
        return ((TypeInsnNode)node).desc;
    }

    public String getFieldOwner() {
        return ((FieldInsnNode)node).owner;
    }

    public String getFieldName() {
        return ((FieldInsnNode)node).name;
    }

    public String getFieldDesc() {
        return ((FieldInsnNode)node).desc;
    }

    public boolean isInvoke() {
        return node instanceof MethodInsnNode;
    }

    public String getMethodOwner() {
        return ((MethodInsnNode)node).owner;
    }

    public String getMethodName() {
        return ((MethodInsnNode)node).name;
    }

    public String getMethodDesc() {
        return ((MethodInsnNode)node).desc;
    }

    public Object getConstant() {
        return ((LdcInsnNode)node).cst;
    }

    public int getMultiDims() {
        return ((MultiANewArrayInsnNode)node).dims;
    }

    public String getMultiArrayDesc() {
        return ((MultiANewArrayInsnNode)node).desc;
    }

    public int getTableSwitchMin() {
        return ((TableSwitchInsnNode)node).min;
    }

    public int getTableSwitchMax() {
        return ((TableSwitchInsnNode)node).max;
    }

    public Instruction getTableSwitchDefault() {
        return toInstruction(((TableSwitchInsnNode)node).dflt);
    }

    /**
     * Including default
     * @return
     */
    public int getTableSwitchBranchCount() {
        return ((TableSwitchInsnNode)node).labels.size() + 1;
    }

    public Instruction getTableSwitchBranch(int branch) {
        return toInstruction((LabelNode) ((TableSwitchInsnNode)node).labels.get(branch));
    }

    public Instruction getLookupSwitchDefault() {
        return toInstruction(((LookupSwitchInsnNode)node).dflt);
    }

    public List<Integer> getLookupSwitchKeys() {
        return ((LookupSwitchInsnNode)node).keys;
    }

    public Instruction getLookupSwitchBranch(int branch) {
        return toInstruction((LabelNode) ((LookupSwitchInsnNode)node).labels.get(branch));
    }

    public int getLookupSwitchBranchCount() {
        return ((LookupSwitchInsnNode)node).labels.size() + 1;
    }

    public String toString() {
        InsnNodePrinter p = new InsnNodePrinter();
        node.accept(p);
        return p.toString();
    }

    static class InsnNodePrinter extends MethodVisitor {

        String result;
        public InsnNodePrinter() {
            super(Opcodes.ASM5);
        }

        @Override
        public void visitInsn(int opcode) {
            result = Printer.OPCODES[opcode];
        }

        @Override
        public void visitIntInsn(int opcode, int operand) {
            result = Printer.OPCODES[opcode] + " " + operand;
        }

        @Override
        public void visitVarInsn(int opcode, int var) {
            result = Printer.OPCODES[opcode] + " " + var;
        }

        @Override
        public void visitTypeInsn(int opcode, String type) {
            result = Printer.OPCODES[opcode] + " " + type;
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String desc) {
            result = Printer.OPCODES[opcode] + " " + desc + " " + owner + "." + name;
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
            result = Printer.OPCODES[opcode] + " " + owner + "." + name + desc;
        }

        @Override
        public void visitInvokeDynamicInsn(String name, String desc, Handle bsm, Object... bsmArgs) {
            result = Printer.OPCODES[Opcodes.INVOKEDYNAMIC];
        }

        @Override
        public void visitJumpInsn(int opcode, Label label) {
            result = Printer.OPCODES[opcode] + " " + label.hashCode();
        }

        @Override
        public void visitLabel(Label label) {
            result = "LABEL: " + label.hashCode();
        }

        @Override
        public void visitLdcInsn(Object cst) {
            result = "// " + cst;
        }

        @Override
        public void visitIincInsn(int var, int increment) {
            result =  Printer.OPCODES[Opcodes.IINC] + " " + var + " " + increment;
        }

        @Override
        public void visitTableSwitchInsn(int min, int max, Label dflt, Label... labels) {
            result =  Printer.OPCODES[Opcodes.TABLESWITCH];
        }

        @Override
        public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {
            result =  Printer.OPCODES[Opcodes.LOOKUPSWITCH];
        }

        @Override
        public void visitMultiANewArrayInsn(String desc, int dims) {
            result =  Printer.OPCODES[Opcodes.MULTIANEWARRAY] + " " + desc + " " + dims;
        }

        @Override
        public void visitLineNumber(int line, Label start) {
            result = "LINE: " + line + "@" + start.hashCode();
        }

        public String toString() {
            return result;
        }
    }

    public AbstractInsnNode getNode() {
        return node;
    }

    public boolean isIf() {
        switch (getOpcode()) {
        case _ifeq:
        case _ifne:
        case _iflt:
        case _ifge:
        case _ifgt:
        case _ifle:
        case _if_icmpeq:
        case _if_icmpne:
        case _if_icmplt:
        case _if_icmpge:
        case _if_icmpgt:
        case _if_icmple:
        case _if_acmpeq:
        case _if_acmpne:
        case _ifnull:
        case _ifnonnull:
            return true;
        }
        return false;
    }

}
