package org.javelus.aotes.asm.vm;

public class ExecutionInterruptedException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 8266397818730906543L;

    public ExecutionInterruptedException() {
        super();
    }
}
