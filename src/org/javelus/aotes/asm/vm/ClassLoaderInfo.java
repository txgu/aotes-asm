package org.javelus.aotes.asm.vm;

import static org.javelus.aotes.asm.utils.JVMUtils.isArray;
import static org.javelus.aotes.asm.utils.JVMUtils.isPrimitive;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;

public class ClassLoaderInfo {
    /**
     * This class loader must implemented a proper getResource
     */
    static LinkedList<URLClassLoader> classloaders;

    public static void initializeClassLoaders(String classPath) {
        classloaders = new LinkedList<URLClassLoader>();
        if (classPath != null) {
            String[] paths = classPath.split(File.pathSeparator);

            List<URL> urls = new ArrayList<URL>();
            for (String path:paths) {
                path = path.trim();
                if (path.isEmpty()) {
                    continue;
                }

                try {
                    URL url = Paths.get(path).toUri().toURL();
                    urls.add(url);
                } catch (MalformedURLException e) {
                    System.out.println("WARNING: cannot create URL from " + path);
                    continue;
                }
            }

            classloaders.add(new URLClassLoader(urls.toArray(new URL[urls.size()])));
        }

        classloaders.add((URLClassLoader) ClassLoader.getSystemClassLoader());
        ClassInfo.createArrayInterfaces();
    }

    public static List<URLClassLoader> getClassLoaders() {
        if (classloaders == null) {
            initializeClassLoaders();
        }
        return classloaders;
    }

    public static void initializeClassLoaders() {
        classloaders = new LinkedList<URLClassLoader>();
        classloaders.add((URLClassLoader) ClassLoader.getSystemClassLoader());
        ClassInfo.createArrayInterfaces();
    }

    static Map<String, ClassInfo> classes = new HashMap<String, ClassInfo>();

    private static ClassInfo findClass(String key) {
        return classes.get(key);
    }

    public static void addClassLoader(URLClassLoader cl) {
        if (!classloaders.contains(cl)) {
            classloaders.addFirst(cl);
        }
    }

    protected static void addClass(String key, ClassInfo classInfo) {
        if (classes.put(key, classInfo) != null) {
            throw new RuntimeException("sanity check failed");
        }
    }


    public static ClassInfo getResolvedClassInfo(String name) {
        ClassInfo ci = getResolvedClassInfoOrNull(name);
        if (ci == null) {
            throw new RuntimeException("Cannot resolve class " + name);
        }
        return ci;
    }

    public static ClassInfo getResolvedClassInfoOrNull(String name) {
        return resolveClass(name);
    }

    private static URL findResource(String fileName) {
        URL url = null;
        for (URLClassLoader cl:getClassLoaders()) {
            url = cl.findResource(fileName);
            if (url != null) {
                return url;
            }
        }

        return null;
    }

    private static ClassInfo resolveClass(String internalNameOrDescriptor) {
        if (internalNameOrDescriptor.indexOf('.') != -1) {
            System.out.println("WARNING: class name "+ internalNameOrDescriptor + " is not internal");
            internalNameOrDescriptor = internalNameOrDescriptor.replace('.', '/');
        }

        if (isPrimitive(internalNameOrDescriptor)) {
            return resolvePrimitive(internalNameOrDescriptor);
        }

        if (isArray(internalNameOrDescriptor)) {
            return resolveArray(internalNameOrDescriptor);
        }

        ClassInfo ret = findClass(internalNameOrDescriptor);

        if (ret == null) {
            String fileName = internalNameOrDescriptor + ".class";
            URL file = findResource(fileName);
            if (file == null) {
                file = ClassLoader.getSystemResource(fileName);
            }
            if (file != null) {
                ret = loadClass(file);
            }

            if (ret == null) {
                return null;
            }

            if (!ret.isLinked()) {
                throw new RuntimeException("Cannot link class " + internalNameOrDescriptor);
            }

            addClass(internalNameOrDescriptor, ret);
        }

        return ret;
    }

    private static ClassInfo resolvePrimitive(String descriptor) {
        ClassInfo ci = findClass(descriptor);
        if (ci == null) {
            ci = new ClassInfo(descriptor);
            addClass(descriptor, ci);
        }
        return ci;
    }

    private static String getComponenetClassName(String arrayDescriptor) {
        if (arrayDescriptor.charAt(0) != '[') {
            throw new IllegalArgumentException("Not a valid array name " + arrayDescriptor);
        }

        String componentName = arrayDescriptor.substring(1);

        if (componentName.charAt(0) == 'L') {
            return componentName.substring(1, componentName.length() - 1);
        }

        return componentName;
    }

    private static ClassInfo resolveArray(String arrayDescriptor) {
        ClassInfo ci = findClass(arrayDescriptor);
        if (ci == null) {
            ci = new ClassInfo(getResolvedClassInfo(getComponenetClassName(arrayDescriptor)), arrayDescriptor);
            addClass(arrayDescriptor, ci);
        }
        return ci;
    }

    private static ClassInfo loadClass(URL file) {
        InputStream stream = null;

        try {
            stream = file.openStream();
            ClassReader cr = new ClassReader(stream);
            ClassNode cn = new ClassNode();
            cr.accept(cn, 0);
            ClassInfo theClass = null;

            if (cn.superName == null) {
                if (cn.name.equals("java/lang/Object")) {
                    theClass = new ClassInfo(null, Collections.<ClassInfo>emptyList(), cn);
                    return theClass;
                }
                throw new RuntimeException("Malformed class file");
            }

            ClassInfo superClass = getResolvedClassInfoOrNull(cn.superName);
            if (superClass == null) {
                return null;
            }

            List<ClassInfo> itfcClasses = new ArrayList<ClassInfo>();
            for(String itfcName : (List<String>)cn.interfaces) {
                ClassInfo itfcClass = getResolvedClassInfoOrNull(itfcName);
                if (itfcClass == null) {
                    return null;
                }
                itfcClasses.add(itfcClass);
            }
            theClass = new ClassInfo(superClass, itfcClasses, cn);
            return theClass;
        } catch (IOException e) {
            throw new RuntimeException("Cannot load class at " + file, e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public static String getClassName(Type type) {
        int sort = type.getSort();
        switch (sort) {
        case Type.BOOLEAN:
            return "boolean";
        case Type.CHAR:
            return "char";
        case Type.BYTE:
            return "byte";
        case Type.SHORT:
            return "short";
        case Type.INT:
            return "int";
        case Type.FLOAT:
            return "float";
        case Type.LONG:
            return "long";
        case Type.DOUBLE:
            return "double";
        case Type.ARRAY:
        case Type.OBJECT:
            return type.getInternalName();
        default:
            throw new RuntimeException("Should not reach here");
        }
    }

    public static ClassInfo getResolvedClassInfo(Type type) {
        return getResolvedClassInfo(getClassName(type));
    }
}
