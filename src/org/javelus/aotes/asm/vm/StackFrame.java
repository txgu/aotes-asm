package org.javelus.aotes.asm.vm;

public abstract class StackFrame<E>{
    private StackFrame<E> previous;

    private int numOfLocals;
    private int numOfStack;

    private E[] values;

    private int depth;
    private int top;

    private Instruction pc;

    private MethodInfo methodInfo;

    public StackFrame(MethodInfo mi) {
        this(mi.getMaxLocals(), mi.getMaxStack());
        this.pc = mi.getFirstInstruction();
        this.methodInfo = mi;
    }

    public StackFrame(int maxLocals, int maxStack) {
        this.numOfLocals = maxLocals;
        this.numOfStack = maxStack;
        this.values = createLocalsAndExpressionStack(numOfLocals + numOfStack);
        this.top = numOfLocals - 1;
    }

    protected abstract E[] createLocalsAndExpressionStack(int length);

    public void setPrevious(StackFrame<E> previous) {
        this.previous = previous;
        if (previous != null) {
            this.depth = previous.depth + 1;
        }
    }

    public int setLocal(int argOffset, E value) {
        values[argOffset] = value;
        return argOffset + 1;
    }

    public int setLongLocal(int argOffset, E value) {
        values[argOffset] = value;
        return argOffset + 2;
    }

    public E getLocal(int argOffset) {
        return values[argOffset];
    }

    public E getLongLocal(int argOffset) {
        return values[argOffset];
    }


    public ClassInfo resolveClassInfo(String className) {
        return ClassLoaderInfo.getResolvedClassInfo(className);
    }

    public ClassInfo resolveClassInfoOrNull(String className) {
        return ClassLoaderInfo.getResolvedClassInfoOrNull(className);
    }

    public MethodInfo resolveMethodVirtual(ElementInfo receiver, String name, String descriptor) {
        return receiver.getClassInfo().getMethod(name, descriptor);
    }

    public MethodInfo resolveMethod(String owner, String name, String descriptor) {
        ClassInfo target = resolveClassInfoOrNull(owner);
        if (target == null) {
            return null;
        }
        return resolveMethod(target, name, descriptor);
    }

    public MethodInfo resolveMethod(ClassInfo ci, String name, String descriptor) {
        return ci.getMethod(name, descriptor);
    }

    public FieldInfo resolveField(String owner, String name, String descriptor) {
        ClassInfo target = resolveClassInfoOrNull(owner);
        if (target == null) {
            return null;
        }
        return target.getField(name);
    }

    public StackFrame<E> getPrevious() {
        return previous;
    }

    public void push(E value) {
        top++;
        values[top] = value;
    }

    public void pushLong(E value) {
        top++;
        values[top] = value;
        top++;
    }

    public E pop() {
        E ret = values[top];
        values[top] = null;
        top--;
        return ret;
    }

    public E popLong() {
        values[top] = null;
        top--;
        return pop();
    }

    public E peek() {
        return peek(0);
    }

    public E peekLong() {
        return peek(1);
    }

    public E peek(int i) {
        return values[top - i];
    }

    public Instruction getPC() {
        return pc;
    }

    public void setPC(Instruction pc) {
        this.pc = pc;
    }

    public void advance() {
        pc = pc.getNext();
    }



    /**
     * v -> v, v
     */
     public void dup() {
         push(peek());
     }

     /**
      * ..., v2, v1 -> ..., v1, v2, v1
      */
     public void dup_x1() {
         E v1 = pop();
         E v2 = pop();
         push(v1);
         push(v2);
         push(v1);
     }

     /**
      * ..., v3, v2, v1 -> ..., v1, v3, v2, v1
      */
     public void dup_x2() {
         E v1 = pop();
         E v2 = pop();
         E v3 = pop();
         push(v1);
         push(v3);
         push(v2);
         push(v1);
     }

     /**
      * ..., v2, v1 -> ..., v2, v1, v2, v1
      */
     public void dup2() {
         E v1 = pop();
         E v2 = pop();
         push(v2);
         push(v1);
         push(v2);
         push(v1);
     }

     /**
      * ..., v3, v2, v1 -> ..., v2, v1, v3, v2, v1
      */
     public void dup2_x1() {
         E v1 = pop();
         E v2 = pop();
         E v3 = pop();
         push(v2);
         push(v1);
         push(v3);
         push(v2);
         push(v1);
     }

     /**
      * ..., v4, v3, v2, v1 -> ..., v2, v1, v3, v2, v1
      */
     public void dup2_x2() {
         E v1 = pop();
         E v2 = pop();
         E v3 = pop();
         E v4 = pop();
         push(v2);
         push(v1);
         push(v4);
         push(v3);
         push(v2);
         push(v1);
     }

     /**
      * ..., v2, v1 -> ..., v1, v2
      */
     public void swap() {
         E v1 = pop();
         E v2 = pop();
         push(v1);
         push(v2);
     }

     public MethodInfo getMethodInfo() {
         return methodInfo;
     }

     public void pop(int size) {
         for (int i=0; i<size; i++) {
             pop();
         }
     }

     public String toString() {
         StringBuilder sb = new StringBuilder();
         sb.append(methodInfo.toString());
         sb.append(", depth=");
         sb.append(depth);
         sb.append(", values=[");
         for (int i = 0; i <= top; i++) {
             if (i == numOfLocals) {
                 sb.append('|');
             } else if (i > 0) {
                 sb.append(',');
             }
             sb.append(values[i]);
         }
         sb.append(']');
         return sb.toString();
     }

     public void clearExpressionStack() {
         int base = numOfLocals - 1;
         for (int i=base; i<=top; i++) {
             values[i] = null;
         }
         top = base;
     }

     public void setupArguments(int size) {
         System.arraycopy(previous.values, previous.top - size + 1, values, 0, size);
     }

}
