package org.javelus.aotes.asm.vm;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.utils.MapUtils;
import org.javelus.aotes.executor.java.JavaEvalMap;
import org.javelus.aotes.executor.java.location.DummyArray;
import org.javelus.aotes.executor.java.location.WrappedArray;
import org.javelus.aotes.executor.java.location.WrappedObject;
import org.objectweb.asm.Type;

public class ReflectionHelper {


    public static void setArrayElement(Object array, int arrayIndex, Object value) {
        if ( arrayIndex < 0 || arrayIndex >= Array.getLength(array)) {
            throw new ArrayIndexOutOfBoundsException();
        }

        Class<?> arrayClass = array.getClass();
        if (arrayClass.getComponentType() == int.class) {
            Integer intValue = (Integer) value;
            Array.setInt(array, arrayIndex, intValue);
            return;
        }

        if (arrayClass.getComponentType() == boolean.class) {
            Boolean byteValue = (Boolean) value;
            Array.setBoolean(array, arrayIndex, byteValue);
            return;
        }

        if (arrayClass.getComponentType() == byte.class) {
            Byte byteValue = (Byte) value;
            Array.setByte(array, arrayIndex, byteValue);
            return;
        }

        if (arrayClass.getComponentType() == char.class) {
            Character charValue = (Character) value;
            Array.setChar(array, arrayIndex, charValue);
            return;
        }

        if (arrayClass.getComponentType() == short.class) {
            Short shortValue = (Short) value;
            Array.setShort(array, arrayIndex, shortValue);
            return;
        }

        if (arrayClass.getComponentType() == long.class) {
            Long longValue = (Long) value;
            Array.setLong(array, arrayIndex, longValue);
            return;
        }

        if (arrayClass.getComponentType() == float.class) {
            Float floatValue = (Float) value;
            Array.setFloat(array, arrayIndex, floatValue);
            return;
        }

        if (arrayClass.getComponentType() == double.class) {
            Double doubleValue = (Double) value;
            Array.setDouble(array, arrayIndex, doubleValue);
            return;
        }

        Array.set(array, arrayIndex, value);
    }

    public static int getArrayLength(Object array) {
        if (array instanceof DummyArray) {
            throw new RuntimeException("sanity check failed");
        }
        if (array instanceof WrappedArray) {
            return ((WrappedArray) array).getLength();
        }
        return Array.getLength(array);
    }

    public static Object getArrayElement(Object array, int arrayIndex) {
        if (!array.getClass().isArray()) {
            throw new RuntimeException("parameter is not an array but a " + array);
        }
        if ( arrayIndex < 0 || arrayIndex >= Array.getLength(array)) {
            throw new ArrayIndexOutOfBoundsException();
        } 

        Class<?> arrayClass = array.getClass();
        if (arrayClass.getComponentType() == int.class) {
            return Array.getInt(array, arrayIndex);
        }

        if (arrayClass.getComponentType() == boolean.class) {
            return Array.getBoolean(array, arrayIndex);
        }

        if (arrayClass.getComponentType() == byte.class) {
            return Array.getByte(array, arrayIndex);
        }

        if (arrayClass.getComponentType() == char.class) {
            return Array.getChar(array, arrayIndex);
        }

        if (arrayClass.getComponentType() == short.class) {
            return Array.getShort(array, arrayIndex);
        }

        if (arrayClass.getComponentType() == long.class) {
            return Array.getLong(array, arrayIndex);
        }

        if (arrayClass.getComponentType() == float.class) {
            return Array.getFloat(array, arrayIndex);
        }

        if (arrayClass.getComponentType() == double.class) {
            return Array.getDouble(array, arrayIndex);
        }

        return Array.get(array, arrayIndex);
    }


    public static Object getDefaultValue(Class<?> type) {
        if (type == int.class) {
            return Integer.valueOf(0);
        }

        if (type == byte.class) {
            return Integer.valueOf(0);
        }

        if (type == char.class) {
            return Integer.valueOf(0);
        }

        if (type == short.class) {
            return Integer.valueOf(0);
        }

        if (type == double.class) {
            return Double.valueOf(0.0D);
        }

        if (type == float.class) {
            return Float.valueOf(0.0F);
        }

        if (type == long.class) {
            return Long.valueOf(0);
        }

        if (type == boolean.class) {
            return Boolean.valueOf(false);
        }

        return null;
    }

    public static boolean isDefaultValue(Integer intValue) {
        return intValue.intValue() == 0;
    }

    public static boolean isDefaultValue(Byte byteValue) {
        return byteValue.intValue() == 0;
    }

    public static boolean isDefaultValue(Boolean boolValue) {
        return boolValue.booleanValue() == false;
    }

    public static boolean isDefaultValue(Character charValue) {
        return charValue.equals(Character.valueOf('\0'));
    }

    public static boolean isDefaultValue(Short shortValue) {
        return shortValue.intValue() == 0;
    }

    public static boolean isDefaultValue(Long longValue) {
        return longValue.longValue() == 0L;
    }

    public static boolean isDefaultValue(Float floatValue) {
        return floatValue.floatValue() == 0.0F;
    }

    public static boolean isDefaultValue(Double doubleValue) {
        return doubleValue.doubleValue() == 0.0D;
    }

    public static boolean isDefaultValue(Object value) {
        if (value instanceof Integer) {
            return isDefaultValue(((Integer) value));
        }

        if (value instanceof Byte) {
            return isDefaultValue(((Byte) value));
        }

        if (value instanceof Character) {
            return isDefaultValue((Character)value);
        }

        if (value instanceof Short) {
            return isDefaultValue((Short)value);
        }

        if (value instanceof Double) {
            return isDefaultValue((Double) value);
        }

        if (value instanceof Float) {
            return isDefaultValue((Float) value);
        }

        if (value instanceof Long) {
            return isDefaultValue((Long) value);
        }

        if (value instanceof Boolean) {
            return isDefaultValue((Boolean) value);
        }

        return value == null;
    }

    public static Class<?> getObjectClass(Object object) {
        if (object instanceof WrappedObject) {
            return ((WrappedObject) object).getType();
        } else {
            return object.getClass();
        }
    }

    public static Field getField(Object object, String encodedFieldName) {
        Class<?> cls = null;
        String fieldName = encodedFieldName;
        object = JavaEvalMap.getValue(object);
        int index = encodedFieldName.lastIndexOf('.');
        fieldName = encodedFieldName.substring(index + 1);

        if (object == null) {
            if (index == -1) {
                throw new RuntimeException("Malformed field name for static field [" + encodedFieldName + "] or the receiver is indeed null.");
            }
            String className = encodedFieldName.substring(0, index);
            try {
                cls = Class.forName(className);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("Cannot resolve class " + className, e);
            }
        } else {
            cls = getObjectClass(object);
        }

        if (cls == null) {
            throw new RuntimeException("Class cannot be null");
        }

        Field field = MapUtils.getValueFromMapMap(fields, cls, fieldName);
        if (field != null) {
            return field;
        }

        try {
            while (cls != Object.class) {
                try {
                    field = cls.getDeclaredField(fieldName);
                    field.setAccessible(true);
                    MapUtils.addToMapMap(fields, cls, fieldName, field);
                    return field;
                } catch(NoSuchFieldException e) {
                    cls = cls.getSuperclass();
                }
            }
        } catch (SecurityException e) {
            throw new RuntimeException("Resolve field " + encodedFieldName + " in " + cls + " results in security exception", e);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Get field " + encodedFieldName + " in " + cls + " results in an exception", e);
        }
        throw new RuntimeException("No such field " + encodedFieldName + " in " + object + " and its super classes");
    }

    public static Object getFieldValue(Object object, String fieldName) {
        try {
            return getField(object, fieldName).get(object);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException("Get field " + fieldName + " results in an exception", e);
        }
    }

    public static Class<?> loadClassOrNull(String internalName) {
        if (internalName.equals("int")) {
            return int.class;
        } else if (internalName.equals("char")) {
            return char.class;
        } else if (internalName.equals("byte")) {
            return byte.class;
        } else if (internalName.equals("short")) {
            return short.class;
        } else if (internalName.equals("boolean")) {
            return boolean.class;
        } else if (internalName.equals("float")) {
            return short.class;
        } else if (internalName.equals("long")) {
            return long.class;
        } else if (internalName.equals("double")) {
            return double.class;
        }
        try {
            if (internalName.startsWith("[")) {
                internalName = internalName.replace('/', '.');
            }
            return Class.forName(internalName);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }



    static Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
    static Map<MethodInfo, Method> methods = new HashMap<MethodInfo, Method>();

    static Map<Class<?>, Map<String, Field>> fields = new HashMap<Class<?>, Map<String, Field>>();


    public static Class<?> getResolvedClass(String name) throws ClassNotFoundException {
        Class<?> clazz = classes.get(name);
        if (clazz != null) {
            return clazz;
        }
        for (URLClassLoader cl:ClassLoaderInfo.getClassLoaders()) {
            try {
                clazz = cl.loadClass(name);
                classes.put(name, clazz);
            } catch (ClassNotFoundException e) {
            }

        }
        if (clazz == null) {
            clazz = Class.forName(name);
            classes.put(name, clazz);
        }
        return clazz;
    }

    public static Class<?> typeToClass(Type type) throws ClassNotFoundException {
        int sort = type.getSort();
        switch(sort) {
        case Type.VOID:
            return void.class;
        case Type.INT:
            return int.class;
        case Type.BOOLEAN:
            return boolean.class;
        case Type.BYTE:
            return byte.class;
        case Type.CHAR:
            return char.class;
        case Type.SHORT:
            return short.class;
        case Type.FLOAT:
            return float.class;
        case Type.LONG:
            return long.class;
        case Type.DOUBLE:
            return double.class;
        case Type.ARRAY:
            return getResolvedClass(type.getDescriptor().replace('/', '.'));
        case Type.OBJECT:
            return getResolvedClass(type.getClassName());
        }

        throw new RuntimeException("Uunsupport type " + type);
    }

    public static Object cloneObject(Object object, MethodInfo mi) {
        if (object instanceof WrappedObject) {
            return null;
        }

        Method m = methodInfoToMethod(mi);
        try {
            return m.invoke(object, new Object[0]);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot clone object " + object);
        }
    }

    public static Object cloneObject(Object object) {
        if (object instanceof WrappedObject) {
            return null;
        }
        Class<?> clazz = object.getClass();
        try {
            Method m = clazz.getMethod("clone", new Class<?>[0]);
            return m.invoke(object, new Object[0]);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot clone object " + object, e);
        }
    }

    public static Method methodInfoToMethod(MethodInfo mi) {
        Method method = methods.get(mi);
        if (method != null) {
            return method;
        }
        Class<?> clazz = classInfoToClass(mi.getClassInfo());
        Type[] argumentTypes = mi.getArgumentTypes();
        Class<?>[] argumentClasses = new Class<?>[argumentTypes.length];
        for (int i = 0; i < argumentClasses.length; i++) {
            try {
                argumentClasses[i] = typeToClass(argumentTypes[i]);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException("Cannot resovle class " + argumentTypes[i]);
            }
        }
        try {
            method = clazz.getDeclaredMethod(mi.getName(), argumentClasses);
            methods.put(mi, method);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new RuntimeException("Cannot resovle method for " + mi);
        }
        return method;
    }

    public static Class<?> classInfoToClass(ClassInfo ci) {
        try {
            return typeToClass(ci.getType());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Cannot load class " + ci);
        }
    }

    public static ClassInfo classToClassInfo(Class<?> clazz) {
        String className = clazz.getName().replace('.', '/');
        String transformed = toPrimitive(className);
        if (transformed != null) {
            return ClassLoaderInfo.getResolvedClassInfo(transformed);
        }
        transformed = toArray(className);
        if (transformed != null) {
            return getClassInfo(className, clazz);
        }
        return getClassInfo(className, clazz);
    }

    private static ClassInfo getClassInfo(String className, Class<?> clazz) {
        ClassInfo ci = ClassLoaderInfo.getResolvedClassInfoOrNull(className);
        if (ci == null) {
            ClassLoader cl = clazz.getClassLoader();
            if (cl instanceof URLClassLoader) {
                ClassLoaderInfo.addClassLoader((URLClassLoader) cl);
                ci = ClassLoaderInfo.getResolvedClassInfo(className);
            }
        }
        return ci;
    }

    private static String toPrimitive(String className) {
        if (className.equals("void")) {
            return "V";
        } else if (className.equals("byte")) {
            return "B";
        } else if (className.equals("boolean")) {
            return "Z";
        } else if (className.equals("char")) {
            return "C";
        } else if (className.equals("short")) {
            return "S";
        } else if (className.equals("int")) {
            return "I";
        } else if (className.equals("float")) {
            return "F";
        } else if (className.equals("long")) {
            return "J";
        } else if (className.equals("double")) {
            return "D";
        }
        return null;
    }

    static final String[] arrayDimentions = new String[] {
            null,"[","[[","[[[","[[[["
    };

    private static String toArray(String className) {
        if (className.endsWith("[]")) {
            int index = className.lastIndexOf('[');
            int dim = (className.length() - index) >> 1;
        if (dim >= arrayDimentions.length) {
            throw new RuntimeException("Add dimention " + dim + " to arrayDimentions!");
        }
        String prefix = arrayDimentions[dim];
        String elementType = className.substring(0, index);
        String transformed = toPrimitive(elementType);
        if (transformed != null) {
            return prefix + transformed;
        } else {
            return prefix + "L" + elementType.replace('.', '/') + ';';
        }
        }
        return null;
    }

    public static MethodInfo constructorToMethodInfo(Constructor<?> ctor) {
        ClassInfo ci = classToClassInfo(ctor.getDeclaringClass());
        return ci.getDeclaredMethod("<init>", Type.getConstructorDescriptor(ctor));
    }

    public static MethodInfo methodToMethodInfo(Method method) {
        ClassInfo ci = classToClassInfo(method.getDeclaringClass());
        return ci.getDeclaredMethod(method.getName(), Type.getMethodDescriptor(method));
    }

}
