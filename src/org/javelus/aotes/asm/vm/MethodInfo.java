package org.javelus.aotes.asm.vm;

import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TryCatchBlockNode;

public class MethodInfo {

    private ClassInfo classInfo;
    private MethodNode methodNode;
    private String uniqueName;

    private int numOfArguments;
    private int argumentSize;
    private int returnSize;

    private Map<AbstractInsnNode, Instruction> instructions;

    private Type type;
    private Type[] argumentTypes;
    private Type returnType;

    public MethodInfo(ClassInfo classInfo, MethodNode methodNode) {
        this.classInfo = classInfo;
        this.methodNode = methodNode;
        this.uniqueName = getUniqueName(methodNode.name, methodNode.desc);
        this.type = Type.getType(methodNode.desc);
        this.argumentTypes = Type.getArgumentTypes(methodNode.desc);
        this.returnType = Type.getReturnType(methodNode.desc);
        buildInstructions();
        calcArgumentsAndReturnSizes();
    }

    private void buildInstructions() {
        if (methodNode.instructions.size() == 0) {
            instructions = Collections.emptyMap();
        } else {
            instructions = new HashMap<AbstractInsnNode, Instruction>();
            InsnList insns = methodNode.instructions;
            AbstractInsnNode node = insns.getFirst();
            while (node != null) {
                instructions.put(node, new Instruction(this, node));
                node = node.getNext();
            }
        }
    }

    private void calcArgumentsAndReturnSizes() {
        int size = Type.getArgumentsAndReturnSizes(methodNode.desc);
        argumentSize = size >> 2;
        returnSize = size & 0x03;
        if (isStatic()) {
            argumentSize--;
        }

        numOfArguments = Type.getArgumentTypes(methodNode.desc).length;
    }

    /**
     * Covariance is implemented via synthetic methods.
     * @param name
     * @param desc
     * @return
     */
    public static String getUniqueName(String name, String desc) {
        return name + desc;
    }

    public String getName() {
        return methodNode.name;
    }

    public String getDescriptor() {
        return methodNode.desc;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public int getNumOfArguments() {
        return numOfArguments;
    }

    public boolean isStatic() {
        return Modifier.isStatic(methodNode.access);
    }


    public boolean isPrivate() {
        return Modifier.isPrivate(methodNode.access);
    }

    public boolean isInit() {
        return methodNode.name.equals("<init>");
    }

    public boolean isClinit() {
        return methodNode.name.equals("<clinit>");
    }

    public Type getType() {
        return type;
    }

    public Type getReturnType() {
        return returnType;
    }

    public int getReturnTypeSort() {
        return getReturnType().getSort();
    }

    public String getReturnTypeDescriptor() {
        return getReturnType().getDescriptor();
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }

    public String getClassName() {
        return getClassInfo().getName();
    }

    public String getFullName() {
        return (methodNode.name + methodNode.desc);
    }

    public int getArgumentsSize() {
        return argumentSize;
    }

    public int getReturnSize() {
        return returnSize;
    }

    public boolean isAbstract() {
        return Modifier.isAbstract(methodNode.access);
    }

    public boolean isNative() {
        return Modifier.isNative(methodNode.access);
    }

    public int getMaxLocals() {
        return methodNode.maxLocals;
    }

    public int getMaxStack() {
        return methodNode.maxStack;
    }

    protected Instruction getInstruction(AbstractInsnNode node) {
        return instructions.get(node);
    }

    public Instruction getFirstInstruction() {
        if (methodNode.instructions.size() == 0) {
            throw new RuntimeException("An abstract method " + this);
        }
        return instructions.get(methodNode.instructions.getFirst());
    }

    public Instruction exceptionHandler(StackFrame<?> frame, ClassInfo ci) {
        if (methodNode.tryCatchBlocks.isEmpty()) {
            return null;
        }

        InsnList insnList = methodNode.instructions;
        Instruction pc = frame.getPC();
        int pcIndex = insnList.indexOf(pc.node);
        for (TryCatchBlockNode tcb:(List<TryCatchBlockNode>)methodNode.tryCatchBlocks) {
            int startIndex = insnList.indexOf(tcb.start);
            int endIndex = insnList.indexOf(tcb.end);
            if (pcIndex >= startIndex && pcIndex < endIndex) {
                if (tcb.type == null) { // finally block
                    return getInstruction(tcb.handler);
                }
                ClassInfo type = frame.resolveClassInfo(tcb.type);
                if (ci.isInstanceOf(type)) {
                    return getInstruction(tcb.handler);
                }
            }
        }

        return null;
    }

    public String toString() {
        return classInfo.toString() + " "  + getName() + " " + getDescriptor();
    }

    public Type[] getArgumentTypes() {
        return argumentTypes;
    }

    public boolean isPublic() {
        return Modifier.isPublic(methodNode.access);
    }
}
