package org.javelus.aotes.asm.vm;

import java.lang.reflect.Modifier;

import org.objectweb.asm.Type;
import org.objectweb.asm.tree.FieldNode;

public class FieldInfo {

    private ClassInfo classInfo;
    private FieldNode fieldNode;
    private Type type;
    private String typeDescriptor;
    public FieldInfo(ClassInfo classInfo, FieldNode fieldNode) {
        this.classInfo = classInfo;
        this.fieldNode = fieldNode;
        type = Type.getType(fieldNode.desc);
    }

    public boolean isStatic() {
        return Modifier.isStatic(fieldNode.access);
    }

    public Type getType() {
        return type;
    }

    public String getTypeDescriptor() {
        if (typeDescriptor == null) {
            typeDescriptor = getType().getDescriptor();
        }
        return typeDescriptor;
    }

    public int getSize() {
        return getType().getSize();
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }


    public String getClassName() {
        return getClassInfo().getName();
    }


    public String getName() {
        return fieldNode.name;
    }

    public String getDescriptor() {
        return fieldNode.desc;
    }

    public String getFullName() {
        return (fieldNode.name + fieldNode.desc);
    }

    public boolean isReference() {
        char code = fieldNode.desc.charAt(0);
        return code == 'L' || code == '[';
    }

    public boolean isOverriden() {
        ClassInfo superClass = classInfo.getSuperClass();
        if (superClass != null && superClass.getField(getName()) != null) {
            return true;
        }
        return false;
    }

    public String toString() {
        return classInfo.toString() + " "  + getName() + " " + getDescriptor();
    }
}
