package org.javelus.aotes.asm.vm;

import org.javelus.aotes.asm.value.Value;

public class MultiArrayElementInfo extends ArrayElementInfo {

    Value[] arrayLengths;

    int dimension;

    public MultiArrayElementInfo(ClassInfo classInfo, Value[] arrayLengths, int dimension) {
        super(classInfo);
        this.arrayLengths = arrayLengths;
        this.dimension = dimension;
    }

}
