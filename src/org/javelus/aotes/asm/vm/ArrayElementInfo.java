package org.javelus.aotes.asm.vm;

import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.ArrayLength;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.value.Value;

public class ArrayElementInfo extends ElementInfo implements Cloneable {

    public ArrayElementInfo(ClassInfo classInfo) {
        super(classInfo);
    }

    private ArrayLength arrayLength;

    public ArrayLength arrayLength() {
        if (arrayLength == null) {
            arrayLength = new ArrayLength(this);
            newValue(arrayLength);
        }
        return arrayLength;
    }

    public ArrayElement arrayElement(Value index) {
        ArrayElement location = (ArrayElement) findHeapLocation(index);
        if (location == null) {
            location = new ArrayElement(this, index);
            addHeapLocation(index, location);
        }
        return location;
    }

    public ArrayElementInfo clone() {
        ArrayElementInfo aei = (ArrayElementInfo) super.clone();
        if (arrayLength != null) {
            aei.arrayLength = new ArrayLength(aei);
            aei.arrayLength.value(this.arrayLength.value());
        }
        for (HeapLocation hl:locations()) {
            ArrayElement ae = (ArrayElement) hl;
            aei.arrayElement(ae.getIndex()).value(ae.value());
        }
        return aei;
    }

    public final boolean isArray() {
        return true;
    }


    public boolean isReferenceArray() {
        char code = getClassInfo().getComponentClassInfo().getTypeDescriptor().charAt(0);
        return code == 'L' || code == '[';
    }
}
