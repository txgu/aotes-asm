package org.javelus.aotes.asm.vm;

import org.javelus.aotes.asm.value.Value;

public class ValueStackFrame extends StackFrame<Value> {

    public ValueStackFrame(MethodInfo mi) {
        super(mi);
    }

    @Override
    protected Value[] createLocalsAndExpressionStack(int length) {
        return new Value[length];
    }

}
