package org.javelus.aotes.asm.vm;

import org.javelus.aotes.asm.location.StaticField;

public class StaticElementInfo extends ElementInfo {

    public StaticElementInfo(ClassInfo classInfo) {
        super(classInfo);
        setAllocPre();
    }

    public StaticField staticField(FieldInfo field) {
        StaticField location = (StaticField) findHeapLocation(field);
        if (location == null) {
            location = new StaticField(this, field);
            addHeapLocation(field, location);
        }
        return location;
    }

    public final boolean isStatic() {
        return true;
    }
}
