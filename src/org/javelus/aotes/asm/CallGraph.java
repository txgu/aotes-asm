package org.javelus.aotes.asm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.javelus.aotes.asm.utils.MapUtils;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ClassLoaderInfo;
import org.javelus.aotes.asm.vm.Instruction;
import org.javelus.aotes.asm.vm.MethodInfo;

public class CallGraph {

    public static Map<MethodInfo, Set<MethodInfo>> unsatisfiedNativeMethods = new HashMap<MethodInfo, Set<MethodInfo>>();

    public static void addUnsatisfiedNativeMethod(MethodInfo theNative, MethodInfo entry) {
        MapUtils.addToMapSet(unsatisfiedNativeMethods, theNative, entry);
    }

    public static void printUnsatisfiedNativeMethods() {
        for (Entry<MethodInfo, Set<MethodInfo>> entry:unsatisfiedNativeMethods.entrySet()) {
            MethodInfo theNative = entry.getKey();
            if (theNative.getName().equals("fillInStackTrace")) {
                continue;
            }
            System.out.println("Native method " + theNative + " is called during evaluting the following methods:");
            for (MethodInfo entryMethod:entry.getValue()) {
                System.out.println("    " + entryMethod);
            }
        }
    }

    public static Map<MethodInfo, Set<MethodInfo>> getUnsatisfiedNativeMethods() {
        return unsatisfiedNativeMethods;
    }

    static Map<MethodInfo, Map<Instruction, Set<MethodInfo>>> callerToCallees = new HashMap<MethodInfo, Map<Instruction, Set<MethodInfo>>>();;
    static Map<MethodInfo, Set<MethodInfo>> calleeToCallers = new HashMap<MethodInfo, Set<MethodInfo>>();;

    static void initialize(String classPath) throws IOException {
        List<String> classFileNames = collectClassFileNames(classPath);
        for (String classFile : classFileNames) {
            String className = classFile.replace(File.separatorChar, '/').substring(0, classFile.lastIndexOf('.'));
            ClassInfo ci = ClassLoaderInfo.getResolvedClassInfoOrNull(className);
            if (ci == null) {
                continue;
            }
            collectCallGraphEdges(ci);
        }
    }

    private static void collectCallGraphEdges(ClassInfo ci) {
        for (MethodInfo mi:ci.getDeclaredMethods()) {
            collectCallGraphEdges(mi);
        }
    }

    private static void collectCallGraphEdges(MethodInfo caller) {
        if (caller.isNative() || caller.isAbstract()) {
            return;
        }
        Instruction insn = caller.getFirstInstruction();
        while (insn != null) {
            if (insn.isInvoke()) {
                String owner = insn.getMethodOwner();
                String name = insn.getMethodName();
                String desc = insn.getMethodDesc();
                MethodInfo callee = resolveMethod(owner, name, desc);
                if (callee != null) {
                    addEdge(caller, insn, callee);
                }
            }
            insn = insn.getNext();
        }
    }

    public static void addEdge(MethodInfo caller, Instruction insn, MethodInfo callee) {
        MapUtils.addToMapMapSet(callerToCallees, caller, insn, callee);
        MapUtils.addToMapSet(calleeToCallers, callee, caller);
    }

    public static Collection<MethodInfo> getCaller(MethodInfo callee) {
        return calleeToCallers.get(callee);
    }

    private static MethodInfo resolveMethod(String owner, String name, String desc) {
        ClassInfo ci = ClassLoaderInfo.getResolvedClassInfoOrNull(owner);
        if (ci == null) {
            return null;
        }
        return ci.getMethod(name, desc);
    }

    static List<String> collectClassFileNames(String classPath) throws IOException {
        String[] paths = classPath.split(File.pathSeparator);
        List<String> classFileNames = new ArrayList<String>();
        for (String path:paths) {
            path = path.trim();
            if (path.isEmpty()) {
                continue;
            }

            File file = new File(path);
            if (file.isDirectory()) {
                collectClassFileNamesInDirectory(Paths.get(file.toURI()), file, classFileNames);
            } else if (file.isFile()) {
                if (file.getName().endsWith(".jar")) {
                    ZipInputStream input = null;
                    ZipEntry entry = null;

                    try {
                        input = new ZipInputStream(new FileInputStream(file));
                        while((entry = input.getNextEntry()) != null){
                            if(!entry.isDirectory() && entry.getName().endsWith(".class")){
                                classFileNames.add(entry.getName());
                            }
                        }
                    } finally {
                        if(input != null){
                            input.close();
                        }
                    }
                }
            }
        }
        return classFileNames;
    }

    private static void collectClassFileNamesInDirectory(Path root, File folder, List<String> classFileNames) {
        File[] files = folder.listFiles();
        for (File file:files) {
            if (file.isDirectory()) {
                collectClassFileNamesInDirectory(root, file, classFileNames);
            } else if (file.getName().endsWith(".class")) {
                classFileNames.add(root.relativize(Paths.get(file.toURI())).toString());
            }
        }
    }

    static Collection<MethodInfo> getCallers(MethodInfo callee) {
        return null;
    }
}
