package org.javelus.aotes.asm;

public class Options {

    private static final int DEFAULT_BOUND = 100;

    private static int verbose;

    static void setVerbose(int value) {
        verbose = value;
    }

    public static boolean verbose() {
        return verbose > 0;
    }

    public static boolean verbose(int level) {
        return verbose > level;
    }

    public static int verboseLevel() {
        return verbose;
    }

    public static boolean pruneNoAddedFieldClasses() {
        return Boolean.valueOf(System.getProperty("aotes.prune.noaddedfield", "false"));
    }

    public static boolean pruneUnmatchedMethods() {
        return Boolean.valueOf(System.getProperty("aotes.prune.unmatched", "true"));
    }

    public static boolean pruneUnreachableMethods() {
        return Boolean.valueOf(System.getProperty("aotes.prune.unreachable", "true"));
    }

    public static boolean pruneStaticMethods() {
        return Boolean.valueOf(System.getProperty("aotes.prune.static", "true"));
    }

    public static void singleClassMode() {
        System.getProperty("aotes.prune.noaddedfield", "false");
        System.getProperty("aotes.prune.unmatched", "false");
        System.getProperty("aotes.prune.unreachable", "false");
        System.getProperty("aotes.prune.static", "true");
    }

    public static int executionBound() {
        return Integer.getInteger("aotes.execution.bound", DEFAULT_BOUND);
    }

    public static void setExecutionBound(Object value) {
        System.setProperty("aotes.execution.bound", String.valueOf(value));

    }

    public static void setDebug(String type, Object value) {
        System.setProperty("aotes.debug." + type, String.valueOf(value));
    }

    public static boolean debug(String type) {
        return Boolean.valueOf(System.getProperty("aotes.debug." + type, "false"));
    }

    public static boolean oopsla() {
        return Boolean.valueOf(System.getProperty("aotes.oopsla", "false"));
    }

}
