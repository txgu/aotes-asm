package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.LocationPath;

public class Output<T extends Location> extends SymbolicValue {

    public Output(PathCondition pathCondition, LocationPath locationPath, T location, Value value) {
        super(pathCondition);
        this.location = location;
        this.value = value;
        this.locationPath = locationPath;
    }

    private T location;
    private Value value;
    private LocationPath locationPath;

    public T getLocation() {
        return location;
    }

    public Value getValue() {
        return value;
    }

    public LocationPath getLocationPath() {
        return locationPath;
    }

    @Override
    public String getTypeDescriptor() {
        return location.getTypeDescriptor();
    }

    public String toString() {
        return "Output<" + location + ">";
    }
}
