package org.javelus.aotes.asm.value.constant;

import org.javelus.aotes.asm.value.Constant;

public class AddressValue extends Constant {
    private Object address;

    public AddressValue(Object address) {
        this.address = address;
    }

    public Object getAddress() {
        return address;
    }
    
    public String getLiteral() {
        return address.toString();
    }

    @Override
    public String getTypeDescriptor() {
        return "I";
    }
}
