package org.javelus.aotes.asm.value.constant;

import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.value.Constant;

public class IntegerValue extends Constant {
    private int value;

    public IntegerValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    @Override
    public String getTypeDescriptor() {
        return "I";
    }
    
    public String getLiteral() {
        return String.valueOf(value);
    }

    static Map<Integer, IntegerValue> values = new HashMap<Integer, IntegerValue>();

    public static IntegerValue defaultValue = valueOf(0);

    public static IntegerValue zero = defaultValue;

    public static IntegerValue m1 = valueOf(-1);

    public static IntegerValue one = valueOf(1);
    public static IntegerValue two = valueOf(2);
    public static IntegerValue three = valueOf(3);
    public static IntegerValue four = valueOf(4);
    public static IntegerValue five = valueOf(5);

    public static IntegerValue valueOf(int v) {
        IntegerValue value = values.get(v);
        if (value == null) {
            value = new IntegerValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        return "(int)" + getValue();
    }
}
