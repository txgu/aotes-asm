package org.javelus.aotes.asm.value.constant;

import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.value.Constant;

public class LongValue extends Constant {
    private long value;

    public LongValue(long value) {
        this.value = value;
    }

    public long getValue() {
        return this.value;
    }

    @Override
    public String getTypeDescriptor() {
        return "J";
    }
    
    public String getLiteral() {
        return String.valueOf(value);
    }

    static Map<Long, LongValue> values = new HashMap<Long, LongValue>();

    public static LongValue defaultValue = valueOf(0L);

    public static LongValue zero = defaultValue;
    public static LongValue one = valueOf(1L);


    public static LongValue valueOf(long v) {
        LongValue value = values.get(v);
        if (value == null) {
            value = new LongValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        return "(long)" + getValue() + 'L';
    }
}
