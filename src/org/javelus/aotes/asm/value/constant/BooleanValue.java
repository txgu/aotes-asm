package org.javelus.aotes.asm.value.constant;

import org.javelus.aotes.asm.value.Constant;

public class BooleanValue extends Constant {
    private boolean value;

    private BooleanValue(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return this.value;
    }

    @Override
    public String getTypeDescriptor() {
        return "Z";
    }

    public String getLiteral() {
        return String.valueOf(value);
    }
    
    public static BooleanValue TRUE = new BooleanValue(true);
    public static BooleanValue FALSE = new BooleanValue(false);
    public static BooleanValue defaultValue = FALSE;

    public String toString() {
        return "(boolean)" + getValue();
    }
}
