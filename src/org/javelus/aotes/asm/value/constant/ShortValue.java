package org.javelus.aotes.asm.value.constant;

import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.value.Constant;

public class ShortValue extends Constant {
    private short value;

    public ShortValue(short value) {
        this.value = value;
    }

    public short getValue() {
        return this.value;
    }

    @Override
    public String getTypeDescriptor() {
        return "S";
    }

    public String getLiteral() {
        return String.valueOf(value);
    }
    
    static Map<Short, ShortValue> values = new HashMap<Short, ShortValue>();

    public static ShortValue defaultValue = new ShortValue((short) 0);

    public static ShortValue valueOf(short v) {
        ShortValue value = values.get(v);
        if (value == null) {
            value = new ShortValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        return "(short)" + getValue();
    }
}
