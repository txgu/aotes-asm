package org.javelus.aotes.asm.value.constant;

import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.value.Constant;

public class FloatValue extends Constant {


    private float value;

    public FloatValue(float value) {
        this.value = value;
    }

    public float getValue() {
        return this.value;
    }

    @Override
    public String getTypeDescriptor() {
        return "F";
    }

    public String getLiteral() {
        return String.valueOf(value);
    }
    
    static Map<Float, FloatValue> values = new HashMap<Float, FloatValue>();

    public static FloatValue defaultValue = valueOf(0.0F);

    public static FloatValue zero = valueOf(0.0F);

    public static FloatValue one = valueOf(1.0F);

    public static FloatValue two = valueOf(2.0F);

    public static FloatValue valueOf(float v) {
        FloatValue value = values.get(v);
        if (value == null) {
            value = new FloatValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        if (Float.isNaN(getValue())) {
            return "Float.NaN";
        }
        if (Float.isInfinite(getValue())) {
            return String.format("Float.intBitsToFloat(%d)", Float.floatToIntBits(getValue()));
        }
        return "(float)" + getValue() + 'F';
    }
}
