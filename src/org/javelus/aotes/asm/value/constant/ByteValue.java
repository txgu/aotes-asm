package org.javelus.aotes.asm.value.constant;

import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.value.Constant;

public class ByteValue extends Constant {
    private byte value;

    public ByteValue(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return this.value;
    }

    public String getLiteral() {
        return String.valueOf(value);
    }

    @Override
    public String getTypeDescriptor() {
        return "B";
    }

    static Map<Byte, ByteValue> values = new HashMap<Byte, ByteValue>();

    public static ByteValue defaultValue = new ByteValue((byte) 0);

    public static ByteValue valueOf(byte v) {
        ByteValue value = values.get(v);
        if (value == null) {
            value = new ByteValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        return "(byte)" + getValue();
    }
}
