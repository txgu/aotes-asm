package org.javelus.aotes.asm.value.constant;

import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.value.Constant;

public class DoubleValue extends Constant {
    double value;

    public DoubleValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }

    @Override
    public String getTypeDescriptor() {
        return "D";
    }

    public String getLiteral() {
        return String.valueOf(value);
    }
    
    @Override
    public int hashCode() {
        long bits = Double.doubleToLongBits(value);
        return (int) (bits ^ (bits >>> 32));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DoubleValue other = (DoubleValue) obj;
        if (Double.doubleToLongBits(value) != Double
                .doubleToLongBits(other.value))
            return false;
        return true;
    }

    static Map<Double, DoubleValue> values = new HashMap<Double, DoubleValue>();

    public static DoubleValue defaultValue = valueOf(0.0D);

    public static DoubleValue zero = defaultValue;

    public static DoubleValue one = valueOf(1.0D);

    public static DoubleValue valueOf(double v) {
        DoubleValue value = values.get(v);
        if (value == null) {
            value = new DoubleValue(v);
            values.put(v, value);
        }
        return value;
    }

    public String toString() {
        if (Double.isNaN(getValue())) {
            return "Double.NaN";
        }
        if (Double.isInfinite(getValue())) {
            return String.format("Double.longBitsToDouble(%dL)", Double.doubleToLongBits(getValue()));
        }
        return "(double)" + getValue() + 'D';
    }
}
