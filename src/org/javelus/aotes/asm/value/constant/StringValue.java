package org.javelus.aotes.asm.value.constant;

import org.javelus.aotes.asm.value.Constant;


public class StringValue extends Constant {

    private String value;

    public StringValue(String value) {
        this.value = escape(value);
    }

    public String getValue() {
        return value;
    }

    public static StringValue valueOf(String s){
        return new StringValue(s);
    }
    
    public String getLiteral() {
        return value;
    }

    @Override
    public String getTypeDescriptor() {
        return "Ljava/lang/String;";
    }

    public static String escape(String value) {
        return value.replace("\\", "\\\\").replace("\n", "\\n").replace("\"", "\\\"").replace("\r", "\\r");
    }

    public String toString() {
        return "\"" + getValue() + "\"";
    }
}
