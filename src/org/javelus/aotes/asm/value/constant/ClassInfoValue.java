package org.javelus.aotes.asm.value.constant;

import org.javelus.aotes.asm.value.Constant;
import org.javelus.aotes.asm.vm.ClassInfo;

public class ClassInfoValue extends Constant {
    private ClassInfo ci;

    public ClassInfoValue(ClassInfo ci) {
        this.ci = ci;
    }

    public ClassInfo getClassInfo() {
        return ci;
    }

    public String getLiteral() {
        return ci.getName();
    }
    
    public static ClassInfoValue valueOf(ClassInfo ci) {
        return new ClassInfoValue(ci);
    }

    public String getTypeDescriptor() {
        return "Ljava/lang/Class;";
    }

    public String toString() {
        return ci.getName().replace('$', '.') + ".class";
    }
}
