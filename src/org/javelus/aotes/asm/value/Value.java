package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.value.constant.BooleanValue;
import org.javelus.aotes.asm.value.constant.ByteValue;
import org.javelus.aotes.asm.value.constant.CharValue;
import org.javelus.aotes.asm.value.constant.ClassInfoValue;
import org.javelus.aotes.asm.value.constant.DoubleValue;
import org.javelus.aotes.asm.value.constant.FloatValue;
import org.javelus.aotes.asm.value.constant.IntegerValue;
import org.javelus.aotes.asm.value.constant.LongValue;
import org.javelus.aotes.asm.value.constant.ShortValue;
import org.javelus.aotes.asm.value.constant.StringValue;
import org.javelus.aotes.asm.vm.ElementInfo;

public abstract class Value {

    /**
     * Null Pointer
     */
    public static final Constant NULL = new Constant() {

        @Override
        public String getTypeDescriptor() {
            return "null";
        }

        public String toString() {
            return "NULL";
        }
        
        public String getLiteral() {
            return "null";
        }

    };

    public static Value getDefaultValue(String typeDescriptor) {
        Value v = null;
        if (typeDescriptor.equals("I")) {
            v = IntegerValue.defaultValue;
        } else if(typeDescriptor.equals("C")) {
            v = CharValue.defaultValue;
        } else if(typeDescriptor.equals("S")) {
            v = ShortValue.defaultValue;
        } else if(typeDescriptor.equals("Z")) {
            v = BooleanValue.defaultValue;
        } else if(typeDescriptor.equals("B")) {
            v = ByteValue.defaultValue;
        } else if(typeDescriptor.equals("J")) {
            v = LongValue.defaultValue;
        } else if (typeDescriptor.equals("D")) {
            v = DoubleValue.defaultValue;
        } else if (typeDescriptor.equals("F")) {
            v = FloatValue.defaultValue;
        } else if (typeDescriptor.equals("V")) {
            throw new RuntimeException("Sanity check failed");
        } else {
            v = Value.NULL;
        }

        return v;
    }

    public static ElementInfo getElementInfo(Value v) {
        if (v instanceof Ref) {
            return ((Ref)v).getElementInfo();
        }

        if (v instanceof Input) {
            return ((Input<?>) v).getPreallocated();
        }

        if (v instanceof OperatorValue) {
            return null;
        }

        if (v instanceof StringValue || v instanceof ClassInfoValue) {
            return null;
        }

        if (v == NULL) {
            return null;
        }

        throw new RuntimeException("Invalid value type " + v);
    }

    public abstract String getTypeDescriptor();

    public String toString() {
        return getClass().getSimpleName();
    }
}
