package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.location.HeapLocation;

public class HeapLocationValue<T extends HeapLocation> extends Value {

    T location;
    Value value;

    public HeapLocationValue(T location, Value value) {
        this.location = location;
        this.value = value;
    }

    public T getLocation() {
        return location;
    }

    public Value getValue() {
        return value;
    }

    @Override
    public String getTypeDescriptor() {
        return location.getTypeDescriptor();
    }

}
