package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.vm.Instruction;

public class BinopExpr extends InsnExpr {

    private Value left;
    private Value right;

    public BinopExpr(PathCondition pathCondition, Instruction instruction, Value left, Value right) {
        super(pathCondition, instruction);
        this.left = left;
        this.right = right;
    }

    public Value getLeftOperand() {
        return left;
    }

    public Value getRightOperand() {
        return right;
    }
}
