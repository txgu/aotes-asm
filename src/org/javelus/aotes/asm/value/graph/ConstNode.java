package org.javelus.aotes.asm.value.graph;

public class ConstNode extends ValueNode {

    /**
     * 
     */
    private static final long serialVersionUID = -4704729392806033833L;
    ConstType type;
    String literal;
    public ConstNode(ConstType type, String literal) {
        super();
        this.type = type;
        this.literal = literal;
    }
    
    
}
