package org.javelus.aotes.asm.value.graph;

public class InstanceFieldNode extends LocationNode {

    /**
     * 
     */
    private static final long serialVersionUID = 5769548122717257960L;
    ValueNode container;
    FieldID field;
    public InstanceFieldNode(ValueNode container, FieldID field) {
        super();
        this.container = container;
        this.field = field;
    }

    public ValueNode getContainer() {
        return container;
    }


}
