package org.javelus.aotes.asm.value.graph;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class ValueGraph implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4296205553146856995L;
    Set<ValueNode> nodes;
    Map<ValueNode, Set<ValueNode>> nodeToSuccs;
    Map<ValueNode, Set<ValueNode>> nodeToPreds;

    Set<ValueNode> heads;
    Set<ValueNode> tails;
    MethodID methodID;
    public ValueGraph(MethodID methodID, Set<ValueNode> nodes, Map<ValueNode, Set<ValueNode>> nodeToSuccs,
            Map<ValueNode, Set<ValueNode>> nodeToPreds, Set<ValueNode> heads, Set<ValueNode> tails) {
        super();
        this.methodID = methodID;
        this.nodes = nodes;
        this.nodeToSuccs = nodeToSuccs;
        this.nodeToPreds = nodeToPreds;
        this.heads = heads;
        this.tails = tails;
    }

    public Set<ValueNode> getNodes() {
        return nodes;
    }

    public Set<ValueNode> getSuccs(ValueNode node) {
        return nodeToSuccs.get(node);
    }

    public Set<ValueNode> getPreds(ValueNode node) {
        return nodeToPreds.get(node);
    }

    public Set<ValueNode> getHeads() {
        return heads;
    }

    public Set<ValueNode> getTails() {
        return tails;
    }

}
