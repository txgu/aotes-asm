package org.javelus.aotes.asm.value.graph;

public class OperatorValueNode extends ValueNode {

    /**
     * 
     */
    private static final long serialVersionUID = -2499655354827518071L;


    ValueNode receiver;
    ValueNode previous;
    ValueNode[] operands;

    public OperatorValueNode(ValueNode receiver, ValueNode previous, ValueNode[] operands) {
        super();
        this.receiver = receiver;
        this.previous = previous;
        this.operands = operands;
    }


}
