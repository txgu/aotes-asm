package org.javelus.aotes.asm.value.graph;

public enum ConstType {
    NULL,
    THIS,
    BYTE,
    BOOLEAN,
    CHAR,
    SHORT,
    INT,
    FLOAT,
    LONG,
    DOUBLE,
    STRING,
    CLASS,
}
