package org.javelus.aotes.asm.value.graph;

public class NewNode extends ValueNode {
    /**
     * 
     */
    private static final long serialVersionUID = 4881143654703286490L;
    String className;

    public NewNode(String className) {
        super();
        this.className = className;
    }


}
