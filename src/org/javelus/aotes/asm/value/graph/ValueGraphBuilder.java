package org.javelus.aotes.asm.value.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.InstanceField;
import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.MethodParameterLocation;
import org.javelus.aotes.asm.location.StaticField;
import org.javelus.aotes.asm.location.TargetLocation;
import org.javelus.aotes.asm.summary.ExecutionSummary;
import org.javelus.aotes.asm.trace.IfFalseTraceNode;
import org.javelus.aotes.asm.trace.IfTrueTraceNode;
import org.javelus.aotes.asm.trace.SwitchTraceNode;
import org.javelus.aotes.asm.trace.Trace;
import org.javelus.aotes.asm.trace.TraceNode;
import org.javelus.aotes.asm.utils.MapUtils;
import org.javelus.aotes.asm.value.BinopExpr;
import org.javelus.aotes.asm.value.Constant;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.NewRef;
import org.javelus.aotes.asm.value.OperatorValue;
import org.javelus.aotes.asm.value.Output;
import org.javelus.aotes.asm.value.TargetRef;
import org.javelus.aotes.asm.value.UnopExpr;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.value.constant.BooleanValue;
import org.javelus.aotes.asm.value.constant.ByteValue;
import org.javelus.aotes.asm.value.constant.CharValue;
import org.javelus.aotes.asm.value.constant.ClassInfoValue;
import org.javelus.aotes.asm.value.constant.DoubleValue;
import org.javelus.aotes.asm.value.constant.FloatValue;
import org.javelus.aotes.asm.value.constant.IntegerValue;
import org.javelus.aotes.asm.value.constant.LongValue;
import org.javelus.aotes.asm.value.constant.ShortValue;
import org.javelus.aotes.asm.value.constant.StringValue;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.MethodInfo;


public class ValueGraphBuilder {

    MethodID getMethodID(MethodInfo mi) {
        MethodID mid = methodInfoToMethodID.get(mi);
        if (mid != null) {
            return mid;
        }
        mid = new MethodID(mi.getClassName(), mi.getName(), mi.getDescriptor());
        methodInfoToMethodID.put(mi, mid);
        return mid;
    }

    FieldID getFieldID(FieldInfo fi) {
        FieldID fid = fieldInfoToFieldID.get(fi);
        if (fid != null) {
            return fid;
        }
        fid = new FieldID(fi.getClassName(), fi.getName(), fi.getDescriptor());
        fieldInfoToFieldID.put(fi, fid);
        return fid;
    }

    void addPredViaLocation(ValueNode node, LocationNode locNode) {
        if (locNode instanceof InstanceFieldNode) {
            InstanceFieldNode ifn = (InstanceFieldNode) locNode;
            addPred(node, ifn.getContainer());
        } else if (locNode instanceof ArrayElementNode) {
            ArrayElementNode aen = (ArrayElementNode) locNode;
            addPred(node, aen.getContainer());
            addPred(node, aen.getIndex());
        }
    }

    @SuppressWarnings("unchecked")
    ValueNode getValueNode(Value value) {
        ValueNode valueNode = valueToValueNode.get(value);
        if (valueNode != null) {
            return valueNode;
        }
        if (value instanceof Output) {
            Output<? extends Location> output = (Output<? extends Location>) value;
            Location loc = output.getLocation();
            Value locValue = output.getValue();
            LocationNode locNode = getLocationNode(loc);
            ValueNode locValueNode = getValueNode(locValue);
            valueNode = new OutputNode(locNode, locValueNode);
            addPred(valueNode, locValueNode);
        } else if (value instanceof Input) {
            Input<? extends Location> input = (Input<? extends Location>) value;
            Location loc = input.getLocation();
            LocationNode locNode = getLocationNode(loc);
            valueNode = new InputNode(locNode);
        } else if (value instanceof Constant) {
            ConstType type;
            if (value == Value.NULL) {
                type = ConstType.NULL;
            } else if (value instanceof ClassInfoValue) {
                type = ConstType.CLASS;
            } else if (value instanceof StringValue) {
                type = ConstType.STRING;
            } else if (value instanceof ByteValue) {
                type = ConstType.BYTE;
            } else if (value instanceof BooleanValue) {
                type = ConstType.BOOLEAN;
            } else if (value instanceof CharValue) {
                type = ConstType.CHAR;
            } else if (value instanceof ShortValue) {
                type = ConstType.SHORT;
            } else if (value instanceof IntegerValue) {
                type = ConstType.INT;
            } else if (value instanceof FloatValue) {
                type = ConstType.FLOAT;
            } else if (value instanceof LongValue) {
                type = ConstType.LONG;
            } else if (value instanceof DoubleValue) {
                type = ConstType.DOUBLE;
            } else {
                throw new RuntimeException("Unknown constant type: " + value);
            }
            String literal = ((Constant) value).getLiteral();
            valueNode = new ConstNode(type, literal);
        } else if (value instanceof NewRef) {
            NewRef nr = (NewRef) value;
            ElementInfo ei = nr.getElementInfo();
            if (ei.isArray()) {
                ValueNode lengthNode = getValueNode(ei.arrayLength().value());
                valueNode = new NewArrayNode(ei.getClassInfo().getName(), lengthNode);
                addPred(valueNode, lengthNode);
            } else {
                valueNode = new NewNode(ei.getClassInfo().getName());
            }
        } else if (value instanceof TargetRef) {
            valueNode = new ConstNode(ConstType.THIS, "this");
        } else if (value instanceof BinopExpr) {
            BinopExpr be = (BinopExpr) value;
            int opcode = be.getInstruction().getOpcode();
            Value v1 = be.getLeftOperand();
            Value v2 = be.getRightOperand();
            ValueNode vn1 = getValueNode(v1);
            ValueNode vn2 = getValueNode(v2);
            valueNode = new BinaryExprNode(opcode, vn1, vn2);
            addPred(valueNode, vn1);
            addPred(valueNode, vn2);
        } else if (value instanceof UnopExpr) {
            UnopExpr ue = (UnopExpr) value;
            int opcode = ue.getInstruction().getOpcode();
            Value v = ue.getOperand();
            ValueNode vn = getValueNode(v);
            valueNode = new UnaryExprNode(opcode, vn);
            addPred(valueNode, vn);
        } else if (value instanceof OperatorValue) {
            OperatorValue ov = (OperatorValue) value;
            ValueNode prevNode = null;
            if (ov.getPrevious() != null) {
                prevNode = getValueNode(ov.getPrevious());
            }
            ValueNode recvNode = null;
            if (ov.getReceiverValue() != null) {
                recvNode = getValueNode(ov.getReceiverValue());
            }
            Value[] ops = ov.getOperands();
            ValueNode[] opNodes = new ValueNode[ops.length];
            valueNode = new OperatorValueNode(prevNode, recvNode, opNodes);
            for (int i = 0; i < ops.length; i++) {
                opNodes[i] = getValueNode(ops[i]);
                addPred(valueNode, opNodes[i]);
            }
            if (recvNode != null) {
                addPred(valueNode, recvNode);
            }
            if (prevNode != null) {
                addPred(valueNode, prevNode);
            }
        } else {
            throw new RuntimeException("Unknown value type: " + value);
        }
        if (valueToValueNode.put(value, valueNode) != null) {
            throw new RuntimeException("Circular dependencies detected!");
        }
        return valueNode;
    }

    LocationNode getLocationNode(Location loc) {
        LocationNode locNode = locationToLocationNode.get(loc);
        if (locNode != null) {
            return locNode;
        }
        if (loc instanceof TargetLocation) {
            locNode = new ThisNode(getMethodID(es.getEntryMethod()));
        } else if (loc instanceof MethodParameterLocation) {
            MethodParameterLocation mpl = (MethodParameterLocation) loc;
            locNode = new ParameterNode(mpl.getIndex(), getMethodID(mpl.getMethodInfo()));
        } else if (loc instanceof InstanceField) {
            InstanceField iF = (InstanceField) loc;
            Value ref = iF.getContainer().getReference();
            FieldInfo fi = iF.getFieldInfo();
            ValueNode refNode = getValueNode(ref);
            locNode = new InstanceFieldNode(refNode, getFieldID(fi));
        } else if (loc instanceof StaticField) {
            locNode = new StaticFieldNode(getFieldID(((StaticField) loc).getFieldInfo()));
        } else if (loc instanceof ArrayElement) {
            ArrayElement ae = (ArrayElement) loc;
            Value ref = ae.getContainer().getReference();
            Value index = ae.getIndex();
            ValueNode refNode = getValueNode(ref);
            ValueNode indexNode = getValueNode(index);
            locNode = new ArrayElementNode(refNode, indexNode);
        } else {
            throw new RuntimeException();
        }
        locationToLocationNode.put(loc, locNode);
        return locNode;
    }

    void addPred(ValueNode node, ValueNode pred) {
        MapUtils.addToMapSet(nodeToPreds, node, pred);
        MapUtils.addToMapSet(nodeToSuccs, pred, node);
    }

    Map<Value, ValueNode> valueToValueNode = new HashMap<Value, ValueNode>();
    Map<Location, LocationNode> locationToLocationNode = new HashMap<Location, LocationNode>();
    Map<MethodInfo, MethodID> methodInfoToMethodID = new HashMap<MethodInfo, MethodID>();
    Map<FieldInfo, FieldID> fieldInfoToFieldID = new HashMap<FieldInfo, FieldID>();
    Map<ValueNode, Set<ValueNode>> nodeToSuccs;
    Map<ValueNode, Set<ValueNode>> nodeToPreds;

    ExecutionSummary es;
    public ValueGraphBuilder(ExecutionSummary es) {
        this.es = es;
    }

    public ValueGraph build() {
        nodeToSuccs = new HashMap<ValueNode, Set<ValueNode>>();
        nodeToPreds = new HashMap<ValueNode, Set<ValueNode>>();
        Set<ValueNode> nodes = new HashSet<ValueNode>();

        // 1). add all input
        for (Input<? extends Location> input:es.getUsedUnknownValues()) {
            getValueNode(input);
        }

        // 2). add all output
        for (Output<? extends HeapLocation> output:es.getPostHeapValues()) {
            getValueNode(output);
        }

        nodes.addAll(valueToValueNode.values());
        // 3). add all predicates
        Trace trace = es.getTrace();
        TraceNode tn = trace.getEnd();
        while (tn != null) {
            Value v1 = null;
            Value v2 = null;
            int opcode = -1;
            int branch = -1;
            if (tn instanceof IfFalseTraceNode) {
                IfFalseTraceNode iftn = (IfFalseTraceNode) tn;
                branch = 0;
                opcode = iftn.getInstruction().getOpcode();
                v1 = iftn.getLeftValue();
                v2 = iftn.getRightValue();
                ValueNode vn1 = getValueNode(v1);
                ValueNode vn2 = getValueNode(v2);
                AssertNode node = new AssertNode(vn1, vn2, opcode, branch);
                nodes.add(node);
                addPred(node, vn1);
                addPred(node, vn2);
            } else if (tn instanceof IfTrueTraceNode) {
                IfTrueTraceNode ittn = (IfTrueTraceNode) tn;
                branch = 1;
                opcode = ittn.getInstruction().getOpcode();
                v1 = ittn.getLeftValue();
                v2 = ittn.getRightValue();
                ValueNode vn1 = getValueNode(v1);
                ValueNode vn2 = getValueNode(v2);
                AssertNode node = new AssertNode(vn1, vn2, opcode, branch);
                nodes.add(node);
                addPred(node, vn1);
                addPred(node, vn2);
            } else if (tn instanceof SwitchTraceNode) {
                SwitchTraceNode stn = (SwitchTraceNode) tn;
                branch = stn.getBranch();
                opcode = stn.getInstruction().getOpcode();
                v1 = stn.getValue();
                ValueNode vn = getValueNode(v1);
                AssertNode node = new AssertNode(vn, vn, opcode, branch);
                nodes.add(node);
                addPred(node, vn);
            }
            tn = tn.getPrevious();
        }

        Set<ValueNode> heads = new HashSet<ValueNode>();
        Set<ValueNode> tails = new HashSet<ValueNode>();

        for (ValueNode node : nodes) {
            Set<ValueNode> succs = nodeToSuccs.get(node);
            if (succs == null) {
                tails.add(node);
                nodeToSuccs.put(node, Collections.emptySet());
            }
            Set<ValueNode> preds = nodeToPreds.get(node);
            if (preds == null) {
                heads.add(node);
                nodeToPreds.put(node, Collections.emptySet());
            }
        }

        ValueGraph vg = new ValueGraph(getMethodID(es.getEntryMethod()), 
                nodes, nodeToSuccs, nodeToPreds, heads, tails);
        return vg;
    }
}
