package org.javelus.aotes.asm.value.graph;

public class InputNode extends ValueNode {
    /**
     * 
     */
    private static final long serialVersionUID = -3425066659597912859L;
    LocationNode location;

    public InputNode(LocationNode location) {
        super();
        this.location = location;
    }
    
    
}
