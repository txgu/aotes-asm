package org.javelus.aotes.asm.value.graph;

public class NewArrayNode extends NewNode {

    /**
     * 
     */
    private static final long serialVersionUID = 4685767443902259958L;

    ValueNode length;

    public NewArrayNode(String className, ValueNode length) {
        super(className);
        this.length = length;
    }


}
