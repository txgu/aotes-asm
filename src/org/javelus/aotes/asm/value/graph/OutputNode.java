package org.javelus.aotes.asm.value.graph;

public class OutputNode extends ValueNode {
    /**
     * 
     */
    private static final long serialVersionUID = -7621766659728845163L;
    LocationNode location;
    ValueNode value;
    public OutputNode(LocationNode location, ValueNode value) {
        super();
        this.location = location;
        this.value = value;
    }


}
