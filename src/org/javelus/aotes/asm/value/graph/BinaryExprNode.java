package org.javelus.aotes.asm.value.graph;

public class BinaryExprNode extends ExprNode {

    /**
     * 
     */
    private static final long serialVersionUID = -4236032472426016951L;
    ValueNode value1;
    ValueNode value2;
    
    public BinaryExprNode(int opcode, ValueNode value1, ValueNode value2) {
        super(opcode);
        this.value1 = value1;
        this.value2 = value2;
    }

}
