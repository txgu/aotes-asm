package org.javelus.aotes.asm.value.graph;

public class AssertNode extends ValueNode {
    /**
     * 
     */
    private static final long serialVersionUID = 4817092792040349735L;
    ValueNode value1;
    ValueNode value2;
    int opcode;
    int branch;

    public AssertNode(ValueNode value1, ValueNode value2, int opcode, int branch) {
        super();
        this.value1 = value1;
        this.value2 = value2;
        this.opcode = opcode;
        this.branch = branch;
    }


}
