package org.javelus.aotes.asm.value.graph;

public class FieldID {
    public final String declaringClass;
    public final String name;
    public final String desc;
    public FieldID(String declaringClass, String name, String desc) {
        super();
        this.declaringClass = declaringClass;
        this.name = name;
        this.desc = desc;
    }
    
    
}
