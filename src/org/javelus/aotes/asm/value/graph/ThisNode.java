package org.javelus.aotes.asm.value.graph;

public class ThisNode extends LocationNode {
    /**
     * 
     */
    private static final long serialVersionUID = 8864791368123747077L;
    /**
     * 
     */
    MethodID method;

    public ThisNode(MethodID method) {
        super();
        this.method = method;
    }

}
