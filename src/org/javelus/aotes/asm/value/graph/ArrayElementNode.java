package org.javelus.aotes.asm.value.graph;

public class ArrayElementNode extends LocationNode {
    /**
     * 
     */
    private static final long serialVersionUID = -3282435028917901904L;
    /**
     * Can only be Input or Output
     */
    public final ValueNode container;
    public final ValueNode index;
    public ArrayElementNode(ValueNode container, ValueNode index) {
        super();
        this.container = container;
        this.index = index;
    }

    public ValueNode getContainer() {
        return container;
    }

    public ValueNode getIndex() {
        return index;
    }

}
