package org.javelus.aotes.asm.value.graph;

public class UnaryExprNode extends ExprNode {


    /**
     * 
     */
    private static final long serialVersionUID = -2793425334618836405L;
    ValueNode value;

    public UnaryExprNode(int opcode, ValueNode value) {
        super(opcode);
        this.value = value;
    }
}
