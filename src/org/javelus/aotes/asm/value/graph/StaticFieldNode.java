package org.javelus.aotes.asm.value.graph;

public class StaticFieldNode extends LocationNode {
    /**
     * 
     */
    private static final long serialVersionUID = -2683015895139409219L;

    FieldID field;

    public StaticFieldNode(FieldID field) {
        super();
        this.field = field;
    }


}
