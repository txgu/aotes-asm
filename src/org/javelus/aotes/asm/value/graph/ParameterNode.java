package org.javelus.aotes.asm.value.graph;

public class ParameterNode extends LocationNode {
    /**
     * 
     */
    private static final long serialVersionUID = 6445206221819033L;
    int index;
    MethodID method;
    public ParameterNode(int index, MethodID method) {
        super();
        this.index = index;
        this.method = method;
    }
}
