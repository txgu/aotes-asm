package org.javelus.aotes.asm.value;


public interface PathCondition {
    /**
     * Get previous condition
     * @return
     */
    PathCondition getCondition();
    boolean isConditional();
}
