package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.vm.ClassInfo;

public class InstanceOfOperator implements Operator {

    ClassInfo target;
    public InstanceOfOperator(ClassInfo target) {
        this.target = target;
    }

    public ClassInfo getTarget() {
        return target;
    }

    @Override
    public boolean isReversible() {
        return false;
    }

    @Override
    public String getPrintName() {
        return "instanceof";
    }

    @Override
    public boolean isMutator() {
        return false;
    }

    @Override
    public boolean isGetter() {
        return false;
    }

    @Override
    public boolean isPure() {
        return true;
    }

    @Override
    public String getTypeDescriptor() {
        return "Z";
    }

}
