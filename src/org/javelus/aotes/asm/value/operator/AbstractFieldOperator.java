package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.vm.FieldInfo;

public abstract class AbstractFieldOperator implements Operator {

    protected FieldInfo fieldInfo;

    public AbstractFieldOperator(FieldInfo fieldInfo) {
        this.fieldInfo = fieldInfo;
    }

    @Override
    public String getTypeDescriptor() {
        return fieldInfo.getTypeDescriptor();
    }

    @Override
    public boolean isPure() {
        return false;
    }

}
