package org.javelus.aotes.asm.value.operator;

public enum RelationalOperator {

    EQ("=="),
    LT("<"),
    LE("<="),
    GT(">"),
    GE(">="),
    NE("!=");

    String printable;
    String evalName;
    RelationalOperator(String p) {
        this.printable = p;
        this.evalName = name().toLowerCase();
    }

    public String getEvalName() {
        return evalName;
    }

    public String toString() {
        return this.printable;
    }

    public RelationalOperator getOpposite() {
        switch(this) {
        case EQ:
            return NE;
        case NE:
            return EQ;
        case GT:
            return LE;
        case GE:
            return LT;
        case LT:
            return GE;
        case LE:
            return GT;
        }

        throw new RuntimeException();
    }
}
