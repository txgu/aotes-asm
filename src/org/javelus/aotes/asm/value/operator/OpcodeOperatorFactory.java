package org.javelus.aotes.asm.value.operator;

import static org.javelus.aotes.asm.utils.Opcodes.*;
import static org.javelus.aotes.asm.value.operator.RelationalOperator.EQ;
import static org.javelus.aotes.asm.value.operator.RelationalOperator.GE;
import static org.javelus.aotes.asm.value.operator.RelationalOperator.GT;
import static org.javelus.aotes.asm.value.operator.RelationalOperator.LE;
import static org.javelus.aotes.asm.value.operator.RelationalOperator.LT;
import static org.javelus.aotes.asm.value.operator.RelationalOperator.NE;

import org.javelus.aotes.asm.vm.Instruction;



public class OpcodeOperatorFactory {

    private static final boolean aggressiveBinaryOperator = Boolean.valueOf(System.getProperty("aotes.aggressiveBinaryOperator", "true"));

    static OpcodeOperator[] opcodeOperators = new OpcodeOperator[256];

    static OpcodeOperator[] inverseOpcodeOperators = new OpcodeOperator[256];

    static {
        opcodeOperators[_nop] = new OpcodeOperator(_nop, "nop", false);
        opcodeOperators[_aconst_null] = new OpcodeOperator(_aconst_null, "aconst_null", false);
        opcodeOperators[_iconst_m1] = new OpcodeOperator(_iconst_m1, "iconst_m1", false);
        opcodeOperators[_iconst_0] = new OpcodeOperator(_iconst_0, "iconst_0", false);
        opcodeOperators[_iconst_1] = new OpcodeOperator(_iconst_1, "iconst_1", false);
        opcodeOperators[_iconst_2] = new OpcodeOperator(_iconst_2, "iconst_2", false);
        opcodeOperators[_iconst_3] = new OpcodeOperator(_iconst_3, "iconst_3", false);
        opcodeOperators[_iconst_4] = new OpcodeOperator(_iconst_4, "iconst_4", false);
        opcodeOperators[_iconst_5] = new OpcodeOperator(_iconst_5, "iconst_5", false);
        opcodeOperators[_lconst_0] = new OpcodeOperator(_lconst_0, "lconst_0", false);
        opcodeOperators[_lconst_1] = new OpcodeOperator(_lconst_1, "lconst_1", false);
        opcodeOperators[_fconst_0] = new OpcodeOperator(_fconst_0, "fconst_0", false);
        opcodeOperators[_fconst_1] = new OpcodeOperator(_fconst_1, "fconst_1", false);
        opcodeOperators[_fconst_2] = new OpcodeOperator(_fconst_2, "fconst_2", false);
        opcodeOperators[_dconst_0] = new OpcodeOperator(_dconst_0, "dconst_0", false);
        opcodeOperators[_dconst_1] = new OpcodeOperator(_dconst_1, "dconst_1", false);
        opcodeOperators[_bipush] = new OpcodeOperator(_bipush, "bipush", false);
        opcodeOperators[_sipush] = new OpcodeOperator(_sipush, "sipush", false);
        opcodeOperators[_ldc] = new OpcodeOperator(_ldc, "ldc", false);
        opcodeOperators[_ldc_w] = new OpcodeOperator(_ldc_w, "ldc_w", false);
        opcodeOperators[_ldc2_w] = new OpcodeOperator(_ldc2_w, "ldc2_w", false);
        opcodeOperators[_iload] = new OpcodeOperator(_iload, "iload", false);
        opcodeOperators[_lload] = new OpcodeOperator(_lload, "lload", false);
        opcodeOperators[_fload] = new OpcodeOperator(_fload, "fload", false);
        opcodeOperators[_dload] = new OpcodeOperator(_dload, "dload", false);
        opcodeOperators[_aload] = new OpcodeOperator(_aload, "aload", false);
        opcodeOperators[_iload_0] = new OpcodeOperator(_iload_0, "iload_0", false);
        opcodeOperators[_iload_1] = new OpcodeOperator(_iload_1, "iload_1", false);
        opcodeOperators[_iload_2] = new OpcodeOperator(_iload_2, "iload_2", false);
        opcodeOperators[_iload_3] = new OpcodeOperator(_iload_3, "iload_3", false);
        opcodeOperators[_lload_0] = new OpcodeOperator(_lload_0, "lload_0", false);
        opcodeOperators[_lload_1] = new OpcodeOperator(_lload_1, "lload_1", false);
        opcodeOperators[_lload_2] = new OpcodeOperator(_lload_2, "lload_2", false);
        opcodeOperators[_lload_3] = new OpcodeOperator(_lload_3, "lload_3", false);
        opcodeOperators[_fload_0] = new OpcodeOperator(_fload_0, "fload_0", false);
        opcodeOperators[_fload_1] = new OpcodeOperator(_fload_1, "fload_1", false);
        opcodeOperators[_fload_2] = new OpcodeOperator(_fload_2, "fload_2", false);
        opcodeOperators[_fload_3] = new OpcodeOperator(_fload_3, "fload_3", false);
        opcodeOperators[_dload_0] = new OpcodeOperator(_dload_0, "dload_0", false);
        opcodeOperators[_dload_1] = new OpcodeOperator(_dload_1, "dload_1", false);
        opcodeOperators[_dload_2] = new OpcodeOperator(_dload_2, "dload_2", false);
        opcodeOperators[_dload_3] = new OpcodeOperator(_dload_3, "dload_3", false);
        opcodeOperators[_aload_0] = new OpcodeOperator(_aload_0, "aload_0", false);
        opcodeOperators[_aload_1] = new OpcodeOperator(_aload_1, "aload_1", false);
        opcodeOperators[_aload_2] = new OpcodeOperator(_aload_2, "aload_2", false);
        opcodeOperators[_aload_3] = new OpcodeOperator(_aload_3, "aload_3", false);
        opcodeOperators[_iaload] = new OpcodeOperator(_iaload, "iaload", false);
        opcodeOperators[_laload] = new OpcodeOperator(_laload, "laload", false);
        opcodeOperators[_faload] = new OpcodeOperator(_faload, "faload", false);
        opcodeOperators[_daload] = new OpcodeOperator(_daload, "daload", false);
        opcodeOperators[_aaload] = new OpcodeOperator(_aaload, "aaload", false);
        opcodeOperators[_baload] = new OpcodeOperator(_baload, "baload", false);
        opcodeOperators[_caload] = new OpcodeOperator(_caload, "caload", false);
        opcodeOperators[_saload] = new OpcodeOperator(_saload, "saload", false);
        opcodeOperators[_istore] = new OpcodeOperator(_istore, "istore", false);
        opcodeOperators[_lstore] = new OpcodeOperator(_lstore, "lstore", false);
        opcodeOperators[_fstore] = new OpcodeOperator(_fstore, "fstore", false);
        opcodeOperators[_dstore] = new OpcodeOperator(_dstore, "dstore", false);
        opcodeOperators[_astore] = new OpcodeOperator(_astore, "astore", false);
        opcodeOperators[_istore_0] = new OpcodeOperator(_istore_0, "istore_0", false);
        opcodeOperators[_istore_1] = new OpcodeOperator(_istore_1, "istore_1", false);
        opcodeOperators[_istore_2] = new OpcodeOperator(_istore_2, "istore_2", false);
        opcodeOperators[_istore_3] = new OpcodeOperator(_istore_3, "istore_3", false);
        opcodeOperators[_lstore_0] = new OpcodeOperator(_lstore_0, "lstore_0", false);
        opcodeOperators[_lstore_1] = new OpcodeOperator(_lstore_1, "lstore_1", false);
        opcodeOperators[_lstore_2] = new OpcodeOperator(_lstore_2, "lstore_2", false);
        opcodeOperators[_lstore_3] = new OpcodeOperator(_lstore_3, "lstore_3", false);
        opcodeOperators[_fstore_0] = new OpcodeOperator(_fstore_0, "fstore_0", false);
        opcodeOperators[_fstore_1] = new OpcodeOperator(_fstore_1, "fstore_1", false);
        opcodeOperators[_fstore_2] = new OpcodeOperator(_fstore_2, "fstore_2", false);
        opcodeOperators[_fstore_3] = new OpcodeOperator(_fstore_3, "fstore_3", false);
        opcodeOperators[_dstore_0] = new OpcodeOperator(_dstore_0, "dstore_0", false);
        opcodeOperators[_dstore_1] = new OpcodeOperator(_dstore_1, "dstore_1", false);
        opcodeOperators[_dstore_2] = new OpcodeOperator(_dstore_2, "dstore_2", false);
        opcodeOperators[_dstore_3] = new OpcodeOperator(_dstore_3, "dstore_3", false);
        opcodeOperators[_astore_0] = new OpcodeOperator(_astore_0, "astore_0", false);
        opcodeOperators[_astore_1] = new OpcodeOperator(_astore_1, "astore_1", false);
        opcodeOperators[_astore_2] = new OpcodeOperator(_astore_2, "astore_2", false);
        opcodeOperators[_astore_3] = new OpcodeOperator(_astore_3, "astore_3", false);
        opcodeOperators[_iastore] = new OpcodeOperator(_iastore, "iastore", false);
        opcodeOperators[_lastore] = new OpcodeOperator(_lastore, "lastore", false);
        opcodeOperators[_fastore] = new OpcodeOperator(_fastore, "fastore", false);
        opcodeOperators[_dastore] = new OpcodeOperator(_dastore, "dastore", false);
        opcodeOperators[_aastore] = new OpcodeOperator(_aastore, "aastore", false);
        opcodeOperators[_bastore] = new OpcodeOperator(_bastore, "bastore", false);
        opcodeOperators[_castore] = new OpcodeOperator(_castore, "castore", false);
        opcodeOperators[_sastore] = new OpcodeOperator(_sastore, "sastore", false);
        opcodeOperators[_pop] = new OpcodeOperator(_pop, "pop", false);
        opcodeOperators[_pop2] = new OpcodeOperator(_pop2, "pop2", false);
        opcodeOperators[_dup] = new OpcodeOperator(_dup, "dup", false);
        opcodeOperators[_dup_x1] = new OpcodeOperator(_dup_x1, "dup_x1", false);
        opcodeOperators[_dup_x2] = new OpcodeOperator(_dup_x2, "dup_x2", false);
        opcodeOperators[_dup2] = new OpcodeOperator(_dup2, "dup2", false);
        opcodeOperators[_dup2_x1] = new OpcodeOperator(_dup2_x1, "dup2_x1", false);
        opcodeOperators[_dup2_x2] = new OpcodeOperator(_dup2_x2, "dup2_x2", false);
        opcodeOperators[_swap] = new OpcodeOperator(_swap, "swap", false);
        opcodeOperators[_iadd] = new OpcodeOperator(_iadd, "iadd", true);             
        opcodeOperators[_ladd] = new OpcodeOperator(_ladd, "ladd", true);
        opcodeOperators[_fadd] = new OpcodeOperator(_fadd, "fadd", true);
        opcodeOperators[_dadd] = new OpcodeOperator(_dadd, "dadd", true);
        opcodeOperators[_isub] = new OpcodeOperator(_isub, "isub", true);
        opcodeOperators[_lsub] = new OpcodeOperator(_lsub, "lsub", true);
        opcodeOperators[_fsub] = new OpcodeOperator(_fsub, "fsub", true);
        opcodeOperators[_dsub] = new OpcodeOperator(_dsub, "dsub", true);
        
        if (aggressiveBinaryOperator) {
            // a * b = c <==> a = c/b or b = c / b
            opcodeOperators[_imul] = new OpcodeOperator(_imul, "imul", true);
            opcodeOperators[_lmul] = new OpcodeOperator(_lmul, "lmul", true);
            opcodeOperators[_fmul] = new OpcodeOperator(_fmul, "fmul", true);
            opcodeOperators[_dmul] = new OpcodeOperator(_dmul, "dmul", true);
            
            // a / b = c <==> a = b * c or b = a / c
            opcodeOperators[_idiv] = new OpcodeOperator(_idiv, "idiv", true);
            opcodeOperators[_ldiv] = new OpcodeOperator(_ldiv, "ldiv", true);
            opcodeOperators[_fdiv] = new OpcodeOperator(_fdiv, "fdiv", true);
            opcodeOperators[_ddiv] = new OpcodeOperator(_ddiv, "ddiv", true);
            
            // a % b = c <==> a = c
            opcodeOperators[_irem] = new OpcodeOperator(_irem, "irem", false);
            opcodeOperators[_lrem] = new OpcodeOperator(_lrem, "lrem", false);
            opcodeOperators[_frem] = new OpcodeOperator(_frem, "frem", false);
            opcodeOperators[_drem] = new OpcodeOperator(_drem, "drem", false);
        } else {
            opcodeOperators[_imul] = new OpcodeOperator(_imul, "imul", true);
            opcodeOperators[_lmul] = new OpcodeOperator(_lmul, "lmul", false);
            opcodeOperators[_fmul] = new OpcodeOperator(_fmul, "fmul", false);
            opcodeOperators[_dmul] = new OpcodeOperator(_dmul, "dmul", false);
            opcodeOperators[_idiv] = new OpcodeOperator(_idiv, "idiv", false);
            opcodeOperators[_ldiv] = new OpcodeOperator(_ldiv, "ldiv", false);
            opcodeOperators[_fdiv] = new OpcodeOperator(_fdiv, "fdiv", false);
            opcodeOperators[_ddiv] = new OpcodeOperator(_ddiv, "ddiv", false);
            opcodeOperators[_irem] = new OpcodeOperator(_irem, "irem", false);
            opcodeOperators[_lrem] = new OpcodeOperator(_lrem, "lrem", false);
            opcodeOperators[_frem] = new OpcodeOperator(_frem, "frem", false);
            opcodeOperators[_drem] = new OpcodeOperator(_drem, "drem", false);
        }

        opcodeOperators[_ineg] = new OpcodeOperator(_ineg, "ineg", true);
        opcodeOperators[_lneg] = new OpcodeOperator(_lneg, "lneg", true);
        opcodeOperators[_fneg] = new OpcodeOperator(_fneg, "fneg", true);
        opcodeOperators[_dneg] = new OpcodeOperator(_dneg, "dneg", true);
        opcodeOperators[_ishl] = new OpcodeOperator(_ishl, "ishl", false);
        opcodeOperators[_lshl] = new OpcodeOperator(_lshl, "lshl", false);
        opcodeOperators[_ishr] = new OpcodeOperator(_ishr, "ishr", false);
        opcodeOperators[_lshr] = new OpcodeOperator(_lshr, "lshr", false);
        opcodeOperators[_iushr] = new OpcodeOperator(_iushr, "iushr", false);
        opcodeOperators[_lushr] = new OpcodeOperator(_lushr, "lushr", false);
        opcodeOperators[_iand] = new OpcodeOperator(_iand, "iand", false);
        opcodeOperators[_land] = new OpcodeOperator(_land, "land", false);
        opcodeOperators[_ior] = new OpcodeOperator(_ior, "ior", false);
        opcodeOperators[_lor] = new OpcodeOperator(_lor, "lor", false);
        opcodeOperators[_ixor] = new OpcodeOperator(_ixor, "ixor", false);
        opcodeOperators[_lxor] = new OpcodeOperator(_lxor, "lxor", false);
        opcodeOperators[_iinc] = new OpcodeOperator(_iinc, "iinc", true);
        opcodeOperators[_i2l] = new OpcodeOperator(_i2l, "i2l", true);
        opcodeOperators[_i2f] = new OpcodeOperator(_i2f, "i2f", true);
        opcodeOperators[_i2d] = new OpcodeOperator(_i2d, "i2d", true);
        opcodeOperators[_l2i] = new OpcodeOperator(_l2i, "l2i", false);
        opcodeOperators[_l2f] = new OpcodeOperator(_l2f, "l2f", false);
        opcodeOperators[_l2d] = new OpcodeOperator(_l2d, "l2d", false);
        opcodeOperators[_f2i] = new OpcodeOperator(_f2i, "f2i", false);
        opcodeOperators[_f2l] = new OpcodeOperator(_f2l, "f2l", false);
        opcodeOperators[_f2d] = new OpcodeOperator(_f2d, "f2d", true);
        opcodeOperators[_d2i] = new OpcodeOperator(_d2i, "d2i", false);
        opcodeOperators[_d2l] = new OpcodeOperator(_d2l, "d2l", false);
        opcodeOperators[_d2f] = new OpcodeOperator(_d2f, "d2f", false);
        opcodeOperators[_i2b] = new OpcodeOperator(_i2b, "i2b", false);
        opcodeOperators[_i2c] = new OpcodeOperator(_i2c, "i2c", false);
        opcodeOperators[_i2s] = new OpcodeOperator(_i2s, "i2s", false);
        opcodeOperators[_lcmp] = new OpcodeOperator(_lcmp, "lcmp", false);
        opcodeOperators[_fcmpl] = new OpcodeOperator(_fcmpl, "fcmpl", false);
        opcodeOperators[_fcmpg] = new OpcodeOperator(_fcmpg, "fcmpg", false);
        opcodeOperators[_dcmpl] = new OpcodeOperator(_dcmpl, "dcmpl", false);
        opcodeOperators[_dcmpg] = new OpcodeOperator(_dcmpg, "dcmpg", false);
        opcodeOperators[_ifeq] = new OpcodeOperator(_ifeq, "ifeq", false);
        opcodeOperators[_ifne] = new OpcodeOperator(_ifne, "ifne", false);
        opcodeOperators[_iflt] = new OpcodeOperator(_iflt, "iflt", false);
        opcodeOperators[_ifge] = new OpcodeOperator(_ifge, "ifge", false);
        opcodeOperators[_ifgt] = new OpcodeOperator(_ifgt, "ifgt", false);
        opcodeOperators[_ifle] = new OpcodeOperator(_ifle, "ifle", false);
        opcodeOperators[_if_icmpeq] = new OpcodeOperator(_if_icmpeq, "if_icmpeq", false);
        opcodeOperators[_if_icmpne] = new OpcodeOperator(_if_icmpne, "if_icmpne", false);
        opcodeOperators[_if_icmplt] = new OpcodeOperator(_if_icmplt, "if_icmplt", false);
        opcodeOperators[_if_icmpge] = new OpcodeOperator(_if_icmpge, "if_icmpge", false);
        opcodeOperators[_if_icmpgt] = new OpcodeOperator(_if_icmpgt, "if_icmpgt", false);
        opcodeOperators[_if_icmple] = new OpcodeOperator(_if_icmple, "if_icmple", false);
        opcodeOperators[_if_acmpeq] = new OpcodeOperator(_if_acmpeq, "if_acmpeq", false);
        opcodeOperators[_if_acmpne] = new OpcodeOperator(_if_acmpne, "if_acmpne", false);
        opcodeOperators[_goto] = new OpcodeOperator(_goto, "goto", false);
        opcodeOperators[_jsr] = new OpcodeOperator(_jsr, "jsr", false);
        opcodeOperators[_ret] = new OpcodeOperator(_ret, "ret", false);
        opcodeOperators[_tableswitch] = new OpcodeOperator(_tableswitch, "tableswitch", false);
        opcodeOperators[_lookupswitch] = new OpcodeOperator(_lookupswitch, "lookupswitch", false);
        opcodeOperators[_ireturn] = new OpcodeOperator(_ireturn, "ireturn", false);
        opcodeOperators[_lreturn] = new OpcodeOperator(_lreturn, "lreturn", false);
        opcodeOperators[_freturn] = new OpcodeOperator(_freturn, "freturn", false);
        opcodeOperators[_dreturn] = new OpcodeOperator(_dreturn, "dreturn", false);
        opcodeOperators[_areturn] = new OpcodeOperator(_areturn, "areturn", false);
        opcodeOperators[_return] = new OpcodeOperator(_return, "return", false);
        opcodeOperators[_getstatic] = new OpcodeOperator(_getstatic, "getstatic", false);
        opcodeOperators[_putstatic] = new OpcodeOperator(_putstatic, "putstatic", false);
        opcodeOperators[_getfield] = new OpcodeOperator(_getfield, "getfield", false);
        opcodeOperators[_putfield] = new OpcodeOperator(_putfield, "putfield", false);
        opcodeOperators[_new] = new OpcodeOperator(_new, "new", false);
        opcodeOperators[_newarray] = new OpcodeOperator(_newarray, "newarray", false);
        opcodeOperators[_anewarray] = new OpcodeOperator(_anewarray, "anewarray", false);
        opcodeOperators[_arraylength] = new OpcodeOperator(_arraylength, "arraylength", false);
        opcodeOperators[_athrow] = new OpcodeOperator(_athrow, "athrow", false);
        opcodeOperators[_checkcast] = new OpcodeOperator(_checkcast, "checkcast", false);
        opcodeOperators[_instanceof] = new OpcodeOperator(_instanceof, "instanceof", false);
        opcodeOperators[_monitorenter] = new OpcodeOperator(_monitorenter, "monitorenter", false);
        opcodeOperators[_monitorexit] = new OpcodeOperator(_monitorexit, "monitorexit", false);
        opcodeOperators[_wide] = new OpcodeOperator(_wide, "wide", false);
        opcodeOperators[_multianewarray] = new OpcodeOperator(_multianewarray, "multianewarray", false);
        opcodeOperators[_ifnull] = new OpcodeOperator(_ifnull, "ifnull", false);
        opcodeOperators[_ifnonnull] = new OpcodeOperator(_ifnonnull, "ifnonnull", false);
        opcodeOperators[_goto_w] = new OpcodeOperator(_goto_w, "goto_w", false);
        opcodeOperators[_jsr_w] = new OpcodeOperator(_jsr_w, "jsr_w", false);

        // iinc is treaded as a binary operator that plus increment
        inverseOpcodeOperators[_iinc] = opcodeOperators[_isub];
        inverseOpcodeOperators[_iadd] = opcodeOperators[_isub]; // iadd, isub
        inverseOpcodeOperators[_ladd] = opcodeOperators[_lsub]; // ladd, lsub
        inverseOpcodeOperators[_fadd] = opcodeOperators[_fsub]; // fadd, fsub
        inverseOpcodeOperators[_dadd] = opcodeOperators[_dsub]; // dadd, dsub

        inverseOpcodeOperators[_isub] = opcodeOperators[_iadd]; // iadd, isub
        inverseOpcodeOperators[_lsub] = opcodeOperators[_ladd]; // ladd, lsub
        inverseOpcodeOperators[_fsub] = opcodeOperators[_fadd]; // fadd, fsub
        inverseOpcodeOperators[_dsub] = opcodeOperators[_dadd]; // dadd, dsub

        inverseOpcodeOperators[_ineg] = opcodeOperators[_ineg]; // 
        inverseOpcodeOperators[_fneg] = opcodeOperators[_fneg]; // 
        inverseOpcodeOperators[_lneg] = opcodeOperators[_lneg]; // 
        inverseOpcodeOperators[_dneg] = opcodeOperators[_dneg]; // 

        inverseOpcodeOperators[_i2l] = opcodeOperators[_l2i];
        inverseOpcodeOperators[_i2f] = opcodeOperators[_f2i];
        inverseOpcodeOperators[_i2d] = opcodeOperators[_d2i];
        inverseOpcodeOperators[_f2d] = opcodeOperators[_d2f];
        
        if (aggressiveBinaryOperator) {
            // a * b = c <==> a = c/b or b = c / b
            inverseOpcodeOperators[_imul] = opcodeOperators[_idiv];
            inverseOpcodeOperators[_lmul] = opcodeOperators[_ldiv];
            inverseOpcodeOperators[_fmul] = opcodeOperators[_fdiv];
            inverseOpcodeOperators[_dmul] = opcodeOperators[_ddiv];
            
            // a / b = c <==> a = b * c or b = a / c
            inverseOpcodeOperators[_idiv] = opcodeOperators[_imul];
            inverseOpcodeOperators[_ldiv] = opcodeOperators[_lmul];
            inverseOpcodeOperators[_fdiv] = opcodeOperators[_fmul];
            inverseOpcodeOperators[_ddiv] = opcodeOperators[_dmul];
        }

    }

    public static OpcodeOperator getOpcodeOperator(int i) {
        if (i < 0 || i > 255) {
            throw new RuntimeException("Invalid bytecode " + i);
        }

        OpcodeOperator ret = opcodeOperators[i];

        if (ret == null) {
            throw new RuntimeException("Non-existing opcode " + i);
        }

        return ret;
    }

    public static OpcodeOperator getInverseOpcodeOperator(int i) {
        if (i < 0 || i > 255) {
            throw new RuntimeException("Invalid bytecode " + i);
        }
        OpcodeOperator ret = inverseOpcodeOperators[i];

        if (ret == null) {
            throw new RuntimeException("Non-existing opcode " + i);
        }

        return ret;
    }

    public static RelationalOperator getOppositeComparator(Instruction instruction) {

        switch (instruction.getOpcode()) {
        case _ifeq:
            return NE;
        case _ifne:
            return EQ;
        case _iflt:
            return GE;
        case _ifge:
            return LT;
        case _ifgt:
            return LE;
        case _ifle:
            return GT;
        case _if_icmpeq:
            return NE;
        case _if_icmpne:
            return EQ;
        case _if_icmplt:
            return GE;
        case _if_icmpge:
            return LT;
        case _if_icmpgt:
            return LE;
        case _if_icmple:
            return GT;
        case _if_acmpeq:
            return NE; 
        case _if_acmpne:
            return EQ;
        case _ifnull:
            return NE;
        case _ifnonnull:
            return EQ;
        }
        throw new RuntimeException("Cannot get opposite operator for " + instruction);
    }


    public static RelationalOperator getComparator(Instruction instruction) {
        switch (instruction.getOpcode()) {
        case _ifeq:
            return EQ;
        case _ifne:
            return NE;
        case _iflt:
            return LT;
        case _ifge:
            return GE;
        case _ifgt:
            return GT;
        case _ifle:
            return LE;
        case _if_icmpeq:
            return EQ;
        case _if_icmpne:
            return NE;
        case _if_icmplt:
            return LT;
        case _if_icmpge:
            return GE;
        case _if_icmpgt:
            return GT;
        case _if_icmple:
            return LE;
        case _if_acmpeq:
            return EQ;
        case _if_acmpne:
            return NE;
        case _ifnull:
            return EQ;
        case _ifnonnull:
            return NE;
        }
        throw new RuntimeException();
    }
}
