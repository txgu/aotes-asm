package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.MethodInfo;

public class MethodOperator implements Operator {
    private MethodInfo mi;
    private boolean isGetter;
    private boolean isMutator;
    private boolean isPure;
    private boolean isReversible;
    private String uniqueName;

    MethodOperator(MethodInfo mi, boolean isGetter, boolean isMutator, boolean isPure) {
        this.mi = mi;
        this.isGetter = isGetter;
        this.isMutator = isMutator;
        this.isPure = isPure;
        this.uniqueName = mi.getClassName() + "." + mi.getName() + mi.getDescriptor();
    }

    public void setReversible(boolean value) {
        this.isReversible = value;
    }

    public boolean isMutator() {
        return isMutator;
    }

    public boolean isGetter() {
        return isGetter;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(mi.getFullName());
        if (isMutator()) {
            sb.append(",mutator");
        }

        if (isReversible()) {
            sb.append(",reversible");
        }

        if (isGetter()) {
            sb.append(",getter");
        }

        if (isPure()) {
            sb.append(",pure");
        }

        return sb.toString();
    }

    public boolean isStatic() {
        return mi.isStatic();
    }

    public MethodInfo getMethodInfo() {
        return mi;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public ClassInfo getClassInfo() {
        return mi.getClassInfo();
    }

    @Override
    public boolean isReversible() {
        return isReversible;
    }

    @Override
    public String getTypeDescriptor() {
        return mi.getReturnTypeDescriptor();
    }

    @Override
    public String getPrintName() {
        return getUniqueName();
    }

    @Override
    public boolean isPure() {
        return isPure;
    }
}