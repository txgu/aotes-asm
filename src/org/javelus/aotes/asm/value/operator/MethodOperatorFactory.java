package org.javelus.aotes.asm.value.operator;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.utils.JVMUtils;
import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.value.Reversible;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ClassLoaderInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.MethodInfo;
import org.objectweb.asm.Type;


public class MethodOperatorFactory {

    static Map<MethodInfo, MethodOperator> methodInfoToOperator = new HashMap<MethodInfo, MethodOperator>();

    static Map<MethodInfo, Reversible> methodInfoToInvereMethodOperator = new HashMap<MethodInfo, Reversible>();

    static Map<FieldInfo, GetFieldOperator> fieldToGetField = new HashMap<FieldInfo, GetFieldOperator>();

    static Map<ClassInfo, InstanceOfOperator> classToInstanceOf = new HashMap<ClassInfo, InstanceOfOperator>();

    static Map<ClassInfo, GetElementOperator> classToGetElement = new HashMap<ClassInfo, GetElementOperator>();

    static final boolean useLoggerMethodOperator = Boolean.valueOf(System.getProperty("aotes.loggerOperator", "true"));
    static final boolean useHashCodeAndEqualsMethodOperator = Boolean.valueOf(System.getProperty("aotes.hashCodeOperator", "true"));

    static MethodOperator registerMethodOperator(MethodInfo mi, 
            boolean isGetter, boolean isMutator, boolean isPure) {
        MethodOperator op = methodInfoToOperator.get(mi);
        if (op != null) {
            throw new RuntimeException("Sanity check failed!");
        }

        op = new MethodOperator(mi, isGetter, isMutator, isPure);

        methodInfoToOperator.put(mi, op);

        return op;
    }

    static void registerInverseMethodOperator(MethodInfo mi, 
            Reversible r) {
        methodInfoToInvereMethodOperator.put(mi, r);
    }

    public static boolean hasMethodOperator(MethodInfo mi) {
        return methodInfoToOperator.containsKey(mi);
    }

    public static MethodOperator getInverseMethodOperator(Operator mo) {
        if (!(mo instanceof MethodOperator)) {
            return null;
        }
        MethodInfo mi = ((MethodOperator)mo).getMethodInfo();
        Reversible r = methodInfoToInvereMethodOperator.get(mi);

        if (r == null) {
            return null;
        }

        ClassInfo ici = ClassLoaderInfo.getResolvedClassInfo(r.clazz());
        MethodInfo imi = ici.getDeclaredMethod(r.name(), r.desc()); 

        MethodOperator imo = methodInfoToOperator.get(imi);

        if (imo == null) {
            throw new RuntimeException();
        }

        return imo;
    }


    private static boolean isMethodOperatorSupportedType(Type type) {
        int sort = type.getSort();
        switch (sort) {
        case Type.VOID:
        case Type.BOOLEAN:
        case Type.BYTE:
        case Type.CHAR:
        case Type.SHORT:
        case Type.INT:
        case Type.FLOAT:
        case Type.LONG:
        case Type.DOUBLE:
            return true;
        case Type.OBJECT:
            String className = type.getClassName();
            return JVMUtils.isImmutable(className);
        case Type.ARRAY:
            return false;
        }
        throw new RuntimeException("Should not reach here");
    }


    private static boolean isMethodOperator(MethodInfo method) {
        Type[] types = method.getArgumentTypes();
        for (Type type:types) {
            if (!isMethodOperatorSupportedType(type)) {
                return false;
            }
        }
        return isMethodOperatorSupportedType(method.getReturnType());
    }

    public static boolean isNullReceiverMethodOperator(MethodInfo mi) {
        if (mi.isStatic()) {
            //throw new RuntimeException("not implemented yet");
            return false;
        }

        ClassInfo ci = mi.getClassInfo();
        String className = ci.getName();
        String methodName = mi.getName();
        if (className.equals("java.lang.String")) {
            if (methodName.equals("getChars")
                    || methodName.equals("<init>")) {
                return false;
            }
            return true;
        } else if (className.equals("java.lang.Class")) {
            if (isMethodOperator(mi)) {
                return true;
            }
            return false;
        } else if (ci.getName().equals("java.lang.Object")) {
            return true;
        }
        return false;
    }


    public static boolean isLoggerMethodOperator(MethodInfo mi) {
        if (!useLoggerMethodOperator) {
            return false;
        }
        ClassInfo ci = mi.getClassInfo();
        String className = ci.getName();
        if (className.equals("org.slf4j.LoggerFactory")
                || className.equals("org.slf4j.Logger")) {
            return true;
        }
        return false;
    }

    public static boolean isHashCodeOrEqualsMethodOperator(MethodInfo mi) {
        if (!useHashCodeAndEqualsMethodOperator) {
            return false;
        }
        String name = mi.getName();
        String desc = mi.getDescriptor();
        if ((name.equals("equals") && desc.equals("(Ljava/lang/Object;)V"))
                || (name.equals("hashCode") && desc.equals("()I"))) {
            return true;
        }
        return false;
    }

    public static boolean isNativeMethodOperator(MethodInfo mi) {
        if (!mi.isNative()) {
            throw new RuntimeException("Must be native method");
        }
        ClassInfo ci = mi.getClassInfo();
        String className = ci.getName();
        String methodName = mi.getName();
        if (className.equals("java.lang.Object")) {
            if (methodName.equals("hashCode")
                    || methodName.equals("wait")
                    || methodName.equals("notify")
                    || methodName.equals("notifyAll")
                    || methodName.equals("getClass")) {
                return true;
            }
        } else if (className.equals("java.lang.String")) {
            if (methodName.equals("intern")) {
                return true;
            }
        } else if (className.equals("java.lang.Class")) {
            if (methodName.equals("getPrimitiveClass")
                    || methodName.equals("forName0")) {
                return true;
            }
        } else if (className.equals("java.lang.StrictMath")) {
            return true;
        } else if (className.equals("java.lang.System")) {
            if (methodName.equals("mapLibraryName")
                    || methodName.equals("identityHashCode")) {
                return true;
            }
        } else if (className.equals("java.lang.Float")) {
            if (methodName.equals("floatToRawIntBits")
                    || methodName.equals("intBitsToFloat")) {
                return true;
            }
        } else if (className.equals("java.lang.Double")) {
            if (methodName.equals("doubleToRawLongBits")
                    || methodName.equals("longBitsToDouble")) {
                return true;
            }
        } else if (className.equals("java.lang.Thread")) {
            if (methodName.equals("currentThread")
                    || methodName.equals("holdsLock")
                    || methodName.equals("yield")
                    || methodName.equals("sleep")
                    || methodName.equals("isAlive")
                    || methodName.equals("isInterrupted")) {
                return true;
            }
        } else if (className.equals("java.lang.Runtime")) {
            if (methodName.equals("gc")
                    || methodName.equals("availableProcessors")
                    || methodName.equals("maxMemory")
                    || methodName.equals("totalMemory")
                    || methodName.equals("freeMemory")) {
                return true;
            }
        } else if (className.equals("java.lang.ProcessEnvironment")) {
            if (methodName.equals("gc")
                    || methodName.equals("environmentBlock")) {
                return true;
            }
        } else if (className.equals("java.lang.Throwable")) {
            if (methodName.equals("fillInStackTrace")) {
                return true;
            }
        } else if (className.equals("java.lang.reflect.Array")) {
            if (methodName.equals("newArray")
                    || methodName.equals("getLength")
                    || methodName.equals("get")
                    || methodName.equals("getInt")
                    || methodName.equals("getByte")
                    || methodName.equals("getChar")
                    || methodName.equals("getBoolean")
                    || methodName.equals("getLong")
                    || methodName.equals("getShort")
                    || methodName.equals("getFloat")
                    || methodName.equals("getDouble")) {
                return true;
            }
            Array.class.getName();
        }
        return false;
    }

    public static MethodOperator getMethodOperator(MethodInfo mi) {
        MethodOperator op = methodInfoToOperator.get(mi);

        if (op == null) {
            op = new MethodOperator(mi, false, true, false);
            methodInfoToOperator.put(mi, op);
        }

        return op;
    }

    public static GetElementOperator getElementOperator(ClassInfo ci) {
        GetElementOperator op = classToGetElement.get(ci);

        if (op == null) {
            op = new GetElementOperator(ci);
            classToGetElement.put(ci, op);
        }
        return op;
    }

    public static PutElementOperator putElementOperator() {
        return PutElementOperator.singleton;
    }

    public static ArrayLengthOperator arrayLengthOperator() {
        return ArrayLengthOperator.singleton;
    }

    public static GetFieldOperator getFieldOperator(FieldInfo fi) {
        GetFieldOperator op = fieldToGetField.get(fi);
        if (op == null) {
            op = new GetFieldOperator(fi);
            fieldToGetField.put(fi, op);
        }

        return op;
    }

    public static InstanceOfOperator instanceOfOperator(ClassInfo ci) {
        InstanceOfOperator op = classToInstanceOf.get(ci);
        if (op == null) {
            op = new InstanceOfOperator(ci);
            classToInstanceOf.put(ci, op);
        }

        return op;
    }

    public static PutFieldOperator putFieldOperator() {
        return PutFieldOperator.singleton;
    }

    public static boolean isImmutableMethodOperator(MethodInfo mi) {
        if (mi.isInit()) {
            return false;
        }
        ClassInfo ci = mi.getClassInfo();
        String className = ci.getName();
        String methodName = mi.getName();
        if (className.equals("java.lang.Class")) {
            if (methodName.equals("newInstance")) {
                return false;
            }
            return mi.isPublic();
        } else if (className.equals("java.lang.Integer")) {
            return mi.isPublic();
        } else if (className.equals("java.lang.Float")) {
            return mi.isPublic();
        } else if (className.equals("java.lang.Double")) {
            return mi.isPublic();
        } else if (className.equals("java.lang.Short")) {
            return mi.isPublic();
        } else if (className.equals("java.lang.Byte")) {
            return mi.isPublic();
        } else if (className.equals("java.lang.Character")) {
            return mi.isPublic();
        } else if (className.equals("java.lang.Boolean")) {
            return mi.isPublic();
        } else if (className.equals("java.lang.Long")) {
            return mi.isPublic();
        } else if (className.equals("java.math.BigInteger")) {
            return mi.isPublic();
        }
        return false;
    }

    public static boolean isStringBuilderMethodOperator(MethodInfo mi) {
        ClassInfo ci = mi.getClassInfo();
        String className = ci.getName();
        if (className.equals("java.lang.StringBuilder")) {
            return true;
        } else if (className.equals("java.lang.StringBuffer")) {
            return true;
        }
        return false;
    }

    public static boolean isSecurityMethodOperator(MethodInfo mi) {
        ClassInfo ci = mi.getClassInfo();
        String className = ci.getName();
        String methodName = mi.getName();
        if (className.equals("java.lang.System")) {
            if (methodName.equals("getSecurityManager")) {
                return true;
            }
        }
        return false;
    }
}
