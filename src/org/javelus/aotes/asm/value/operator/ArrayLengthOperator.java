package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.value.Operator;

public class ArrayLengthOperator implements Operator {

    public static ArrayLengthOperator singleton = new ArrayLengthOperator();

    private ArrayLengthOperator() {
    }

    @Override
    public boolean isGetter() {
        return true;
    }

    @Override
    public boolean isReversible() {
        return false;
    }

    @Override
    public String getPrintName() {
        return "arraylength";
    }

    @Override
    public boolean isMutator() {
        return false;
    }

    @Override
    public boolean isPure() {
        return true;
    }

    @Override
    public String getTypeDescriptor() {
        return "I";
    }

}
