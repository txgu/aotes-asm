package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.vm.ClassInfo;

public class GetElementOperator extends AbstractArrayOperator {

    public GetElementOperator(ClassInfo arrayClassInfo) {
        super(arrayClassInfo);
    }

    @Override
    public boolean isGetter() {
        return true;
    }

    @Override
    public boolean isReversible() {
        return false;
    }

    @Override
    public String getPrintName() {
        return "xaload";
    }

    @Override
    public boolean isMutator() {
        return false;
    }
}
