package org.javelus.aotes.asm.value.operator;

import java.io.Serializable;


public class OpcodeOperator implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 363691239017157045L;


    public final int opcode;
    public final String type;
    public final boolean reversible;

    public OpcodeOperator(int opcode, String type, boolean reversible) {
        this.opcode = opcode;
        this.type = type;
        this.reversible = reversible;
    }
}
