package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.value.Operator;

public class PutFieldOperator implements Operator {

    static PutFieldOperator singleton = new PutFieldOperator();

    private PutFieldOperator() {}

    @Override
    public boolean isGetter() {
        return false;
    }

    @Override
    public boolean isReversible() {
        return false;
    }

    @Override
    public boolean isMutator() {
        return true;
    }

    @Override
    public String getPrintName() {
        return "putfield_or_static";
    }

    @Override
    public String getTypeDescriptor() {
        return "V";
    }

    @Override
    public boolean isPure() {
        return false;
    }

}
