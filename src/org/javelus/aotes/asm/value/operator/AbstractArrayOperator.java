package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.vm.ClassInfo;

public abstract class AbstractArrayOperator implements Operator {

    ClassInfo arrayClassInfo;

    public AbstractArrayOperator(ClassInfo arrayClassInfo) {
        this.arrayClassInfo = arrayClassInfo;
    }

    public ClassInfo getArrayClassInfo() {
        return arrayClassInfo;
    }

    @Override
    public String getTypeDescriptor() {
        return arrayClassInfo.getComponentClassInfo().getTypeDescriptor();
    }

    @Override
    public boolean isPure() {
        return false;
    }
}
