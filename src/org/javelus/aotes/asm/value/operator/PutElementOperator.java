package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.value.Operator;

public class PutElementOperator implements Operator {

    public static final PutElementOperator singleton = new PutElementOperator();

    private PutElementOperator() {}

    @Override
    public boolean isGetter() {
        return false;
    }

    @Override
    public boolean isReversible() {
        return false;
    }

    @Override
    public String getPrintName() {
        return "xastore";
    }

    @Override
    public String getTypeDescriptor() {
        return "V";
    }

    @Override
    public boolean isMutator() {
        return true;
    }

    @Override
    public boolean isPure() {
        return false;
    }
}
