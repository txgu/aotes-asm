package org.javelus.aotes.asm.value.operator;

import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.vm.FieldInfo;

public class GetFieldOperator implements Operator {

    protected FieldInfo fieldInfo;

    public GetFieldOperator(FieldInfo fieldInfo) {
        this.fieldInfo = fieldInfo;
    }

    @Override
    public String getTypeDescriptor() {
        return fieldInfo.getTypeDescriptor();
    }

    @Override
    public boolean isPure() {
        return false;
    }

    @Override
    public boolean isGetter() {
        return true;
    }

    @Override
    public boolean isReversible() {
        return false;
    }

    @Override
    public String getPrintName() {
        return "getfield_or_static";
    }

    @Override
    public boolean isMutator() {
        return false;
    }

}
