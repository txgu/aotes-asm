package org.javelus.aotes.asm.value;

public abstract class SymbolicValue extends Value {
    private PathCondition pathCondition;
    public SymbolicValue (PathCondition pathCondition) {
        this.pathCondition = pathCondition;
    }

    public PathCondition getPathCondition() {
        return pathCondition;
    }

}
