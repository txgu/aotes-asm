package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ElementInfo;

public abstract class Ref extends SymbolicValue {

    private ElementInfo elementInfo;

    public Ref(PathCondition pathCondition, ElementInfo elementInfo) {
        super(pathCondition);
        this.elementInfo = elementInfo;
        elementInfo.setReference(this);
    }

    public ClassInfo getClassInfo() {
        return elementInfo.getClassInfo();
    }

    public ElementInfo getElementInfo() {
        return elementInfo;
    }

    @Override
    public String getTypeDescriptor() {
        return getClassInfo().getTypeDescriptor();
    }
}
