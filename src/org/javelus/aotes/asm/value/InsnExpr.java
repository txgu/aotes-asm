package org.javelus.aotes.asm.value;

import static org.objectweb.asm.Opcodes.D2F;
import static org.objectweb.asm.Opcodes.D2I;
import static org.objectweb.asm.Opcodes.D2L;
import static org.objectweb.asm.Opcodes.DADD;
import static org.objectweb.asm.Opcodes.DCMPG;
import static org.objectweb.asm.Opcodes.DCMPL;
import static org.objectweb.asm.Opcodes.DDIV;
import static org.objectweb.asm.Opcodes.DMUL;
import static org.objectweb.asm.Opcodes.DNEG;
import static org.objectweb.asm.Opcodes.DREM;
import static org.objectweb.asm.Opcodes.DSUB;
import static org.objectweb.asm.Opcodes.F2D;
import static org.objectweb.asm.Opcodes.F2I;
import static org.objectweb.asm.Opcodes.F2L;
import static org.objectweb.asm.Opcodes.FADD;
import static org.objectweb.asm.Opcodes.FCMPG;
import static org.objectweb.asm.Opcodes.FCMPL;
import static org.objectweb.asm.Opcodes.FDIV;
import static org.objectweb.asm.Opcodes.FMUL;
import static org.objectweb.asm.Opcodes.FNEG;
import static org.objectweb.asm.Opcodes.FREM;
import static org.objectweb.asm.Opcodes.FSUB;
import static org.objectweb.asm.Opcodes.I2B;
import static org.objectweb.asm.Opcodes.I2C;
import static org.objectweb.asm.Opcodes.I2D;
import static org.objectweb.asm.Opcodes.I2F;
import static org.objectweb.asm.Opcodes.I2L;
import static org.objectweb.asm.Opcodes.I2S;
import static org.objectweb.asm.Opcodes.IADD;
import static org.objectweb.asm.Opcodes.IAND;
import static org.objectweb.asm.Opcodes.IDIV;
import static org.objectweb.asm.Opcodes.IINC;
import static org.objectweb.asm.Opcodes.IMUL;
import static org.objectweb.asm.Opcodes.INEG;
import static org.objectweb.asm.Opcodes.INSTANCEOF;
import static org.objectweb.asm.Opcodes.IOR;
import static org.objectweb.asm.Opcodes.IREM;
import static org.objectweb.asm.Opcodes.ISHL;
import static org.objectweb.asm.Opcodes.ISHR;
import static org.objectweb.asm.Opcodes.ISUB;
import static org.objectweb.asm.Opcodes.IUSHR;
import static org.objectweb.asm.Opcodes.IXOR;
import static org.objectweb.asm.Opcodes.L2D;
import static org.objectweb.asm.Opcodes.L2F;
import static org.objectweb.asm.Opcodes.L2I;
import static org.objectweb.asm.Opcodes.LADD;
import static org.objectweb.asm.Opcodes.LAND;
import static org.objectweb.asm.Opcodes.LCMP;
import static org.objectweb.asm.Opcodes.LDIV;
import static org.objectweb.asm.Opcodes.LMUL;
import static org.objectweb.asm.Opcodes.LNEG;
import static org.objectweb.asm.Opcodes.LOR;
import static org.objectweb.asm.Opcodes.LREM;
import static org.objectweb.asm.Opcodes.LSHL;
import static org.objectweb.asm.Opcodes.LSHR;
import static org.objectweb.asm.Opcodes.LSUB;
import static org.objectweb.asm.Opcodes.LUSHR;

import org.javelus.aotes.asm.vm.Instruction;

public abstract class InsnExpr extends Expr {

    private Instruction instruction;

    public InsnExpr(PathCondition pathCondition, Instruction instruction) {
        super(pathCondition);
        this.instruction = instruction;
    }

    public Instruction getInstruction() {
        return instruction;
    }

    public String toString() {
        return this.instruction.toString();
    }

    @Override
    public String getTypeDescriptor() {
        int opcode = instruction.getOpcode();

        switch (opcode) {case D2F:
        case D2I:
            return "I";
        case D2L:
            return "J";
        case DADD:
            return "D";
        case DCMPG:
        case DCMPL:
            return "I";
        case DDIV:
        case DMUL:
        case DNEG:
        case DREM:
        case DSUB:
            return "D";
        case F2D:
            return "D";
        case F2I:
            return "I";
        case F2L:
            return "J";
        case FADD:
            return "F";
        case FCMPG:
        case FCMPL:
            return "I";
        case FDIV:
        case FMUL:
        case FNEG:
        case FREM:
        case FSUB:
            return "F";
        case I2B:
            return "B";
        case I2C:
            return "B";
        case I2D:
            return "D";
        case I2F:
            return "F";
        case I2L:
            return "J";
        case I2S:
            return "S";
        case IADD:
        case IAND:
        case IDIV:
        case IMUL:
        case INEG:
        case IOR:
        case IREM:
        case ISHL:
        case ISHR:
        case ISUB:
        case IUSHR:
        case IXOR:
            return "I";
        case L2D:
            return "D";
        case L2F:
            return "F";
        case L2I:
            return "I";
        case LADD:
        case LAND:
            return "J";
        case LCMP:
            return "I";
        case LDIV:
        case LMUL:
        case LNEG:
        case LOR:
        case LREM:
        case LSHL:
        case LSHR:
        case LSUB:
        case LUSHR:
            return "J";
        case IINC:
            return "I";
        case INSTANCEOF:
            return "I";
        }
        return null;
    }
}
