package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.LocationPath;
import org.javelus.aotes.asm.vm.ElementInfo;

/**
 * Heap location in the pre-heap, including method parameters of the entry method.
 * @author user
 *
 * @param <T>
 */
public class Input<T extends Location> extends Value {

    public Input(LocationPath locationPath, T location) {
        this.locationPath = locationPath;
        this.location = location;
    }

    private T location;
    private LocationPath locationPath;

    private ElementInfo preallocated;

    public T getLocation() {
        return location;
    }

    public LocationPath getLocationPath() {
        return locationPath;
    }

    @Override
    public String getTypeDescriptor() {
        return location.getTypeDescriptor();
    }

    public ElementInfo getPreallocated() {
        return preallocated;
    }

    public void setPreallocated(ElementInfo preallocated) {
        this.preallocated = preallocated;
        preallocated.setReference(this);
    }

    public String toString() {
        return "Input<" + location + ">";
    }

    public static Input<Location> INPUT_PLACEHOLDER = new Input<Location>(null, null) {
        public String getTypeDescriptor() {
            return null;
        }

        public String toString() {
            return "INPUT_PH";
        }
    };
}
