package org.javelus.aotes.asm.value;

public class OperatorValue extends SymbolicValue {

    // TODO: remove 
    private OperatorValue previous;
    private OperatorValue next;

    private Operator operator;
    private Value[] operands;
    private Value receiver;

    public OperatorValue(PathCondition pathCondition, OperatorValue previous, Operator operator, Value receiver, Value[] operands) {
        super(pathCondition);
        this.previous = previous;
        if (previous != null) {
            previous.next = this;
        }
        this.receiver = receiver;
        this.operator = operator;
        this.operands = operands;
    }

    // TODO
    public boolean isResolved() {
        for (Value v:operands) {
            if (!isResolved(v)) {
                return false;
            }
        }
        if (previous != null) {
            return previous.isResolved();
        }
        return true;
    }

    public static boolean isResolved(Value value) {
        if (value instanceof Output) {
            return true;
        }

        // other unallocated unknown value or method parameter should be unresolved 
        if (value instanceof Input) {
            return false;
        }

        if (value instanceof OperatorValue) {
            return ((OperatorValue)value).isResolved();
        }

        return true;
    }

    public Operator getOperator() {
        return operator;
    }

    public Value[] getOperands() {
        return operands;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[MethodOperatorValue]: ").append(operator).append(" [");

        for (int i = 0; i<operands.length; i++) {
            sb.append('(').append(i).append(')').append(operands[i]);
        }

        sb.append(']');
        return sb.toString();
    }

    @Override
    public String getTypeDescriptor() {
        return operator.getTypeDescriptor();
    }

    public OperatorValue getPrevious() {
        return previous;
    }


    public Value getReceiverValue() {
        return receiver;
    }

    public OperatorValue getPreviousMutator() {
        OperatorValue mov = getPrevious();
        while (mov != null) {
            if (mov.getOperator().isMutator()) {
                return mov;
            }
            mov = mov.getPrevious();
        }

        return mov;
    }

    public OperatorValue getNext() {
        return next;
    }

    public OperatorValue getNextMutator() {
        OperatorValue mov = getNext();
        if (mov == null) {
            return null;
        }

        do {
            if (mov.getOperator().isMutator()) {
                return mov;
            }
            mov = mov.getNext();
        } while(mov != null);

        return mov;
    }
}
