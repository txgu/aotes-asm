package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.vm.ElementInfo;

public class NewRef extends Ref {

    public NewRef(PathCondition pathCondition, ElementInfo ei) {
        super(pathCondition, ei);
    }
}
