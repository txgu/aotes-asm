package org.javelus.aotes.asm.value;

public class GuessIndexValue extends Value {

    Value array;

    public GuessIndexValue(Value array) {
        this.array = array;
    }

    @Override
    public String getTypeDescriptor() {
        return "I";
    }

    public Value getArray() {
        return array;
    }

}
