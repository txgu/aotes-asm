package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.vm.Instruction;

public class UnopExpr extends InsnExpr {

    public UnopExpr(PathCondition pathCondition, Instruction instruction, Value operand) {
        super(pathCondition, instruction);
        this.operand = operand;
    }

    private Value operand;

    public Value getOperand() {
        return operand;
    }
}
