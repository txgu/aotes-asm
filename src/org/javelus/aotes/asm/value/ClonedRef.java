package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.ArrayLength;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.InstanceField;
import org.javelus.aotes.asm.vm.ElementInfo;

public class ClonedRef extends NewRef {

    private Value source;

    public ClonedRef(PathCondition pathCondition, Value source, ElementInfo cloned) {
        super(pathCondition, cloned);
        this.source = source;
    }

    public Value getSource() {
        return source;
    }

    public HeapLocation getSourceLocation(HeapLocation loc) {
        Value source = getSource();
        ElementInfo sourceEI = Value.getElementInfo(source);
        HeapLocation sourceLoc = null;
        if (loc instanceof ArrayElement) {
            sourceLoc = sourceEI.arrayElement(((ArrayElement) loc).getIndex());
        } else if (loc instanceof InstanceField) {
            sourceLoc = sourceEI.instanceField(((InstanceField) loc).getFieldInfo());
        } else if (loc instanceof ArrayLength) {
            sourceLoc = sourceEI.arrayLength();
        } else {
            throw new RuntimeException("Unknown type of loc " + loc);
        }
        return sourceLoc;
    }
}
