package org.javelus.aotes.asm.value;

public interface ExecutionIndex {
    int getExecutionIndex();
}
