package org.javelus.aotes.asm.value;

public abstract class Constant extends Value{

    public Constant() {
        
    }
    
    public abstract String getLiteral();

}
