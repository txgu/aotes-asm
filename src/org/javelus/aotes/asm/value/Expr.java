package org.javelus.aotes.asm.value;

public abstract class Expr extends SymbolicValue {

    public Expr(PathCondition pathCondition) {
        super(pathCondition);
    }

}
