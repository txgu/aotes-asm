package org.javelus.aotes.asm.value;

public interface Operator {
    boolean isMutator();
    boolean isGetter();
    boolean isPure();
    boolean isReversible();

    String getTypeDescriptor();

    String getPrintName();
}
