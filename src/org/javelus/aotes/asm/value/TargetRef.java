package org.javelus.aotes.asm.value;

import org.javelus.aotes.asm.vm.ElementInfo;

public class TargetRef extends Ref {

    public TargetRef(PathCondition pathCondition, ElementInfo ei) {
        super(pathCondition, ei);
    }

    public String toString() {
        return "this";
    }
}
