package org.javelus.aotes.asm.accesspath;

import org.javelus.aotes.asm.vm.FieldInfo;


public class StaticFieldAccessPath extends FieldAccessPath {

    public StaticFieldAccessPath(AccessPath parent, FieldInfo fieldInfo) {
        super(parent, fieldInfo);
    }

    public final boolean isRoot() {
        return true;
    }

    @Override
    public AccessPath getCanonical() {
        return this;
    }

    @Override
    public AccessPath getRoot() {
        return this;
    }

    @Override
    public final boolean isPrecise() {
        return true;
    }
}
