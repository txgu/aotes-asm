package org.javelus.aotes.asm.accesspath;

import java.io.PrintWriter;

import org.javelus.aotes.asm.vm.FieldInfo;


public class FieldAccessPath extends AbstractAccessPath {

    protected FieldInfo fieldInfo;

    public FieldAccessPath(AccessPath parent, FieldInfo fieldInfo) {
        super(parent);
        this.fieldInfo = fieldInfo;
    }

    @Override
    public AccessPath getCanonical() {
        if (canonical != null) {
            return canonical;
        }

        AccessPath parent = getParent();
        if (parent instanceof TargetAccessPath) {
            TargetAccessPath tp = (TargetAccessPath) parent;
            if (tp.getClassInfo() == fieldInfo.getClassInfo()) {
                canonical = this;
                return this;
            }
            TargetAccessPath realTp = (TargetAccessPath) AccessPathRoots.getTargetAccessPath(fieldInfo.getClassInfo());
            canonical = realTp.getChild(fieldInfo);
            return canonical;
        }

        return super.getCanonical();
    }

    @Override
    public Object getAccessor() {
        return fieldInfo;
    }

    public FieldInfo getFieldInfo() {
        return fieldInfo;
    }

    @Override
    public boolean isPrecise() {
        return getParent().isPrecise();
    }

    public void toString(PrintWriter pw) {
        parent.toString(pw);
        pw.print('.');
        pw.print(fieldInfo.getName());
    }

    @Override
    public String getTypeDescriptor() {
        return fieldInfo.getTypeDescriptor();
    }

}
