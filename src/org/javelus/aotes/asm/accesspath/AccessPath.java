package org.javelus.aotes.asm.accesspath;

import java.io.PrintWriter;
import java.util.Collection;

public interface AccessPath {
    AccessPath getCanonical();
    AccessPath getRoot();
    AccessPath getParent();
    AccessPath getChild(Object accessor);
    Collection<AccessPath> getChildren();
    Object getAccessor();
    boolean isPrecise();
    boolean isRoot();
    void toString(PrintWriter pw);

    String getTypeDescriptor();
}
