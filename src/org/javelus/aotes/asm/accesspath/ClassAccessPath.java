package org.javelus.aotes.asm.accesspath;

import java.io.PrintWriter;

import org.javelus.aotes.asm.vm.ClassInfo;

public class ClassAccessPath extends AbstractAccessPath {

    private ClassInfo classInfo;

    public ClassAccessPath(ClassInfo classInfo) {
        super(null);
        this.classInfo = classInfo;
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }

    @Override
    public Object getAccessor() {
        return null;
    }

    @Override
    public boolean isPrecise() {
        return true;
    }

    @Override
    public String getTypeDescriptor() {
        return classInfo.getTypeDescriptor();
    }

    public void toString(PrintWriter pw) {
        pw.print(classInfo);
    }
}
