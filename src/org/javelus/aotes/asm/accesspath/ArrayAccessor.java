package org.javelus.aotes.asm.accesspath;

public enum ArrayAccessor {
    LENGTH("#"),
    INDEX("@"),
    ELEMENT("*");

    private String shortName;

    private ArrayAccessor(String shortName) {
        this.shortName = shortName;
    }

    public String toString() {
        return shortName;
    }
}
