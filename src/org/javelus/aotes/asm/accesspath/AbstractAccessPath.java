package org.javelus.aotes.asm.accesspath;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractAccessPath implements AccessPath {

    protected AccessPath parent;

    protected AccessPath canonical;

    protected Map<Object, AccessPath> children = new HashMap<Object, AccessPath>();


    public AbstractAccessPath(AccessPath parent) {
        this.parent = parent;
    }

    @Override
    public AccessPath getCanonical() {
        if (this.isRoot()) {
            return this;
        }

        if (canonical == null) {
            canonical = getParent().getCanonical().getChild(getAccessor());
        }
        return canonical;
    }

    @Override
    public AccessPath getRoot() {
        return parent.getRoot();
    }

    @Override
    public final AccessPath getParent() {
        return parent;
    }

    @Override
    public boolean isRoot() {
        return false;
    }

    @Override
    public AccessPath getChild(Object accessor) {
        AccessPath ap = children.get(accessor);
        if (ap == null) {
            ap = AccessPathRoots.dot(this, accessor);
            children.put(accessor, ap);
        }
        return ap;
    }

    @Override
    public Collection<AccessPath> getChildren() {
        return children.values();
    }

    public void toString(PrintWriter pw) {
        parent.toString(pw);
        pw.print('.');
        pw.print(getAccessor());
    }

    public String toString() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(baos);

        toString(pw);

        pw.close();

        return baos.toString();
    }

    public boolean isMethodParameter() {
        return false;
    }

}
