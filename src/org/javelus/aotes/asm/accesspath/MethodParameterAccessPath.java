package org.javelus.aotes.asm.accesspath;

import java.io.PrintWriter;

import org.javelus.aotes.asm.vm.MethodInfo;



public class MethodParameterAccessPath extends AbstractAccessPath {

    private MethodInfo methodInfo;
    private int index;

    public MethodParameterAccessPath(MethodInfo mi, int argIndex) {
        super(null);
        this.methodInfo = mi;
        this.index = argIndex;
    }

    @Override
    public Object getAccessor() {
        return null;
    }

    public MethodInfo getMethodInfo() {
        return methodInfo;
    }

    public int getArgumentIndex() {
        return index;
    }

    @Override
    public boolean isPrecise() {
        return true;
    }

    @Override
    public boolean isRoot() {
        return true;
    }

    @Override
    public void toString(PrintWriter pw) {
        pw.format("%s@%d", methodInfo.getName(), index);
    }

    public String toString() {
        return String.format("%s@%d", methodInfo.getName(), index);
    }

    public final boolean isMethodParameter() {
        return true;
    }

    @Override
    public String getTypeDescriptor() {
        return methodInfo.getArgumentTypes()[index].getDescriptor();
    }

}
