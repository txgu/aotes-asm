package org.javelus.aotes.asm.accesspath;

import java.io.PrintWriter;

public final class ArrayLengthAccessPath extends AbstractAccessPath {

    public ArrayLengthAccessPath(AccessPath parent) {
        super(parent);
    }

    @Override
    public Object getAccessor() {
        return ArrayAccessor.LENGTH;
    }

    @Override
    public boolean isPrecise() {
        return getParent().isPrecise();
    }

    @Override
    public String getTypeDescriptor() {
        return "I";
    }

    @Override
    public void toString(PrintWriter pw) {
        parent.toString(pw);
        pw.print(".length");
    }
}
