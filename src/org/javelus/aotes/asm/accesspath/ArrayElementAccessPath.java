package org.javelus.aotes.asm.accesspath;

import java.io.PrintWriter;

public final class ArrayElementAccessPath extends AbstractAccessPath {

    public ArrayElementAccessPath(AccessPath parent) {
        super(parent);
    }

    @Override
    public Object getAccessor() {
        return ArrayAccessor.ELEMENT;
    }

    @Override
    public boolean isPrecise() {
        return false;
    }

    @Override
    public String getTypeDescriptor() {
        throw new RuntimeException("Not implemented yet!");
    }

    @Override
    public void toString(PrintWriter pw) {
        parent.toString(pw);
        pw.print("[*]");
    }
}
