package org.javelus.aotes.asm.accesspath;

import java.io.PrintWriter;

import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.FieldInfo;


public class TargetAccessPath extends AbstractAccessPath {

    private ClassInfo ci;
    public TargetAccessPath(ClassInfo ci){
        super(null);
        this.ci = ci;
    }

    public ClassInfo getClassInfo() {
        return ci;
    }

    @Override
    public AccessPath getRoot() {
        return this;
    }

    @Override
    public Object getAccessor() {
        return null;
    }

    @Override
    public final boolean isPrecise() {
        return true;
    }

    @Override
    public final boolean isRoot() {
        return true;
    }

    @Override
    public AccessPath getChild(Object accessor) {
        if (!(accessor instanceof FieldInfo)) {
            throw new IllegalArgumentException("Invalid accessor!");
        }
        return super.getChild(accessor);
    }

    @Override
    public void toString(PrintWriter pw) {
        pw.print("this");
    }

    public String toString() {
        return "this";
    }

    public String getTypeDescriptor() {
        return ci.getTypeDescriptor();
    }
}
