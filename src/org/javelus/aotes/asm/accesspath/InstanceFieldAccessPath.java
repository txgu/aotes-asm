package org.javelus.aotes.asm.accesspath;

import org.javelus.aotes.asm.vm.FieldInfo;


public class InstanceFieldAccessPath extends FieldAccessPath {


    public InstanceFieldAccessPath(AccessPath parent, FieldInfo fieldInfo) {
        super(parent, fieldInfo);
    }

    @Override
    public AccessPath getCanonical() {
        if (canonical != null) {
            return canonical;
        }

        AccessPath parent = getParent();
        if (parent instanceof TargetAccessPath) {
            TargetAccessPath tp = (TargetAccessPath) parent;
            if (tp.getClassInfo() == fieldInfo.getClassInfo()) {
                canonical = this;
                return this;
            }
            TargetAccessPath realTp = (TargetAccessPath) AccessPathRoots.getTargetAccessPath(fieldInfo.getClassInfo());
            canonical = realTp.getChild(fieldInfo);
            return canonical;
        }

        return super.getCanonical();
    }

    @Override
    public boolean isPrecise() {
        return getParent().isPrecise();
    }
}
