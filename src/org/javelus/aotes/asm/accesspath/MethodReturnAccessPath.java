package org.javelus.aotes.asm.accesspath;

import java.io.PrintWriter;

import org.javelus.aotes.asm.vm.MethodInfo;



public class MethodReturnAccessPath extends AbstractAccessPath {

    private MethodInfo entry;

    public MethodReturnAccessPath(MethodInfo mi) {
        super(null);
        this.entry = mi;
    }

    @Override
    public Object getAccessor() {
        return null;
    }

    public MethodInfo getEntryMethodInfo() {
        return entry;
    }

    @Override
    public boolean isPrecise() {
        return true;
    }

    @Override
    public boolean isRoot() {
        return true;
    }

    @Override
    public void toString(PrintWriter pw) {
        pw.format("%s@ret", entry.getName());
    }

    public String toString() {
        return String.format("%s@ret", entry.getName());
    }

    @Override
    public String getTypeDescriptor() {
        return entry.getReturnTypeDescriptor();
    }
}
