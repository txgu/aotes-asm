package org.javelus.aotes.asm.accesspath;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.MethodInfo;


public class AccessPathRoots {

    private static Map<ClassInfo, TargetAccessPath> classToThis = new HashMap<ClassInfo, TargetAccessPath>();

    private static Map<MethodInfo, List<MethodParameterAccessPath>> methodToParameter = new HashMap<MethodInfo, List<MethodParameterAccessPath>>();

    private static Map<ClassInfo, ClassAccessPath> classes = new HashMap<ClassInfo, ClassAccessPath>();


    public static TargetAccessPath getTargetAccessPath(ClassInfo ci) {
        TargetAccessPath ap = classToThis.get(ci);
        if (ap == null) {
            ap = new TargetAccessPath(ci);
            classToThis.put(ci, ap);
        }
        if (!ap.isRoot()) {
            throw new RuntimeException();
        }
        return ap;
    }

    public static ClassAccessPath getClassAccessPath(ClassInfo ci) {
        ClassAccessPath cap = classes.get(ci);
        if (cap == null) {
            cap = new ClassAccessPath(ci);
            classes.put(ci, cap);
        }
        return cap;
    }

    public static StaticFieldAccessPath getStaticField(FieldInfo fi) {
        if (!fi.isStatic()) {
            throw new RuntimeException("Only static field can be root!");
        }

        ClassInfo ci = fi.getClassInfo();
        ClassAccessPath cap = getClassAccessPath(ci);

        return (StaticFieldAccessPath) cap.getChild(fi);
    }

    public static MethodParameterAccessPath getMethodParameter(MethodInfo mi, int index) {
        List<MethodParameterAccessPath> parameters = methodToParameter.get(mi);

        if (parameters == null) {
            parameters = new ArrayList<MethodParameterAccessPath>(mi.getNumOfArguments());

            for (int i=0; i<mi.getNumOfArguments(); i++) {
                parameters.add(new MethodParameterAccessPath(mi, i));
            }

            methodToParameter.put(mi, parameters);
        }

        if (index == -1) {
            return parameters.get(parameters.size()-1);
        }

        return parameters.get(index);
    }

    static AccessPath dot(AccessPath parent, Object accessor) {
        if (accessor.equals(ArrayAccessor.ELEMENT)) {
            return new ArrayElementAccessPath(parent);
        } else if (accessor.equals(ArrayAccessor.LENGTH)) {
            return new ArrayLengthAccessPath(parent);
        } else if (accessor instanceof FieldInfo) {
            FieldInfo fi = (FieldInfo) accessor;
            if (fi.isStatic()) {
                return new StaticFieldAccessPath(parent, fi);
            }
            return new InstanceFieldAccessPath(parent, fi);
        }
        throw new IllegalArgumentException("Not implemented!");
    }
}
