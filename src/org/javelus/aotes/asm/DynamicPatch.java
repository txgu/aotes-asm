package org.javelus.aotes.asm;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.javelus.DSU;
import org.javelus.DSUClass;
import org.javelus.DSUClassLoader;
import org.javelus.DSUField;
import org.javelus.DSUMethod;
import org.javelus.MethodUpdateType;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ClassLoaderInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.MethodInfo;
import org.javelus.impl.DSUSpecReader;
import org.xml.sax.SAXException;

public class DynamicPatch {
    Map<MethodInfo, DSUMethod> methodInfoToDSUMethod = new HashMap<MethodInfo, DSUMethod>();
    /**
     * Changed classes including BC, BOTH and excluding ADD, DEL
     */
    private Set<ClassInfo> changedClasses = new HashSet<ClassInfo>();

    /**
     * A list of signature unchanged but body changed methods.
     */
    private Set<MethodInfo> matchedChangedMethods = new HashSet<MethodInfo>();
    /**
     * A map from mi.getFullName() to mi
     */
    private Map<String, MethodInfo> unmatchedChangedMethods = new HashMap<String, MethodInfo>();

    /**
     * A list of changed fields.
     */
    private Set<FieldInfo>  unmatchedChangedFields = new HashSet<FieldInfo>();


    private Set<ClassInfo> hasNewInstanceFields = new HashSet<ClassInfo>();

    private DynamicPatch() {}

    public static ClassInfo loadClassInfo(String className) {
        return ClassLoaderInfo.getResolvedClassInfo(className);
    }

    /**
     * A phantom dynamic patch, all methods are assumed to be matched changed.
     * @return
     */
    public static DynamicPatch createFromClassInfo(Collection<ClassInfo> classes) {
        DynamicPatch dp = new DynamicPatch();

        for (ClassInfo ci:classes) {
            dp.changedClasses.add(ci);

            while (!ci.isObjectClassInfo()) {
                for(MethodInfo mi:ci.getDeclaredMethodInfos()) {
                    if (mi.isPublic()) {
                        dp.matchedChangedMethods.add(mi);
                    } else {
                        // Use full name as this map contains methods of all classes.
                        dp.unmatchedChangedMethods.put(mi.getClassName() + mi.getFullName(), mi);
                    }
                }
                ci = ci.getSuperClass();
            }
        }

        return dp;
    }

    public static DynamicPatch createFromXMLDynamicPatch(String filePath, boolean old) {
        DynamicPatch dp = new DynamicPatch();

        try {
            DSU dsu = DSUSpecReader.read(filePath);
            for (DSUClassLoader cl:dsu.getClassLoaders()) {
                for (DSUClass c:cl.getDSUClasses()) {
                    if (!c.getUpdateType().isChanged()) {
                        continue;
                    }
                    ClassInfo ci = loadClassInfo(c.getName());
                    dp.changedClasses.add(ci);
                    for (DSUMethod m:c.getDSUMethods()) {
                        if (m.getUpdateType().isChanged()) {
                            MethodInfo mi = ci.getDeclaredMethod(m.getName(), m.getSignature());
                            dp.methodInfoToDSUMethod.put(mi, m);
                            dp.matchedChangedMethods.add(mi);
                        } else if ((old && m.getUpdateType().isDeleted()) || (!old && m.getUpdateType().isAdded()) ) {
                            MethodInfo mi = ci.getDeclaredMethod(m.getName(), m.getSignature());
                            // Use full name as this map contains methods of all classes.
                            dp.unmatchedChangedMethods.put(mi.getClassName() + mi.getFullName(), mi);
                            dp.methodInfoToDSUMethod.put(mi, m);
                        }
                    }
                    for (DSUField f:c.getDSUFields()) {
                        if ((old && f.getUpdateType().isDeleted()) || (!old && f.getUpdateType().isAdded()) ) {
                            FieldInfo fi = ci.getDeclaredField(f.getName());
                            if (fi == null) {
                                throw new NullPointerException("Cannot find field " + f.getName() + " " + f.getSignature());
                            }
                            dp.unmatchedChangedFields.add(fi);
                        }
                        if (old && f.getUpdateType().isAdded() && !f.isStatic()) {
                            dp.hasNewInstanceFields.add(ci);
                        }
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException("Fail to create dynamic patch from " + filePath, e);
        }

        return dp;
    }

    /**
     * Get a set of matched methods.
     * @return
     */
    public Set<MethodInfo> getMatchedChangedMethods() {
        return matchedChangedMethods;
    }

    public boolean isMatchedChangedMethods(MethodInfo mi) {
        return this.matchedChangedMethods.contains(mi);
    }

    public boolean isUnmatchedChangedMethods(MethodInfo mi) {
        String id = mi.getClassName() + mi.getFullName();
        return unmatchedChangedMethods.containsKey(id);
    }

    public boolean hasNewFields(ClassInfo ci) {
        return this.hasNewInstanceFields.contains(ci);
    }

    public boolean isChangedClass(ClassInfo ci) {
        return changedClasses.contains(ci);
    }

    public Set<ClassInfo> getChangedClasses() {
        return changedClasses;
    }

    public MethodUpdateType getMethodUpdateType(MethodInfo mi) {
        DSUMethod m = methodInfoToDSUMethod.get(mi);
        if (m != null) {
            return m.getUpdateType();
        }
        return MethodUpdateType.NONE;
    }
}
