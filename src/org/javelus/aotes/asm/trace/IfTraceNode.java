package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.PathCondition;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public abstract class IfTraceNode extends InsnTraceNode implements PathCondition {

    private Value leftValue;
    private Value rightValue;

    private Instruction target;

    public IfTraceNode(TraceNode previous, Instruction ifInsn, Value leftValue, Value rightValue,
            Instruction target) {
        super(previous, ifInsn);
        this.leftValue = leftValue;
        this.rightValue = rightValue;
        this.target = target;
    }

    public Instruction getTarget() {
        return target;
    }

    public Value getLeftValue() {
        return leftValue;
    }

    public Value getRightValue() {
        return rightValue;
    }

    public String toString() {
        return "If[" + isIfTrue() + ",p=" + getInstruction() + ",to=" + getTarget() + "]";
    }

    public final boolean isConditional() {
        return true;
    }

    public abstract boolean isIfTrue();
}
