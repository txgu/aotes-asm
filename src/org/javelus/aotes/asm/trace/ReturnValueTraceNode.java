package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public class ReturnValueTraceNode extends InsnTraceNode {

    private Value ret;

    public ReturnValueTraceNode(TraceNode previous, Instruction instruction, Value ret) {
        super(previous, instruction);

        this.ret = ret;
    }

    public Value getReturnValue() {
        return ret;
    }

}
