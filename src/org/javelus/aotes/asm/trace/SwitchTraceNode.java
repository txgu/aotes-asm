package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.PathCondition;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public class SwitchTraceNode extends InsnTraceNode  implements PathCondition {

    private Value value;
    private int branch;

    public SwitchTraceNode(TraceNode previous, Instruction instruction, Value value, int branch) {
        super(previous, instruction);
        this.branch = branch;
        this.value = value;
    }

    public int getBranch() {
        return branch;
    }

    public Value getValue() {
        return value;
    }

    @Override
    public boolean isConditional() {
        return true;
    }
}
