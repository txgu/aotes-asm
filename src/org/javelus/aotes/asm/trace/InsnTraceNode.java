package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.vm.Instruction;

public class InsnTraceNode extends TraceNode {
    private Instruction instruction;

    public InsnTraceNode(TraceNode previous, Instruction instruction) {
        super(previous);
        this.instruction = instruction;
    }

    public Instruction getInstruction() {
        return instruction;
    }

    public void setInstruction(Instruction instruction) {
        this.instruction = instruction;
    }

    public String toString() {
        return this.instruction.toString();
    }
}
