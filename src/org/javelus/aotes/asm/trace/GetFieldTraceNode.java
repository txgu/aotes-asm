package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.Instruction;

public class GetFieldTraceNode extends InsnTraceNode {

    private ElementInfo object;
    private FieldInfo fi;
    private Value value;

    public GetFieldTraceNode(TraceNode previous, Instruction instruction, ElementInfo object, FieldInfo fi, Value value) {
        super(previous, instruction);
        this.object = object;
        this.fi = fi;
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

    public ElementInfo getObject() {
        return object;
    }

    public FieldInfo getFieldInfo() {
        return fi;
    }


}
