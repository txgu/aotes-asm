package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.ExecutionIndex;
import org.javelus.aotes.asm.value.ExecutionPoint;
import org.javelus.aotes.asm.value.PathCondition;

public class TraceNode implements ExecutionPoint, ExecutionIndex {

    static class EntryTraceNode extends TraceNode implements PathCondition {

        @Override
        public boolean isConditional() {
            return false;
        }

    }

    private int index;
    private TraceNode previous;
    private TraceNode next;

    private PathCondition condition;

    private TraceNode() {
        this.index = 0;
    }

    public TraceNode(TraceNode previous) {
        if (previous == null) {
            throw new RuntimeException("Should provide previous trace node");
        }
        this.previous = previous;
        this.previous.next = this;
        this.index = this.previous.index + 1;
    }

    public TraceNode getPrevious() {
        return previous;
    }

    public TraceNode getNext() {
        return next;
    }

    @Override
    public int getExecutionIndex() {
        return index;
    }

    public static TraceNode createEntryTraceNode() {
        return new EntryTraceNode();
    }

    public void setCondition(PathCondition previous) {
        this.condition = previous;
    }

    public PathCondition getCondition() {
        return condition;
    }

}
