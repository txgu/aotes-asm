package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.vm.MethodInfo;


public class Trace {
    private TraceNode begin;
    private TraceNode end;

    private TraceKey key;

    private MethodInfo entryMethod;

    public Trace(MethodInfo entryMethod, TraceNode begin, TraceNode end, TraceKey key) {
        this.begin = begin;
        this.end = end;
        this.key = key;
        this.entryMethod = entryMethod;
    }

    public MethodInfo getEntryMethod() {
        return entryMethod;
    }

    public TraceNode getBegin() {
        return begin;
    }

    public TraceNode getEnd() {
        return end;
    }

    public TraceKey getKey() {
        return key;
    }
}
