package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.Instruction;

public class PutStaticTraceNode extends InsnTraceNode {

    private FieldInfo fi;
    private Value value;

    public PutStaticTraceNode(TraceNode previous, Instruction instruction, FieldInfo fi, Value value) {
        super(previous, instruction);
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

    public FieldInfo getFieldInfo() {
        return fi;
    }
}
