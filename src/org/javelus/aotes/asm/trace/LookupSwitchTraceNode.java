package org.javelus.aotes.asm.trace;

import java.util.List;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public class LookupSwitchTraceNode extends SwitchTraceNode {

    List<Integer> keys;

    public LookupSwitchTraceNode(TraceNode previous, Instruction instruction, Value key, List<Integer> keys, int branch) {
        super(previous, instruction, key, branch);
        this.keys = keys;
    }

    public Value getKey() {
        return getValue();
    }

    public List<Integer> getKeys() {
        return keys;
    }
}
