package org.javelus.aotes.asm.trace;

import java.util.ArrayList;
import java.util.List;

import org.javelus.aotes.asm.vm.Instruction;
import org.javelus.aotes.asm.vm.MaxConditionException;
import org.javelus.aotes.asm.vm.MethodInfo;


public class TraceKey {

    static final int MAX_CONDITIONS = Integer.getInteger("aotes.trace.condtions.max", 100);

    private MethodInfo entryMethod;

    private List<Instruction> predicates = new ArrayList<Instruction>();
    private List<Integer> branches = new ArrayList<Integer>();
    private String keyString;
    public TraceKey(MethodInfo entryMethod) {
        this.entryMethod = entryMethod;
    }

    public MethodInfo getEntryMethodInfo() {
        return this.entryMethod;
    }

    public void addPathCondition(Instruction insn, int branch) {
        this.predicates.add(insn);
        this.branches.add(branch);
        if (this.predicates.size() > MAX_CONDITIONS) {
            throw new MaxConditionException();
        }
        keyString = null;
    }

    public int getNumOfConditions() {
        return predicates.size();
    }

    public Instruction getInstruction(int i) {
        return predicates.get(i);
    }

    public int getBranch(int i) {
        return this.branches.get(i);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((branches == null) ? 0 : branches.hashCode());
        result = prime * result + ((entryMethod == null) ? 0 : entryMethod.hashCode());
        result = prime * result + ((predicates == null) ? 0 : predicates.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TraceKey other = (TraceKey) obj;
        if (branches == null) {
            if (other.branches != null)
                return false;
        } else if (!branches.equals(other.branches))
            return false;
        if (entryMethod == null) {
            if (other.entryMethod != null)
                return false;
        } else if (!entryMethod.equals(other.entryMethod))
            return false;
        if (predicates == null) {
            if (other.predicates != null)
                return false;
        } else if (!predicates.equals(other.predicates))
            return false;
        return true;
    }

    public String getKeyString() {
        if (keyString == null) {
            StringBuilder sb = new StringBuilder();
            int size = branches.size();
            for (int i=0; i<size; i++) {
                sb.append(predicates.get(i).hashCode());
                sb.append(':');
                sb.append(branches.get(i));
                sb.append(';');
            }
            keyString = sb.toString();
        }
        return keyString;
    }
}
