package org.javelus.aotes.asm.trace;

import static org.objectweb.asm.Opcodes.D2F;
import static org.objectweb.asm.Opcodes.D2I;
import static org.objectweb.asm.Opcodes.D2L;
import static org.objectweb.asm.Opcodes.DADD;
import static org.objectweb.asm.Opcodes.DCMPG;
import static org.objectweb.asm.Opcodes.DCMPL;
import static org.objectweb.asm.Opcodes.DDIV;
import static org.objectweb.asm.Opcodes.DMUL;
import static org.objectweb.asm.Opcodes.DNEG;
import static org.objectweb.asm.Opcodes.DREM;
import static org.objectweb.asm.Opcodes.DSUB;
import static org.objectweb.asm.Opcodes.F2D;
import static org.objectweb.asm.Opcodes.F2I;
import static org.objectweb.asm.Opcodes.F2L;
import static org.objectweb.asm.Opcodes.FADD;
import static org.objectweb.asm.Opcodes.FCMPG;
import static org.objectweb.asm.Opcodes.FCMPL;
import static org.objectweb.asm.Opcodes.FDIV;
import static org.objectweb.asm.Opcodes.FMUL;
import static org.objectweb.asm.Opcodes.FNEG;
import static org.objectweb.asm.Opcodes.FREM;
import static org.objectweb.asm.Opcodes.FSUB;
import static org.objectweb.asm.Opcodes.I2B;
import static org.objectweb.asm.Opcodes.I2C;
import static org.objectweb.asm.Opcodes.I2D;
import static org.objectweb.asm.Opcodes.I2F;
import static org.objectweb.asm.Opcodes.I2L;
import static org.objectweb.asm.Opcodes.I2S;
import static org.objectweb.asm.Opcodes.IADD;
import static org.objectweb.asm.Opcodes.IAND;
import static org.objectweb.asm.Opcodes.IDIV;
import static org.objectweb.asm.Opcodes.IFEQ;
import static org.objectweb.asm.Opcodes.IFGE;
import static org.objectweb.asm.Opcodes.IFGT;
import static org.objectweb.asm.Opcodes.IFLE;
import static org.objectweb.asm.Opcodes.IFLT;
import static org.objectweb.asm.Opcodes.IFNE;
import static org.objectweb.asm.Opcodes.IFNONNULL;
import static org.objectweb.asm.Opcodes.IFNULL;
import static org.objectweb.asm.Opcodes.IF_ACMPEQ;
import static org.objectweb.asm.Opcodes.IF_ACMPNE;
import static org.objectweb.asm.Opcodes.IF_ICMPEQ;
import static org.objectweb.asm.Opcodes.IF_ICMPGE;
import static org.objectweb.asm.Opcodes.IF_ICMPGT;
import static org.objectweb.asm.Opcodes.IF_ICMPLE;
import static org.objectweb.asm.Opcodes.IF_ICMPLT;
import static org.objectweb.asm.Opcodes.IF_ICMPNE;
import static org.objectweb.asm.Opcodes.IINC;
import static org.objectweb.asm.Opcodes.IMUL;
import static org.objectweb.asm.Opcodes.INEG;
import static org.objectweb.asm.Opcodes.IOR;
import static org.objectweb.asm.Opcodes.IREM;
import static org.objectweb.asm.Opcodes.ISHL;
import static org.objectweb.asm.Opcodes.ISHR;
import static org.objectweb.asm.Opcodes.ISUB;
import static org.objectweb.asm.Opcodes.IUSHR;
import static org.objectweb.asm.Opcodes.IXOR;
import static org.objectweb.asm.Opcodes.L2D;
import static org.objectweb.asm.Opcodes.L2F;
import static org.objectweb.asm.Opcodes.L2I;
import static org.objectweb.asm.Opcodes.LADD;
import static org.objectweb.asm.Opcodes.LAND;
import static org.objectweb.asm.Opcodes.LCMP;
import static org.objectweb.asm.Opcodes.LDIV;
import static org.objectweb.asm.Opcodes.LMUL;
import static org.objectweb.asm.Opcodes.LNEG;
import static org.objectweb.asm.Opcodes.LOR;
import static org.objectweb.asm.Opcodes.LREM;
import static org.objectweb.asm.Opcodes.LSHL;
import static org.objectweb.asm.Opcodes.LSHR;
import static org.objectweb.asm.Opcodes.LSUB;
import static org.objectweb.asm.Opcodes.LUSHR;
import static org.objectweb.asm.Opcodes.LXOR;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.javelus.aotes.asm.CallGraph;
import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.accesspath.AccessPathRoots;
import org.javelus.aotes.asm.accesspath.TargetAccessPath;
import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.ArrayLength;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.InstanceField;
import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.LocationPath;
import org.javelus.aotes.asm.location.MethodParameterLocation;
import org.javelus.aotes.asm.location.StaticField;
import org.javelus.aotes.asm.summary.ExecutionSummary;
import org.javelus.aotes.asm.summary.MDG;
import org.javelus.aotes.asm.summary.MethodSummary;
import org.javelus.aotes.asm.summary.PostPartialHeap;
import org.javelus.aotes.asm.summary.PrePartialHeap;
import org.javelus.aotes.asm.value.BinopExpr;
import org.javelus.aotes.asm.value.ClonedRef;
import org.javelus.aotes.asm.value.Constant;
import org.javelus.aotes.asm.value.ExecutionPoint;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.NewRef;
import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.value.OperatorValue;
import org.javelus.aotes.asm.value.Output;
import org.javelus.aotes.asm.value.PathCondition;
import org.javelus.aotes.asm.value.Ref;
import org.javelus.aotes.asm.value.TargetRef;
import org.javelus.aotes.asm.value.UnopExpr;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.value.constant.BooleanValue;
import org.javelus.aotes.asm.value.constant.ByteValue;
import org.javelus.aotes.asm.value.constant.CharValue;
import org.javelus.aotes.asm.value.constant.ClassInfoValue;
import org.javelus.aotes.asm.value.constant.DoubleValue;
import org.javelus.aotes.asm.value.constant.FloatValue;
import org.javelus.aotes.asm.value.constant.IntegerValue;
import org.javelus.aotes.asm.value.constant.LongValue;
import org.javelus.aotes.asm.value.constant.ShortValue;
import org.javelus.aotes.asm.value.operator.MethodOperatorFactory;
import org.javelus.aotes.asm.vm.ArrayElementInfo;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.ExecutionInterruptedException;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.FrameEventListener;
import org.javelus.aotes.asm.vm.InstanceElementInfo;
import org.javelus.aotes.asm.vm.Instruction;
import org.javelus.aotes.asm.vm.Interpreter;
import org.javelus.aotes.asm.vm.MethodInfo;
import org.javelus.aotes.asm.vm.MultiArrayElementInfo;
import org.javelus.aotes.asm.vm.StackFrame;
import org.javelus.aotes.asm.vm.StaticElementInfo;
import org.javelus.aotes.asm.vm.ThreadInfo;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;


public class TraceEngine implements FrameEventListener<Value> {


    static void freeSet(Set<?> set){
        if (set != null) {
            set.clear();
        }
    }

    /**
     * 
     * @param className
     * @return
     */


    public static String getElementTypeName(String typeName) {
        if(!typeName.endsWith("[]")){
            throw new RuntimeException("Invalid type name for array type " + typeName);
        }

        return typeName.substring(0, typeName.lastIndexOf("[]"));
    }

    private Tracer tracer;

    private MDG mdg;

    private Random rand = new Random();

    /**
     * PrePartialHeap used to create allocated unknown values.
     */
    private PrePartialHeap prePartialHeap;

    private ThreadInfo<Value> attachedThread;

    /**
     * A set of objects allocated during execution
     */
    private Set<ElementInfo> allocatedObjects;
    private Set<ElementInfo> internalAllocatedObjects;

    /**
     * A set of objects allocated BEFORE execution
     */
    private Set<ElementInfo> allocatedSkeletonObjects;

    /**
     * A set of accessed unallocated values.
     */
    private Set<Input<?>> unknownNullPointers;

    /**
     * A set of used heap locations
     */
    private Set<HeapLocation> usedHeapLocations;

    /**
     * A set of defined heap locations 
     */
    private Set<HeapLocation> definedHeapLocations;

    /**
     * A set of used unknown values in the PrePartialHeap.
     */
    private Set<Input<?>> usedUnknownValues;

    /**
     * A list of method operator values.
     */
    private List<OperatorValue> usedMethodOperatorValues;

    private Map<Value, OperatorValue> receiverToOperatorValues;

    /**
     * method parameter input to this symbolic execution
     */
    private Input<MethodParameterLocation>[] unknownParameters;

    private Map<ClassInfo, StaticElementInfo> staticElementInfos;

    /**
     * method return output of this symbolic execution
     */
    private Value entryReturnValue;
    private ClassInfo pendingExceptionClassInfo;

    private TerminationStatus terminationStatus;

    private MethodSummary ms;

    public Input<MethodParameterLocation>[] getUnknownParameters() {
        return unknownParameters;
    }

    public static enum TerminationStatus {
        Exception,
        Return,
        Interrupted;

        public final boolean isReturn() {
            return this == Return;
        }

        public final boolean isException() {
            return this == Exception;
        }

        public final boolean isInterrupted() {
            return this == Interrupted;
        }
    }

    private ExecutionSummary es;
    private Interpreter interpreter;
    public TraceEngine(MDG mdg, MethodSummary ms) {
        this.mdg = mdg;
        this.ms = ms;
        this.es = new ExecutionSummary(ms, this);
        this.prePartialHeap = new PrePartialHeap(mdg.getPartialHeapSkeleton());
        this.tracer = new Tracer(ms.getMethodInfo());
    }

    public MDG mdg() {
        return mdg;
    }

    /**
     * If we invoke a method or access a field on an unknown reference, 
     * we trigger this require action.
     * @param ref
     */
    public boolean accessUnknownNullPointer(Value ref) {
        if (unknownNullPointers == null) {
            unknownNullPointers = new HashSet<Input<?>>();
        }
        if (ref instanceof Input) {
            if (!unknownNullPointers.isEmpty()) {
                throw new RuntimeException("Now we abort once we encounter an unknown null pointer");
            }
            unknownNullPointers.add((Input<?>)ref);
            return true;
        }
        return false;
    }

    void clearUnknownNullPointers () {
        if (unknownNullPointers != null) {
            unknownNullPointers.clear();
        }
        es.clearUnallocatedAccessPath();
    }

    public UnopExpr createAndTraceUnopExpr(Instruction insn, Value v1) {
        checkPathCondition(insn);
        UnopExpr expr = new UnopExpr(getPathCondition(), insn, v1);
        traceUnopExpr(insn, expr);
        return expr;
    }

    public Value createAndTraceBinopExpr(Instruction insn, Value v1, Value v2) {
        checkPathCondition(insn);
        if (insn.getOpcode() == Opcodes.INSTANCEOF) {
            BinopExpr expr =  new BinopExpr(getPathCondition(), insn, v1, v2);
            traceBinopExpr(insn, expr);
            return expr;
        }

        if (v1 instanceof Constant && v2 instanceof Constant) {
            return evaluateConstantExpr(insn, (Constant)v1, (Constant)v2);
        }
        BinopExpr expr =  new BinopExpr(getPathCondition(), insn, v1, v2);
        traceBinopExpr(insn, expr);
        return expr;
    }

    private void checkPathCondition(Instruction insn) {
        if (insn == null) {
            return;
        }
        PathCondition pc = getPathCondition();
        if (pc instanceof InsnTraceNode) {
            InsnTraceNode itn = (InsnTraceNode) pc;
            if (itn.getInstruction().getMethodInfo() != insn.getMethodInfo()) {
                throw new RuntimeException("Unpaired pc");
            }
        } else if (pc instanceof MethodEntryTraceNode) {
            MethodEntryTraceNode metn = (MethodEntryTraceNode) pc;
            if (metn.getMethodInfo() != insn.getMethodInfo()) {
                throw new RuntimeException("Unpaired pc");
            }
        } else if (pc instanceof MethodExitTraceNode) {
            MethodExitTraceNode metn = (MethodExitTraceNode) pc;
            MethodEntryTraceNode caller = metn.getEntryNode().getCaller();
            if (caller != null && caller.getMethodInfo() != insn.getMethodInfo()) {
                throw new RuntimeException("Unpaired pc");
            }
        }
    }

    public static int getIntValue(Constant v) {
        if (v instanceof IntegerValue) {
            return ((IntegerValue) v).getValue();
        } else if (v instanceof ByteValue) {
            return ((ByteValue)v).getValue();
        } else if (v instanceof BooleanValue) {
            return ((BooleanValue)v).getValue() ? 1 : 0;
        } else if (v instanceof CharValue) {
            return ((CharValue)v).getValue();
        }

        throw new RuntimeException("Unknown type: " + v);
    }

    private Value evaluateConstantExpr(Instruction insn, Constant v1, Constant v2) {
        int opcode = insn.getOpcode();

        switch (opcode) {
        case IINC:
        case IADD: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 + c2); }
        case LADD: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() + c2.getValue()); }
        case FADD: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() + c2.getValue()); }
        case DADD: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() + c2.getValue()); }
        case ISUB: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 - c2); }
        case LSUB: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() - c2.getValue()); }
        case FSUB: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() - c2.getValue()); }
        case DSUB: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() - c2.getValue()); }
        case IMUL: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 * c2); }
        case LMUL: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() * c2.getValue()); }
        case FMUL: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() * c2.getValue()); }
        case DMUL: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() - c2.getValue()); }
        case IDIV: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 / c2); }
        case LDIV: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() / c2.getValue()); }
        case FDIV: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() / c2.getValue()); }
        case DDIV: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() / c2.getValue()); }
        case IREM: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 % c2); }
        case LREM: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() % c2.getValue()); }
        case FREM: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            return FloatValue.valueOf(c1.getValue() % c2.getValue()); }
        case DREM: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            return DoubleValue.valueOf(c1.getValue() % c2.getValue()); }
        case INEG: {
            int c1 = getIntValue(v1);
            return IntegerValue.valueOf(-c1); }
        case LNEG: {
            LongValue c1 = (LongValue) v1;
            return LongValue.valueOf(-c1.getValue()); }
        case FNEG: {
            FloatValue c1 = (FloatValue) v1;
            return FloatValue.valueOf(-c1.getValue()); }
        case DNEG: {
            DoubleValue c1 = (DoubleValue) v1;
            return DoubleValue.valueOf(-c1.getValue()); }
        case ISHL: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 << c2); }
        case LSHL: {
            LongValue c1 = (LongValue) v1;
            int c2 = getIntValue(v2);
            return LongValue.valueOf(c1.getValue() << c2); }
        case ISHR: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 >> c2); }
        case LSHR: {
            LongValue c1 = (LongValue) v1;
            int c2 = getIntValue(v2);
            return LongValue.valueOf(c1.getValue() >> c2); }
        case IUSHR: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 >>> c2); }
        case LUSHR: {
            LongValue c1 = (LongValue) v1;
            int c2 = getIntValue(v2);
            return LongValue.valueOf(c1.getValue() >>> c2); }
        case IAND: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 & c2); }
        case LAND: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() & c2.getValue()); }
        case IOR: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 | c2); }
        case LOR: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() | c2.getValue()); }
        case IXOR: {
            int c1 = getIntValue(v1);
            int c2 = getIntValue(v2);
            return IntegerValue.valueOf(c1 ^ c2); }
        case LXOR: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            return LongValue.valueOf(c1.getValue() ^ c2.getValue()); }
        case I2L: {
            int c1 = getIntValue(v1);
            return LongValue.valueOf(c1); }
        case I2F: {
            int c1 = getIntValue(v1);
            return FloatValue.valueOf(c1); }
        case I2D: {
            int c1 = getIntValue(v1);
            return DoubleValue.valueOf(c1); }
        case L2I: {
            LongValue c1 = (LongValue) v1;
            return IntegerValue.valueOf((int)c1.getValue()); }
        case L2F: {
            LongValue c1 = (LongValue) v1;
            return FloatValue.valueOf((float)c1.getValue()); }
        case L2D: {
            LongValue c1 = (LongValue) v1;
            return DoubleValue.valueOf((double)c1.getValue()); }
        case F2I: {
            FloatValue c1 = (FloatValue) v1;
            return IntegerValue.valueOf((int)c1.getValue()); }
        case F2L: {
            FloatValue c1 = (FloatValue) v1;
            return LongValue.valueOf((long)c1.getValue()); }
        case F2D: {
            FloatValue c1 = (FloatValue) v1;
            return DoubleValue.valueOf(c1.getValue()); }
        case D2I: {
            DoubleValue c1 = (DoubleValue) v1;
            return IntegerValue.valueOf((int)c1.getValue()); }
        case D2L: {
            DoubleValue c1 = (DoubleValue) v1;
            return LongValue.valueOf((long)c1.getValue()); }
        case D2F: {
            DoubleValue c1 = (DoubleValue) v1;
            return FloatValue.valueOf((float)c1.getValue()); }
        case I2B: {
            int c1 = getIntValue(v1);
            return ByteValue.valueOf((byte)c1); }
        case I2C: {
            int c1 = getIntValue(v1);
            return CharValue.valueOf((char)c1); }
        case I2S: {
            int c1 = getIntValue(v1);
            return ShortValue.valueOf((short)c1); }
        case LCMP: {
            LongValue c1 = (LongValue) v1;
            LongValue c2 = (LongValue) v2;
            long l1 = c1.getValue();
            long l2 = c2.getValue();
            if (l1 > l2) {
                return IntegerValue.one;
            } else if (l1 == l2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        case FCMPL: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            float f1 = c1.getValue();
            float f2 = c2.getValue();
            if (Float.isNaN(f1) || Float.isNaN(f2)) {
                return IntegerValue.m1;
            }
            if (f1 > f2) {
                return IntegerValue.one;
            } else if (f1 == f2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        case FCMPG: {
            FloatValue c1 = (FloatValue) v1;
            FloatValue c2 = (FloatValue) v2;
            float f1 = c1.getValue();
            float f2 = c2.getValue();
            if (Float.isNaN(f1) || Float.isNaN(f2)) {
                return IntegerValue.one;
            }
            if (f1 > f2) {
                return IntegerValue.one;
            } else if (f1 == f2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        case DCMPL: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            double d1 = c1.getValue();
            double d2 = c2.getValue();
            if (Double.isNaN(d1) || Double.isNaN(d2)) {
                return IntegerValue.m1;
            }
            if (d1 > d2) {
                return IntegerValue.one;
            } else if (d1 == d2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        case DCMPG: {
            DoubleValue c1 = (DoubleValue) v1;
            DoubleValue c2 = (DoubleValue) v2;
            double d1 = c1.getValue();
            double d2 = c2.getValue();
            if (Double.isNaN(d1) || Double.isNaN(d2)) {
                return IntegerValue.one;
            }
            if (d1 > d2) {
                return IntegerValue.one;
            } else if (d1 == d2) {
                return IntegerValue.zero;
            } else {
                return IntegerValue.m1;
            }}
        }
        throw new RuntimeException("Should not reach here " + insn);
    }

    public Input<MethodParameterLocation> createMethodParameterUnknownValue(MethodInfo mi, int index) {
        return new Input<MethodParameterLocation>(new LocationPath(new MethodParameterLocation(mi, index)),
                new MethodParameterLocation(mi, index));
    }

    public TargetRef createTargetRef(ElementInfo recv) {
        TargetRef top = new TargetRef(getPathCondition(), recv);
        return top;
    }

    public void useUnknownValue(Input<?> unknownValue) {
        if (usedUnknownValues == null) {
            usedUnknownValues = new HashSet<Input<?>>();
        }
        usedUnknownValues.add(unknownValue);
    }

    public <T extends HeapLocation> Input<T> createUnknownInput(LocationPath lp, T loc) {
        return new Input<T>(lp, loc);
    }

    protected OperatorValue createOperatorValue(Operator op, Value receiver, Value[] operands) {
        checkPathCondition(attachedThread.getTopFrame().getPC());
        OperatorValue previous = null;
        OperatorValue next;

        if (receiverToOperatorValues == null) {
            receiverToOperatorValues = new HashMap<Value, OperatorValue>();
        }

        previous = this.receiverToOperatorValues.get(receiver);
        next = new OperatorValue(getPathCondition(), previous, op, receiver, operands);

        this.receiverToOperatorValues.put(receiver, next);
        return next;
    }

    public OperatorValue createMethodOperatorValue(MethodInfo mi, Value ... args) {
        Operator op = MethodOperatorFactory.getMethodOperator(mi);

        if (op == null) {
            throw new RuntimeException("Unregistered method operator for method " + mi);
        }

        Value receiver;
        if (mi.isStatic()) {
            receiver = ClassInfoValue.valueOf(mi.getClassInfo());
        } else {
            receiver = args[0];
        }

        Value[] operands = new Value[args.length];
        if (args.length > 0) {
            System.arraycopy(args, 0, operands, 0, args.length);
        }

        return createOperatorValue(op, receiver, operands);
    }

    public NewRef newObject(Instruction insn, ClassInfo ci) {
        checkPathCondition(insn);
        ElementInfo ei = allocateObject(ci);
        if (insn == null) {
            internalAllocateObject(ei);
        }
        return new NewRef(getPathCondition(), ei);
    }

    public NewRef newArray(Instruction insn, ClassInfo ci, Value length) {
        checkPathCondition(insn);
        ArrayElementInfo ei = (ArrayElementInfo) allocateObject(ci);
        ei.arrayLength().value(length);
        NewRef nao = new NewRef(getPathCondition(), ei);
        return nao;
    }

    public NewRef newMultiArray(Instruction insn, ClassInfo ci, Value[] lengths, int d) {
        checkPathCondition(insn);
        MultiArrayElementInfo ei = new MultiArrayElementInfo(ci, lengths, d);
        NewRef nao = new NewRef(getPathCondition(), ei);
        return nao;
    }

    public ClonedRef cloneObject(Value source) {
        ElementInfo from = Value.getElementInfo(source);
        ElementInfo to   = from.clone();

        if (allocatedObjects == null) {
            allocatedObjects = new HashSet<ElementInfo>();
        }
        allocatedObjects.add(to);
        for (HeapLocation loc:to.locations()) {
            defineHeapLocation(loc);
        }
        ClonedRef cref = new ClonedRef(getPathCondition(), source, to);
        return cref;
    }

    public ElementInfo allocateObject(ClassInfo ci) {
        ElementInfo ei = null;
        if (ci.isArray()) {
            ei = new ArrayElementInfo(ci);
        } else {
            ei = new InstanceElementInfo(ci);
        }
        if (allocatedObjects == null) {
            allocatedObjects = new HashSet<ElementInfo>();
        }
        allocatedObjects.add(ei);
        return ei;
    }

    public void internalAllocateObject(ElementInfo ei) {
        if (internalAllocatedObjects == null) {
            internalAllocatedObjects = new HashSet<ElementInfo>();
        }
        internalAllocatedObjects.add(ei);
    }

    public ElementInfo allocateSkeletonObject(ClassInfo ci) {
        ElementInfo ei = null;
        if (ci.isArray()) {
            ei = new ArrayElementInfo(ci);
        } else {
            ei = new InstanceElementInfo(ci);
        }
        ei.setAllocPre();
        if (allocatedSkeletonObjects == null) {
            allocatedSkeletonObjects = new HashSet<ElementInfo>(); 
        }
        allocatedSkeletonObjects.add(ei);
        return ei;
    }

    public void defineHeapLocation(HeapLocation location) {
        if (definedHeapLocations == null) {
            definedHeapLocations = new HashSet<HeapLocation>(); 
        }
        definedHeapLocations.add(location);
    }

    public Trace detachTrace() {
        return tracer.detachTrace();
    }

    public void downcast(ElementInfo ei, String newType) {
        //        ClassInfo ci = ClassLoaderInfo.getCurrentResolvedClassInfo(newType);
        //        if (ci == null) {
        //            throw new RuntimeException("Cannot resolve " + newType);
        //        }
        //        downcast(ei, ci);
        throw new RuntimeException("Not implemented");
    }



    /**
     * Downcast an object to an object of its subclass
     * @param ei
     * @param newType
     */
    public void downcast(ElementInfo ei, ClassInfo newType) {
        //DowncastHelper.downcast(this, ei, newType);
        throw new RuntimeException("Not implemented");
    }

    public Set<Input<?>> getAccessedUnknownNullPointers() {
        if (unknownNullPointers == null) {
            return Collections.emptySet();
        }
        return unknownNullPointers;
    }


    public TraceNode getActiveTraceNode() {
        return tracer.getActiveTraceNode();
    }

    public Set<ElementInfo> getAllocatedObjects() {
        if (allocatedObjects == null) {
            return Collections.emptySet();
        }
        return allocatedObjects;
    }

    public Tracer getTracer() {
        return tracer;
    }

    public Set<HeapLocation> getDefinedHeapLocations() {
        if (definedHeapLocations == null) {
            return Collections.emptySet();
        }
        return definedHeapLocations;
    }

    public ExecutionPoint getExecutionPoint() {
        return tracer.getActiveTraceNode();
    }

    public PathCondition getPathCondition() {
        return tracer.getCondition();
    }

    public ThreadInfo<Value> getThreadInfo() {
        return attachedThread;
    }

    /**
     * The entry of this trace engine.
     * @return
     */
    public MethodInfo getEntryMethodInfo() {
        return this.ms.getMethodInfo();
    }

    /**
     * The target class info.
     * @return
     */
    public ClassInfo getEntryClassInfo() {
        return this.getEntryMethodInfo().getClassInfo();
    }

    public ClassInfo getTargetClassInfo() {
        return this.ms.getTargetClassInfo();
    }

    public Set<HeapLocation> getUsedHeapLocations() {
        if (usedHeapLocations == null) {
            return Collections.emptySet(); 
        }
        return usedHeapLocations;
    }

    public Set<Input<?>> getUsedUnknownValues() {
        if (usedUnknownValues == null) {
            usedUnknownValues = new HashSet<Input<?>>(); 
        }
        return usedUnknownValues;
    }

    protected void initializeCallTree(MethodInfo entry) {
        tracer = new Tracer(entry);
    }

    public boolean isNewAllocated(ElementInfo ei) {
        if(allocatedObjects == null){
            return false;
        }

        return allocatedObjects.contains(ei);
    }

    @SuppressWarnings("unchecked")
    public void pushMethodArgs(MethodInfo mi, StackFrame<Value> frame, Value recv) {
        int argOffset = 0;
        if (!mi.isStatic()) {
            argOffset = frame.setLocal(argOffset, recv);
        }

        Type[] types = mi.getArgumentTypes();
        unknownParameters = new Input[types.length];
        for (int i=0; i<types.length; i++) {
            int sort = types[i].getSort();
            Input<MethodParameterLocation> unknownValue = 
                    createMethodParameterUnknownValue(mi, i);
            unknownParameters[i] = unknownValue;
            switch (sort) {
            case Type.DOUBLE:
            case Type.LONG:
                argOffset = frame.setLongLocal(argOffset, unknownValue);
                break;
            case Type.FLOAT:
            case Type.INT:
            case Type.CHAR:
            case Type.BYTE:
            case Type.BOOLEAN:
            case Type.SHORT:
            default:
                argOffset = frame.setLocal(argOffset, unknownValue);
            }
        }
    }

    public Value[] popMethodArgs(MethodInfo mi, StackFrame<Value> frame) {
        int numOfArguments = mi.getNumOfArguments();
        int argOffset;
        Value[] results;
        if (mi.isStatic()) {
            results = new Value[numOfArguments];
            argOffset = numOfArguments - 1;
        } else {
            results = new Value[numOfArguments + 1];
            argOffset = numOfArguments;
        }

        Type[] types = mi.getArgumentTypes();
        for (int i=numOfArguments - 1; i>=0; i--, argOffset--){
            int sort = types[i].getSort();
            switch (sort) {
            case Type.DOUBLE:
            case Type.LONG:
                results[argOffset] = frame.popLong();
                break;
            case Type.FLOAT:
            case Type.INT:
            case Type.CHAR:
            case Type.BYTE:
            case Type.BOOLEAN:
            case Type.SHORT:
            default:
                results[argOffset] = frame.pop();
            }
        }

        if (!mi.isStatic()) {
            results[argOffset] = frame.pop();
        }

        return results;
    }

    public Value[] peekMethodArgs(MethodInfo mi, StackFrame<Value> frame) {
        int numOfArguments = mi.getNumOfArguments();
        int argOffset;
        Value[] results;
        if (mi.isStatic()) {
            results = new Value[numOfArguments];
            argOffset = numOfArguments - 1;
        } else {
            results = new Value[numOfArguments + 1];
            argOffset = numOfArguments;
        }

        Type[] types = mi.getArgumentTypes();
        int topOffset = 0;
        for (int i=numOfArguments - 1; i>=0; i--, argOffset--){
            int sort = types[i].getSort();
            switch (sort) {
            case Type.DOUBLE:
            case Type.LONG:
                topOffset++;
                results[argOffset] = frame.peek(topOffset++);
                break;
            case Type.FLOAT:
            case Type.INT:
            case Type.CHAR:
            case Type.BYTE:
            case Type.BOOLEAN:
            case Type.SHORT:
            default:
                results[argOffset] = frame.peek(topOffset++);
            }
        }

        if (!mi.isStatic()) {
            results[argOffset] = frame.peek(topOffset);
        }

        return results;
    }

    public String getTerminationStatusLine() {
        if (terminationStatus == TerminationStatus.Return) {
            return String.format("Return(%s)", entryReturnValue == null ? "void" : entryReturnValue);
        }

        if (terminationStatus == TerminationStatus.Exception) {
            return String.format("Exception(%s)", pendingExceptionClassInfo.getName());
        }

        if (terminationStatus == TerminationStatus.Interrupted) {
            return String.format("Interrupted");
        }

        throw new RuntimeException("Should not reach here!");
    }

    public void execute() {
        if (interpreter != null) {
            throw new RuntimeException("sanity check failed"); 
        }
        interpreter = new Interpreter(this);

        try {
            interpreter.execute();
            terminate();
        } catch (ExecutionInterruptedException e) {
            interrput();
        }
        summarize();
        mdg.addSummary(ms, es);
    }


    public void resume() {
        try {
            clearUnknownNullPointers();
            interpreter.resume();
            terminate();
        } catch (ExecutionInterruptedException e) {
            interrput();
        }
        summarize();
        mdg.addSummary(ms, es);
    }

    protected void interrput() {
        terminationStatus = TerminationStatus.Interrupted;
    }

    public TargetRef allocateTarget() {
        TargetRef targetRefValue = prePartialHeap.createTargetRef(this, ms.getTargetClassInfo());

        // TODO: in fact, the target object of a constructor should not pre-allocated
        ElementInfo targetObject = targetRefValue.getElementInfo();
        if (ms.getMethodInfo().isInit()) {
            targetObject.setAllocNew();
        }
        return targetRefValue;
    }


    protected void terminate() {
        TraceNode activeNode = tracer.getActiveTraceNode();

        if (!(activeNode instanceof MethodExitTraceNode)) {
            throw new RuntimeException("Invalid terminate status");
        }
        MethodExitTraceNode metn = (MethodExitTraceNode)activeNode;

        if (metn.getEntryNode().getMethodInfo() != tracer.getEntryMethodInfo()) {
            throw new RuntimeException("Sanity check failed");
        }

        if (metn.getReturnValue() != null) {
            entryReturnValue = (Value) metn.getReturnValue();
            terminationStatus = TerminationStatus.Return;
        } else if (metn.getExceptionClassInfo() != null) {
            // blacklist the last conditional
            blacklistLastConditionalCondition();
            pendingExceptionClassInfo = metn.getExceptionClassInfo();
            terminationStatus = TerminationStatus.Exception;
        } else if (tracer.getEntryMethodInfo().getReturnSize() == 0) {
            terminationStatus = TerminationStatus.Return;
        } else {
            throw new RuntimeException("Sanity check failed!");
        }
    }

    public Value getEntryReturnValue() {
        return entryReturnValue;
    }

    public void traceBinopExpr(Instruction insn, BinopExpr binopExpr) {
        getTracer().binopExpr(insn, binopExpr);
    }

    /**
     * Trace define of an allocated array element.
     * TODO: For unallocated element, use defineHeapLocation directly.
     * @param insn
     * @param ei
     * @param index
     * @param value
     */
    public void traceDefineArrayElement(Instruction insn, ArrayElement ae) {
        defineHeapLocation(ae);
        getTracer().defineArrayElement(insn, ae.getContainer(), ae.getIndex(), ae.value());
    }

    /**
     * Trace define of an allocated instance field.
     * TODO: For unallocated element, use defineHeapLocation directly.
     * @param insn
     * @param ei
     * @param fi
     * @param value
     */
    public void traceDefineInstanceField(Instruction insn, InstanceField inf) {
        defineHeapLocation(inf);
        getTracer().putField(insn, inf.getContainer(), inf.getFieldInfo(), inf.value());
    }

    public void traceIfFalse(Instruction insn, Value leftValue, Value rightValue, Instruction target){
        getTracer().ifFalse(insn, leftValue, rightValue, target);
    }

    public static final int CONSTANT_IF_FALSE = 0;
    public static final int CONSTANT_IF_TRUE = 1;
    public static final int CONSTANT_IF_UNKNOWN = -1;

    private static final boolean debug = false;

    private static final boolean EvaluateConstantIf = true;

    private static final boolean useSimpleProfiler = true;

    /**
     * 
     * @param insn
     * @param v1
     * @param v2
     * @return 0 for false, and 1 for true and -1 for unknown
     */
    public static int evaluateConstantIf(Instruction insn, Value v1, Value v2) {
        if (!EvaluateConstantIf) {
            return CONSTANT_IF_UNKNOWN;
        }
        int opcode = insn.getOpcode();
        switch (opcode) {
        case IFEQ: {
            if (v1 instanceof Constant) {
                int i = getIntValue((Constant)v1);
                return i == 0 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IFNE: {
            if (v1 instanceof Constant) {
                int i = getIntValue((Constant)v1);
                return i != 0 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IFLT: {
            if (v1 instanceof Constant) {
                int i = getIntValue((Constant)v1);
                return i < 0 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IFGE: {
            if (v1 instanceof Constant) {
                int i = getIntValue((Constant)v1);
                return i >= 0 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IFGT: {
            if (v1 instanceof Constant) {
                int i = getIntValue((Constant)v1);
                return i > 0 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IFLE: {
            if (v1 instanceof Constant) {
                int i = getIntValue((Constant)v1);
                return i <= 0 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IF_ICMPEQ: {
            if (v1 == v2) {
                return CONSTANT_IF_TRUE;
            }
            if (v1 instanceof Constant && v2 instanceof Constant) {
                int i1 = getIntValue((Constant)v1);
                int i2 = getIntValue((Constant)v2);
                return i1 == i2 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IF_ICMPNE: {
            if (v1 == v2) {
                return CONSTANT_IF_FALSE;
            }
            if (v1 instanceof Constant && v2 instanceof Constant) {
                int i1 = getIntValue((Constant)v1);
                int i2 = getIntValue((Constant)v2);
                return i1 != i2 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IF_ICMPLT: {
            if (v1 == v2) {
                return CONSTANT_IF_FALSE;
            }
            if (v1 instanceof Constant && v2 instanceof Constant) {
                int i1 = getIntValue((Constant)v1);
                int i2 = getIntValue((Constant)v2);
                return i1 < i2 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IF_ICMPGE: {
            if (v1 == v2) {
                return CONSTANT_IF_TRUE;
            }
            if (v1 instanceof Constant && v2 instanceof Constant) {
                int i1 = getIntValue((Constant)v1);
                int i2 = getIntValue((Constant)v2);
                return i1 >= i2 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IF_ICMPGT: {
            if (v1 == v2) {
                return CONSTANT_IF_FALSE;
            }
            if (v1 instanceof Constant && v2 instanceof Constant) {
                int i1 = getIntValue((Constant)v1);
                int i2 = getIntValue((Constant)v2);
                return i1 > i2 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IF_ICMPLE: {
            if (v1 == v2) {
                return CONSTANT_IF_TRUE;
            }
            if (v1 instanceof Constant && v2 instanceof Constant) {
                int i1 = getIntValue((Constant)v1);
                int i2 = getIntValue((Constant)v2);
                return i1 <= i2 ? CONSTANT_IF_TRUE : CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IF_ACMPEQ: {
            if (v1 == v2) {
                return CONSTANT_IF_TRUE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IF_ACMPNE: {
            if (v1 == v2) {
                return CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IFNULL: {
            if (v1 instanceof Ref) {
                if (Value.getElementInfo(v1) == null) {
                    return CONSTANT_IF_TRUE;
                } else {
                    return CONSTANT_IF_FALSE;
                }
            }
            if (v1 == Value.NULL) {
                return CONSTANT_IF_TRUE;
            }
            return CONSTANT_IF_UNKNOWN; }
        case IFNONNULL: {
            if (v1 instanceof Ref) {
                if (Value.getElementInfo(v1) == null) {
                    return CONSTANT_IF_FALSE;
                } else {
                    return CONSTANT_IF_TRUE;
                }
            }
            if (v1 == Value.NULL) {
                return CONSTANT_IF_FALSE;
            }
            return CONSTANT_IF_UNKNOWN; }
        }
        return CONSTANT_IF_UNKNOWN;
    }

    public Instruction evaluateIf(Instruction insn, Value v1, Value v2) {
        Instruction trueTarget = insn.getTarget();
        Instruction falseTarget = insn.getNext();

        int constResult = evaluateConstantIf(insn, v1, v2);
        if (constResult == CONSTANT_IF_TRUE) {
            traceIfTrue(insn, v1, v2, trueTarget);
            return trueTarget;
        } else if (constResult == CONSTANT_IF_FALSE) {
            traceIfFalse(insn, v1, v2, falseTarget);
            return falseTarget;
        }

        TraceKey traceKey = tracer.getTraceKey();
        boolean trueVisited = mdg.getProfiler().isBranchVisited(ms, traceKey, insn, 1);
        boolean falseVisited = mdg.getProfiler().isBranchVisited(ms, traceKey, insn, 0);

        if (useSimpleProfiler) {
            trueVisited = es.getMethodSummary().isBranchVisited(insn, 1);
            falseVisited = es.getMethodSummary().isBranchVisited(insn, 0);
        }

        boolean branch;

        if (trueVisited && falseVisited
                || !trueVisited && !falseVisited) {
            boolean trueAvailable = mdg.getProfiler().takenIf(insn, true);
            boolean falseAvailable = mdg.getProfiler().takenIf(insn, false);
            if (trueAvailable && falseAvailable) {
                branch = rand.nextBoolean();
            } else if (trueAvailable) {
                branch = true;
            } else if (falseAvailable) {
                branch = false;
            } else {
                branch = rand.nextBoolean();
            }
        } else if (trueVisited) {
            branch = false;
        } else if (falseVisited) {
            branch =  true;
        } else {
            throw new RuntimeException("Should not reach here");
        }

        if (branch) {
            traceIfTrue(insn, v1, v2, trueTarget);
            return trueTarget;
        } else {
            traceIfFalse(insn, v1, v2, falseTarget);
            return falseTarget;
        }
    }

    public void traceIfTrue(Instruction insn, Value leftValue, Value rightValue, Instruction target){
        getTracer().ifTrue(insn, leftValue, rightValue, target);
    }

    public void traceTableSwitch(Instruction insn, Value index, int min, int max, int branch){
        getTracer().tableSwitch(insn, index, min, max, branch);
    }

    public void traceLookupSwitch(Instruction insn, Value key, List<Integer> keys, int branch) {
        getTracer().lookupSwitch(insn, key, keys, branch);
    }


    public void traceInvoke(MethodInfo caller, Instruction insn, MethodInfo callee, Value[] arguments) {
        CallGraph.addEdge(caller, insn, callee);
        int begin = callee.isStatic() ? 0 : 1;
        Type[] argTypes = callee.getArgumentTypes();
        for (int i = begin; i<arguments.length; i++) {
            int sort = argTypes[i - begin].getSort();
            if (sort == Type.ARRAY || sort == Type.OBJECT) {
                Value v = arguments[i];
                ElementInfo ei = Value.getElementInfo(v);
                if (ei != null) {
                    AccessPath ap = AccessPathRoots.getMethodParameter(callee, i - begin);
                    ClassInfo ci = ei.getClassInfo();
                    mdg.getPartialHeapSkeleton().merge(ap, ci);
                }
            }
        }
    }

    /**
     * callee must be the actual callee.
     * The simplest way is to add trace hook during setupCallee.
     * @param thread
     * @param insn
     * @param callee
     */
    public void traceMethodEntry(Instruction insn, MethodInfo callee){
        getTracer().enterMethod(insn, callee);
    }

    /**
     * 
     * @param thread
     * @param insn: must be return now, cannot exit a method with exception...
     */
    public void traceMethodExit(Instruction insn, Object retValue, ClassInfo exInfo){
        getTracer().exitMethod(insn, retValue, exInfo);
    }

    protected void traceNewObject(Instruction insn, ElementInfo objRef) {
        getTracer().newObject(insn, objRef);
    }

    public void traceUnopExpr(Instruction insn, UnopExpr unopExpr) {
        getTracer().unopExpr(insn, unopExpr);
    }

    public void traceUseArrayElement(Instruction insn, ArrayElement ae) {
        useHeapLocation(ae);
        getTracer().useArrayElement(insn, ae.getContainer(), ae.getIndex(), ae.value());
    }

    public void traceUseInstanceField(Instruction insn, InstanceField inf) {
        useHeapLocation(inf);
        getTracer().getField(insn, inf.getContainer(), inf.getFieldInfo(), inf.value());
    }

    public void useArrayLength(ArrayLength al) {
        useHeapLocation(al);
    }

    public void useHeapLocation(HeapLocation location) {
        if (usedHeapLocations == null) {
            usedHeapLocations = new HashSet<HeapLocation>();
        }
        usedHeapLocations.add(location);
    }

    public void traceMethodOperator(OperatorValue value) {
        if (usedMethodOperatorValues == null) {
            usedMethodOperatorValues = new ArrayList<OperatorValue>();
        }
        this.usedMethodOperatorValues.add(value);
    }

    public List<OperatorValue> getMethodOperatorValues() {
        if (usedMethodOperatorValues == null) {
            return Collections.emptyList();
        }
        return this.usedMethodOperatorValues;
    }

    public void traceLoadStack(Instruction insn, Value attr) {
        getTracer().loadStack(insn, attr);
    }

    public void traceStoreStack(Instruction insn, Value attr) {
        getTracer().storeStack(insn, attr);
    }

    public void framePush(ThreadInfo<Value> ti) {

    }


    public void framePop(ThreadInfo<Value> ti) {
        ClassInfo exInfo = null;
        Object retValue = null;
        StackFrame<Value> topFrame = ti.getTopFrame();
        MethodInfo exitingMethod = topFrame.getMethodInfo();
        if (ti.getPendingException() != null) {
            exInfo = Value.getElementInfo(ti.getPendingException()).getClassInfo();
        } else if (exitingMethod.getReturnSize() == 1) {
            retValue = topFrame.peek();
            if (retValue == null) {
                throw new RuntimeException("Return value is null for " + exitingMethod);
            }
        } else if (exitingMethod.getReturnSize() == 2) {
            retValue = topFrame.peekLong();
            if (retValue == null) {
                throw new RuntimeException("Return value is null for " + exitingMethod);
            }
        } else if (exitingMethod.getReturnSize() == 0) {

        } else {
            throw new RuntimeException("Sanity check failed!");
        }

        traceMethodExit(null, retValue, exInfo);
    }

    public Collection<StaticElementInfo> getStaticElementInfos() {
        if (staticElementInfos == null) {
            return Collections.emptyList();
        }
        return staticElementInfos.values();
    }

    public StaticElementInfo getStaticElementInfo(ClassInfo ci) {
        if (staticElementInfos == null) {
            staticElementInfos = new HashMap<ClassInfo, StaticElementInfo>(); 
        }
        StaticElementInfo sei = staticElementInfos.get(ci);
        if (sei == null) {
            sei = new StaticElementInfo(ci);
            prePartialHeap.registerStaticElementInfo(sei);
            sei.setReference(ClassInfoValue.valueOf(ci));
            staticElementInfos.put(ci, sei);
        }
        return sei;
    }

    public void traceDefineStaticField(Instruction putstatic, StaticField sf) {
        defineHeapLocation(sf);
        getTracer().putStatic(putstatic, sf.getFieldInfo(), sf.value());
    }

    public void traceUseStaticField(Instruction getstatic, StaticField sf) {
        useHeapLocation(sf);
        getTracer().getStatic(getstatic, sf.getFieldInfo(), sf.value());
    }

    public TerminationStatus getTerminationStatus() {
        return this.terminationStatus;
    }

    public void setPrePartialHeap(PrePartialHeap prePartialHeap) {
        this.prePartialHeap = prePartialHeap;
    }

    public PrePartialHeap getPrePartialHeap() {
        return prePartialHeap;
    }

    public void attachThreadInfo(ThreadInfo<Value> ti) {
        this.attachedThread = ti;
        ti.addFrameListener(this);
    }

    public void detachThreadInfo(ThreadInfo<Value> ti) {
        if (this.attachedThread != ti)
            throw new IllegalStateException("The detaching thread should be attached first!");
        attachedThread.removeFrameListener(this);
        this.attachedThread = null;
    }

    protected void summarize() {
        PrePartialHeap prePartialHeap = getPrePartialHeap();
        PostPartialHeap postPartialHeap = PostPartialHeap.createdFromPreHeap(this, prePartialHeap);

        es.setPostPartialHeap(postPartialHeap);
        es.setTrace(detachTrace());

        {
            // Collect unknown null pointers
            Iterator<Input<?>> unknownNullPointersIt = getAccessedUnknownNullPointers().iterator();
            while (unknownNullPointersIt.hasNext()) {
                Input<?> unknown = unknownNullPointersIt.next();
                Location loc = unknown.getLocation();
                if (loc instanceof HeapLocation) {
                    HeapLocation ahl = (HeapLocation) loc;
                    LocationPath lp = prePartialHeap.getLocationPath(ahl);
                    if (lp != null) {
                        es.addUnallocatedAccessPaths(lp.getAccessPath());
                    } else {
                        throw new RuntimeException("Not implemented");
                    }
                } else if (loc instanceof MethodParameterLocation) {
                    MethodParameterLocation mpl = (MethodParameterLocation) loc;
                    es.addUnallocatedAccessPaths(AccessPathRoots.getMethodParameter(mpl.getMethodInfo(), mpl.getIndex()));
                } else {
                    throw new RuntimeException("Not implemented");
                }
            }
        }

        {
            // Compute allocation information.
            for (Output<? extends HeapLocation> output:postPartialHeap.getPostHeapValues()) {
                HeapLocation hl = output.getLocation();
                Value v = output.getValue();
                if (v instanceof Ref) {
                    if (!hl.isReference()) {
                        throw new RuntimeException("Sanity check faield, a ref " + v + " at " + hl + " and " + hl + " must be a reference");
                    }
                    Ref ref = (Ref) v;
                    LocationPath lp = output.getLocationPath();
                    es.addAllocatedAccessPath(lp.getAccessPath(), ref.getClassInfo());
                }
            }

            if (ms.getMethodInfo().isInit()) {
                ClassInfo targetClassInfo = ms.getTargetClassInfo();
                for(LocationPath lp : prePartialHeap.getRootRefs()) {
                    AccessPath ap = lp.getAccessPath();
                    if (ap instanceof TargetAccessPath) {
                        TargetAccessPath tf = (TargetAccessPath) ap;
                        if (tf.getClassInfo() == targetClassInfo) {
                            es.addAllocatedAccessPath(tf, targetClassInfo);
                        }
                    }
                }
            }
        }

        {
            // Collect all used inputs in PrePartialHeap
            Iterator<Input<?>> unknownValuesIt = getUsedUnknownValues().iterator();
            while (unknownValuesIt.hasNext()) {
                Input<?> unknown = unknownValuesIt.next();
                Location loc = unknown.getLocation();
                es.addUsedUnknownLocation(loc);
                if (loc instanceof HeapLocation) {
                    HeapLocation ahl = (HeapLocation) loc;
                    LocationPath lp = prePartialHeap.getLocationPath(ahl);
                    if (lp != null) {
                        es.addUsedUnknownAccessPath(lp.getAccessPath());
                    } else {
                        throw new RuntimeException("An used allocated unknown value should has an access path!");
                    }
                } else {
                    throw new RuntimeException("Not implemented");
                }
            }
        }

        {
            // Collect defined heap locations in the PostPartialHeap
            Iterator<HeapLocation> definedLocationsIt = getDefinedHeapLocations().iterator();
            while (definedLocationsIt.hasNext()) {
                HeapLocation hl = definedLocationsIt.next();
                LocationPath lp = postPartialHeap.getLocationPath(hl);
                if (lp != null) { 
                    es.addDefinedAccessPaths(lp.getAccessPath());
                }
            }
        }
    }

    public <T extends HeapLocation> Output<T> createPostHeapOutput(LocationPath locationPath, T location) {
        return new Output<T>(getPathCondition(), locationPath, location, location.value());
    }

    public ElementInfo allocateSkeletonObjectAndUpdateInput(Input<?> unknownNullPointer) {
        Location loc = unknownNullPointer.getLocation();
        if (loc instanceof HeapLocation) {
            return this.prePartialHeap.allocateSkeletonObject(this, unknownNullPointer);
        }

        if (loc instanceof MethodParameterLocation) {
            return this.prePartialHeap.allocateSkeletonObject(this, unknownNullPointer);
        }

        throw new RuntimeException("Not implemented");
    }

    public <T extends HeapLocation> Value checkHeapLocationPlaceholder(T loc) {
        return this.prePartialHeap.checkHeapLocationPlaceholder(this, loc);
    }

    /**
     * Only new allocated exceptions are explicitly thrown
     * @param exception
     * @return
     */
    public boolean isExplicitlyThrown(Object exception) {
        if (internalAllocatedObjects == null) {
            return true;
        }

        return !internalAllocatedObjects.contains(exception);
    }

    /**
     * Decide whether we enter the method.
     * @param mi
     * @return
     */
    public boolean shouldDoOnTheFlyAnnotating(MethodInfo mi) {
        ClassInfo ci = mi.getClassInfo();
        ClassInfo targetci = getTargetClassInfo();

        // if ci is a super class of target ci, return false;
        if (targetci.isInstanceOf(ci)) {
            return false;
        }

        // TODO: always return false
        if (mdg.isChangedClass(ci)) {
            return true;
        }

        return false;
    }

    public Map<Value, OperatorValue> getReceiverToOperatorValues() {
        if (receiverToOperatorValues == null) {
            return Collections.emptyMap();
        }
        return receiverToOperatorValues;
    }

    public OperatorValue createGetElementOperator(Value array, ClassInfo ci, Value index) {
        Operator mo = MethodOperatorFactory.getElementOperator(ci);
        Value[] operands = new Value[] { array, index };
        return createOperatorValue(mo, array, operands);
    }

    public OperatorValue createPutElementOperator(Value array, Value index, Value value) {
        Operator mo = MethodOperatorFactory.putElementOperator();
        Value[] operands = new Value[] { array, index, value };
        return createOperatorValue(mo, array, operands);
    }

    public OperatorValue createArrayLengthOperator(Value array) {
        Operator mo = MethodOperatorFactory.arrayLengthOperator();
        Value[] operands = new Value[] { array };
        return createOperatorValue(mo, array, operands);
    }

    public OperatorValue createPutFieldOperator(FieldInfo fi, Value object, Value value) {
        Operator mo = MethodOperatorFactory.putFieldOperator();
        Value[] operands = new Value[] { object, value };
        return createOperatorValue(mo, object, operands);
    }

    public OperatorValue createGetFieldOperator(FieldInfo fi, Value object) {
        Operator mo = MethodOperatorFactory.getFieldOperator(fi);
        Value[] operands = new Value[] { object };
        return createOperatorValue(mo, object, operands);
    }

    public OperatorValue createInstanceOfOperator(ClassInfo ci, Value object, Value className) {
        Operator mo = MethodOperatorFactory.instanceOfOperator(ci);
        Value[] operands = new Value[] { object, className };
        return createOperatorValue(mo, object, operands);
    }

    public int evaluateTableSwitch(Instruction insn) {
        return evaluateSwitch(insn, insn.getTableSwitchMax() - insn.getTableSwitchMin() + 1);
    }

    public int evaluateLookupSwitch(Instruction insn) {
        return evaluateSwitch(insn, insn.getLookupSwitchKeys().size());
    }

    public int evaluateSwitch(Instruction insn, int total) {
        List<Integer> validBranches = new ArrayList<Integer>(total + 1);
        for (int i=-1; i<total; i++) {
            if (mdg.getProfiler().isBranchVisited(ms, tracer.getTraceKey(), insn, i)) {
                validBranches.add(i);
            } else {
                validBranches.add(i);
                validBranches.add(i);
                validBranches.add(i);
            }
        }
        return validBranches.get(rand.nextInt(validBranches.size()));
    }

    public void registerUnsatisfiedMethod(MethodInfo callee) {
        CallGraph.addUnsatisfiedNativeMethod(callee, getEntryMethodInfo());
    }

    private void blacklistLastConditionalCondition() {
        ThreadInfo<Value> ti = this.getThreadInfo();

        ElementInfo ei = Value.getElementInfo(ti.getPendingException());
        if (ei == null) {
            throw new RuntimeException("sanity check failed!");
        }

        if (!isExplicitlyThrown(ei)) {
            return;
        }

        Ref ref = (Ref) ei.getReference();
        PathCondition lastCondition = ref.getPathCondition();
        while (lastCondition != null) {
            if (lastCondition instanceof MethodExitTraceNode) {
                MethodExitTraceNode metn = (MethodExitTraceNode) lastCondition;
                if (metn.getEntryNode().getCaller() == null) {
                    throw new RuntimeException("sanity check failed");
                }
                lastCondition = metn.getEntryNode();
            }

            if (lastCondition.isConditional()) {
                break;
            }
            lastCondition = lastCondition.getCondition();
        }

        if (lastCondition == null) {
            return;
        }
        blacklistLastConditionalCondition(lastCondition);
    }

    private void blacklistLastConditionalCondition(PathCondition lastCondition) {
        if (lastCondition instanceof IfTraceNode) {
            IfTraceNode lastIf = (IfTraceNode) lastCondition;
            boolean blacklisted = mdg.getProfiler().blacklistIf(lastIf.getInstruction(), lastIf.isIfTrue());
            if (!blacklisted) {
                if (debug) {
                    System.out.println("TODO: a diamond CF for " + lastIf.getInstruction()
                    + " in method " + lastIf.getInstruction().getMethodInfo());
                }
                lastCondition = lastCondition.getCondition();
                while (lastCondition != null) {
                    if (lastCondition instanceof MethodExitTraceNode) {
                        MethodExitTraceNode metn = (MethodExitTraceNode) lastCondition;
                        if (metn.getEntryNode().getCaller() == null) {
                            throw new RuntimeException("sanity check failed");
                        }
                        lastCondition = metn.getEntryNode();
                    }

                    if (lastCondition.isConditional()) {
                        break;
                    }
                    lastCondition = lastCondition.getCondition();
                }
                if (debug) {
                    System.out.println("TODO: try to blacklist last condition for this condition, last is " + lastCondition);
                }
                blacklistLastConditionalCondition(lastCondition);
            }
            return;
        }

        if (lastCondition instanceof TableSwitchTraceNode) {
            TableSwitchTraceNode lastSwitch = (TableSwitchTraceNode) lastCondition;
            mdg.getProfiler().blacklistSwitch(lastSwitch.getInstruction(), lastSwitch.getBranch());
            return;
        }

        if (lastCondition instanceof LookupSwitchTraceNode) {
            LookupSwitchTraceNode lastSwitch = (LookupSwitchTraceNode) lastCondition;
            mdg.getProfiler().blacklistSwitch(lastSwitch.getInstruction(), lastSwitch.getBranch());
            return;
        }
    }

}
