package org.javelus.aotes.asm.trace;

import java.util.List;

import org.javelus.aotes.asm.value.BinopExpr;
import org.javelus.aotes.asm.value.PathCondition;
import org.javelus.aotes.asm.value.UnopExpr;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.Instruction;
import org.javelus.aotes.asm.vm.MethodInfo;


public class Tracer {

    public static final boolean debug = true;

    private TraceNode entryTraceNode;
    private TraceNode activeTraceNode;
    private MethodEntryTraceNode activeMethodEntryNode;

    private PathCondition lastPathCondition;
    private TraceKey traceKey;
    private MethodInfo entryMethod;

    Tracer(MethodInfo entryMethod) {
        entryTraceNode = TraceNode.createEntryTraceNode();
        activeTraceNode = entryTraceNode;
        this.entryMethod = entryMethod;
        this.traceKey = new TraceKey(entryMethod);
    }

    public TraceKey getTraceKey() {
        return traceKey;
    }

    public MethodInfo getEntryMethodInfo() {
        return entryMethod;
    }

    public TraceNode getActiveTraceNode() {
        return activeTraceNode;
    }

    public PathCondition getCondition() {
        return lastPathCondition;
    }

    public TraceNode getEntryTraceNode() {
        return entryTraceNode;
    }

    public void enterMethod(Instruction insn, MethodInfo method) {
        activeMethodEntryNode = createMethodEntryTraceNode(activeTraceNode, activeMethodEntryNode, method);
        activeTraceNode = activeMethodEntryNode;
        updatePathCondition();
    }

    protected void updatePathCondition() {
        activeTraceNode.setCondition(lastPathCondition);

        if (activeTraceNode instanceof PathCondition) {
            lastPathCondition = (PathCondition) activeTraceNode;

            if (activeTraceNode instanceof IfTraceNode) {
                IfTraceNode ifTrace = (IfTraceNode) activeTraceNode;
                traceKey.addPathCondition(ifTrace.getInstruction(), ifTrace.isIfTrue() ? 1 : 0);
            }

            if (activeTraceNode instanceof SwitchTraceNode) {
                SwitchTraceNode switchTrace = (SwitchTraceNode) activeTraceNode;
                traceKey.addPathCondition(switchTrace.getInstruction(), switchTrace.getBranch());
            }
        }
    }

    public void exitMethod(Instruction insn, Object retValue, ClassInfo exceptionInfo) {
        activeTraceNode = createMethodExitTraceNode(activeTraceNode, getActiveMethodEntryNode(), retValue, exceptionInfo);
        activeMethodEntryNode = activeMethodEntryNode.getCaller();
        updatePathCondition();
    }

    public MethodEntryTraceNode getActiveMethodEntryNode() {
        return activeMethodEntryNode;
    }

    public void ifTrue(Instruction insn, Value leftValue,
            Value rightValue, Instruction target) {
        activeTraceNode = createIfTrueTraceNode(activeTraceNode, insn, leftValue, rightValue, target);
        updatePathCondition();
    }

    public void ifFalse(Instruction insn, Value leftValue,
            Value rightValue, Instruction target) {
        activeTraceNode = createIfFalseTraceNode(activeTraceNode, insn, leftValue, rightValue, target);
        updatePathCondition();
    }

    public Trace detachTrace() {
        Trace trace = new Trace(entryMethod, entryTraceNode, activeTraceNode, traceKey);
        return trace;
    }

    public void unopExpr(Instruction insn, UnopExpr unopExpr) {
        activeTraceNode = createUnopExprTraceNode(activeTraceNode, insn, unopExpr);
        updatePathCondition();
    }

    public void binopExpr(Instruction insn,
            BinopExpr binopExpr) {
        activeTraceNode = createBinopExprTraceNode(activeTraceNode, insn, binopExpr);
        updatePathCondition();
    }

    public void newObject(Instruction insn, ElementInfo ei) {
        activeTraceNode = createNewObjectTraceNode(activeTraceNode, insn, ei);
        updatePathCondition();
    }

    protected TraceNode createNewObjectTraceNode(TraceNode thread,
            Instruction insn, ElementInfo ei) {
        return new NewObjectTraceNode(thread, insn, ei);
    }

    protected MethodEntryTraceNode createMethodEntryTraceNode(TraceNode previous, MethodEntryTraceNode caller, MethodInfo method) {
        MethodEntryTraceNode node = new MethodEntryTraceNode(previous, caller, method);
        return node;
    }

    protected TraceNode createMethodExitTraceNode(TraceNode previous, MethodEntryTraceNode methodEntryNode, Object retValue, ClassInfo exceptionInfo) {
        MethodExitTraceNode node = new MethodExitTraceNode(previous, methodEntryNode, retValue, exceptionInfo);
        return node;
    }

    protected TraceNode createIfTrueTraceNode(TraceNode previous, Instruction ifInsn, Value leftValue, Value rightValue, Instruction target) {
        return new IfTrueTraceNode(previous, ifInsn, leftValue, rightValue, target);
    }

    protected TraceNode createIfFalseTraceNode(TraceNode previous, Instruction ifInsn, Value leftValue, Value rightValue, Instruction target) {
        return new IfFalseTraceNode(previous, ifInsn, leftValue, rightValue, target);
    }

    protected TraceNode createBinopExprTraceNode(TraceNode previous,
            Instruction insn, BinopExpr binopExpr) {
        return new BinopExprTraceNode(previous, insn, binopExpr);
    }

    protected TraceNode createUnopExprTraceNode(TraceNode previous,
            Instruction insn, UnopExpr unopExpr) {
        return new UnopExprTraceNode(previous, insn, unopExpr);
    }



    public void getField(Instruction insn, ElementInfo ei,
            FieldInfo fi, Value value) {
        activeTraceNode = createFieldLoadTraceNode(activeTraceNode, insn, ei, fi, value);
        updatePathCondition();
    }

    protected TraceNode createFieldLoadTraceNode(TraceNode previous,
            Instruction insn, ElementInfo ei, FieldInfo fi, Value value) {
        return new GetFieldTraceNode(previous, insn, ei, fi, value);
    }

    public void putField(Instruction insn, ElementInfo ei,
            FieldInfo fi, Value value) {
        activeTraceNode = createFieldStoreTraceNode(activeTraceNode, insn, ei, fi, value);
        updatePathCondition();
    }

    protected TraceNode createFieldStoreTraceNode(TraceNode previous,
            Instruction insn, ElementInfo ei, FieldInfo fi, Value value) {
        return new PutFieldTraceNode(previous, insn, ei, fi, value);
    }

    public void methodReturn(Instruction insn, Value ret) {
        activeTraceNode = createReturnValueTraceNode(activeTraceNode, insn, ret);
        updatePathCondition();
    }

    protected TraceNode createReturnValueTraceNode(TraceNode previous,
            Instruction insn, Value ret) {
        return new ReturnValueTraceNode(previous, insn, ret);
    }

    public void defineArrayElement(Instruction insn, ElementInfo ei,
            Value index, Value value) {
        activeTraceNode = createDefineArrayElementTraceNode(activeTraceNode, insn, ei, index, value);
        updatePathCondition();
    }

    protected TraceNode createDefineArrayElementTraceNode(
            TraceNode previous, Instruction insn, ElementInfo ei,
            Value index, Value value) {
        return new ArrayStoreTraceNode(previous, insn, ei, index, value);
    }

    public void useArrayElement(Instruction insn, ElementInfo ei,
            Value index, Value value) {
        activeTraceNode = createUseArrayElementTraceNode(activeTraceNode, insn, ei, index, value);
        updatePathCondition();
    }

    protected TraceNode createUseArrayElementTraceNode(
            TraceNode previous, Instruction insn, ElementInfo ei,
            Value index, Value value) {
        return new ArrayLoadTraceNode(previous, insn, ei, index, value);
    }

    public void loadConstant(Instruction insn, Value value) {
        activeTraceNode = createLoadConstantTraceNode(activeTraceNode, insn, value);
        updatePathCondition();
    }

    protected TraceNode createLoadConstantTraceNode(TraceNode previous,
            Instruction insn, Value value) {
        return new PushConstantTraceNode(previous, insn, value);
    }

    public void storeStack(Instruction insn, Value value) {
        activeTraceNode = createStoreStackTraceNode(activeTraceNode, insn, value);
        updatePathCondition();
    }

    protected TraceNode createStoreStackTraceNode(TraceNode previous,
            Instruction insn, Value value) {
        return new StackStoreTraceNode(previous, insn, value);
    }

    public void loadStack(Instruction insn, Value value) {
        activeTraceNode = createLoadStackTraceNode(activeTraceNode, insn, value);
        updatePathCondition();
    }

    protected TraceNode createLoadStackTraceNode(TraceNode previous,
            Instruction insn, Value value) {
        return new StackLoadTraceNode(previous, insn, value);
    }

    public void putStatic(Instruction insn, FieldInfo fi, Value value) {
        activeTraceNode = createPutStaticTraceNode(activeTraceNode, insn, fi, value);
        updatePathCondition();
    }

    protected PutStaticTraceNode createPutStaticTraceNode(TraceNode activeTraceNode,
            Instruction insn, FieldInfo fi, Value value) {
        PutStaticTraceNode node = new PutStaticTraceNode(activeTraceNode, insn, fi, value);
        return node;
    }

    private GetStaticTraceNode createGetStaticTraceNode(TraceNode activeTraceNode,
            Instruction insn, FieldInfo fi, Value value) {
        GetStaticTraceNode node = new GetStaticTraceNode(activeTraceNode, insn, fi, value);
        return node;
    }

    public void getStatic(Instruction insn, FieldInfo fi, Value value) {
        activeTraceNode = createGetStaticTraceNode(activeTraceNode, insn, fi, value);
        updatePathCondition();
    }

    protected TraceNode createTableSwitchTraceNode(TraceNode previous, Instruction insn, Value index, int min, int max, int branch) {
        return new TableSwitchTraceNode(previous, insn, index, min, max, branch);
    }

    public void tableSwitch(Instruction insn, Value index, int min, int max, int branch) {
        activeTraceNode = createTableSwitchTraceNode(activeTraceNode, insn, index, min, max, branch);
        updatePathCondition();
    }

    protected TraceNode createLookupSwitchTraceNode(TraceNode previous, Instruction insn, Value key, List<Integer> keys, int branch) {
        return new LookupSwitchTraceNode(previous, insn, key, keys, branch);
    }

    public void lookupSwitch(Instruction insn, Value key, List<Integer> keys, int branch) {
        activeTraceNode = createLookupSwitchTraceNode(activeTraceNode, insn, key, keys, branch);
        updatePathCondition();
    }

}
