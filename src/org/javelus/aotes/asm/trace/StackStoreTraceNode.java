package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public class StackStoreTraceNode extends InsnTraceNode {

    private Value value;

    public StackStoreTraceNode(TraceNode previous, Instruction instruction, Value value) {
        super(previous, instruction);
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

}
