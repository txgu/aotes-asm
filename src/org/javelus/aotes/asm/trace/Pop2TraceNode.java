package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.vm.Instruction;

public class Pop2TraceNode extends InsnTraceNode {

    public Pop2TraceNode(TraceNode previous, Instruction instruction) {
        super(previous, instruction);
    }

}
