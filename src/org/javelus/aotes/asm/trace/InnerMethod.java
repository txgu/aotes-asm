package org.javelus.aotes.asm.trace;

import java.util.Set;

import org.javelus.aotes.asm.accesspath.AccessPath;


public class InnerMethod {
    TraceNode beginNode;
    TraceNode endNode;

    Set<AccessPath> inputs;
    Set<AccessPath> outputs;
}
