package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.UnopExpr;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public class UnopExprTraceNode extends InsnTraceNode {

    private UnopExpr value;

    public UnopExprTraceNode(TraceNode previous, Instruction instruction, UnopExpr expr) {
        super(previous, instruction);
        this.value = expr;
    }

    public Value getLeftValue() {
        return value;
    }

    public Value getRightValue() {
        return value.getOperand();
    }

}
