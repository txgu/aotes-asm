package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public class TableSwitchTraceNode extends SwitchTraceNode {

    int min;
    int max;

    public TableSwitchTraceNode(TraceNode previous, Instruction instruction, Value index, int min, int max, int branch) {
        super(previous, instruction, index, branch);
        this.min = min;
        this.max = max;
    }

    public Value getIndex() {
        return getValue();
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
