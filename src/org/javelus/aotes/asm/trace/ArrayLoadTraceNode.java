package org.javelus.aotes.asm.trace;


import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.Instruction;


public class ArrayLoadTraceNode extends InsnTraceNode {

    private ElementInfo array;

    private Value index;

    private Value value;

    public ArrayLoadTraceNode(TraceNode previous, Instruction instruction, ElementInfo array, Value index, Value value) {
        super(previous, instruction);

        this.array = array;
        this.index = index;
        this.value = value;
    }

    public ElementInfo getArray() {
        return array;
    }

    public Value getIndex() {
        return index;
    }

    public Value getValue() {
        return value;
    }



}
