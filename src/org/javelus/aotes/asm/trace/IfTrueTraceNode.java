package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public final class IfTrueTraceNode extends IfTraceNode {

    public IfTrueTraceNode(TraceNode previous, Instruction ifInsn,
            Value leftValue, Value rightValue, Instruction target) {
        super(previous, ifInsn, leftValue, rightValue, target);
    }

    @Override
    public boolean isIfTrue() {
        return true;
    }

}
