package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.PathCondition;
import org.javelus.aotes.asm.vm.ClassInfo;

public class MethodExitTraceNode extends TraceNode implements PathCondition {
    MethodEntryTraceNode entryNode;

    ClassInfo exceptionInfo;

    Object  returnValue;

    public MethodExitTraceNode(TraceNode previous, MethodEntryTraceNode entryNode, Object returnValue, ClassInfo exceptionInfo){
        super(previous);
        this.entryNode = entryNode;
        entryNode.setExitNode(this);
        this.returnValue = returnValue;
        this.exceptionInfo = exceptionInfo;
    }

    public MethodEntryTraceNode getEntryNode(){
        return entryNode;
    }

    public void setEntryNode(MethodEntryTraceNode entryNode) {
        this.entryNode = entryNode;
    }

    public Object getReturnValue() {
        return returnValue;
    }

    public ClassInfo getExceptionClassInfo() {
        return exceptionInfo;
    }

    public final boolean isConditional() {
        return false;
    }

    public String toString() {
        return String.format("Exit{%s}", entryNode.getMethodInfo());
    }
}
