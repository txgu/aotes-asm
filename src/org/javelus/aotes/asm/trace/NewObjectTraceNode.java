package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.Instruction;

public class NewObjectTraceNode extends InsnTraceNode {

    private ElementInfo object;

    public NewObjectTraceNode(TraceNode previous, Instruction instruction, ElementInfo object) {
        super(previous, instruction);
        this.object = object;
    }

    public ElementInfo getObject() {
        return object;
    }

}
