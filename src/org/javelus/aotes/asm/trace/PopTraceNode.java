package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;

public class PopTraceNode extends InsnTraceNode {

    private Value value;

    public PopTraceNode(TraceNode previous, Instruction instruction, Value value) {
        super(previous, instruction);
    }

    public Value getValue() {
        return value;
    }
}
