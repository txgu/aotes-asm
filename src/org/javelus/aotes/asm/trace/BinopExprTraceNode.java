package org.javelus.aotes.asm.trace;


import org.javelus.aotes.asm.value.BinopExpr;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.Instruction;


public class BinopExprTraceNode extends InsnTraceNode {

    private BinopExpr value;

    public BinopExprTraceNode(TraceNode previous, Instruction instruction, BinopExpr expr) {
        super(previous, instruction);
        this.value = expr;
    }

    public Value getLeftValue() {
        return value;
    }

    public Value getRightValue1() {
        return value.getLeftOperand();
    }

    public Value getRightValue2() {
        return value.getRightOperand();
    }
}
