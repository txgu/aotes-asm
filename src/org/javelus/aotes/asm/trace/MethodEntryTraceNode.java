package org.javelus.aotes.asm.trace;

import org.javelus.aotes.asm.value.PathCondition;
import org.javelus.aotes.asm.vm.MethodInfo;

public class MethodEntryTraceNode extends TraceNode implements PathCondition {


    private MethodExitTraceNode exitNode;

    private MethodInfo methodInfo;

    private MethodEntryTraceNode caller;

    public MethodEntryTraceNode(TraceNode previous, MethodEntryTraceNode caller, MethodInfo methodInfo) {
        super(previous);
        this.methodInfo = methodInfo;
        this.caller = caller;
    }

    public MethodExitTraceNode getExitNode() {
        return exitNode;
    }

    public void setExitNode(MethodExitTraceNode exitNode) {
        this.exitNode = exitNode;
    }

    public MethodEntryTraceNode getCaller() {
        return caller;
    }

    public MethodInfo getMethodInfo() {
        return methodInfo;
    }

    public final boolean isConditional() {
        return false;
    }

    public String toString() {
        return String.format("Enter{%s}", methodInfo);
    }
}
