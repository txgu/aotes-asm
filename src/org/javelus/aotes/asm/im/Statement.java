package org.javelus.aotes.asm.im;

import org.javelus.aotes.asm.value.Value;

public class Statement {

    Value v;
    Value[] by;
    boolean forward;

    public Statement(Value v, Value[] by, boolean forward) {
        this.v = v;
        this.by = by;
        this.forward = forward;
    }
}
