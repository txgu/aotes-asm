package org.javelus.aotes.asm.im;

import java.util.HashMap;
import java.util.Map;

import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.MethodParameterLocation;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.TargetRef;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.executor.Naming;

/**
 * 
 * @author tianxiao
 *
 */
public class SimpleNaming implements Naming {
    private Map<Object, String> valueToVariableName = new HashMap<Object,String>();

    private int count = 0;

    public Object getVariableName(Object v) {
        if (!(v instanceof Value)) {
            throw new RuntimeException("Error");
        }
        String name = valueToVariableName.get(v);
        if (name == null) {
            name = allocateVariableName(v);
            valueToVariableName.put(v, name);
        }
        return name;
    }

    private String allocateVariableName(Object v) {
        if (v instanceof TargetRef) {
            return "this";
        }

        if (v instanceof Input) {
            Location loc = ((Input<?>) v).getLocation();
            if (loc instanceof MethodParameterLocation) {
                int index = ((MethodParameterLocation) loc).getIndex();
                if (index == -1) {
                    //return "this";
                    throw new RuntimeException("To be decided!");
                }
                return String.format("param%d", index);
            }
        }

        return String.format("v%d",	 count++);
    }

}
