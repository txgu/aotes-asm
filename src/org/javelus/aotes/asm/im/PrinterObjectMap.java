package org.javelus.aotes.asm.im;

import java.io.PrintWriter;
import java.util.Set;


import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.ObjectMap;

public class PrinterObjectMap implements ObjectMap {

    PrintWriter pw;
    public PrinterObjectMap(PrintWriter pw) {
        this.pw = pw;
    }


    @Override
    public void revertField(Object object, Object field) {
        pw.format("om.revertField(%s, \"%s\");\n    ", object, field);
    }

    @Override
    public void revertField(Object object, Object field, Object value) {
        pw.format(";\n    om.revertField(%s, \"%s\", %s)", object, field, value);
    }

    @Override
    public Object getField(Object object, Object field) {
        return String.format("om.getField(%s, \"%s\")", object, field);
    }

    @Override
    public Object popField(Object object, Object field) {
        return String.format("om.popField(%s, \"%s\")", object, field);
    }


    @Override
    public Object popArrayElement(Object array, Object index) {
        return String.format("om.popArrayElement(%s, %s)", array, index);
    }

    @Override
    public Object getArrayElement(Object array, Object index) {
        return String.format("om.getArrayElement(%s, %s)", array, index);
    }

    @Override
    public void revertArrayElement(Object object, Object index) {
        pw.format("om.revertArrayElement(%s, %s);\n    ", object, index);
    }

    @Override
    public void revertArrayElement(Object object, Object index, Object value) {
        pw.format(";\n    om.revertArrayElement(%s, %s, %s)", object, index, value);
    }

    @Override
    public Object getArrayLength(Object array) {
        return String.format("om.getArrayLength(%s)", array);
    }

    @Override
    public Object guessArrayIndex(Object array) {
        return String.format("om.guessArrayIndex(%s)", array);
    }

    @Override
    public Executor getExecutor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void begin() {
        // TODO Auto-generated method stub

    }

    @Override
    public void commit() {
        // TODO Auto-generated method stub

    }

    @Override
    public void rollback() {
        // TODO Auto-generated method stub

    }

    @Override
    public Object getTarget() {
        return "om.getTarget()";
    }

    @Override
    public boolean isDone() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Set<Object> getTargets() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isStrictDone() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getDelta() {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public Object newObject(Object cls) {
        return String.format("om.newObject(\"%s\")", cls);
    }


    @Override
    public Object newArray(Object cls, Object length) {
        return String.format("om.newArray(\"%s\", %s)", cls, length);
    }


    @Override
    public Object cloneObject(Object object) {
        return null;
    }

    @Override
    public void revertStatic(Object field) {
        pw.format("om.revertStatic(\"%s\");\n    ", field);
    }

    @Override
    public void revertStatic(Object field, Object value) {
        pw.format(";\n    om.revertStatic(\"%s\", %s)", field, value);
    }

    @Override
    public Object getStatic(Object field) {
        return String.format("om.getStatic(\"%s\")", field);
    }

    @Override
    public Object popStatic(Object field) {
        return String.format("om.popStatic(\"%s\")", field);
    }


    @Override
    public Object newDefaultValue(Object cls) {
        return String.format("om.newDefaultValue(\"%s\")", cls);
    }
}
