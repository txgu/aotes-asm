package org.javelus.aotes.asm.im;

import java.util.ArrayList;
import java.util.List;

import org.javelus.aotes.asm.vm.MethodInfo;


public class InverseMutator {
    MethodInfo mi;

    List<InverseFragment> fragments;

    public InverseMutator(MethodInfo mi) {
        this.mi = mi;
        this.fragments = new ArrayList<InverseFragment>();
    }

    public void addFragment(InverseFragment fragment) {
        fragments.add(fragment);
    }

    public boolean hasInverseFragment() {
        return !fragments.isEmpty();
    }

    public List<InverseFragment> getFragments() {
        return fragments;
    }

}
