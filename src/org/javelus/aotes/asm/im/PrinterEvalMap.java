package org.javelus.aotes.asm.im;

import java.io.PrintWriter;

import org.javelus.aotes.executor.Eval;
import org.javelus.aotes.executor.EvalMap;
import org.javelus.aotes.executor.Executor;

public class PrinterEvalMap implements EvalMap {
    PrintWriter pw;

    public PrinterEvalMap(PrintWriter pw) {
        this.pw = pw;
    }

    @Override
    public Executor getExecutor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean eq(Object v1, Object v2) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean lt(Object v1, Object v2) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean le(Object v1, Object v2) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean gt(Object v1, Object v2) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean ge(Object v1, Object v2) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean ne(Object v1, Object v2) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Eval op(final Object opType) {
        return new Eval() {

            @Override
            public Executor getExecutor() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public Object eval(Object... args) {
                StringBuilder sb = new StringBuilder();
                sb.append("em.op(\"" + opType + "\").eval(");
                if (args.length > 0) {
                    sb.append("\"");
                    sb.append(args[0]);
                    sb.append("\"");
                    for (int i=1; i<args.length; i++) {
                        sb.append(", \"");
                        sb.append(args[i]);
                        sb.append("\"");
                    }
                }
                sb.append(")");
                return sb.toString();
            }

        };
    }
}
