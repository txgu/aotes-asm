package org.javelus.aotes.asm.im;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.summary.ExecutionSummary;
import org.javelus.aotes.asm.summary.MDG;
import org.javelus.aotes.asm.value.Constant;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.value.operator.RelationalOperator;
import org.javelus.aotes.asm.vm.MethodInfo;


/**
 * Predefined variable:
 * <ul>
 *  <li> vm: ValueMap
 *  <li> om: ObjectMap
 *  <li> em: EvalMap
 *  <li> oa: ObjectAllocator
 *  <li> this: the target object it self
 * </ul>
 * @author tianxiao
 *
 */
public class InverseFragmentPrinter extends InverseStatementExecutor {

    PrintWriter pw;


    public InverseFragmentPrinter(InverseFragment iF) {
        super(iF);
    }

    protected Object defaultValue(Value v) {
        String descriptor = v.getTypeDescriptor();

        if (descriptor.equals("Z")) {
            return "false";
        }

        if (descriptor.equals("C")
                || descriptor.equals("B")
                || descriptor.equals("S")
                || descriptor.equals("I")
                || descriptor.equals("J")
                || descriptor.equals("F")
                || descriptor.equals("D")) {
            return "0";
        }

        return "null";
    }

    protected Object constant(Constant c) {
        if (c == Value.NULL) {
            return "null";
        }
        return c.toString();
    }

    protected Object accesspath(AccessPath ap) {
        //return ap.getUniqueString();
        return ap.toString();
    }

    public void print(PrintWriter pw) {
        this.pw = pw;
        this.vm = new PrinterValueMap(pw);
        this.va = new SimpleNaming();
        this.om = new PrinterObjectMap(pw);
        this.em = new PrinterEvalMap(pw);
        List<Statement> statements = iF.getStatements();
        MethodInfo entry = iF.es.getEntryMethod();
        pw.format("  @IM(clazz=\"%s\", name=\"%s\", desc=\"%s\"", entry.getClassInfo().getName(), entry.getName(), entry.getDescriptor());
        if (!iF.getRevertedInput().isEmpty()) {
            pw.format(",revertedInput={");
            Iterator<AccessPath> it = iF.getRevertedInput().iterator();
            AccessPath ap = it.next();
            pw.print("\"");
            pw.print(accesspath(ap));
            pw.print("\"");
            while (it.hasNext()) {
                ap = it.next();
                pw.print(",\"");
                pw.print(accesspath(ap));
                pw.print("\"");
            }
            pw.format("}");
        }
        if (!iF.getDefinedOutput().isEmpty()) {
            pw.format(",definedOutput={");
            Iterator<AccessPath> it = iF.getDefinedOutput().iterator();
            AccessPath ap = it.next();
            pw.print("\"");
            pw.print(accesspath(ap));
            pw.print("\"");
            while (it.hasNext()) {
                ap = it.next();
                pw.print(",\"");
                pw.print(accesspath(ap));
                pw.print("\"");
            }
            pw.format("}");
        }
        if (!iF.getMutatedReceivers().isEmpty()) {
            pw.format(",mutatedReceivers={");
            Iterator<AccessPath> it = iF.getMutatedReceivers().iterator();
            AccessPath ap = it.next();
            pw.print("\"");
            pw.print(accesspath(ap));
            pw.print("\"");
            while (it.hasNext()) {
                ap = it.next();
                pw.print(",\"");
                pw.print(accesspath(ap));
                pw.print("\"");
            }
            pw.format("}");
        }
        if (!iF.getDelta().isEmpty()) {
            pw.format(",delta={");
            Iterator<AccessPath> it = iF.getDelta().iterator();
            AccessPath ap = it.next();
            pw.print("\"");
            pw.print(accesspath(ap));
            pw.print("\"");
            while (it.hasNext()) {
                ap = it.next();
                pw.print(",\"");
                pw.print(accesspath(ap));
                pw.print("\"");
            }
            pw.format("}");
        }
        pw.print(")\n");


        Set<ExecutionSummary> preds = iF.es.getPredecessors();
        Set<ExecutionSummary> succs = iF.es.getSuccessors();
        if (!preds.isEmpty() || !succs.isEmpty()) {
            pw.print("  @DG(");
            if (!preds.isEmpty()) {
                pw.print("pred={");
                Iterator<ExecutionSummary> it = preds.iterator();
                ExecutionSummary p = it.next();
                pw.print("\"");
                pw.print(p.getUniqueName());
                pw.print("\"");
                while (it.hasNext()) {
                    p = it.next();
                    pw.print(",\"");
                    pw.print(p.getUniqueName());
                    pw.print("\"");
                }
                pw.print("}");
                if (!succs.isEmpty()) {
                    pw.print(",");
                }
            }
            if (!succs.isEmpty()) {
                pw.print("succ={");
                Iterator<ExecutionSummary> it = succs.iterator();
                ExecutionSummary s = it.next();
                pw.print("\"");
                pw.print(s.getUniqueName());
                pw.print("\"");
                while (it.hasNext()) {
                    s = it.next();
                    pw.print(",\"");
                    pw.print(s.getUniqueName());
                    pw.print("\"");
                }
                pw.print("}");
            }
            pw.print(")\n");
        }
        {
            MDG mdg = iF.es.getMethodSummary().getClassSummary().getMDG();
            if (mdg.isDeleted(iF.es.getMethodSummary())) {
                pw.println("@UpdateType(\"DEL\")");
            }
        }

        pw.format("  static void %s(ObjectMap om, ValueMap vm, EvalMap em, Executor e) {\n", 
                this.iF.getUniqueName());

        for (Statement stmt:statements) {
            pw.print("    ");
            evaluateStatement(stmt);
            pw.print(";\n");
            pw.flush();
        }
        List<Predicate> predicates = iF.getPredicates();
        for (Predicate p:predicates) {
            pw.print("    ");
            evaluatePredicate(p);
            pw.print(";\n");
            pw.flush();
        }

        pw.println("  }");
    }

    public static void print(InverseFragment iF, OutputStream out) {
        PrintWriter pw = new PrintWriter(out);
        print(iF, pw);
    }

    public static void print(InverseFragment iF, PrintWriter pw) {
        InverseFragmentPrinter ifp = new InverseFragmentPrinter(iF);
        ifp.print(pw);
        pw.flush();
    }

    @Override
    protected void initializeEnvironment() {

    }

    @Override
    public Object transform(Object old) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object transformClass(Class<?> old) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void abort(String message) {
        // TODO Auto-generated method stub
    }

    @Override
    protected Object comparator(Object c) {
        return ((RelationalOperator)c).getEvalName();
    }


}
