package org.javelus.aotes.asm.im;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.javelus.aotes.asm.utils.MapUtils;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.ValueMap;

public class PrinterValueMap implements ValueMap {

    Map<Object, Set<Object>> confilictValues = new HashMap<Object, Set<Object>>();
    Map<Object, Object> values = new HashMap<Object, Object>();

    PrintWriter pw;

    public PrinterValueMap(PrintWriter pw) {
        this.pw = pw;
    }

    @Override
    public void put(Object key, Object value) {
        if (value == null) {
            throw new RuntimeException("Sanity check failed, should not put null into "
                    + "printer value map");
        }
        Object oldValue = values.get(key);
        if (value.equals(oldValue)) {
           pw.print("//");
           return; 
        }
        if (oldValue != null) {
            MapUtils.addToMapSet(confilictValues, key, oldValue);
            MapUtils.addToMapSet(confilictValues, key, value);
            pw.format("if (!em.eq(vm.get(\"%s\"), (%s))) e.abort(\"Inconsistent value for \\\"%s\\\": \" + vm.get(\"%s\") + \" ne \" + (%s))", key, value, key, key, value);
            return;
        }
        values.put(key, value);

        pw.format("vm.put(\"%s\", %s)", key, value);
    }

    @Override
    public Object get(Object key) {
        if (key.toString().contains("->")) {
            throw new RuntimeException();
        }
        //return values.get(key);
        return String.format("vm.get(\"%s\")", key);
    }

    @Override
    public Map<Object, Set<Object>> getConflictValues() {
        return confilictValues;
    }

    @Override
    public void checkOrFail(Object key1, Object key2, Object comparator) {
        Object v1 = values.get(key1);
        Object v2 = values.get(key2);
        if (v1 == null) {
            throw new RuntimeException("No such key1, key1 is " + key1 + " and key2 is " + key2);
        }
        if (v2 == null) {
            throw new RuntimeException("No such key2, key1 is " + key1 + " and key2 is " + key2);
        }
        pw.format("if (!em.%s(vm.get(\"%s\"), vm.get(\"%s\"))) "
                + "e.abort(String.format(\"!((%s=%%s) %s (%s=%%s))\", vm.get(\"%s\"), vm.get(\"%s\")))", comparator, key1, key2, key1, comparator, key2, key1, key2);
    }

    @Override
    public Executor getExecutor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

}
