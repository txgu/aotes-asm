package org.javelus.aotes.asm.im;

import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.value.operator.RelationalOperator;

public class Predicate {
    Value value1;
    Value value2;
    RelationalOperator comparator;

    public Predicate(Value value1, Value value2, RelationalOperator comparator) {
        this.value1 = value1;
        this.value2 = value2;
        this.comparator = comparator;
    }

    public Value getValue1() {
        return value1;
    }

    public void setValue1(Value value1) {
        this.value1 = value1;
    }

    public Value getValue2() {
        return value2;
    }

    public void setValue2(Value value2) {
        this.value2 = value2;
    }

    public RelationalOperator getComparator() {
        return comparator;
    }

    public void setComparator(RelationalOperator comparator) {
        this.comparator = comparator;
    }
}
