package org.javelus.aotes.asm.im;

import java.util.List;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.location.ArrayElement;
import org.javelus.aotes.asm.location.ArrayLength;
import org.javelus.aotes.asm.location.FieldLocation;
import org.javelus.aotes.asm.location.HeapLocation;
import org.javelus.aotes.asm.location.InstanceField;
import org.javelus.aotes.asm.location.Location;
import org.javelus.aotes.asm.location.MethodParameterLocation;
import org.javelus.aotes.asm.location.StaticField;
import org.javelus.aotes.asm.utils.Opcodes;
import org.javelus.aotes.asm.value.BinopExpr;
import org.javelus.aotes.asm.value.Constant;
import org.javelus.aotes.asm.value.GuessIndexValue;
import org.javelus.aotes.asm.value.Input;
import org.javelus.aotes.asm.value.NewRef;
import org.javelus.aotes.asm.value.Operator;
import org.javelus.aotes.asm.value.OperatorValue;
import org.javelus.aotes.asm.value.Output;
import org.javelus.aotes.asm.value.TargetRef;
import org.javelus.aotes.asm.value.UnopExpr;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.value.operator.MethodOperator;
import org.javelus.aotes.asm.value.operator.MethodOperatorFactory;
import org.javelus.aotes.asm.value.operator.OpcodeOperator;
import org.javelus.aotes.asm.value.operator.OpcodeOperatorFactory;
import org.javelus.aotes.asm.vm.ArrayElementInfo;
import org.javelus.aotes.asm.vm.ClassInfo;
import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.Instruction;
import org.javelus.aotes.executor.EvalMap;
import org.javelus.aotes.executor.Executor;
import org.javelus.aotes.executor.Naming;
import org.javelus.aotes.executor.ObjectMap;
import org.javelus.aotes.executor.ValueMap;
import org.objectweb.asm.Type;



@SuppressWarnings({"unchecked"})
public abstract class InverseStatementExecutor implements Executor {

    protected InverseFragment iF;

    protected Naming va;
    protected ObjectMap om;
    protected ValueMap vm;
    protected EvalMap em;


    public InverseStatementExecutor(InverseFragment inverseFragment) {
        this.iF = inverseFragment;
    }

    public void execute() {
        initializeEnvironment();

        List<Statement> statements = getStatements();
        for (Statement stmt:statements) {
            evaluateStatement(stmt);
        }

        List<Predicate> predicates = getPredicates();
        for (Predicate p:predicates) {
            evaluatePredicate(p);
        }

    }


    public List<Predicate> getPredicates() {
        return iF.getPredicates();
    }

    public List<Statement> getStatements() {
        return iF.getStatements();
    }

    protected void initializeEnvironment() {

    }

    protected void evaluatePredicate(Predicate p) {
        vm.checkOrFail(variable(p.getValue1()), variable(p.getValue2()), comparator(p.comparator));
    }

    /**
     * Write all logic in this method.
     * Refactor later.
     */
    protected void evaluateStatement(Statement stmt) {
        Value v = stmt.v;
        if (v == null) {
            throw new RuntimeException();
        }
        Value[] by = stmt.by;
        boolean forward = stmt.forward;
        if (by.length == 0) {
            if (forward) {
                if (v instanceof Constant) { // Constant
                    loadConstant((Constant)v);
                } else if (v instanceof Input) {
                    Input<?> input = (Input<?>) v;
                    Location loc = input.getLocation();
                    if (loc instanceof HeapLocation) {
                        loadDefaultHeapValue((Input<? extends HeapLocation>)v);
                    } else if (loc instanceof MethodParameterLocation) {
                        loadDefaultMethodParameter((Input<? extends MethodParameterLocation>)v);
                    } else  {
                        throw new RuntimeException("Not implemented");
                    }
                } else if (v instanceof TargetRef) {
                    loadTargetObjectRef((TargetRef)v);
                } else if (v instanceof OperatorValue) {
                    OperatorValue ov = (OperatorValue) v;
                    evaluateOperator(ov, ov.getOperator());
                } else if (v instanceof NewRef) {
                    newObjectOrArray((NewRef)v);
                } else if (v instanceof GuessIndexValue) {
                    guessArrayIndex((GuessIndexValue)v);
                } else {
                    throw new RuntimeException("Unknown value type " + v);
                }
            } else {
                if (v instanceof Output) {
                    Output<?> output = (Output<?>) v;
                    Location loc = output.getLocation();
                    if (loc instanceof ArrayLength) {
                        loadPostArrayLength((Output<ArrayLength>)v);
                    } else if (loc instanceof ArrayElement) {
                        //loadPostArrayElement((PostArrayElementValue)v);
                        throw new RuntimeException("Sanity check failed!");
                    } else if (loc instanceof InstanceField) {
                        loadPostInstanceField((Output<InstanceField>)v);
                    } else if (loc instanceof StaticField) {
                        loadPostStaticField((Output<StaticField>)v);
                    } else {
                        throw new RuntimeException("Not implemented!");
                    }
                } else if (v instanceof OperatorValue) {
                    OperatorValue mov = (OperatorValue) v;
                    Operator mo = mov.getOperator();
                    // TODO 
                    if (!mo.isMutator()) {
                        throw new RuntimeException("Sanity check: a method operator without 'by' should be target mutator.");
                    }
                    revertMethodOperator(mov);
                } else {
                    throw new RuntimeException("Unknown value type " + v);
                }
            }
        } else {
            if (forward) {
                if (v instanceof BinopExpr) {
                    BinopExpr be = (BinopExpr) v;
                    Instruction insn = be.getInstruction(); 
                    evaluateOpcodeOperator(v, getOperator(insn), by);
                } else if (v instanceof UnopExpr) {
                    UnopExpr ue = (UnopExpr) v;
                    Instruction insn = ue.getInstruction();
                    evaluateOpcodeOperator(v, getOperator(insn), by);
                } else if (v instanceof OperatorValue) {
                    OperatorValue ov = (OperatorValue) v;
                    evaluateOperator(ov, ov.getOperator(), by);
                } else if (v instanceof Output) {
                    Output<?> output = (Output<?>) v;
                    Location loc = output.getLocation();
                    if (loc instanceof ArrayElement) {
                        loadPostArrayElement((Output<ArrayElement>)v);
                    } else {
                        throw new RuntimeException("Not implemented");
                    }
                } else if (v instanceof Input) {
                    Input<?> input = (Input<?>) v;
                    Location loc = input.getLocation();
                    if (loc instanceof ArrayElement) {
                        loadPreArrayElement((Input<ArrayElement>)v);
                        revertHeapValue((Input<ArrayElement>)v);
                    } else {
                        throw new RuntimeException("Not implemented");
                    }
                } else if (v instanceof NewRef) {
                    newObjectOrArray((NewRef)v);
                } else {
                    throw new RuntimeException("Unknown value type " + v);
                }
            } else { // backward by other values
                Value fv = by[0];
                if (v instanceof Output) {
                    Output<?> output = (Output<?>) v;
                    Location loc = output.getLocation();
                    if (loc instanceof ArrayElement) {
                        throw new RuntimeException("Sanity check failed, array should be loaded via index is provided");
                    } else if (loc instanceof InstanceField) {
                        loadPostInstanceField((Output<InstanceField>)v);
                    } else if (loc instanceof ArrayLength) {
                        loadPostArrayLength((Output<ArrayLength>)v);
                    } else if (loc instanceof StaticField) {
                        loadPostStaticField((Output<StaticField>)v);
                    } else {
                        throw new RuntimeException("Not implemented");
                    }
                } else if (fv instanceof BinopExpr) {
                    BinopExpr be = (BinopExpr) fv;
                    Instruction insn = be.getInstruction(); 
                    evaluateBinaryInverseOpcodeOperator(v, getOperator(insn), by);
                } else if (fv instanceof UnopExpr) {
                    UnopExpr ue = (UnopExpr) fv;
                    Instruction insn = ue.getInstruction();
                    evaluateUnaryInverseOpcodeOperator(v, getOperator(insn), by);
                } else if (fv instanceof OperatorValue) {
                    throw new RuntimeException("Not implemented");
                } else if (fv instanceof NewRef) {
                    throw new RuntimeException("Unknown value type " + v);
                } else if (fv instanceof Output) {
                    Output<?> output = (Output<?>) fv;
                    Location loc = output.getLocation();
                    if (loc instanceof ArrayLength) {
                        copy(v, fv);
                    } else if (loc instanceof ArrayElement) {
                        copy(v, fv);
                    } else if (loc instanceof InstanceField) {
                        copy(v, fv);
                    } else if (loc instanceof StaticField) {
                        copy(v, fv);
                    } else {
                        throw new RuntimeException("Unknown value type " + v);
                    }
                } else if (fv instanceof GuessIndexValue) {
                    copy(v, fv);
                } else if (fv instanceof Input) {
                    throw new RuntimeException("Not implemented");
                } else {
                    throw new RuntimeException("Unknown value type " + v);
                }

                if (v instanceof Input) {
                    Input<?> input = (Input<?>) v;
                    Location loc = input.getLocation();
                    if (loc instanceof HeapLocation) {
                        if (iF.es.isDefined((HeapLocation) loc)) {
                            // an overridden input, so we must revert the value to the location.
                            revertHeapValue((Input<? extends HeapLocation>) input);
                        }
                    }
                }
            }
        }
    }

    private void evaluateUnaryInverseOpcodeOperator(Value v, OpcodeOperator operator, Value[] by) {
        OpcodeOperator iop = getInverseOperator(operator.opcode);
        switch (operator.opcode) {
        case Opcodes._ineg:
        case Opcodes._fneg:
        case Opcodes._lneg:
        case Opcodes._dneg:
        case Opcodes._i2l:
        case Opcodes._i2f:
        case Opcodes._i2d:
            evaluateOpcodeOperator(v, iop, by);
            return;
        default:
            throw new RuntimeException("Should not reach here");
        }
    }

    private void evaluateBinaryInverseOpcodeOperator(Value v1, OpcodeOperator operator, Value[] by) {
        if (by.length != 2) {
            throw new RuntimeException("sanity check failed!");
        }
        BinopExpr be = (BinopExpr) by[0];
        Value v2 = by[1];

        OpcodeOperator iop = getInverseOperator(operator.opcode);
        switch (operator.opcode) {
        case Opcodes._iinc:
        case Opcodes._iadd:
        case Opcodes._ladd:
        case Opcodes._fadd:
        case Opcodes._dadd:
            // be = v1 + v2
            // so v1 = be - v2;
            evaluateOpcodeOperator(v1, iop, by);
            return;
        case Opcodes._isub:
        case Opcodes._lsub:
        case Opcodes._fsub:
        case Opcodes._dsub:
            // be = v1 - v2; => v1 = be + v2;
            if (be.getLeftOperand() == v1 && be.getRightOperand() == v2) {
                evaluateOpcodeOperator(v1, iop, by);
                return;
            }
            // be = v2 - v1; => v1 = v2 - be;
            if (be.getLeftOperand() == v2 && be.getRightOperand() == v1) {
                evaluateOpcodeOperator(v1, operator, v2, be);
                return;
            }
        case Opcodes._imul:
        case Opcodes._lmul:
        case Opcodes._fmul:
        case Opcodes._dmul:
            // be = v1 * v2
            // so v1 = be / v2;
            evaluateOpcodeOperator(v1, iop, by);
            return;
        case Opcodes._idiv:
        case Opcodes._ldiv:
        case Opcodes._fdiv:
        case Opcodes._ddiv:
            // be = v1 / v2; => v1 = be * v2;
            if (be.getLeftOperand() == v1 && be.getRightOperand() == v2) {
                evaluateOpcodeOperator(v1, iop, by);
                return;
            }
            // be = v2 / v1; => v1 = v2 / be;
            if (be.getLeftOperand() == v2 && be.getRightOperand() == v1) {
                evaluateOpcodeOperator(v1, operator, v2, be);
                return;
            }
        default:
            throw new RuntimeException("Should not reach here");
        }
    }

    private void newObjectOrArray(NewRef v) {
        ElementInfo ei = Value.getElementInfo(v);
        ClassInfo ci = ei.getClassInfo();
        if (ci.isArray()) {
            ArrayElementInfo aei = (ArrayElementInfo) ei;
            storeVariable(variable(v), om.newArray(loadClass(ci), loadVariable(variable(aei.arrayLength().value()))));
        } else {
            storeVariable(variable(v), om.newObject(loadClass(ci)));
        }
    }

    private void guessArrayIndex(GuessIndexValue v) {
        Value array = v.getArray();
        storeVariable(variable(v), om.guessArrayIndex(loadVariable(variable(array))));
    }

    protected void evaluateOperator(Value result, Operator operator, Value ... operands) {
        StringBuilder sb = new StringBuilder();

        sb.append("em.op(\"").append(operator.getPrintName()).append("\").eval(");
        if (operands.length > 0) {
            sb.append(loadVariable(variable(operands[0])));
            for (int i=1; i<operands.length; i++) {
                sb.append(",").append(loadVariable(variable(operands[i])));
            }
        }
        sb.append(")");
        storeVariable(variable(result), sb.toString());
        //storeVariable(variable(result), em.op(operator.getPrintName()).eval(variables((Value[])operands)));
    }

    protected void copy(Value v, Value fv) {
        storeVariable(variable(v), loadVariable(variable(fv)));
    }

    protected void revertMethodOperatorParameter(Value to, OperatorValue mov, int ai) {
        throw new RuntimeException("Not implemented");
    }

    protected MethodOperator getInverseMethodOperator(MethodOperator mo) {
        return MethodOperatorFactory.getInverseMethodOperator(mo);
    }

    protected OpcodeOperator getInverseOperator(int opcode) {
        return OpcodeOperatorFactory.getInverseOpcodeOperator(opcode);
    }

    protected OpcodeOperator getOperator(Instruction insn) {
        return OpcodeOperatorFactory.getOpcodeOperator(insn.getOpcode());
    }

    protected void revertMethodOperator(OperatorValue ov) {
        throw new RuntimeException("Not implemented yet");
    }

    protected void evaluateOpcodeOperator(Value result, OpcodeOperator op, Value ... operands) {
        StringBuilder sb = new StringBuilder();

        sb.append("em.op(\"").append(op.type).append("\").eval(");
        if (operands.length > 0) {
            sb.append(loadVariable(variable(operands[0])));
            for (int i=1; i<operands.length; i++) {
                sb.append(",").append(loadVariable(variable(operands[i])));
            }
        }
        sb.append(")");
        storeVariable(variable(result), sb.toString());
    }

    protected void loadPostInstanceField(Output<InstanceField> v) {
        InstanceField inf = v.getLocation();
        Value ref = inf.getContainer().getReference();
        if (iF.es.isDefined(inf)) {
            if (iF.es.isUsedUnknown(inf)) {
                // its input has been used and must be reverted by others.
                storeVariable(variable(v), om.getField(loadVariable(variable(ref)), field(inf.getFieldInfo())));
            } else {
                // its input has not been used, revert it to a default value.
                storeVariable(variable(v), om.popField(loadVariable(variable(ref)), field(inf.getFieldInfo())));
            }
        } else {
            // A load of unknown value
            if (!(v.getValue() instanceof Input || v.getValue() instanceof Constant)) {
                throw new RuntimeException("Sanity check failed!");
            }
            storeVariable(variable(v), om.getField(loadVariable(variable(ref)), field(inf.getFieldInfo())));
        }
    }

    protected void loadPostStaticField(Output<StaticField> v) {
        StaticField sf = v.getLocation();
        if (iF.es.isDefined(sf)) {
            if (iF.es.isUsedUnknown(sf)) {
                // its input has been used and must be reverted by others.
                storeVariable(variable(v), om.getStatic(field(sf.getFieldInfo())));
            } else {
                // its input has not been used, revert it to a default value.
                storeVariable(variable(v), om.popStatic(field(sf.getFieldInfo())));
            }
        } else {
            // A load of unknown value
            if (!(v.getValue() instanceof Input || v.getValue() instanceof Constant)) {
                throw new RuntimeException("Sanity check failed!");
            }
            storeVariable(variable(v), om.getStatic(field(sf.getFieldInfo())));
        }
    }

    protected void loadPostArrayElement(Output<ArrayElement> v) {
        ArrayElement ae = v.getLocation();
        Value ref = ae.getContainer().getReference();

        if (iF.es.isDefined(ae)) {
            if (iF.es.isUsedUnknown(ae)) {
                // its input has been used and must be reverted by others.
                storeVariable(variable(v), om.getArrayElement(loadVariable(variable(ref)), loadVariable(variable(v.getLocation().getIndex()))));
            } else {
                // its input has not been used, revert it to a default value.
                storeVariable(variable(v), om.popArrayElement(loadVariable(variable(ref)), loadVariable(variable(v.getLocation().getIndex()))));
            }
        } else {
            // A load of unknown value
            if (!(v.getValue() instanceof Input || v.getValue() instanceof Constant)) {
                throw new RuntimeException("Sanity check failed! unknown value type " + v.getValue());
            }
            storeVariable(variable(v), om.getArrayElement(loadVariable(variable(ref)), loadVariable(variable(v.getLocation().getIndex()))));
        }
    }

    protected void loadPreArrayElement(Input<ArrayElement> v) {
        ArrayElement ae = v.getLocation();
        Value ref = ae.getContainer().getReference();
        storeVariable(variable(v), om.getArrayElement(loadVariable(variable(ref)), loadVariable(variable(v.getLocation().getIndex()))));
    }

    protected void loadPostArrayLength(Output<ArrayLength> v) {
        storeVariable(variable(v), om.getArrayLength(loadVariable(variable(v.getLocation().getContainer().getReference()))));
    }

    /**
     * 
     * @param v
     */
    protected void loadTargetObjectRef(TargetRef v) {
        storeVariable(variable(v), om.getTarget());
    }

    protected abstract Object defaultValue(Value v);

    protected abstract Object comparator(Object c);

    protected Object field(FieldInfo fi) {
        if (fi.isStatic()) {
            return fi.getClassInfo().getName() + '.' + fi.getName();
        }
        String name = fi.getName();
        ClassInfo superClass = fi.getClassInfo().getSuperClass();
        if (superClass != null && superClass.getField(name) == null) {
            return name;
        }
        return fi.getClassInfo().getName() + '.' + fi.getName();
    }

    protected Object loadClass(ClassInfo ci) {
        if (ci.isArray()) {
            return ci.getTypeDescriptor();
        }
        if (ci.isPrimitive()) {
            return ci.getName();
        }
        return ci.getName();
    }

    protected Object variable(Value v) {
        return va.getVariableName(v);
    }

    protected Object[] variables(Value ... operands) {
        if (operands.length == 0) {
            return operands;
        }
        Object[] r = new Object[operands.length];
        for (int i=0; i<r.length; i++) {
            r[i] = variable(operands[i]); 
        }
        return r;
    }

    protected abstract Object constant(Constant c);

    protected abstract Object accesspath(AccessPath ap);

    protected void loadDefaultHeapValue(Input<? extends HeapLocation> v) {
        revertDefaultHeapValue(v);
        storeVariable(variable(v), getHeapValue(v));
    }

    private void loadDefaultMethodParameter(Input<? extends MethodParameterLocation> v) {
        MethodParameterLocation mpl = v.getLocation();
        String typeDescriptor = mpl.getTypeDescriptor();
        Type type = Type.getType(typeDescriptor);
        // Checkout loadClass in this file
        if (type.getSort() == Type.ARRAY) {
            storeVariable(variable(v), om.newDefaultValue(typeDescriptor));
        } else {
            storeVariable(variable(v), om.newDefaultValue(type.getClassName()));
        }
    }

    protected Object getHeapValue(Input<? extends HeapLocation> v) {
        HeapLocation hl = v.getLocation();
        Value ref = hl.getContainer().getReference();
        if (hl instanceof ArrayElement) {
            ArrayElement ae = (ArrayElement) hl;
            return om.getArrayElement(vm.get(variable(ref)), vm.get(variable(ae.getIndex())));
        } else if (hl instanceof InstanceField) {
            FieldLocation fl = (FieldLocation) hl;
            return om.getField(vm.get(variable(ref)), field(fl.getFieldInfo()));
        } else if (hl instanceof StaticField) {
            FieldLocation fl = (FieldLocation) hl;
            return om.getStatic(field(fl.getFieldInfo()));
        } else if (hl instanceof ArrayLength) {
            return om.getArrayLength(vm.get(variable(ref)));
        }
        throw new RuntimeException();
    }

    protected void revertDefaultHeapValue(Input<? extends HeapLocation> v) {
        HeapLocation hl = v.getLocation();
        Value ref = hl.getContainer().getReference();
        if (hl instanceof ArrayElement) {
            ArrayElement ae = (ArrayElement) hl;
            om.revertArrayElement(vm.get(variable(ref)), vm.get(variable(ae.getIndex())));
        } else if (hl instanceof InstanceField) {
            FieldLocation fl = (FieldLocation) hl;
            om.revertField(vm.get(variable(ref)), field(fl.getFieldInfo()));
        } else if (hl instanceof StaticField) {
            FieldLocation fl = (FieldLocation) hl;
            om.revertStatic(field(fl.getFieldInfo()));
        } else {
            throw new RuntimeException("Should not reach here");
        }
    }

    protected void revertHeapValue(Input<? extends HeapLocation> v) {
        HeapLocation hl = v.getLocation();
        Value ref = hl.getContainer().getReference();
        if (hl instanceof ArrayElement) {
            ArrayElement ae = (ArrayElement) hl;
            om.revertArrayElement(vm.get(variable(ref)), vm.get(variable(ae.getIndex())), loadVariable(variable(v)));
        } else if (hl instanceof InstanceField) {
            FieldLocation fl = (FieldLocation) hl;
            om.revertField(vm.get(variable(ref)), field(fl.getFieldInfo()),  loadVariable(variable(v)));
        } else if (hl instanceof StaticField) {
            FieldLocation fl = (FieldLocation) hl;
            om.revertStatic(field(fl.getFieldInfo()),  loadVariable(variable(v)));
        } else {
            throw new RuntimeException("Should not reach here");
        }
    }

    protected void loadConstant(Constant v) {
        storeVariable(variable(v), constant(v));
    }

    protected void storeVariable(Object name, Object value) {
        vm.put(name, value);
    }

    protected Object loadVariable(Object name) {
        return vm.get(name);
    }

    @Override
    public ValueMap getValueMap() {
        return vm;
    }

    @Override
    public ObjectMap getObjectMap() {
        return om;
    }

    @Override
    public EvalMap getEvalMap() {
        return em;
    }

}
