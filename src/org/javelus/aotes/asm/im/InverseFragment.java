package org.javelus.aotes.asm.im;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.summary.ExecutionSummary;

public class InverseFragment {

    ExecutionSummary es;

    private List<Statement> statements;
    private List<Predicate> predicates;

    // Used before defined heap locations
    private Set<AccessPath> revertedInput;
    private Set<AccessPath> definedOutput;
    private Set<AccessPath> mutatedReceivers;

    // TODO use special names in generated code.
    //	private Object[] methodParameters;
    //	private Object methodReturn;
    private Set<AccessPath> delta;

    public InverseFragment(
            ExecutionSummary es,
            List<Statement> statements,
            List<Predicate> predicates,
            Set<AccessPath> revertedInput,
            Set<AccessPath> definedOutput,
            Set<AccessPath> mutatedReceivers) {
        this.es = es;
        this.es.setInverseFragment(this);
        this.statements = statements;
        this.predicates = predicates;
        this.revertedInput = revertedInput;
        this.definedOutput = definedOutput;
        this.mutatedReceivers = mutatedReceivers;
    }

    public ExecutionSummary getExecutionSummary() {
        return es;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public List<Predicate> getPredicates() {
        return predicates;
    }

    public Set<AccessPath> getRevertedInput() {
        return revertedInput;
    }

    public Set<AccessPath> getDefinedOutput() {
        return definedOutput;
    }

    public Set<AccessPath> getDelta() {
        if (delta == null) {
            updateDelta();
        }
        return delta;
    }

    private void updateDelta() {
        delta = new HashSet<AccessPath>();
        for (AccessPath ap:definedOutput) {
            if (!revertedInput.contains(ap)) {
                delta.add(ap);
            }
        }
        for (AccessPath ap:mutatedReceivers) {
            if (!revertedInput.contains(ap)) {
                delta.add(ap);
            }
        }
    }

    public String getUniqueName() {
        return es.getUniqueName();
    }

    public Set<AccessPath> getMutatedReceivers() {
        return mutatedReceivers;
    }
}
