package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.vm.MethodInfo;

public class MethodReturnLocation extends MethodLocation {

    public MethodReturnLocation(MethodInfo method, AccessPath name) {
        super(method, name);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((methodInfo == null) ? 0 : methodInfo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MethodLocation other = (MethodLocation) obj;
        if (methodInfo == null) {
            if (other.methodInfo != null)
                return false;
        } else if (!methodInfo.equals(other.methodInfo))
            return false;
        return true;
    }

    @Override
    public String getTypeDescriptor() {
        return methodInfo.getReturnType().getDescriptor();
    }
}
