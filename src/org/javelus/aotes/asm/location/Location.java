package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.value.Value;

public abstract class Location {
    private Value value;

    public void value(Value value) {
        if (value == null) {
            throw new RuntimeException("Value should not be null!");
        }
        this.value = value;
    }

    public Value value() {
        return value;
    }

    /**
     * Declared type of this location
     * @return
     */
    public abstract String getTypeDescriptor();
}
