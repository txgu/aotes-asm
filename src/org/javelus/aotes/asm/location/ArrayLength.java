package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.accesspath.ArrayAccessor;
import org.javelus.aotes.asm.vm.ArrayElementInfo;

public class ArrayLength extends HeapLocation {

    public ArrayLength(ArrayElementInfo container) {
        super(container);
    }

    public Object getAccessPathAccessor() {
        return ArrayAccessor.LENGTH;
    }

    public ArrayAccessor getAccessor() {
        return ArrayAccessor.LENGTH;
    }

    @Override
    public String getTypeDescriptor() {
        return "I";
    }

    public String toString() {
        return "" + getContainer().getReference() + ".length";
    }

    @Override
    public final boolean isReference() {
        return false;
    }
}
