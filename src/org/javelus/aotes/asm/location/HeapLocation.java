package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.vm.ElementInfo;

public abstract class HeapLocation extends Location {

    private ElementInfo container;

    public HeapLocation(ElementInfo container) {
        this.container = container;
    }

    public ElementInfo getContainer() {
        return container;
    }

    public abstract Object getAccessor();

    public abstract boolean isReference();
}
