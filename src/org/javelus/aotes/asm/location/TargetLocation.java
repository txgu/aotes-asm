package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.accesspath.AccessPathRoots;
import org.javelus.aotes.asm.vm.ClassInfo;

public class TargetLocation extends NamedLocation {

    ClassInfo classInfo;

    public TargetLocation(ClassInfo classInfo) {
        super(AccessPathRoots.getTargetAccessPath(classInfo));
        this.classInfo = classInfo;
    }

    @Override
    public String getTypeDescriptor() {
        return classInfo.getTypeDescriptor();
    }

}
