package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.vm.FieldInfo;
import org.javelus.aotes.asm.vm.StaticElementInfo;

public class StaticField extends FieldLocation {

    public StaticField(StaticElementInfo container, FieldInfo fieldInfo) {
        super(container, fieldInfo);
    }

    public String toString() {
        return getFieldInfo().getClassInfo().getSimpleName() + '.' + getFieldInfo().getName();
    }
}
