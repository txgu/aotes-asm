package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.accesspath.AccessPath;
import org.javelus.aotes.asm.vm.MethodInfo;

public abstract class MethodLocation extends NamedLocation {

    protected MethodInfo methodInfo;

    public MethodLocation(MethodInfo method, AccessPath name) {
        super(name);
        this.methodInfo = method;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((methodInfo == null) ? 0 : methodInfo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MethodLocation other = (MethodLocation) obj;
        if (methodInfo == null) {
            if (other.methodInfo != null)
                return false;
        } else if (!methodInfo.equals(other.methodInfo))
            return false;
        return true;
    }

    public MethodInfo getMethodInfo() {
        return methodInfo;
    }
}
