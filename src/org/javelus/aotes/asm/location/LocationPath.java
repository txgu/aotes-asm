package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.accesspath.AccessPath;

public class LocationPath {
    LocationPath parent;
    Location location;
    AccessPath accessPatth;
    public LocationPath(NamedLocation location) {
        this.location = location;
        this.accessPatth = location.getName();
    }

    private LocationPath(LocationPath parent, HeapLocation location) {
        this.parent = parent;
        this.location = location;
        this.accessPatth = parent.getAccessPath().getChild(location.getAccessor());
    }

    public LocationPath getParent() {
        return parent;
    }

    public AccessPath getAccessPath() {
        return accessPatth;
    }

    public LocationPath dot(HeapLocation location) {
        return new LocationPath(this, location);
    }

    public Location getLocation() {
        return location;
    }

    public String toString() {
        return this.location.toString() + "@" + accessPatth.toString();
    }
}
