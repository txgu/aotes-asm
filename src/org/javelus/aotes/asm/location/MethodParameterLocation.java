package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.accesspath.AccessPathRoots;
import org.javelus.aotes.asm.vm.MethodInfo;

public class MethodParameterLocation extends MethodLocation {

    int index;

    public MethodParameterLocation(MethodInfo method, int index) {
        super(method, AccessPathRoots.getMethodParameter(method, index));
        this.index = index;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + index;
        result = result + super.hashCode() * prime;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MethodParameterLocation other = (MethodParameterLocation) obj;
        if (index != other.index)
            return false;
        if (methodInfo.equals(other.methodInfo)) {
            return false;
        }
        return true;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String getTypeDescriptor() {
        return methodInfo.getArgumentTypes()[index].getDescriptor();
    }
}
