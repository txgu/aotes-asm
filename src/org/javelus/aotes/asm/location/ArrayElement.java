package org.javelus.aotes.asm.location;


import org.javelus.aotes.asm.accesspath.ArrayAccessor;
import org.javelus.aotes.asm.value.Value;
import org.javelus.aotes.asm.vm.ArrayElementInfo;


public class ArrayElement extends HeapLocation {

    private Value index;
    public ArrayElement(ArrayElementInfo container, Value index) {
        super(container);
        this.index = index;
    }

    @Override
    public ArrayElementInfo getContainer() {
        return (ArrayElementInfo) super.getContainer();
    }

    @Override
    public Object getAccessor() {
        return ArrayAccessor.ELEMENT;
    }

    public Value getIndex() {
        return index;
    }

    @Override
    public String getTypeDescriptor() {
        return getContainer().getClassInfo().getComponentClassInfo().getTypeDescriptor();
    }

    public boolean isReference() {
        char code = getTypeDescriptor().charAt(0);
        return code == 'L' || code == '[';
    }

    public String toString() {
        return "" + getContainer().getReference() + '[' + index + ']';
    }
}
