package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.accesspath.AccessPathRoots;
import org.javelus.aotes.asm.vm.ClassInfo;

public class ClassLocation extends NamedLocation {

    private ClassInfo classInfo;

    public ClassLocation(ClassInfo classInfo) {
        super(AccessPathRoots.getClassAccessPath(classInfo));
        this.classInfo = classInfo;
    }

    @Override
    public String getTypeDescriptor() {
        return "static";
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }
}
