package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.accesspath.AccessPath;

public abstract class NamedLocation extends Location {
    private AccessPath name;

    public NamedLocation(AccessPath name) {
        this.name = name;
    }

    public AccessPath getName() {
        return name;
    }

    public String toString() {
        return name.toString();
    }
}
