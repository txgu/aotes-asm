package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.FieldInfo;

public class InstanceField extends FieldLocation{

    public InstanceField(ElementInfo container, FieldInfo fieldInfo) {
        super(container, fieldInfo);
    }

    public String toString() {
        return "" + getContainer().getReference() + '.' + getFieldInfo().getName();
    }
}
