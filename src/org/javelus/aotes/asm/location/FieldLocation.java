package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.vm.ElementInfo;
import org.javelus.aotes.asm.vm.FieldInfo;

public class FieldLocation extends HeapLocation {

    private FieldInfo fieldInfo;

    public FieldLocation(ElementInfo container, FieldInfo fieldInfo) {
        super(container);
        this.fieldInfo = fieldInfo;
    }

    public FieldInfo getAccessor() {
        return fieldInfo;
    }

    @Override
    public String getTypeDescriptor() {
        return fieldInfo.getTypeDescriptor();
    }

    public FieldInfo getFieldInfo() {
        return fieldInfo;
    }

    public boolean isReference() {
        return fieldInfo.isReference();
    }
}
