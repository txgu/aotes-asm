package org.javelus.aotes.asm.location;

import org.javelus.aotes.asm.value.Value;

public class MixedValue {

    private Object concrete;
    private Value symbolic;

    public MixedValue(Object concrete, Value symbolic) {
        update(concrete, symbolic);
    }

    public Object getConcrte() {
        return concrete;
    }

    public Object getSymbolic() {
        return symbolic;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + concrete.hashCode();
        result = prime * result + symbolic.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MixedValue other = (MixedValue) obj;
        if (concrete == null) {
            if (other.concrete != null)
                return false;
        } else if (!concrete.equals(other.concrete))
            return false;
        if (symbolic == null) {
            if (other.symbolic != null)
                return false;
        } else if (!symbolic.equals(other.symbolic))
            return false;
        return true;
    }

    public void update(Object concrete, Value symbolic) {
        this.concrete = concrete;
        this.symbolic = symbolic;
    }
}
