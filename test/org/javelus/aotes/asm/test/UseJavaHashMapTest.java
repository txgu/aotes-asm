package org.javelus.aotes.asm.test;

import java.util.HashMap;
import java.util.Map;

public class UseJavaHashMapTest {
    public static void main(String[] args) {
        Map<String, String> t = new HashMap<String, String>();
        t.put(null, "NULL");
        t.put("e1", "v1");
        t.put("e2", "v2");
        t.put("e3", "v3");
        t.remove("e2");
        System.out.println(org.javelus.aotes.executor.java.JavaExecutorDriver.transform(t));
    }
}
