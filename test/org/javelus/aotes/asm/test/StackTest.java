package org.javelus.aotes.asm.test;

public class StackTest {
    public static void main(String[] args) {
        Stack stack = new Stack();
        stack.push("e1");
        stack.push("e2");
        stack.push("e3");
        System.out.println(org.javelus.aotes.executor.java.JavaExecutorDriver.transform(stack));
    }
}
