package org.javelus.aotes.asm.test;

public class CallerCalleeTest {
    static class Caller {
        int depth;
        private Callee callee = new Callee();
        
        public void call() {
            callee.call(this);
        }
    }
    
    static class Callee {
        public void call(Caller caller) {
            caller.depth ++;
        }
    }
}
