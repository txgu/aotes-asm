package org.javelus.aotes.asm.test;

public class SimpleHashMap {
    static class Entry {
        Entry next;
        Object key;
        Object value;

        public Entry(Entry next, Object key, Object value) {
            this.next = next;
            this.key = key;
            this.value = value;
        }

    }

    Entry[] table;

    SimpleHashMap() {
        this(10);
    }

    SimpleHashMap(int capacity) {
        table = new Entry[capacity];
    }

    public Object put(Object key, Object value) {
        int hash = key.hashCode();
        int idx = hashToIndex(hash);
        Entry entry = table[idx];
        if (entry == null) {
            table[idx] = new Entry(null, key, value);
            return null;
        }

        for (;entry != null; entry = entry.next) {
            if (entry.key.equals(key)) {
                Object old = entry.value;
                entry.value = value;
                return old;
            }
        }

        table[idx] = new Entry(table[idx], key, value);
        return null;
    }

    public Object get(Object key) {
        int hash = key.hashCode();
        int idx = hashToIndex(hash);
        Entry entry = table[idx];
        if (entry == null) {
            return null;
        }

        for (;entry != null; entry = entry.next) {
            if (entry.key.equals(key)) {
                return entry.value;
            }
        }

        return null;
    }

    private int hashToIndex(int hash) {
        return hash % table.length;
    }
}
