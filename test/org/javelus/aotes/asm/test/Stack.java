package org.javelus.aotes.asm.test;

public class Stack {
    int size = 0;
    Object[] elements;

    public Stack(int cap){
        elements = new Object[cap];
    }

    public Stack() {
        elements = new Object[10];
    }

    public void push(Object element) {
        elements[size++] = element;
    }

    public void remove(int index) {        
        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(elements, index+1, elements, index,
                    numMoved);
        elements[--size] = null;
    }

    public Object pop() {
        Object val = elements[--size];
        elements[size] = null;
        return val;
    }

    public boolean isEmpty() {
        return size == 0;
    }
}
