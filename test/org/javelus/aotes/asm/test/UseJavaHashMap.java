package org.javelus.aotes.asm.test;

import java.util.HashMap;

public class UseJavaHashMap {
    void useJavaHashMap() {
        Object key = new Object();
        Object value = new Object();
        HashMap<Object, Object> map = new HashMap<Object, Object>();
        map.put(key, value);
        map.get(key);
        map.containsKey(key);
        map.containsValue(value);
        map.remove(key);
    }
}
