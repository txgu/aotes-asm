package org.javelus.aotes.asm.test;

import java.io.IOException;
import java.util.List;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.util.Printer;

public class StringBuilderTest {
    public String test() {
        StringBuilder sb = new StringBuilder();
        sb.append("StringBuilderTest");
        super.hashCode();
        return sb.toString();
    }

    public static void testStringBuilderAppend() throws IOException {
        ClassReader cr = new ClassReader(StringBuilder.class.getResourceAsStream("StringBuilder.class"));
        ClassNode cn = new ClassNode(Opcodes.ASM4);
        cr.accept(cn, 0);
        for(MethodNode mn:(List<MethodNode>)cn.methods) {
            if (mn.desc.startsWith("(Ljava/lang/String;)") && mn.name.equals("append")) {
                AbstractInsnNode node = mn.instructions.getFirst();
                while (node != null) {
                    if (node.getOpcode() < 0) {
                        System.out.println(node.getClass());
                    } else {
                        System.out.println(Printer.OPCODES[node.getOpcode()]);
                    }
                    node = node.getNext();
                }
            }
        }
    }

    public static void testStringBuilderTest() throws IOException {
        ClassReader cr = new ClassReader(StringBuilderTest.class.getResourceAsStream("StringBuilderTest.class"));
        ClassNode cn = new ClassNode(Opcodes.ASM4);
        cr.accept(cn, 0);
        for(MethodNode mn:(List<MethodNode>)cn.methods) {
            if (mn.name.equals("test")) {
                AbstractInsnNode node = mn.instructions.getFirst();
                while (node != null) {
                    if (node.getOpcode() < 0) {
                        System.out.println(node.getClass());
                    } else {
                        System.out.println(Printer.OPCODES[node.getOpcode()]);
                    }
                    node = node.getNext();
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        ClassReader cr = new ClassReader(StringBuilder.class.getResourceAsStream("StringBuilder.class"));
        ClassNode cn = new ClassNode(Opcodes.ASM4);
        cr.accept(cn, 0);
        for(MethodNode mn:(List<MethodNode>)cn.methods) {
            if (mn.desc.startsWith("(Ljava/lang/String;)") && mn.name.equals("append")) {
                AbstractInsnNode node = mn.instructions.getFirst();
                while (node != null) {
                    if (node.getOpcode() < 0) {
                        System.out.println(node.getClass());
                    } else {
                        System.out.println(Printer.OPCODES[node.getOpcode()]);
                    }
                    node = node.getNext();
                }
            }
        }
    }
}
