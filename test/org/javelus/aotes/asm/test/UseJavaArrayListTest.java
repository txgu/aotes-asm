package org.javelus.aotes.asm.test;

import java.util.ArrayList;
import java.util.List;

public class UseJavaArrayListTest {
    public static void main(String[] args) {
        List<String> t = new ArrayList<String>();
        t.add("e1");
        t.add("e2");
        t.add("e3");
        t.remove("e2");
        System.out.println(org.javelus.aotes.executor.java.JavaExecutorDriver.transform(t));
    }
}
