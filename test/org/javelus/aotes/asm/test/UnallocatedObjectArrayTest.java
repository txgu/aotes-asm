package org.javelus.aotes.asm.test;

import java.util.HashMap;

public class UnallocatedObjectArrayTest {

    HashMap<String, String> map;

    String value1;
    String value2;
    
    public UnallocatedObjectArrayTest(HashMap<String, String> map) {
        this.map = map;
        
        value1 = this.map.get("value1");
        value2 = this.map.get("value2");
    }
    
    public static void test() {
        HashMap<String, String> map = new HashMap<String, String>();
        UnallocatedObjectArrayTest t = new UnallocatedObjectArrayTest(map);
        t.getClass(); // avoid unused variable
    }
}
