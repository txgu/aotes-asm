package org.javelus.aotes.asm.test;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTest {

    AtomicInteger ai = new AtomicInteger();

    int testaddAndGet(int delta) {
        return ai.addAndGet(delta);
    }
}
