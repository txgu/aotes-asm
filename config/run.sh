#! /bin/bash

PWD=$( pushd $( dirname $BASH_SOURCE[0] ) >> /dev/null && pwd && popd >> /dev/null )
CP=${PWD}/aotes-asm.jar

for JAR in ${PWD}/lib/*.jar
do
    CP=$CP:$JAR
done

java -cp $CP org.javelus.aotes.asm.Main $@
