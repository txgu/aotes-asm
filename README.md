# READ ME

## Build

1. Make a directory. We later refer this directory as `$AOTES_HOME`.
2. Change directory to `$AOTES_HOME` and clone the following projects

    1. [AOTES](https://bitbucket.org/txgu/aotes-asm)
    2. [Developer Interface of Javelus](https://bitbucket.org/javelus/developer-interface)
    3. [Dynamic Patch Generator](https://bitbucket.org/javelus/dpg)

3. Open eclipse and import all projects in `$AOTES_HOME`

You can find some subjects used to evaluate AOTES at the following projects:

* [Subjects for ICSE 2017](https://bitbucket.org/txgu/aotes-icse17-subjects)

More information can be found at http://moon.nju.edu.cn/dse/aotes/.

## Run

If you set up everything successfully,
you can checkout the options specified in
`org.javelus.aotes.asm.Main`



